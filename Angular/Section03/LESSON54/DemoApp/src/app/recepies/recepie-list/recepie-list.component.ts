import { Component, OnInit } from '@angular/core';
import { Recepie } from '../recepies.model';

@Component({
  selector: 'app-recepie-list',
  templateUrl: './recepie-list.component.html',
  styleUrls: ['./recepie-list.component.css']
})
export class RecepieListComponent implements OnInit {
  recepies: Recepie[] = [
    new Recepie('Chicken Recepie','Butter Chicken','https://realhousemoms.com/wp-content/uploads/Honey-Butter-Chicken-IG.jpg'),
    new Recepie('Paneer Recepie','Paneer Chicken','https://www.indianhealthyrecipes.com/wp-content/uploads/2016/03/kadai-paneer-1.jpg'),

  ];
  constructor() { }

  ngOnInit() {
  }

}
