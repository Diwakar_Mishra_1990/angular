import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  title = 'DemoApp';
  featureType:string= 'recepie' // value is Default recepie 
  onSelected(event){
  this.featureType =event;    
  }


  ngOnInit(): void {
    firebase.initializeApp(
      {
        apiKey: "AIzaSyDiLRKDZE-FzbftBjoxSsZ9bX7BQweDI6Y",
        authDomain: "demoprojectudemy.firebaseapp.com",
      }
    );

  }
}
