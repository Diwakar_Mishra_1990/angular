import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecepiesComponent } from './recepies/recepies.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { RecepieStartComponent } from './recepies/recepie-start/recepie-start.component';
import { RecepieDetailComponent } from './recepies/recepie-detail/recepie-detail.component';
import { RecepieEditComponent } from './recepies/recepie-edit/recepie-edit.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';

const routes: Routes = [
  {path:'' , redirectTo:'/recepies', pathMatch:'full'},//localhpst:4200   =>This will direct to recepies page
  {path:'recepies' , component:RecepiesComponent , children:[
    {path: '' ,component:RecepieStartComponent}, // localhost:4200/recepies
    {path:'new' , component:RecepieEditComponent},// localhost:4200/recepies/new
    {path: ':id' ,component:RecepieDetailComponent}, // localhost:4200/recepies/{id}
    {path:':id/edit' , component:RecepieEditComponent},// localhost:4200/recepies/1/edit
  ]}, 
  {path:'shopping-list' , component:ShoppingListComponent},// localhost:4200/shopping-list  
  {path:'signUp' , component:SignupComponent},// localhost:4200/signUp
  {path:'signIn' , component:SigninComponent}// localhost:4200/signIn
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
