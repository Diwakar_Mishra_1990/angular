import * as firebase from 'firebase';

export class AuthService{
    token:String;
    signUpUser(email:string , password:string){
       firebase.auth().createUserWithEmailAndPassword(email,password).
       catch(
           error => console.log(error)
       )   
    }

    signInUser(email:string , password:string){
        firebase.auth().signInWithEmailAndPassword(email,password).
        then(
            response => console.log(response)
        ).
        catch(
            error => console.log(error)
        )   
    }

    getToken(){
        firebase.auth().currentUser.getIdToken().then(
            (token:String) =>this.token = token
        );
        console.log(this.token);
        return this.token;
    }

    isAuthenticated(){
        console.log(this.token);
        return this.token != null;
    }
}