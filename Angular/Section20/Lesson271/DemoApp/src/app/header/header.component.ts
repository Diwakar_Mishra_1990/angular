import { Component, OnInit } from '@angular/core';
import { DataStorageService } from '../shared/server/data-storage.service';
import {Response} from '@angular/http';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  constructor(private datastorageService:DataStorageService , private authService: AuthService) { }
  ngOnInit() {
  }
  onSaveData(){
    this.datastorageService.storeRecepies().subscribe(
        (response:Response)=>{
          console.log(response);
        }
    );
  }
  onFetchData(){
    this.datastorageService.getRecepie();
  }
}
