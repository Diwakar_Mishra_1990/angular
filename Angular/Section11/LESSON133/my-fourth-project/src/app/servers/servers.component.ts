import { Component, OnInit } from '@angular/core';
import { ServersService } from './servers.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  private servers: {id: number, name: string, status: string}[] = [];

  constructor(private serversService: ServersService, private router:Router , private route:ActivatedRoute)
               { }

  ngOnInit() {
    this.servers = this.serversService.getServers();
  }
  reloadPage(){
    //this.router.navigate(['/servers']);//No idea of relative path and is same a localhost:4200/servers
     //this.router.navigate(['servers']);//No idea of relative path and is same a localhost:4200/servers
    //this.router.navigate(['servers'],{relativeTo:this.route});This will give erro because we have an idea of relative to and issame as localhost:4200/server/server
    this.router.navigate(['/servers'],{relativeTo:this.route});//This is same as localhost:4200/server
  }
}
