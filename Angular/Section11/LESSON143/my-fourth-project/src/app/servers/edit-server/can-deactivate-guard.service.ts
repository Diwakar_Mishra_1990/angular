import { Observable } from 'rxjs';
import { CanDeactivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

export interface CanComponentDeactivate{
    canDeActivate: ()=> Observable<boolean> | Promise<boolean> | boolean;
}

export class CanDeActivateGuard implements CanDeactivate<CanComponentDeactivate>{
    canDeactivate(component: CanComponentDeactivate,
         currentRoute: ActivatedRouteSnapshot, 
         currentState: RouterStateSnapshot,
          nextState?: RouterStateSnapshot):Observable<boolean> | Promise<boolean> | boolean{
       

            return component.canDeActivate();
    }

}