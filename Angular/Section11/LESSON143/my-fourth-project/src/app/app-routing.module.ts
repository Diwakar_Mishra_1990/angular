import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './users/user/user.component';
import { ServersComponent } from './servers/servers.component';
import { ServerComponent } from './servers/server/server.component';
import { EditServerComponent } from './servers/edit-server/edit-server.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthGuard } from './auth-guard.service';
import { CanDeActivateGuard } from './servers/edit-server/can-deactivate-guard.service';
import { ErrorComponent } from './error/error.component';

const appRoutes:Routes = [
    {path: '' , component:HomeComponent}, //localhost:4200
    {path: 'users' , component:UsersComponent,children:[
      {path: ':id/:name' , component:UserComponent}, //localhost:4200/users/{id}/{name}
    ]}, //localhost:4200/users/{id}/{name}
    {path: 'servers' ,  component:ServersComponent , children:[
      {path: ':id' , component:ServerComponent}, //localhost:4200/servers
      {path: ':id/edit' , component:EditServerComponent , canDeactivate:[CanDeActivateGuard]}, //localhost:4200/servers/{1}/edit
    ]}, //localhost:4200/servers
    {path:'**', component:ErrorComponent ,data:{message:'Page Not Found!!!!!'}},
    //{path:'**', redirectTo:'/not-found'}
  ];

@NgModule(
    {
        imports:[
            RouterModule.forRoot(appRoutes, {useHash:true})
        ],
        exports:[
            RouterModule
        ]
    }
)
export class AppRoutingModule{
    

}