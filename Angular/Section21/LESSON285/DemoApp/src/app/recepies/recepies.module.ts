import { NgModule } from "@angular/core";
import { RecepiesComponent } from './recepies.component';
import { RecepieListComponent } from './recepie-list/recepie-list.component';
import { RecepieDetailComponent } from './recepie-detail/recepie-detail.component';
import { RecepieItemComponent } from './recepie-list/recepie-item/recepie-item.component';
import { RecepieStartComponent } from './recepie-start/recepie-start.component';
import { RecepieEditComponent } from './recepie-edit/recepie-edit.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DropdownDirective } from '../shared/directive/dropdown.directive';
import { CommonModule } from '@angular/common';
import { RecepieRoutingModule } from './recepie-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    declarations: [
        RecepieListComponent,
        RecepieDetailComponent,
        RecepieItemComponent,
        RecepieStartComponent,
        RecepieEditComponent,
        RecepiesComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RecepieRoutingModule,
        SharedModule
    ]

})
export class RecepiesModule{
    
}