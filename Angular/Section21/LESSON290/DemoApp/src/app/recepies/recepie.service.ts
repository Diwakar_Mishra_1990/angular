import { Recepie } from './recepies.model';
import { EventEmitter, Injectable } from '@angular/core';
import { Ingrdient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Subject } from 'rxjs';

@Injectable()
export class RecepieService{
    
    //recepieSelected = new EventEmitter<Recepie>();
    recepieChanged = new Subject<Recepie[]>();
    private recepies: Recepie[] = [
        new Recepie('Chicken Recepie',
        'Butter Chicken',
        'https://realhousemoms.com/wp-content/uploads/Honey-Butter-Chicken-IG.jpg',
        [
            new Ingrdient('Chicken',100),
            new Ingrdient('Butter',50),
        ]),
        new Recepie('Paneer Recepie',
        'Paneer Chicken',
        'https://www.indianhealthyrecipes.com/wp-content/uploads/2016/03/kadai-paneer-1.jpg',
        [
            new Ingrdient('Paneer',100),
            new Ingrdient('Butter',400),
        ])
    
      ];

      constructor(private shoppingListService:ShoppingListService){}


      getRecepie(){
          return this.recepies.slice();
      }
      getRecepiebyId(index:number){
        return this.recepies.slice()[index];
      }
      addIngredientsToShoppingList(ingredients:Ingrdient[]){
        this.shoppingListService.addIngredients(ingredients);
      }

      addRecepie(recepie:Recepie){
        this.recepies.push(recepie);
        this.recepieChanged.next(this.recepies.slice());
      }

      updateRecepie(index: number,recepie:Recepie){
        this.recepies[index] = recepie;
        this.recepieChanged.next(this.recepies.slice());
      }
      
      deleteRecepie(index:number){
        this.recepies.splice(index,1);
        this.recepieChanged.next(this.recepies);
      }

      setRecepies(recepies: Recepie[]) {
        this.recepies = recepies;
        this.recepieChanged.next(this.recepies.slice());
    }
      
}