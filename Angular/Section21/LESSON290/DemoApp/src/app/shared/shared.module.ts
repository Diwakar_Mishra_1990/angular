import { NgModel } from '@angular/forms';
import { NgModule } from '@angular/core';
import { DropdownDirective } from './directive/dropdown.directive';

@NgModule({
    declarations:[
        DropdownDirective
    ],
    exports:[DropdownDirective]
})
export class SharedModule{

}