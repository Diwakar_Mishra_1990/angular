import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { HomeComponent } from './core/home/home.component';


const routes: Routes = [
  {path:'' , component:HomeComponent},//localhpst:4200   =>This will direct to recepies page
  {path:'recepies',loadChildren:'./recepies/recepies.module#RecepiesModule'},
  {path:'shopping-list' , component:ShoppingListComponent}// localhost:4200/shopping-list  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
