import { Component, OnInit, Input } from '@angular/core';
import { Recepie } from '../recepies.model';
import { RecepieService } from '../recepie.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-recepie-detail',
  templateUrl: './recepie-detail.component.html',
  styleUrls: ['./recepie-detail.component.css']
})
export class RecepieDetailComponent implements OnInit {
  recepie:Recepie;
  id:number;
  constructor(private recepieService:RecepieService, private route:ActivatedRoute , private router:Router) { }

  ngOnInit() {
    const id = +this.route.params.subscribe(
      (params:Params)=>{
        this.id = +params['id'];
        this.recepie = this.recepieService.getRecepiebyId(this.id);
      }
    );
  }
  onAddToShoppingList(){
    this.recepieService.addIngredientsToShoppingList(this.recepie.ingredients)
  }
  onEditRecepie(){
    this.router.navigate(['../',this.id,'edit'],{relativeTo: this.route});
  }
  onDeleteRecepie(){
    this.recepieService.deleteRecepie(this.id);
    this.router.navigate(['../'],{relativeTo: this.route});
  }

}
