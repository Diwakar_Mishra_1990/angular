import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { RecepieService } from '../recepies/recepie.service';
import { DataStorageService } from '../shared/server/data-storage.service';
import { AuthService } from '../auth/auth.service';
import { AuthGuard } from '../auth/auth-guard.service';
import { AppRoutingModule } from '../app-routing.module';

@NgModule({
    declarations:[
        HeaderComponent,
        HomeComponent
    ],
    imports:[
        CommonModule,
        SharedModule,
        AppRoutingModule
    ],
    exports:[
        HeaderComponent,
        AppRoutingModule
    ],
    providers: [ShoppingListService,RecepieService, DataStorageService, AuthService,AuthGuard]
})
export class CoreModule{

}