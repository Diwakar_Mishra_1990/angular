import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';


const routes: Routes = [
    {path:'signUp' , component:SignupComponent},// localhost:4200/signUp
    {path:'signIn' , component:SigninComponent}// localhost:4200/signIn
  ];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule{

}