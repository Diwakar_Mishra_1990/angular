import { Ingrdient } from '../shared/ingredient.model';

export class Recepie{
    public name:String;
    public description:String;
    public imagePath:String;
    public ingredients:Ingrdient[];

    constructor(name:String,description:String, imagePath:String ,ingredients:Ingrdient[]){
       this.name = name;
       this.description = description;
       this.imagePath = imagePath;
       this.ingredients=ingredients;
    }

}