import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { HomeComponent } from './home/home.component';
import { RecepiesModule } from './recepies/recepies.module';

const routes: Routes = [
  {path:'' , component:HomeComponent},//localhpst:4200   =>This will direct to recepies page
  {path:'recepies',loadChildren:'./recepies/recepies.module#RecepiesModule'},
  {path:'shopping-list' , component:ShoppingListComponent}// localhost:4200/shopping-list  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
