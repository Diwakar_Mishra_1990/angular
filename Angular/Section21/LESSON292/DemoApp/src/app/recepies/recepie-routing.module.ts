import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecepiesComponent } from './recepies.component';
import { RecepieStartComponent } from './recepie-start/recepie-start.component';
import { RecepieEditComponent } from './recepie-edit/recepie-edit.component';
import { RecepieDetailComponent } from './recepie-detail/recepie-detail.component';
import { AuthGuard } from '../auth/auth-guard.service';

const recepieRoutes: Routes = [
      {path:'' , component:RecepiesComponent , children:[
      {path: '' ,component:RecepieStartComponent}, // localhost:4200/recepies
      {path:'new' , component:RecepieEditComponent , canActivate:[AuthGuard]},// localhost:4200/recepies/new
      {path: ':id' ,component:RecepieDetailComponent}, // localhost:4200/recepies/{id}
      {path:':id/edit' , component:RecepieEditComponent ,canActivate:[AuthGuard]},// localhost:4200/recepies/1/edit
    ]} 
    ];

@NgModule({
imports: [RouterModule.forChild(recepieRoutes)],
exports:[RouterModule]
})
export class RecepieRoutingModule{

}