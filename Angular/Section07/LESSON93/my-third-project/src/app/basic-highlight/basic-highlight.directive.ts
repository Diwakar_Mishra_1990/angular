import { Directive, ElementRef, OnInit, HostListener, Renderer2 } from '@angular/core';

@Directive({
    selector: '[appBasicHighlight]'
})
export class BasicHighlightDirective implements OnInit{
    ngOnInit(): void {
        this.elRef.nativeElement.style.backgroundColor = 'green';
    }
    constructor(private elRef:ElementRef ,private renderer:Renderer2) { }
    @HostListener('window:scroll') scroll (eventData: Event){
        this.renderer.setStyle(this.elRef.nativeElement,'background-color','black');
        console.log("test");
      }
}