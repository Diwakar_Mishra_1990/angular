import { Directive, ElementRef, Renderer2, OnInit } from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit{
  ngOnInit(): void {
      this.renderer.setStyle(this.elRef.nativeElement,'background-color','blue');
  }

  constructor(private elRef:ElementRef ,private renderer:Renderer2) { }

}
