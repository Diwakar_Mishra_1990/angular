import { Directive, ElementRef, Renderer2, OnInit, HostListener, HostBinding, Input } from '@angular/core';
import { MockNgModuleResolver } from '@angular/compiler/testing';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit{
  @Input() defaultColor:string = 'transparent';
  @Input() highlightedColor:string = 'blue';
  ngOnInit(): void {
      //this.renderer.setStyle(this.elRef.nativeElement,'background-color','blue');
      this.backgroundColor=this.defaultColor;
  }
  
  constructor(private elRef:ElementRef ,private renderer:Renderer2) { }

  @HostBinding('style.backgroundColor') backgroundColor :string;
  
  @HostListener('mouseenter') mouseOver (eventData: Event){
    //this.renderer.setStyle(this.elRef.nativeElement,'background-color','blue');
    this.backgroundColor=this.highlightedColor;
  }
  @HostListener('mouseleave') mouseLeave (eventData: Event){
    //this.renderer.setStyle(this.elRef.nativeElement,'background-color','transparent');
    this.backgroundColor=this.defaultColor;
  }
  

}
