import { Directive, ElementRef, Renderer2, OnInit, HostListener, HostBinding } from '@angular/core';
import { MockNgModuleResolver } from '@angular/compiler/testing';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit{
  ngOnInit(): void {
      //this.renderer.setStyle(this.elRef.nativeElement,'background-color','blue');
  }
  
  constructor(private elRef:ElementRef ,private renderer:Renderer2) { }

  @HostBinding('style.backgroundColor') backgroundColor :string= 'transparent';
  
  @HostListener('mouseenter') mouseOver (eventData: Event){
    //this.renderer.setStyle(this.elRef.nativeElement,'background-color','blue');
    this.backgroundColor='blue';
  }
  @HostListener('mouseleave') mouseLeave (eventData: Event){
    //this.renderer.setStyle(this.elRef.nativeElement,'background-color','transparent');
    this.backgroundColor='transparent';
  }
  

}
