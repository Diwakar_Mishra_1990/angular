import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Recepie } from '../../recepies.model';

@Component({
  selector: 'app-recepie-item',
  templateUrl: './recepie-item.component.html',
  styleUrls: ['./recepie-item.component.css']
})
export class RecepieItemComponent implements OnInit {
  @Input() recepie: Recepie;
  @Output() recepieSelected =new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  onSelectItem(){
    console.log('recepie-item');
    this.recepieSelected.emit();
  }
}
