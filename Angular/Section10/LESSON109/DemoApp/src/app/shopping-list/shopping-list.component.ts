import { Component, OnInit } from '@angular/core';
import { Ingrdient } from '../shared/ingredient.model';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  ingredients: Ingrdient[] =[
    new Ingrdient('Apples',5),
    new Ingrdient('Tomato',10)
  ];
  constructor() { }

  ngOnInit() {
  }
  onIngredientAdded(event){
    console.log(10);
    this.ingredients.push(event);
  }
}
