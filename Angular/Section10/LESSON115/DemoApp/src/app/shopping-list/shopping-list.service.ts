import { Ingrdient } from '../shared/ingredient.model';
import { EventEmitter } from '@angular/core';

export class ShoppingListService {
    ingredientAdded = new EventEmitter<Ingrdient[]>();
    ingredients: Ingrdient[] =[
        new Ingrdient('Apples',5),
        new Ingrdient('Tomato',10)
      ];
      getIngredient(){
        return this.ingredients.slice();
      }
      addIngredient(ingredient:Ingrdient){
        this.ingredients.push(ingredient);
        this.ingredientAdded.emit(this.ingredients);
      }
      addIngredients(ingredients:Ingrdient[]){
        /*
         for(let ingredient of ingredients){
            this.addIngredient(ingredient);
         }
         */
        this.ingredients.push(...ingredients);
        this.ingredientAdded.emit(this.ingredients.slice());
      }
}