import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Recepie } from '../../recepies.model';
import { RecepieService } from '../../recepie.service';

@Component({
  selector: 'app-recepie-item',
  templateUrl: './recepie-item.component.html',
  styleUrls: ['./recepie-item.component.css']
})
export class RecepieItemComponent implements OnInit {
  @Input() recepie: Recepie;
  //@Output() recepieSelected =new EventEmitter(); // Commented because we are using RecepieService to emit events
  constructor(private recepieService:RecepieService) { }

  ngOnInit() {
  }
  onSelectItem(){
    /*console.log('recepie-item');
    this.recepieSelected.emit();*/
    // removed because we use RecepieService to emit events
    this.recepieService.recepieSelected.emit(this.recepie);
  }
}
