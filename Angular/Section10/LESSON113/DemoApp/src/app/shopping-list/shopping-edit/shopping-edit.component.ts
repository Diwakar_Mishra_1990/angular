import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { Ingrdient } from 'src/app/shared/ingredient.model';
import { ShoppingListService } from '../shopping-list.service';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
  @ViewChild('nameInput') nameInputRef:ElementRef;
  @ViewChild('amountInput') amountInputRef:ElementRef;
  //@Output() ingredientAdded = new EventEmitter<{name:string, amount:number}>(); // commenting because we are using services for communicating
  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {

  }
  onAddItem(){
    const nameinput = this.nameInputRef.nativeElement.value;
    const amountinput = this.amountInputRef.nativeElement.value;
    const newIngredient = new Ingrdient(nameinput,amountinput);
    //this.ingredientAdded.emit(newIngredient);// commenting because we are using services for communicating
    this.shoppingListService.addIngredient(newIngredient);
  }

}
