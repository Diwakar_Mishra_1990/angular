import { Ingrdient } from '../shared/ingredient.model';

export class ShoppingListService {
    ingredients: Ingrdient[] =[
        new Ingrdient('Apples',5),
        new Ingrdient('Tomato',10)
      ];
      getIngredient(){
        return this.ingredients.slice();
      }
      addIngredient(ingredient:Ingrdient){
        this.ingredients.push(ingredient);
      }
}