import { Recepie } from './recepies.model';
import { EventEmitter } from '@angular/core';

export class RecepieService{
    recepieSelected = new EventEmitter<Recepie>();

    private recepies: Recepie[] = [
        new Recepie('Chicken Recepie','Butter Chicken','https://realhousemoms.com/wp-content/uploads/Honey-Butter-Chicken-IG.jpg'),
        new Recepie('Paneer Recepie','Paneer Chicken','https://www.indianhealthyrecipes.com/wp-content/uploads/2016/03/kadai-paneer-1.jpg'),
    
      ];

      getRecepie(){
          return this.recepies.slice();
      }
}