import { Recepie } from './recepies.model';
import { EventEmitter } from '@angular/core';
import { Ingrdient } from '../shared/ingredient.model';

export class RecepieService{
    recepieSelected = new EventEmitter<Recepie>();

    private recepies: Recepie[] = [
        new Recepie('Chicken Recepie',
        'Butter Chicken',
        'https://realhousemoms.com/wp-content/uploads/Honey-Butter-Chicken-IG.jpg',
        [
            new Ingrdient('Chicken',100),
            new Ingrdient('Butter',50),
        ]),
        new Recepie('Paneer Recepie',
        'Paneer Chicken',
        'https://www.indianhealthyrecipes.com/wp-content/uploads/2016/03/kadai-paneer-1.jpg',
        [
            new Ingrdient('Paneer',100),
            new Ingrdient('Butter',400),
        ])
    
      ];

      getRecepie(){
          return this.recepies.slice();
      }
}