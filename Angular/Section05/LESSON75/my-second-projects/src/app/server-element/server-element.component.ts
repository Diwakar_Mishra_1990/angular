import { Component, OnInit, Input , OnChanges, ContentChild} from '@angular/core';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css']
})
export class ServerElementComponent implements OnInit {
  @Input() element:{type:string , name:string, content:string};
  @ContentChild('contentParagraph') contentParagrph;
  ngOnChanges(){
    console.log('ngOnChanges')
  }
  constructor() {
    console.log('Constructor')
   }

  ngAfterContentInit() {
    console.log(this.contentParagrph)
  }
  ngOnInit(){
    
  }


}
