import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  serverElements = [
    {type:'server', name:'TestServer' ,content:'Just a Test!'}  
  ];
  /*
  moved to cockpit.component.ts
  newServerName = '';
  newServerContent = '';
  */
/*
moved to cockpit-component.ts
  onAddServer() {
    this.serverElements.push({
      type: 'server',
      name: this.newServerName,
      content: this.newServerContent
    });
  }

  onAddBlueprint() {
    this.serverElements.push({
      type: 'blueprint',
      name: this.newServerName,
      content: this.newServerContent
    });
  }
  */

 onServerAdded(serverData) {
   this.serverElements.push({
    type: 'server',
    name: serverData.serverName,
    content: serverData.serverContent
  });
}

onBlueprintAdded(blureprintData) {
    this.serverElements.push({
    type: 'blueprint',
    name: blureprintData.serverName,
    content: blureprintData.serverContent
  });
}
}
