import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';


@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {
  //newServerName = '';
  //newServerContent = '';
  @Output() serverCreated = new EventEmitter<{serverName:string, serverContent:string}>();
  @Output() blueprintCreated = new EventEmitter<{serverName:string, serverContent:string}>();
  @ViewChild('serverName') serverNameReference:ElementRef;
  @ViewChild('serverContent') serverContentReference:ElementRef;
  constructor() { }

  ngOnInit() {
  }
  onAddServer() {
    console.log(this.serverNameReference);
    console.log(this.serverContentReference);
    //commented temporarily to avoid failures
    /*
    this.serverElements.push({
      type: 'server',
      name: this.newServerName,
      content: this.newServerContent
    });
    */
   /*this.serverCreated.emit({
     serverName: this.newServerName,
     serverContent: this.newServerContent
   });*/
   this.serverCreated.emit({
     serverName: this.serverNameReference.nativeElement.value,
     serverContent: this.serverContentReference.nativeElement.value
   });
  }

  onAddBlueprint(event) {
    console.log(this.serverNameReference);
    console.log(this.serverContentReference);
    //commented temporarily to avoid failures
    /*
    this.serverElements.push({
      type: 'blueprint',
      name: this.newServerName,
      content: this.newServerContent
    });
    */
   this.blueprintCreated.emit({
    serverName: this.serverNameReference.nativeElement.value,
    serverContent: this.serverContentReference.nativeElement.value
   });
  }
}
