import { Component, OnInit, Input , OnChanges} from '@angular/core';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css']
})
export class ServerElementComponent implements OnInit {
  @Input() element:{type:string , name:string, content:string};
  
  ngOnChanges(){
    console.log('ngOnChanges')
  }
  constructor() {
    console.log('Constructor')
   }

  ngOnInit() {
    console.log('ngOnInit')
  }


}
