import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'DemoApp';
  featureType:string= 'recepie' // value is Default recepie 
  onSelected(event){
  this.featureType =event;    
  }
}
