import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Recepie } from '../recepies.model';
import { RecepieService } from '../recepie.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-recepie-list',
  templateUrl: './recepie-list.component.html',
  styleUrls: ['./recepie-list.component.css']
})
export class RecepieListComponent implements OnInit {
  //@Output() recepieWasSelected = new EventEmitter<Recepie>();
  /*
  recepies: Recepie[] = [
    new Recepie('Chicken Recepie','Butter Chicken','https://realhousemoms.com/wp-content/uploads/Honey-Butter-Chicken-IG.jpg'),
    new Recepie('Paneer Recepie','Paneer Chicken','https://www.indianhealthyrecipes.com/wp-content/uploads/2016/03/kadai-paneer-1.jpg'),

  ];
  */
  recepies: Recepie[];
  @Input() index:number;
  constructor(private recepieService:RecepieService, private router:Router, private route:ActivatedRoute) { }

  ngOnInit() {
    this.recepies=this.recepieService.getRecepie();
  }
  onSelectedRecepie(recepie:Recepie){
    //console.log('recepielist');
    //this.recepieWasSelected.emit(recepie);
  }
  onNewRecepie(){
    this.router.navigate(['new'], {relativeTo: this.route})
  }
}
