import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecepiesComponent } from './recepies/recepies.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { RecepieStartComponent } from './recepies/recepie-start/recepie-start.component';
import { RecepieDetailComponent } from './recepies/recepie-detail/recepie-detail.component';

const routes: Routes = [
  {path:'' , redirectTo:'/recepies', pathMatch:'full'},//localhpst:4200   =>This will direct to recepies page
  {path:'recepies' , component:RecepiesComponent , children:[
    {path: '' ,component:RecepieStartComponent},// localhost:4200/recepies
    {path: ':id' ,component:RecepieDetailComponent},
  ]}, 
  {path:'shopping-list' , component:ShoppingListComponent},// localhost:4200/shopping-list
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
