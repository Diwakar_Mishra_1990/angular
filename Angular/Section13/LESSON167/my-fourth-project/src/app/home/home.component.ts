import { Component, OnInit } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import 'rxjs/Rx';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    /*
    const myNumbers = Observable.interval(1000);
    myNumbers.subscribe(
      (number:number)=>{
        console.log(number);
      }
    );
    */
   const myObservable = Observable.create(
     (observer:Observer<string>)=>{
       setTimeout(
         ()=>{
            observer.next('first package');
         },2000);
         setTimeout(
          ()=>{
             observer.next('first package');
          },4000);
          setTimeout(
            ()=>{
               observer.error('this does not work');
            },8000);
          setTimeout(
              ()=>{
                 observer.complete();
              },6000);  
     });
     myObservable.subscribe(
       (data:string)=>{console.log(data);},
       (error:string)=>{console.log(error);},
       ()=>{console.log("completed");}
     );
  }

}
