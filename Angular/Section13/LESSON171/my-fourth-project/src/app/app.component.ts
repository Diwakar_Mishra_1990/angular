import { Component, OnInit } from '@angular/core';
import { UsersService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(private userService:UsersService){}
  user1Activated = false;
  user2Activated = false;
  ngOnInit(): void {
    this.userService.userActivated.subscribe(
      (id:number)=>{
        if(id ===1){
          this.user1Activated=true;
        }
        else if(id ===2){
          this.user2Activated=true;
        }
      }
    );
  }
}
