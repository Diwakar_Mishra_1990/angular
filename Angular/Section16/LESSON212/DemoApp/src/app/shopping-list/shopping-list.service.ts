import { Ingrdient } from '../shared/ingredient.model';
import { EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';

export class ShoppingListService {
    //ingredientAdded = new EventEmitter<Ingrdient[]>();
    ingredientAdded = new Subject<Ingrdient[]>();
    startedEditing = new Subject<number>();
    ingredients: Ingrdient[] =[
        new Ingrdient('Apples',5),
        new Ingrdient('Tomato',10)
      ];
      getIngredient(){
        return this.ingredients.slice();
      }
      addIngredient(ingredient:Ingrdient){
        this.ingredients.push(ingredient);
        this.ingredientAdded.next(this.ingredients);
      }
      addIngredients(ingredients:Ingrdient[]){
        /*
         for(let ingredient of ingredients){
            this.addIngredient(ingredient);
         }
         */
        this.ingredients.push(...ingredients);
        //this.ingredientAdded.emit(this.ingredients.slice());
        this.ingredientAdded.next(this.ingredients.slice());
      }
}