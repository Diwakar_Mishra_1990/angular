import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { Ingrdient } from 'src/app/shared/ingredient.model';
import { ShoppingListService } from '../shopping-list.service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { shallowEqual } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
  subscription:Subscription;
  editMode=false;
  editedItemIndex:number ; 
  //@ViewChild('nameInput') nameInputRef:ElementRef;
  //@ViewChild('amountInput') amountInputRef:ElementRef;
  //@Output() ingredientAdded = new EventEmitter<{name:string, amount:number}>(); // commenting because we are using services for communicating
  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
    this.subscription = this.shoppingListService.startedEditing.subscribe(
      (index:number) =>{
        this.editedItemIndex = index;
        this.editMode = true;
      }
    );

  }
  onAddItem(form: NgForm){
    //const nameinput = this.nameInputRef.nativeElement.value;
    //const amountinput = this.amountInputRef.nativeElement.value;
    //const newIngredient = new Ingrdient(nameinput,amountinput);
    //this.ingredientAdded.emit(newIngredient);
    const value = form.value;
    console.log(value);
    const newIngredient = new Ingrdient(value.name,value.amount);
    this.shoppingListService.addIngredient(newIngredient);
  }

}
