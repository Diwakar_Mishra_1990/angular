import { Component, OnInit } from '@angular/core';
import { Ingrdient } from '../shared/ingredient.model';
import { ShoppingListService } from './shopping-list.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  /*
  ingredients: Ingrdient[] =[
    new Ingrdient('Apples',5),
    new Ingrdient('Tomato',10)
  ];
  */
  ingredients: Ingrdient[];
  private subscription: Subscription;
  constructor(private shoppingListService:ShoppingListService) { }

  ngOnInit() {
    this.ingredients = this.shoppingListService.getIngredient();
    this.subscription =  this.shoppingListService.ingredientAdded.subscribe(
        (ingredients:Ingrdient[])=>{
          this.ingredients=ingredients;
        }
    );
    this.subscription =  this.shoppingListService.ingredientChanged.subscribe(
      (ingredients:Ingrdient[])=>{
        this.ingredients=ingredients;
      }
  );
  }
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
  /*
  commenting out because we are using shopping-list service for communication
  onIngredientAdded(event){
    console.log(10);
    this.ingredients.push(event);
  }
  */

 onEditItem(index:number){
    this.shoppingListService.startedEditing.next(index);
 }
}
