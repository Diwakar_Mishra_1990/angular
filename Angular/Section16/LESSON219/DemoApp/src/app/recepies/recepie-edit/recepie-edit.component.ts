import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { RecepieService } from '../recepie.service';

@Component({
  selector: 'app-recepie-edit',
  templateUrl: './recepie-edit.component.html',
  styleUrls: ['./recepie-edit.component.css']
})
export class RecepieEditComponent implements OnInit {
  id:number;
  editMode = false;
  recepieForm:FormGroup;
  constructor(private route:ActivatedRoute , private recepieService:RecepieService) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params:Params) =>{
        this.id = +params['id'];
        this.editMode = params['id']!= null;
        this.initForm();
      }
    );
  }
  private initForm(){
    
    let recepieName:String='';
    let recepieImagePath:String = '';
    let recepieDescription:String   = '';
    if(this.editMode){
      const recepie = this.recepieService.getRecepiebyId(this.id);
      recepieName = recepie.name;
      recepieImagePath = recepie.imagePath;
      recepieDescription = recepie.description;
    }
    this.recepieForm=new FormGroup(
      {
        'name': new FormControl(recepieName),
        'imagePath': new FormControl(recepieImagePath),
        'description': new FormControl(recepieDescription)
      }
    );
  }

}
