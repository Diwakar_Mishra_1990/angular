import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { Ingrdient } from 'src/app/shared/ingredient.model';
import { ShoppingListService } from '../shopping-list.service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { shallowEqual } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
  @ViewChild('f') slForm:NgForm;
  subscription:Subscription;
  editMode:boolean=false;
  editedItemIndex:number ; 
  editedItem:Ingrdient;
  //@ViewChild('nameInput') nameInputRef:ElementRef;
  //@ViewChild('amountInput') amountInputRef:ElementRef;
  //@Output() ingredientAdded = new EventEmitter<{name:string, amount:number}>(); // commenting because we are using services for communicating
  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
    this.subscription = this.shoppingListService.startedEditing.subscribe(
      (index:number) =>{
        this.editedItemIndex = index;
        this.editMode = true;
        this.editedItem = this.shoppingListService.getIngredientById(this.editedItemIndex);
        console.log(index);
        this.slForm.setValue(
          {
            name:this.editedItem.name,
            amount:this.editedItem.amount
          }
        );
      }
    );

  }
  onAddItem(form: NgForm){
    //const nameinput = this.nameInputRef.nativeElement.value;
    //const amountinput = this.amountInputRef.nativeElement.value;
    //const newIngredient = new Ingrdient(nameinput,amountinput);
    //this.ingredientAdded.emit(newIngredient);
    const value = form.value;
    const newIngredient = new Ingrdient(value.name,value.amount);
    if(this.editMode){
      this.shoppingListService.updateIngredient(this.editedItemIndex,newIngredient);
    }
    else{
      this.shoppingListService.addIngredient(newIngredient);
    }
    
    this.editMode = false;
    form.reset();
  }
  onClear(){
    this.editMode = false;
    this.slForm.reset();
  }

}
