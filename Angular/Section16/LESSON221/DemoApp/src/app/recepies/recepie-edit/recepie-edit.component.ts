import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { RecepieService } from '../recepie.service';

@Component({
  selector: 'app-recepie-edit',
  templateUrl: './recepie-edit.component.html',
  styleUrls: ['./recepie-edit.component.css']
})
export class RecepieEditComponent implements OnInit {
  id:number;
  editMode = false;
  recepieForm:FormGroup;
  constructor(private route:ActivatedRoute , private recepieService:RecepieService) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params:Params) =>{
        this.id = +params['id'];
        this.editMode = params['id']!= null;
        this.initForm();
      }
    );
  }
  private initForm(){
    
    let recepieName:String='';
    let recepieImagePath:String = '';
    let recepieDescription:String   = '';
    let recepieIngredients = new FormArray([]);
    if(this.editMode){
      const recepie = this.recepieService.getRecepiebyId(this.id);
      recepieName = recepie.name;
      recepieImagePath = recepie.imagePath;
      recepieDescription = recepie.description;
      if(recepie['ingredients']){
        for(let ingredient of recepie.ingredients){
          recepieIngredients.push(
            new FormGroup(
              {
               'name':new FormControl(ingredient.name),
               'amount':new FormControl(ingredient.amount)
              }
            )
          );
        }
      }
    }
    this.recepieForm=new FormGroup(
      {
        'name': new FormControl(recepieName),
        'imagePath': new FormControl(recepieImagePath),
        'description': new FormControl(recepieDescription),
        'ingredients': recepieIngredients
      }
    );
  }
  
  onSubmit(){
    console.log(this.recepieForm);
  }
}
