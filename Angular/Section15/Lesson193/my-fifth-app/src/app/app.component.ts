import { Component, ElementRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('f') signupForm;
  defaultQuestion="pet";
  answer="defaultAnswer";
  genders=['male','female'];
  user={
    username:'',
    email:'',
    secretQuestion:'',
    secretanswer:'',
    gender:''
  };
  submitted=false;
  suggestUserName() {
    const suggestedName = 'SuperuserDefault';
    /*
    this.signupForm.setValue({
      userData:{
        username:suggestedName,
        email:'diwakarmishra1990Default@yahoo.com',
      },
      secret: 'pet',
      questionAnswer: 'DEFAULT ANSWER',
      gender: 'male'
    });
     */
    this.signupForm.form.patchValue({
      userData:{
        username:'patchedUsername'
      }
    });
  }
  /*
  onSubmit(form:NgForm){
    //console.log("SUBMITTED");
    console.log(form);
  }
  */
  onSubmit(){
    this.submitted = true;
   this.user.username=this.signupForm.value.userData.username;
   this.user.email=this.signupForm.value.userData.email;
   this.user.secretQuestion = this.signupForm.value.secret;
   this.user.secretanswer= this.signupForm.value.questionAnswer;
   this.user.gender = this.signupForm.value.gender;
  }
}
