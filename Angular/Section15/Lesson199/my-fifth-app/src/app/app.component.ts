import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  genders = ['male', 'female'];
  signupForm: FormGroup;

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      'username': new FormControl('defaultUserName'),
      'email': new FormControl('diwakarmishra1990default@gmail.com'),
      'gender': new FormControl('male')
    });
  }
  onSubmit(){
    console.log(this.signupForm);
  }
}
