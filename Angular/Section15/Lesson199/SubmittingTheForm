In the last lecture, we successfully synchronized our HTML form and our TypeScript form,
let's now have a look at how we can submit this form. In the template driven approach, we used ngSubmit,
the ngSubmit directive here on this form element.
Well we still do the same here because we still want to react to this default submit event which is
fired by HTML, by Javascript.
So we still add ngSubmit here and we could execute an onSubmit method, any method you like.
So let's add this method here now, onSubmit, the difference to the template driven approach
now is that we don't need to get the form via this local reference, that actually wouldn't work
anymore because we're not using Angular's auto-creation mechanism
but we don't need to get this reference because hey we created the form on our own,
we already got access to it here in our TypeScript code,
actually in every method in the TypeScript code. So we can simply console log this signupForm, just
like this and this is how we get access to the form.
Now if we go back to our running project and we simply enter something here, switch this to female to see
if it is correctly updated and hit submit, we see our FormGroup here and it has the properties we already know,
amongst them the value which correctly represents the object we passed here, key-value pairs with username,
e-mail and gender and the appropriate values.
This is the cool thing about the reactive approach, whatever you set up here as an argument, whatever
you set up here as an object you pass to the FormGroup which makes up your form,
that is what you get out as a value of the form.
So you can bind it to your own model, the model of your application and easily make sure that the form
structure matches the structure of your model.
So this is how you can submit the form, how you can still access the value as you did before but now
using your own form, the form you created in TypeScript.

Please see 01_SUBMIT.png