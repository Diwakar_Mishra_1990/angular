In the last lecture, I was already mentioning that get also takes the path to an element.
What did I mean with that?cIt's very simple,
you can specify a path here because you might have a nested form. Let's say username and e-mail should
be inside a FormGroup, we used such forms in the template driven approach too.
So here, we could create a FormGroup named userData and now this is a new FormGroup again. FormGroup is not only 
there to be used on the overall form, that just happens to be a FormGroup too but you can still have form groups 
in the form groups. So here we could have a FormGroup which of course like the outer FormGroup for the overall form,
also takes a Javascript object and now we can add our form controls inside of this group,
so now we have a nested form.
        ngOnInit(): void {
            this.signupForm = new FormGroup({
                'userData':new FormGroup({
                    'username': new FormControl('defaultUserName',Validators.required),
                    'email': new FormControl('diwakarmishra1990default@gmail.com',[Validators.required, Validators.email]),
                }),
                'gender': new FormControl('male')
            });
        }

We need to reflect this in our HTML template because right now if we have a look at our form, it will break,
you'll see this in the console.
We get an error because correctly, it cannot find a control with the name username because on the overall form, 
there is no such control, it's nested in the userData FormGroup.
So first of all, we need to update our synchronization and we easily do this by wrapping our form controls
here in another element like a div for example,
so the e-mail and username would be the two controls we wrap in there to replicate the structure
we have in the TypeScript object and on this div, we place the formGroupName directive.
So you kind of could see the schema here, formControlName to tell Angular which property in our
TypeScript object representing the form relates to which input and formGroupName to tell it the same
for a FormGroup, so depending on whether you have a new FormGroup or new FormControl here.
So here, the formGroupName is "userData"
    <div formGroupName="userData">
            <div class="form-group">
                <label for="username">Username</label>
                <input
                  type="text"
                  id="username"
                  class="form-control"
                  formControlName="username"
                  >
                  <span class="help-block" *ngIf="!signupForm.get('userData.username').valid && signupForm.get('userData.username').touched">Please enter a valid user name</span>
              </div>
              <div class="form-group">
                <label for="email">email</label>
                <input
                  type="text"
                  id="email"
                  class="form-control"
                  formControlName="email"
                  >
                  <span class="help-block" *ngIf="!signupForm.get('userData.email').valid && signupForm.get('userData.email').touched">Please enter a valid email</span>  
              </div>
        </div>

and now with this tiny addition, I don't need to change anything else, it's almost working again but we still get 
this error here or a different error but still related to the topic, that valid cannot be read on null.
Well that makes sense because get username still fails because what are we trying to do here?
We try to get a form control with the name username on our signupForm, we don't have that here,
we have it nested in userData but that's not the same.
So we need to update get to point to the path or to contain the path to that username and that would be
userData.username and it's as simple as this. You structure your path separated with dots, so if you have a new 
object, just place a dot in between.
So let's paste it everywhere where we used one of these nested controls for e-mail as well
therefore and if we now save this, it should update and not display any errors any more,
instead we should see the behavior we saw before with the messages we get if we entered invalid data
because now the form is working again taking into account form groups, so nesting inside of our form now.
And as you saw, it was really easy to reflect this in our HTML code, IT's all about keeping that in sync in the end.