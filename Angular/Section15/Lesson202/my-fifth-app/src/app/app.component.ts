import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  genders = ['male', 'female'];
  signupForm: FormGroup;

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      'userData':new FormGroup({
        'username': new FormControl('defaultUserName',Validators.required),
        'email': new FormControl('diwakarmishra1990default@gmail.com',[Validators.required, Validators.email]),
      }),
      'gender': new FormControl('male')
    });
  }
  onSubmit(){
    console.log(this.signupForm);
  }
}
