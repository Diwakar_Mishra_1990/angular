import { Component, ElementRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('f') signupForm;
  defaultQuestion="pet";
  answer="defaultAnswer";
  genders=['male','female'];
  suggestUserName() {
    const suggestedName = 'Superuser';
  }
  /*
  onSubmit(form:NgForm){
    //console.log("SUBMITTED");
    console.log(form);
  }
  */
  onSubmit(form:NgForm){
    //console.log("SUBMITTED");
    console.log(this.signupForm);
  }
}
