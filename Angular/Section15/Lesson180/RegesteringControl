In app.component.html we add ngModel in the inputs
        <input type="text" id="username" class="form-control" ngModel>-------------------Line8
        <input type="email" id="email" class="form-control" ngModel>--------------------Line13
        <select id="secret" class="form-control"  ngModel>---------------Line18

This you need to tell Angular hey within that form element, what should be an actual control of my form?
And this is what we're going to do now, tell Angular how does our form look like,
which controls do we want to have? In the template driven approach,
this is super simple. You simply pick the input which you want to add as a control, like this input here
and I'm just going to structure it a bit different to split it up over multiple lines to make it easier
to read and then you add ngModel, like this.
Now you might already know ngModel from the two-way binding, two-way data binding and it actually is
the same directive. In two-way data binding though,
you saw that we use that with square brackets and parentheses wrapped in ngModel.
Now we will have a look at this later again but for now, let's add it without any parentheses, without any square brackets just like this.
This will be enough to tell Angular, hey this input is actually a control of my form, so ngModel in
the end is a directive made available in the forms module, something I mentioned earlier in the course
when we had a look at two-way data binding.
This is key to understand, you can use it to get two-way data binding but it actually is part of a bigger module with more features
giving you full control over forms.

Now for this to work, for this to be recognized as a control in your form, we need to give Angular one other
piece of information, the name of this control.
Right now, it would see OK this input should be part of the Javascript object representation of this
form,so whatever the user enters here as a value should be the value of this control, of what's
the name of that control. We need to give that information to Angular and we do this by adding the normal 
HTML attribute, name. So name is nothing Angular 2 specific, name is the default attribute
you can add to any HTML control.
        <input type="text" id="username" class="form-control" ngModel>-------------------Line8
        <input type="email" id="email" class="form-control" ngModel name = "email">------Line13
        <select id="secret" class="form-control" name = "secret" ngModel>---------------Line18
        

Now here, the name might be username because that is what we can enter in this input
and with this, this control will be registered in this Javascript representation of the form.



Well that is something we're going to have a look at in the next lecture when we see how we can submit