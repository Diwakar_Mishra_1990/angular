import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  genders = ['male', 'female'];
  signupForm: FormGroup;

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      'userData':new FormGroup({
        'username': new FormControl('defaultUserName',Validators.required),
        'email': new FormControl('diwakarmishra1990default@gmail.com',[Validators.required, Validators.email]),
      }),
      'gender': new FormControl('male'),
      'hobbies': new FormArray([
        new FormControl(null)
      ])
    });
    this.signupForm.setValue(
      {
        'userData':{
          'username': 'setValueName',
          'email': 'setValueEmail',
        },
        'gender': 'male',
        'hobbies': ['1','2']
      }
    );
    this.signupForm.patchValue(
      {
        'userData':{
          'username': 'setValueName_patch'
          
        }
       
      }
    );
  }
  onSubmit(){
    console.log(this.signupForm);
  }

  onAddHobby(){
    const control = new FormControl(null, Validators.required);
    (<FormArray>this.signupForm.get('hobbies')).push(control);
  }
}
