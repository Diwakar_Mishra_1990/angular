With the action added, back to the shopping list reducer. Here I'm already handling the

add ingredient case which I'm importing from the shopping list actions.

Now we can also rewrite this import which is not a must but kind of a convention you also see in a lot

of demos and tutorials. Since we export more than just this constant here,

we also export this class here, this add ingredient class into action file, we can import everything from

that file with the star and I don't just want to import it like this but we always have to add an as

keyword and give this an alias to group all the things that are imported from that file into one object

under which we can access them in this file

and here, I'll simply name this shopping list actions.

So this is my shopping lists actions object here,

of course you could name this differently if you wanted to and now we get an error here for add ingredient

because I'm not directly importing this anymore,

so here we can now just say shopping list action.

add ingredient, this is the case we want to check for, this is the constant that holds this identifier.

We also can be more specific about the action we're getting here now,

the action here would be our add ingredient action, so we can set this equal to the type of shopping

list actions.

add ingredient and therefore we can get rid of that action import here in the reducer,

so now we changed the type of the action.

And now here when we want to add a new ingredient, that's not just the action,

instead our action now has a payload and we get auto completion here in TypeScript support because

we are pretty clear about the type of our action.

It's not just the action interface NgRx forces because that only requires us to have a type,

we are more specific,

we have an action which is of type add ingredient which does not just have the type but which also has

a payload and therefore we can access this here without TypeScript yelling at us.

So now with that all done, how can we now use the reducer and our nice setup action to finally setup

that application store which it is all about

to then also use that? Because thus far, we spend a lot of work on implementing something which has no

effect,

well that's about to change. To add our application store,

let's go to our app module and in there, I can remove the login service by the way but that does not

matter too much,

let's import something from @ngrx/store and that something is the store module, that helps

us with setting up that application-wide @ngrx/store. Now store module as the name suggests is an Angular

module and therefore we should add it here, maybe below the other Angular modules we add the store module.

We don't just add it like this though, we also need to tell the store module, we need to tell NgRx

what makes up our store, which reducers are involved and for that, we call for root here and to for root,

we pass a so-called action reducer map. Now that sounds more complicated than it is, in the end it's a Javascript

object where you can define any identifier of your choice, so maybe shopping list and then the reducer

that belongs to that identifier and in my case that's the shopping list reducer function which needs

to be imported from the shopping list folder, there the store folder and there, the shopping list reducer

file, so that shopping list reducer function we worked on over the past lectures. This function is now

assigned as a value to this shopping list key and that key name is totally up to you but of course it

should be descriptive about which feature of the app this reducer belongs to and it is the reducer

for our shopping list part of the app, therefore it makes sense to name this shopping list as well.

With that, we told NgRx where to find our reducer and therefore now when the app restarts, even though

we don't see it yet, NgRx will take that reducer into account and set up an application store

for us where it registers this reducer,

so now any actions that are dispatched will reach that reducer and to see that initial application

state, we can now start fetching some data from that state.

