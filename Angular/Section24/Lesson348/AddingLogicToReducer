So now we have the initial state, we have that function,

what happens inside of the function then?

I mentioned that we now have to find out which kind of action was dispatched

so that we know how to edit our state.

Again don't get confused that we're not dispatching actions yet,

we'll soon add the code to do so. Now to check different kinds of possible actions,

you could add multiple if statements and run different codes depending on which action you have or since

we have multiple possible conditions, you can use a switch case statement and then on the action, you

have to check the type of that action. Now to get better TypeScript support, you can add a type here

to the argument, to the action argument and the type would be action, where action is actually imported

not from this package but where action is actually imported from @ngrx/store. This exports an

action interface

and this simply is an interface that forces any action to have a type property, which is why we can safely

check it here.

So now I'm switching on the action type and now we can just register different cases, so different action

types we want to handle.

Now it's totally up to you as a developer of this application which action types your application has

and here, we could start with a simple one, if we have a look at the shopping service, getting doesn't really

change the state but maybe we start with adding an ingredient because that will change our ingredients

state,

it will add a new one.

So we could start with a simple state which we now give an identifier or type is the identifier but

the value of type is a string.

So here, we could name this add ingredient.

Now it's absolutely up to you what the value here is

but the convention is to use an all uppercase text which kind of is clear about what this action will

do,

so here if I give this action an identifier, a type of add ingredient, it's pretty clear that this

action will probably add an ingredient.

So add ingredient is the case here

and with that case registered, we now have to do what?

If you recall what I said on the slide, we have to return a new state now.

So we get the old state and we need to return a new state,

now what you could do is you could take the state which has the ingredients property and then simply

use push to add a new ingredient and that would be totally wrong because state changes with NgRx

always have to be immutable, which means you must not edit the existing or the previous state,

it's absolutely forbidden and a bad practice if you do so.

So never touch the existing state,

instead return a new object which will replace the old state

and to not lose all the old data, copy the old state with the spread operator. That is next generation

Javascript syntax which is also supported by TypeScript, it essentially pulls out all the properties

of the old state and adds these properties to this new object.

Hence we have a new object with the old data and hence we have a copy of the old state

and if we now edit this copy, it's OK. So we copied all the old state and now I want to overwrite the ingredients,

so I'll add the ingredients property and now we can assign a new value to that.

Now correctly, you would say that the old state was just an object with the ingredients property.

So if I copy the entire old state and I then change the ingredients, I essentially don't need to copy

it and indeed, you could omit this here because we're changing the only thing the state has.

Still, it is a good practice to always copy the old state because later and in other apps, you will certainly

have some states that consist of more than one property

and if you then only change one of the properties and you forget to copy the others, you overwrite the

old state with the new state where you dumped all the properties you didn't change

and that's not something you want.

So rule of thumb, always copy the old state then overwrite what you want to change.

Now ingredients has to be an array and of course here, I don't want to lose my old ingredients. Therefore

in this ingredients array,

I also again copy my old state ingredients now. So I access the previous state which we still have here

in the state argument and there, I access the ingredients property which holds this array of ingredients

and the spread operator can also be used on arrays to pull out all the elements and I then add all these

elements I pulled out to this array here.

So therefore now, the ingredients array is a new array with the old elements in there. Now

side note, the elements themselves are also objects and I didn't copy those,

so here we technically still have the old objects in that array but we don't change them,

so that is fine.

When I do change however is I do add a new element here

and

where do I find this?

It has to be part of the action somehow

but we don't know yet.

So for now, let's just add the entire action object here even though I can already tell you that this

is not the final solution because this is an object that for example has a type property and ingredient

doesn't have a type property,

so adding the action is certainly wrong but it's a first step.

Let's next have a look at these actions and see how we actually create and dispatch actions.