import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import { RecepieService } from 'src/app/recepies/recepie.service';
@Injectable()
export class DataStorageService{
    constructor(private http: Http, private recepieService:RecepieService){}

    storeRecepies(){
        return this.http.put('https://demoprojectudemy.firebaseio.com/recepies.json',this.recepieService.getRecepie())
    }
}