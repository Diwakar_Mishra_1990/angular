import { Component, OnInit } from '@angular/core';
import { DataStorageService } from '../shared/server/data-storage.service';
import {Response} from '@angular/http';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  constructor(private datastorageService:DataStorageService) { }
  ngOnInit() {
  }
  onSaveData(){
    this.datastorageService.storeRecepies().subscribe(
        (response:Response)=>{
          console.log(response);
        }
    );
  }
}
