import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import { RecepieService } from 'src/app/recepies/recepie.service';
import { Recepie } from 'src/app/recepies/recepies.model';
@Injectable()
export class DataStorageService{
    constructor(private http: Http, private recepieService:RecepieService){}

    storeRecepies(){
        return this.http.put('https://demoprojectudemy.firebaseio.com/recepies.json',this.recepieService.getRecepie())
        
    }

    getRecepie(){
       return this.http.get('https://demoprojectudemy.firebaseio.com/recepies.json').subscribe(
           (response:Response)=>{
                const recepies: Recepie[] = response.json();
                this.recepieService.setRecepies(recepies);
           }
       )
    }
    
}