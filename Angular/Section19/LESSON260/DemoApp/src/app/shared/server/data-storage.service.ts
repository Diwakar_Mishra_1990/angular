import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import { RecepieService } from 'src/app/recepies/recepie.service';
import { Recepie } from 'src/app/recepies/recepies.model';
import 'rxjs/rx';
@Injectable()
export class DataStorageService{
    constructor(private http: Http, private recepieService:RecepieService){}

    storeRecepies(){
        return this.http.put('https://demoprojectudemy.firebaseio.com/recepies.json',this.recepieService.getRecepie())
        
    }

    getRecepie(){
       return this.http.get('https://demoprojectudemy.firebaseio.com/recepies.json')
       .map(
           (response:Response) =>{
                const recepies:Recepie[] = response.json();
                for(let recepie of recepies){
                    if(!recepie['ingredients']){
                        console.log(recepie);
                        recepie['ingredients'] =[];
                    }
                } 
                return recepies;
           }
       )
       .subscribe(
           (recepies:Recepie[])=>{
                this.recepieService.setRecepies(recepies);
           }
       )
    }
    
}