import { Component, OnInit } from '@angular/core';

@Component({
  //selector: 'app-servers',//selector is a element tag
  //selector: '[app-servers]',//selector is a property
  selector: '.app-servers',//selector is a class
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
