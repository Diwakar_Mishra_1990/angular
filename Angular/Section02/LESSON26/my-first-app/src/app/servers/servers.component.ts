import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',//selector is a element tag
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer = false;
  serverCreationStatus = 'Np Server was Created!' ;

  constructor() { 
    setTimeout(()=>{
      this.allowNewServer =true;
    },2000);
  }

  onCreateServer() {
    this.serverCreationStatus ="Server Created";
  }
  ngOnInit() {
  }

}
