import { Component, OnInit } from '@angular/core';

@Component({
  //selector: 'app-servers',// selecting based on tag
  //selector: '[app-servers]',// selecting based on attributes
  template: `<app-server></app-server>
  <app-server></app-server>`,
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
