import { Component, EventEmitter, Output } from '@angular/core';
import {LoggingService} from '../logging.service';
import { AccountsService } from '../accounts.service';

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css'],
  //providers:[LoggingService]//Commented because we will use LoggingService in AccountService
})
export class NewAccountComponent {
  //@Output() accountAdded = new EventEmitter<{name: string, status: string}>();
  constructor(private accountService:AccountsService){}//Commented because we will use LoggingService in AccountService
  onCreateAccount(accountName: string, accountStatus: string) {
    
    /*
    Since we created separate AccountsService we dont need any 
    this.accountAdded.emit({
      name: accountName,
      status: accountStatus
    });
    */
    //console.log('A server status changed, new status: ' + accountStatus);
    //const service = new LoggingService();
    //service.logStatusChange(accountStatus);
    this.accountService.addAccount(accountName,accountStatus);
    //this.loggingService.logStatusChange(accountStatus);//Commented because we will use LoggingService in AccountService
  }
}
