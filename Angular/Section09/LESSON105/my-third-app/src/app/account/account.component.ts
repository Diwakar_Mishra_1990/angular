import { Component, EventEmitter, Input, Output } from '@angular/core';
import { LoggingService } from '../logging.service';
import { AccountsService } from '../accounts.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  //providers:[LoggingService]//Commented because we will use LoggingService in AccountService
})
export class AccountComponent {
  @Input() account: {name: string, status: string};
  @Input() id: number;
  //@Output() statusChanged = new EventEmitter<{id: number, newStatus: string}>();
  constructor( private accountsService: AccountsService){}//Commented because we will use LoggingService in AccountService

  onSetTo(status: string) {
    //this.statusChanged.emit({id: this.id, newStatus: status});
    //console.log('A server status changed, new status: ' + status);
    this.accountsService.updateStatus(this.id,status);
    //this.loggingService.logStatusChange(status);//Commented because we will use LoggingService in AccountService
  }
}
