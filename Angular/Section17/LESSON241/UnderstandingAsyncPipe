In this module, we learned a lot about pipes, what they are, how to use them, how to parameterise them, how to change and
how to create our own pipes and how to maybe create an impure pipe forcing update on each data change.
Now there is one built-in pipe I want to have a closer look at which does something different than all
the other pipes, it helps us with handling asynchronous data and to demonstrate how this works,
let's say we have another property right here at the top which says appStatus or which holds our
appStatus which could also be offline, critical, whatever but here this will not be a string like offline,
instead this should get loaded after let's say two seconds and to simulate this, I will set this equal
to a promise and you can imagine this data being returned from a HTTP call, from a server, something like
this. So in this promise, I will initialize this promise with a callback method passed to the constructor
where this method itself takes two arguments, resolve and reject, the two functions we can execute inside
of this promise to resolve or reject the promise and in the promise in this callback function,
I will then set up a timeout, let's say it triggers after 2 seconds, so after 2000 milliseconds and then this
method here will get executed, this method I passed to the set timeout method.


export class AppComponent {
  appStatus = new Promise(
    (resolve,reject)=>{
      setTimeout(
        ()=>{
          resolve('stable');
        },2000
      );
    }
  );


Now here in this function, I want to simply set my appStatus, so I want to resolve to let's say stable, whatever 
the status should be. So what this will do is it will set appStatus to stable but only after 2 seconds.
So if we try to output this appStatus here, let's add a line break or another line break to add a
blank line in between and then a heading where I say appStatus and then I try to output appStatus with
string interpolation, if we do this, you'll see object object and this is correct because it is object,
it's a promise and a promise is an object but after two seconds, we know that this is no longer an object,
it now is a string but Angular doesn't know because it doesn't watch our object in app.component.html
  APP STATUS : {{appStatus | async}}

It doesn't see if this object actually transforms to something else or if this returns us a value, it just knows it's a 
promise, I'm done and it's good that it behaves like this, saves us performance.
We should be explicit about how our app behaves but thankfully, there is a nice little pipe we can use here
to make the transformation of this data easier. We know that it will be a string after two seconds
and we want to output the string or it will resolve a string after two seconds, I should say
and we want to output the string.So we can add the pipe symbol and then async,
this is a built-in pipe and by adding it, well watch for yourself, you see I will reload
the app. There's nothing there at the beginning but after two seconds, you see stable and this is what async
does, it recognizes that this is a promise and as a side note, it would also work with observables,
there it would subscribe automatically and after two seconds, it will simply recognize that something
changed, that the promise resolved or in the case of an observable, that data was sent through the subscription
and it will print this data to the screen and this is the pipe with which I want to conclude this section about pipes.

Now you should feel really comfortable about using them, building your own pipes and as you see here, working
with async data with pipes. Now we will see this async pipes being used in the HTTP section later
but for now, let's move on to some exercises regarding pipes.