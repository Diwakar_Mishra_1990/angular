import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterString:string , propName: string): any {
    console.log(filterString+' & '+ value);
    console.log(value.length);
    if(value.length === 0 || filterString == ''){
      console.log('$$$'+ filterString+' & '+ value);
      return value;
    }
    const resultArray =[];
    for(const item of value){
      if(item[propName]===filterString){
        resultArray.push(item);
      }
    }
    return resultArray;
  }

}
