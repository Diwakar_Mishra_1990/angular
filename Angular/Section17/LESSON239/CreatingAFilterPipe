In the last lecture, we created our own pipe and we also allow the user to pass a parameter.
Now I want to create another new pipe because I want to show you something which might look strange
at the beginning.
Let's add a new element here above our unordered list and this should be an input field,
so input of type text. I want to allow the user to filter our servers, for example by status so that once you 
type stable in there, you'll see servers with the status stable.
So therefore, I need this input text field and I want to use two-way data binding to bind it,
so ngModel and bind this to filteredStatus

<input type = "text" [(ngModel)] = "filteredStatus">--------------At line no 4


for example, this of course would be a new property I set up here in my app.component.ts,
so below all my servers, I have filteredStatus which is an empty string at the beginning, filteredStatus
like this. So if we save this, we should see this new input field here at the top,
let's maybe add a horizontal line below it just for styling reasons and with that, we can enter something here
but of course, this does not do anything in app.component.ts

filteredStatus = '';-----------------at line no 35

Now I want to build a pipe which I can apply to this list somehow, which allows me to then only view
the servers which do indeed fulfill the requirements here.
So to do this, I will add a new pipe here and for this, I will now use the CLI, so 

 ng generate pipe would be the shortcut here or just g p and then the name of your pipe and I will name it filter, filter pipe.

So by typing filter, this gives me also a testing file which I don't need but then the filter pipe here
and there you can see it also pre-populates the name here in my @Pipe decorator and it gives me this
interface and gives me this transform method in its most basic form.
So in here, I want to allow the user to filter and I will indeed get an argument,
the first argument of this list of arguments and this will be what the user entered,
so the filter string you could say and this will be a string.
Well and then here, I want to implement some logic which allows me to only return the elements of the
array which fulfill this argument or which fulfill this filter string here where the status of
the server matches this filter string to be precise.
For this, I will first of all check if value.length and value will here be the array of servers,
it doesn't have to be a string, this is the first important take away, it can also be an array
here, it can be any data you output in the end. So here, I want to check if the value.length is zero, so if it's empty, in which case I will just return
values, so I will basically just quit, there is nothing to display.
And now after checking this, the interesting part happens here.

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterString:string , propName: string): any {
    if(value.length === 0){
      return value;
    }
    const resultArray =[];
    for(const item of value){
      if(item[propName]===filterString){
        resultArray.push(item);
      }
    }
    return resultArray;
  }

}

I simply want to check where I want to loop through all my items in my value and value here will
be an array, keep this in mind and then I want to check if my status of each server matches the filter string up
here. So for this, I can simply check if item.status
and of course you could write this more generic and pass the to be filtered property here as a second argument,
like propName which is a string and then you could simply say item propName in square brackets,
so if this equals my filter string. Only if this is equal, only in this case I want to return it.
So I should add a new result array, this is an array I will use temporarily here and I will push new items on this 
result array. So I will push any items on this array which fulfill my criteria here, which are or which have a
status or a propName in general equal to my filter string.

So here I will then push item on this array and return this result array in the end and therefore, this
result array will only hold the items where the propName I pass as a second argument here or as a
third argument actually, where this propName or this value of the propName is actually equal to my filtered string.
Now with this, pipe is finished. You have to make sure that it has been added to app module, the CLI did this
automatically here for me, if you created it manually, make sure that you add filter pipe to declarations and add your import at
the top and then in the app component, we apply it here in the ngFor loop.
And this might sound strange because before we only used it in string interpolation
but keep in mind that I said at the beginning of this module that pipes transform your output and an
ngFor loop is simply part of your output. Therefore of course you can add a pipe here too, to my servers and then 
I simply say filter, of course we need to pass two parameters here.
The first parameter is the filteredStatus, this property which holds my string for which I want to filter
and the second argument is my propName, so this will be a string and the string be status because I want to filter 
on the status property. Now if we save this and reload the app, you see no servers are displayed by default because 
no servers match this criteria.

In app.component.html we add in ine no 9

<li
          class="list-group-item"
          *ngFor="let server of servers | filter : filteredStatus: 'status'"
          [ngClass]="getStatusClasses(server)"
          >

If I type stable here, you'll see we see the stable server . So our filter pipe is now working as expected.
Please refer 01_OUTPUT.png

Now if we wanted to see all servers, we need to adjust this code that in case our filter string is empty,
so we could add this here as an or condition at the top,
if filterString equals an empty string, that then we see all servers and now we are able to filter
for whichever servers we want to see.
In fileter.pipe.ts we add 

if(value.length === 0 || filterString == ''){
      return value;
}    

This works great but there is one issue we will dive into in the next video.