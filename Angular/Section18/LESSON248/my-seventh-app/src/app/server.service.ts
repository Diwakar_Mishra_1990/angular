import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';


@Injectable()
export class ServerService{
    constructor(private http: Http){
    }
    storeServers(servers: any[]){
        const headers = new Headers(
            {
                'Content-Type':'application/json'
            }
        );
        return this.http.post('https://angularudemy1.firebaseio.com/data.json',
        servers,
        {headers: headers});
    }

    getServers(){
        const headers = new Headers(
            {
                'Content-Type':'application/json'
            }
        );
        return this.http.get('https://angularudemy1.firebaseio.com/data.json');
    }
}