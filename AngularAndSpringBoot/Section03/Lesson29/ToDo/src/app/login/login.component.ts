import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username="DM";
  password="";
  errorMessage="";
  invalidLogin=false;
  constructor(private router:Router) { }

  ngOnInit() {
  }
  handleLogin(){
    console.log(this.username);
    console.log(this.password);
    if(this.username==="DM" && this.password === "dummy"){
      this.router.navigate(['welcome',this.username]);
      this.invalidLogin=false;
    }
    this.invalidLogin = true;
  }

}
