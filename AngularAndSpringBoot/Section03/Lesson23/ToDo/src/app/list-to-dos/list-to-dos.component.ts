import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-to-dos',
  templateUrl: './list-to-dos.component.html',
  styleUrls: ['./list-to-dos.component.css']
})
export class ListToDosComponent implements OnInit {

  todos =[{
    id:1,
    description:'Learn Dance'
  },
{
  id:2,
  description:'No Dance'
}]
  constructor() { }

  ngOnInit() {
  }

}
