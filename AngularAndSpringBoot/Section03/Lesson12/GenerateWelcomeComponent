Welcome back. If you are a Java developer, who's coming into the front-end world,

One of the most important things to get used to is the syntax of typescript.

You would see that this looks similar to a Java class, but there are quite a number of things that you

would need to get used to.

You might be wondering if I would want to create a new component.

It might be a lot of effort.

I need to create 4 files manually link them up.

There is a lot of effort which is involved in doing that.

The thing is Angular CLI makes it very easy to create components. In this step,

Let's create a component and see how easy it is to do that.

Typically we have one window where we have the ng serve running.

I have all my code in this todo folder, and I cd to this folder in here, and we have done ng serve

to launch up the application and that what we are seeing in here.

What I would typically do is, I would open up another terminal window or another command prompt ceding

to the same folder.

So I am opening up the folder.

If you do the present working directory, it's exactly the same as what I have in here.

So I have two command line windows. One of them is running ng serve, and the other one we would typically

use to create the component, to create modules, to create services.

What I would want to create, you use the other window to create it.

So the advantage of doing this is as soon as new code is generated, my code would automatically be picked

up. I need to stop, and after I create a component and then start ng serve again.

If you see that, new code you are writing is not picked up.

Make sure that you are inside the same working directly in these folders.

Now let's get saddled with creating a component.

How do I create a component?

It's very simple indeed! ng generate, and what you want to create.

We want to create a component, and ng generate component.

Actually you can also run it as in ng g c .

So this is kind of a shortcut.

Once you are more familiar with Angular, maybe you can use that! For now,

Let's stick with the usual one: ng generate component. And we would need to specify the name of the

component, that we would want to create.

Let's say we would want to start with creating the welcome page.

So I would want to start creating the welcomeComponent. So: ng generate component welcomeComponent

The most important things that you need to understand.

with ng generate, is

There is no rollback.

Once the component is created, once it's added, you'll not be able to roll it back.

So it's very important to make sure that you get the names right first time. So Welcome to check the

name before you press enter.

The other thing is I'm not saying welcome component.

I'm saying just welcome and you would see that it would create a welcome component for us.

So you didn't need component. for welcome, you'd see that there are four files which I created and one file

which is updated on what's happening before.

Files that I created, that we would expect right!

We have the component.html, which is the template, the .css which is the style.

The .ts file which would contain the code, the typescript code.

This is the test file which is typically generated, and we would keep ignoring it.

The interesting change is what is done to app.module.js

This is an update is done to app.module.js.

Let's look at what update is done.

They open up the app.module.js

Yes let's do a control-B, or command-B, to make sure that we are seeing it in full screen.

If you do that, you'd see that there is a WelcomeComponent is added in in the declaration.

We'll talk about NgModule a little later. This stands for Angular Module. Every component that we create as

part of an Angular application should be associated with Angular module, and the declarations indicate

what are the different things, the different components that are part of this particular NgModule.

You'll see that the WelcomeComponent is automatically added in, as part of these particular declarations.

This particular module contains two components AppComponent and WelcomeComponent.

Now let's go onto the explorer, and look at where the welcome component is created.

So in the app folder, there is a folder called welcome, and inside the welcome.

All these four files are created.

You can see that it says the HTML says welcome.

Looks the .spec file and things.

and see the default test, and the .ts files contains the basic component.

You can see that the name of the component is a WelcomeComponent.

It's very important.

We just specified welcome in here, but the component we just created is WelcomeComponent. So we don't really

need to say WelcomeComponent when we are creating the component.

That's kind of a typical mistake which a lot of beginners make.

You would see that the default selector or the default tag name which is for this specific thing, is

app-welcome

And you would see that the templateUrl and the styleUrl are appropriately matched.

The interesting fact to note is the use of implements on it which is present in here.

The thing is modern Javascript does not really have the concept of interfaces.

This is something which is a typescript concept, which we are making use of in here.

So we have an interface called on it.

The interface defines a method called ngOnInit().

So what you are doing is, in the WelcomeComponent we are implementing the method ngOnInit().

You would see that if I remove this, there is a compilation error.

You see that it says: "I'm not implementing the interface". so this is very similar to interfaces in Java

when you implement an interface, you have to implement all the method to represent them.

Or you can make it abstract.

Let's not worry about it right now.

So the ngOninit(): what would happen is this method would run, then this component is initialized.

Let's save this.

Let's look at the application. You'd see that there is no change because we have not used this component

anywhere.

I would want to include this component.

I would want to show this component.

How can I do that?

One of the ways I can do that is I can go to the app.component.html, and I can include

the tag

, and save it. that's what would happen.

And it loads up, as you see.

Welcome works!

Isn't this awesome!

So what we did is we have included the component that we have created, inside the app component.

So the WelcomeComponent is enough included as part of the AppComponent. In the step,

We created the WelcomeComponent and we have loaded it as part of the AppComponent.

I would like you to do an exercise: Create another component, and include it as part of the

app.component.html

as well.

I would recommend you to play around with it as much as possible to understand a little bit more about

component. I'll see you in the next step