import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { HardcodedAuthenticationService } from '../service/hardcoded-authentication.service';
import { BasicAuthenticationService } from '../service/basic-authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username="DM";
  password="";
  errorMessage="";
  invalidLogin=false;
  constructor(private router:Router, 
    private hardcodedAuthenticationService:HardcodedAuthenticationService,
    private basicAuthenticationService:BasicAuthenticationService) { }

  ngOnInit() {
  }
  handleLogin(){
    console.log(this.username);
    console.log(this.password);
    if(this.hardcodedAuthenticationService.authenticate(this.username,this.password)){
      
      this.router.navigate(['welcome',this.username]);
      console.log("CORRECT")
      this.invalidLogin=false;
    }
    this.invalidLogin = true;
  }

  handleBasicAuthLogin(){
    console.log(this.username);
    console.log(this.password);
    this.basicAuthenticationService.executeBasicAuthenticationService(this.username,this.password)
      .subscribe(
        data=>{
          this.router.navigate(['welcome',this.username]);
          this.invalidLogin=false;
        },
        error =>{
          console.log(error);
          this.errorMessage="Invalid credentials";
          this.invalidLogin = true;
        }
      );      
  }
    
}


