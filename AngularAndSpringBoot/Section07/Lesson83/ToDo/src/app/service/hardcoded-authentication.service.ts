import { Injectable } from '@angular/core';


export class HardcodedAuthenticationService {

  constructor() { }
  authenticate(username , password){
    if(username=="DM" && password == "dummy"){
      console.log("setting data");
      sessionStorage.setItem('authenticatedUser',username)
      return true;
    }
    return false;
  }
  isUserLoggedIn(){
    let user = sessionStorage.getItem('authenticatedUser');
    return (user!=null);
  }
  logout(){
    sessionStorage.removeItem('authenticatedUser');
  }
}
