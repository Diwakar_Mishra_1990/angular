import { TestBed } from '@angular/core/testing';

import { HttpInterCepterBasicAuthService } from './http-inter-cepter-basic-auth.service';

describe('HttpInterCepterBasicAuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpInterCepterBasicAuthService = TestBed.get(HttpInterCepterBasicAuthService);
    expect(service).toBeTruthy();
  });
});
