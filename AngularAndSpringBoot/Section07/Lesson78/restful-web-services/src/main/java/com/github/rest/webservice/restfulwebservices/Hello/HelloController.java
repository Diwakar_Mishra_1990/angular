package com.github.rest.webservice.restfulwebservices.Hello;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin(origins="http://localhost:4200")
@RestController
public class HelloController {
	
	@RequestMapping(method=RequestMethod.GET , path="/hello-world")
	public String helloWorld() {
		return "Hello World";
	}
	
	@RequestMapping(method=RequestMethod.GET , path="/basicauth")
	public AuthenticationBean helloWorldBean() {
		return new AuthenticationBean("You have been authenticated");
		
	}
	
	
}
