package com.github.rest.webservice.restfulwebservices.Hello;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin(origins="http://localhost:4200")
@RestController
public class HelloController {
	
	@RequestMapping(method=RequestMethod.GET , path="/hello-world")
	public String helloWorld() {
		return "Hello World";
	}
	
	@RequestMapping(method=RequestMethod.GET , path="/basicauth")
	public AuthenticationBean basicAuth() {
		return new AuthenticationBean("You have been authenticated");
		
	}
	
	
	
	@RequestMapping(method=RequestMethod.GET , path="/hello-world/path-variable/{name}")
	public HelloWorldBean helloWorldPathVariable(@PathVariable String name) {
		return new HelloWorldBean(String.format("Hello World ,%s",name));
	}
	
}
