import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http'
@Injectable()
export class WelcomeDataService {

  constructor(private http:HttpClient) { }

  executeHelloWorldBeanService(){
    return this.http.get<HelloWorldBean>('http://localhost:8080/hello-world-bean')
  }

  executeHelloWorldBeanServiceWitPathVariable(name){
    let basicAuthHeader = this.createBasicAuthenticationHttpHeader();
    let header = new HttpHeaders(
      {
        Authorization:basicAuthHeader
      }
    );
    return this.http.get<HelloWorldBean>(`http://localhost:8080/hello-world/path-variable/${name}`,{headers:header});
  }

  createBasicAuthenticationHttpHeader(){
    let username = "user";
    let password = "password";
    let basicAuthHeaderString = "Basic "+window.btoa(username+":"+password);
    return basicAuthHeaderString;
  }
}

export class HelloWorldBean{
  message:String;
  constructor(message){
    this.message =message;
  }
}