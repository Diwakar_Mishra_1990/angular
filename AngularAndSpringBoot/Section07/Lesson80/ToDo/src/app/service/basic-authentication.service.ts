import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BasicAuthenticationService {

  constructor(private http:HttpClient) { }

  authenticate(username , password){
    if(username=="DM" && password == "dummy"){
      console.log("setting data");
      sessionStorage.setItem('authenticatedUser',username)
      return true;
    }
    return false;
  }

  executeBasicAuthenticationService(username , password){
    let basicAuthHeader = "Basic "+window.btoa(username+":"+password);
    let header = new HttpHeaders(
      {
        Authorization:basicAuthHeader
      }
    );
    return this.http.get<AuthenticationBean>(`http://localhost:8080/basicauth`,{headers:header});
  }


  isUserLoggedIn(){
    let user = sessionStorage.getItem('authenticatedUser');
    return (user!=null);
  }
  logout(){
    sessionStorage.removeItem('authenticatedUser');
  }
}

export class AuthenticationBean{
  constructor(private message:String){}
}