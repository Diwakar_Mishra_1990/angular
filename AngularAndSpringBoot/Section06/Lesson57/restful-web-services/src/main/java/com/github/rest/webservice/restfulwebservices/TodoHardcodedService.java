package com.github.rest.webservice.restfulwebservices;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
@Service
public class TodoHardcodedService {
	private static List<Todo> todos = new ArrayList<Todo>();
	private static int idCounter=0;
	static {
		todos.add(new Todo(++idCounter,"in28minutes","Learn to Dance",new Date(),false));
		todos.add(new Todo(++idCounter,"in28minutes","Learn about Microservice",new Date(),false));
		todos.add(new Todo(++idCounter,"in28minutes","Learn about Angular",new Date(),false));
		todos.add(new Todo(++idCounter,"in28minutes","Learn to Spring Boot",new Date(),false));		
	}
	
	public List<Todo> findAll(){
		return todos;
	}
}
