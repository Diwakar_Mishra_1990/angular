import { Component, OnInit } from '@angular/core';
import { TodoDataService } from '../service/data/todo-data.service';
import { Todo } from '../list-to-dos/list-to-dos.component';
import { ActivatedRouteSnapshot, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  id: number;
  todo:Todo;
  constructor(private todoService: TodoDataService, private route:ActivatedRoute, private router:Router) { }

  ngOnInit() {
    this.id= this.route.snapshot.params['id'];
    this.todo = new Todo(0,'',false,new Date());
    this.todoService.retrieveTodo('DM',this.id).subscribe(
      (data)=> {this.todo = data;
      console.log(this.id);
      console.log(data);
      }
    );
  }

  onsaveToDo(){
    this.todoService.updateTodo('DM',this.id,this.todo).subscribe(
      (data)=> {
        console.log(data);
        this.router.navigate(['todos']);
      }
    );
  }

}
