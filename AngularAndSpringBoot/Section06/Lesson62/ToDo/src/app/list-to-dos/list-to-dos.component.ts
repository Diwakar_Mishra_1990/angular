import { Component, OnInit } from '@angular/core';
import { TodoDataService } from '../service/data/todo-data.service';
import { Router } from '@angular/router';

export class Todo {
  constructor(
    public id: number,
    public description: string,
    public done: boolean,
    public targetDate: Date
  ){

  }
}
@Component({
  selector: 'app-list-to-dos',
  templateUrl: './list-to-dos.component.html',
  styleUrls: ['./list-to-dos.component.css']
})
export class ListToDosComponent implements OnInit {

  todos :Todo[]=[];
  constructor(private todoService:TodoDataService, private router:Router) { }

  ngOnInit() {
    this.todoService.retrieveAllTodos('in28minutes').subscribe(
      (response)=>{this.todos = response}
    );
  }

  deleteTodo(id){
    this.todoService.deleteTodo('DM',id).subscribe(
      (response) =>{
        console.log(response);
        this.todos = response;
      }
    );
  }

  updateTodo(id){
    this.router.navigate(['todos',id])
  }

}
