import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class WelcomeDataService {

  constructor(private route:ActivatedRoute, private service: WelcomeDataService) { }

  executeHelloWorldBeanService(){
    this.service.executeHelloWorldBeanService();
    console.log("Execute Hello World Bean component");
  }
}
