import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormControl } from '@angular/forms';
import { FormModelModule } from '../form-model/form-model.module';
import { RenderWidgetServiceService } from '../service/render-widget-service.service';

@Component({
  selector: 'app-test-screen',
  templateUrl: './test-screen.component.html',
  styleUrls: ['./test-screen.component.css']
})
export class TestScreenComponent implements OnInit {
  
  widgetArray:FormModelModule[]=[];
  testForm:FormGroup;
  constructor(private widgetService:RenderWidgetServiceService) { }


  ngOnInit(): void {
    this.widgetService.jsonAdded.subscribe(
      (jsonMessage:string)=>{
        var json = JSON.parse(jsonMessage)
        this.initForm(json.widget);
      }
  );
    
  }
initForm(widgets){
  let formWidgets = new FormArray([]);
  for (let i = 0 ;i<widgets.length;i++){
    
    this.widgetArray.push(new FormModelModule(widgets[i].name,
      widgets[i].label,
      widgets[i].validation,
      widgets[i].fieldType,
      widgets[i].options));
        formWidgets.push(
        new FormGroup(
          {
           'name':new FormControl(widgets[i].name)           
          }
        )
      );
      console.log(this.widgetArray[i].fieldType);
  }
  this.testForm=new FormGroup(
    {
      'name': new FormControl('testForm'),
      'widgets': formWidgets
    }
  );
  console.log(this.widgetArray[0].fieldType);
  }
  
}
  
  
  


