import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JsonBoxComponent } from './json-box/json-box.component';
import { TestScreenComponent } from './test-screen/test-screen.component';
import { TextboxComponent } from './widget/textbox/textbox.component';

@NgModule({
  declarations: [
    AppComponent,
    JsonBoxComponent,
    TestScreenComponent,
    TextboxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
