import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class FormModelModule {

  public name:String;
  public label:String;
  public validation:String;
  public fieldType:String;
  public options:String;

    constructor(name:String,label:String, validation:String ,fieldType:String, options:String){
       this.name = name;
       this.label = label;
       this.validation = validation;
       this.fieldType=fieldType;
       this.options = options;
    }
 }
