import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RenderWidgetServiceService {

  constructor() { }
  jsonAdded = new EventEmitter<String>();
  createWidget(jsonMessage){
    this.jsonAdded.emit(jsonMessage);
  }
}
