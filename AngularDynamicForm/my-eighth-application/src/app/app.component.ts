import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-eighth-application';
  @Input('data') items: Array<Object>;
  @Input('key') key: string;

  constructor() {}

}
