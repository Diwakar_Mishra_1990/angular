import { Directive, Input, Type, OnInit, OnChanges, SimpleChanges, ComponentFactoryResolver, ViewContainerRef, ComponentRef } from '@angular/core';
import { GroupConfig } from '../model/group-config/GroupConfig';
import { FormGroup } from '@angular/forms';
import { Field } from '../model/field/field.interface';
import { FormButtonComponent } from 'src/app/dynamic-form/dynamic-fields/form-button/form-button.component';
import { FormInputComponent } from 'src/app/dynamic-form/dynamic-fields/form-input/form-input.component';
import { FormSelectComponent } from 'src/app/dynamic-form/dynamic-fields/form-select/form-select.component';
import { FormGroupComponent } from 'src/app/dynamic-form/dynamic-fields/form-group/form-group.component';
import { FieldConfig } from '../model/field-config/FieldConfig';

const components: { [type: string]: Type<Field> } = {
  button: FormButtonComponent,
  input: FormInputComponent,
  select: FormSelectComponent
};
@Directive({
  selector: '[appDynamicField]'
})
export class DynamicFieldDirective implements Field, OnChanges, OnInit {
  formGroupName: String;
  
  @Input() fieldConfig: FieldConfig;

  component: ComponentRef<Field>;
  @Input() group: FormGroup;

  ngOnInit(): void {

    if (!components[this.fieldConfig.type]) {
      const supportedTypes = Object.keys(components).join(', ');
      throw new Error(
        `Trying to use an unsupported type (${this.fieldConfig.type}).
        Supported types: ${supportedTypes}`
      );
    }
    const component = this.resolver.resolveComponentFactory<Field>(components[this.fieldConfig.type]);
    this.component = this.container.createComponent(component);
    this.component.instance.fieldConfig = this.fieldConfig;

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.component) {
      this.component.instance.fieldConfig = this.fieldConfig;
      this.component.instance.group = this.group;
    }
  }

  constructor(
    private resolver: ComponentFactoryResolver,
    private container: ViewContainerRef
  ) { }
}
