import { FormGroup } from '@angular/forms';
import { FieldConfig } from '../field-config/FieldConfig';

export interface Field {
    fieldConfig: FieldConfig,
    group: FormGroup,
    formGroupName:String
  }
  