import {GroupConfig} from '../../model/group-config/GroupConfig';
import { FieldConfig } from '../../model/field-config/FieldConfig';


export const FieldConfigList:FieldConfig[] = [
    new FieldConfig(
      {
        type: 'input',
        label: 'Full name',
        name: 'name1',
        placeholder: 'Enter your name',
        validation: '',
        value:'val1'
      }
    ),
    new FieldConfig({
      type: 'select',
      label: 'Favourite Food',
      name: 'food1',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'Coffee'],
      placeholder: 'Select an option',
      validation: '',
      value:'Pizza'
    }),
    new FieldConfig(
      {
        type: 'input',
        label: 'Full name',
        name: 'name2',
        placeholder: 'Enter your name',
        validation: '',
        value:'val2'
      }
    ),
    new FieldConfig({
      type: 'select',
      label: 'Favourite Food',
      name: 'food2',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'Coffee'],
      placeholder: 'Select an option',
      validation: '',
      value:'Pizza'
    }),
    new FieldConfig(
      {
        type: 'input',
        label: 'Full name',
        name: 'name3',
        placeholder: 'Enter your name',
        validation: '',
        value:'val3'
      }
    ),
    new FieldConfig({
      type: 'select',
      label: 'Favourite Food',
      name: 'food3',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'Coffee'],
      placeholder: 'Select an option',
      validation: '',
      value:'Pizza'
    }),
    new FieldConfig(
      {
        type: 'input',
        label: 'Full name',
        name: 'name4',
        placeholder: 'Enter your name',
        validation: '',
        value:'val1'
      }
    ),
    new FieldConfig({
      type: 'select',
      label: 'Favourite Food',
      name: 'food4',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'Coffee'],
      placeholder: 'Select an option',
      validation: '',
      value:'Pizza'
    }),
    new FieldConfig(
      {
        type: 'input',
        label: 'Full name',
        name: 'name5',
        placeholder: 'Enter your name',
        validation: '',
        value:'val1'
      }
    ),
    new FieldConfig({
      type: 'select',
      label: 'Favourite Food',
      name: 'food5',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'Coffee'],
      placeholder: 'Select an option',
      validation: '',
      value:'Pizza'
    }),
    new FieldConfig(
      {
        type: 'input',
        label: 'Full name',
        name: 'name6',
        placeholder: 'Enter your name',
        validation: '',
        value:'val1'
      }
    ),
    new FieldConfig({
      type: 'select',
      label: 'Favourite Food',
      name: 'food6',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'Coffee'],
      placeholder: 'Select an option',
      validation: '',
      value:'Pizza'
    }),
    new FieldConfig(
      {
        type: 'input',
        label: 'Full name',
        name: 'name7',
        placeholder: 'Enter your name',
        validation: '',
        value:'val1'
      }
    ),
    new FieldConfig({
      type: 'select',
      label: 'Favourite Food',
      name: 'food7',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'Coffee'],
      placeholder: 'Select an option',
      validation: '',
      value:'Pizza'
    }),
    new FieldConfig(
      {
        type: 'input',
        label: 'Full name',
        name: 'name8',
        placeholder: 'Enter your name',
        validation: '',
        value:'val1'
      }
    ),
    new FieldConfig({
      type: 'select',
      label: 'Favourite Food',
      name: 'food8',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'Coffee'],
      placeholder: 'Select an option',
      validation: '',
      value:'Pizza'
    }),
    new FieldConfig(
      {
        type: 'input',
        label: 'Full name',
        name: 'name8',
        placeholder: 'Enter your name',
        validation: '',
        value:'val1'
      }
    ),
    new FieldConfig({
      type: 'select',
      label: 'Favourite Food',
      name: 'food8',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'Coffee'],
      placeholder: 'Select an option',
      validation: '',
      value:'Pizza'
    }),
    new FieldConfig(
      {
        type: 'input',
        label: 'Full name',
        name: 'name9',
        placeholder: 'Enter your name',
        validation: '',
        value:'val1'
      }
    ),
    new FieldConfig({
      type: 'select',
      label: 'Favourite Food',
      name: 'food9',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'Coffee'],
      placeholder: 'Select an option',
      validation: '',
      value:'Pizza'
    }),
    new FieldConfig(
      {
        type: 'input',
        label: 'Full name',
        name: 'name10',
        placeholder: 'Enter your name',
        validation: '',
        value:'val1'
      }
    ),
    new FieldConfig({
      type: 'select',
      label: 'Favourite Food',
      name: 'food10',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'Coffee'],
      placeholder: 'Select an option',
      validation: '',
      value:'Pizza10'
    }),
    new FieldConfig(
      {
        label: 'Submit',
        name: 'submit9',
        type: 'button'
      }
    )
  ];

export const GroupConfigList:GroupConfig[] = [
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'aa'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'bb'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'cc'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'dd'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'ee'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'ff'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'gg'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'hh'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'ii'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'jj'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'kk'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'ll'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'mm'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'nn'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'oo'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'pp'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'qq'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'rr'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'ss'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'tt'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'uu'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'vv'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'ww'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'xx'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'yy'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'zz'
  })
]

export const FinalGroupList :GroupConfig[]=[
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'a'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'b'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'c'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'d'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'e'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'f'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'g'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'h'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'i'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'j'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'k'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'l'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'m'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'n'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'o'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'p'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'q'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'r'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'s'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'t'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'u'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'v'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'w'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'x'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'y'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'z'
  })
]