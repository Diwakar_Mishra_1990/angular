import { TestBed } from '@angular/core/testing';

import { WidgetRenderingService } from './widget-rendering.service';

describe('WidgetRenderingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WidgetRenderingService = TestBed.get(WidgetRenderingService);
    expect(service).toBeTruthy();
  });
});
