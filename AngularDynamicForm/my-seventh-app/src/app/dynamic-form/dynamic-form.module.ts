import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormComponent } from './dynamic-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DynamicFormGroupDirective } from '../shared/dynamicForm/dynamic-form-group.directive';
import { FormButtonComponent } from './dynamic-fields/form-button/form-button.component';
import { FormInputComponent } from './dynamic-fields/form-input/form-input.component';
import { FormSelectComponent } from './dynamic-fields/form-select/form-select.component';
import { FormGroupComponent } from './dynamic-fields/form-group/form-group.component';
import { DynamicFieldDirective } from '../shared/dynamicForm/dynamic-field.directive';

@NgModule({
  declarations: [
    DynamicFormComponent,
    DynamicFormGroupDirective,
    DynamicFieldDirective,
    FormButtonComponent,
    FormInputComponent,
    FormSelectComponent,
    FormGroupComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    FormButtonComponent,
    FormInputComponent,
    FormSelectComponent,
    FormGroupComponent
  ],
  exports:[DynamicFormComponent]
})
export class DynamicFormModule { 
   
}
