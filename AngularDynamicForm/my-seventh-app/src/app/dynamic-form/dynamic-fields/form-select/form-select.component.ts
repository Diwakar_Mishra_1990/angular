import { Component, OnInit } from '@angular/core';
import { GroupConfig } from 'src/app/shared/model/group-config/GroupConfig';
import { FormGroup } from '@angular/forms';
import { Field } from 'src/app/shared/model/field/field.interface';
import { FieldConfig } from 'src/app/shared/model/field-config/FieldConfig';

@Component({
  selector: 'app-form-select',
  templateUrl: './form-select.component.html',
  styleUrls: ['./form-select.component.css']
})
export class FormSelectComponent implements OnInit ,Field{
  formGroupName:String;
  fieldConfig: FieldConfig;
  group: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
