import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DynamicFormModule } from './dynamic-form/dynamic-form.module';
import { JsonModuleModule } from './json-module/json-module.module';
import { TestScreenModuleModule } from './test-screen-module/test-screen-module.module';
import { WidgetRenderingService } from './shared/service/widgetRendering/widget-rendering.service';
import { DynamicFieldDirective } from './shared/dynamicForm/dynamic-field.directive';


@NgModule({
  declarations: [
    AppComponent 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    DynamicFormModule,
    JsonModuleModule,
    TestScreenModuleModule
  ],
  providers: [WidgetRenderingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
