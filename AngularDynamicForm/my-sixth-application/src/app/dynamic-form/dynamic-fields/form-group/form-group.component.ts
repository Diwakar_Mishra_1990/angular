import { Component, OnInit } from '@angular/core';
import { Field } from 'src/app/shared/model/field/field.interface';

@Component({
  selector: 'app-form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.css']
})
export class FormGroupComponent implements OnInit, Field {
  config: import("../../../shared/model/group-config/GroupConfig").GroupConfig;
  group: import("@angular/forms").FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
