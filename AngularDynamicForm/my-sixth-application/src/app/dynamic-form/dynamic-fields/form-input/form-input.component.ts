import { Component, OnInit } from '@angular/core';
import { GroupConfig } from 'src/app/shared/model/group-config/GroupConfig';
import { FormGroup } from '@angular/forms';
import { Field } from 'src/app/shared/model/field/field.interface';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.css']
})
export class FormInputComponent implements OnInit ,Field{
  config: GroupConfig;
  group: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
