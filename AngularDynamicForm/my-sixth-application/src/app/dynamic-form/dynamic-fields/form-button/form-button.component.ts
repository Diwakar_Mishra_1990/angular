import { Component, OnInit } from '@angular/core';
import { Field } from 'src/app/shared/model/field/field.interface';
import { GroupConfig } from 'src/app/shared/model/group-config/GroupConfig';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-button',
  templateUrl: './form-button.component.html',
  styleUrls: ['./form-button.component.css']
})
export class FormButtonComponent implements OnInit,Field {
  config: GroupConfig;
  group: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
