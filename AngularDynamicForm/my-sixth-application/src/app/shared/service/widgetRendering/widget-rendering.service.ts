import { Injectable, EventEmitter } from '@angular/core';
import { GroupConfig } from '../../model/group-config/GroupConfig';
import { Subject } from 'rxjs';

@Injectable()
export class WidgetRenderingService {
  jsonAdded = new Subject<GroupConfig[]>();


  constructor() { }

  createWidget(fieldConfigList:GroupConfig[]){
    this.jsonAdded.next(fieldConfigList);  
  }
}
