import { Directive, Input, Type, OnInit, OnChanges, SimpleChanges, ComponentFactoryResolver, ViewContainerRef, ComponentRef } from '@angular/core';
import { GroupConfig } from '../model/group-config/GroupConfig';
import { FormGroup } from '@angular/forms';
import { Field } from '../model/field/field.interface';
import { FormButtonComponent } from 'src/app/dynamic-form/dynamic-fields/form-button/form-button.component';
import { FormInputComponent } from 'src/app/dynamic-form/dynamic-fields/form-input/form-input.component';
import { FormSelectComponent } from 'src/app/dynamic-form/dynamic-fields/form-select/form-select.component';
import { FormGroupComponent } from 'src/app/dynamic-form/dynamic-fields/form-group/form-group.component';
import { GroupConfigList } from '../store/fields/field';


const components: { [type: string]: Type<Field> } = {
  button: FormButtonComponent,
  input: FormInputComponent,
  select: FormSelectComponent,
  group: FormGroupComponent
};


@Directive({
  selector: '[appDynamicForm]'
})
export class DynamicFormDirective implements Field, OnChanges, OnInit {
  @Input()
  config: GroupConfig;
  component: ComponentRef<Field>;
  @Input() group: FormGroup;

  ngOnInit(): void {
    /*
    if (!components[this.config.type]) {
      const supportedTypes = Object.keys(components).join(', ');
      throw new Error(
        `Trying to use an unsupported type (${this.config.type}).
        Supported types: ${supportedTypes}`
      );
    }
    */
    //this.createComponents(this.config);
/*
    const component = this.resolver.resolveComponentFactory<Field>(components[this.config.type]);
    this.component = this.container.createComponent(component);
    this.component.instance.config = this.config;
    this.component.instance.group = this.group;
    */
  }
  createComponents(config: GroupConfig) {
    if (this.config.fieldConfigList) {
      for (var i = 0; i < this.config.fieldConfigList.length; i++) {
        const component = this.resolver.resolveComponentFactory<Field>(components[this.config.fieldConfigList[i].type]);
        this.component = this.container.createComponent(component);
        this.component.instance.config = this.config;
        this.component.instance.group = this.group;
      }
    }
    if (this.config.groupConfigList) {
      for (var i = 0; i < this.config.groupConfigList.length; i++) {
        const component = this.resolver.resolveComponentFactory<Field>(components['group']);
        this.component = this.container.createComponent(component);
        this.component.instance.config = this.config.groupConfigList[i];
        this.component.instance.group = this.group;
        this.createComponents(this.config.groupConfigList[i]);
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.component) {
      this.component.instance.config = this.config;
      this.component.instance.group = this.group;
    }
  }

  constructor(
    private resolver: ComponentFactoryResolver,
    private container: ViewContainerRef
  ) { }
}
