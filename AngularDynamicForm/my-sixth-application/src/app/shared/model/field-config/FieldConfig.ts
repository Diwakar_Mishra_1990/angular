export class FieldConfig {
    disabled: boolean=false;
    label: string;
    name: string;
    options: string[];
    placeholder: string;
    type: string;
    validation: string;
    value: any;
  
    constructor(value){
      this.disabled =value.disabled;
      this.label = value.label;
      this.name = value.name;
      this.options = value.options;
      this.placeholder =value.placeholder;
      this.type = value.type;
      this.validation = value.validation;
      this.value= value.value;    
    }
    getValue(){
      return {
      'disabled' :this.disabled,
      'label': this.label,
      'name' : this.name,
      'options' : this.options,
      'placeholder' :this.placeholder,
      'type' :this.type,
      'validation' :this.validation,
      'value': this.value
      }
    }
  }
  