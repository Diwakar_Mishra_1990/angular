import { FieldConfig } from '../field-config/FieldConfig';

export class GroupConfig {
  name:string;
  fieldConfigList: FieldConfig[];
  groupConfigList: GroupConfig[];

  constructor(value){
    this.name=value.name;
    this.fieldConfigList =value.fieldConfigList;
    this.groupConfigList = value.groupConfigList;
  }
  getValue(){
    return {
    'fieldConfigList' :this.fieldConfigList,
    'groupConfigList': this.groupConfigList,
    'name': this.name
    }
  }
}
