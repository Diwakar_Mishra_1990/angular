import { GroupConfig } from '../group-config/GroupConfig';
import { FormGroup } from '@angular/forms';

export interface Field {
    config: GroupConfig,
    group: FormGroup
  }
  