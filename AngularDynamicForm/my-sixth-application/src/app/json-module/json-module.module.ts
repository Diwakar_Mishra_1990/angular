import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JsonBoxComponent } from './json-box/json-box.component';
import { FormsModule } from '@angular/forms';
import { AppModule } from '../app.module';

@NgModule({
  declarations: [JsonBoxComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports:[JsonBoxComponent]
})
export class JsonModuleModule { }
