import { Component, OnInit } from '@angular/core';
import { WidgetRenderingService } from 'src/app/shared/service/widgetRendering/widget-rendering.service';
import { FieldConfigList, FinalGroupList } from 'src/app/shared/store/fields/field';
import { GroupConfig } from 'src/app/shared/model/group-config/GroupConfig';

@Component({
  exportAs: 'jsonBox',
  selector: 'app-json-box',
  templateUrl: './json-box.component.html',
  styleUrls: ['./json-box.component.css']
})
export class JsonBoxComponent implements OnInit {
  jsonMessage="";
  constructor(private widgetRenderingService:WidgetRenderingService) { }

  ngOnInit() {
    let list = FinalGroupList;
    this.jsonMessage="[";
    for(var i=0;i<list.length;i++){
      this.jsonMessage = this.jsonMessage +  JSON.stringify(list[i].getValue())+",";
    }
    this.jsonMessage=this.jsonMessage.slice(0, -1)+"]";
    //console.log(this.jsonMessage);
  }
  onSubmitJson(){
    var fieldConfigList:GroupConfig[]=[];
    var jsonList= JSON.parse(this.jsonMessage);
    for(var i=0;i<jsonList.length;i++){
      fieldConfigList.push(new GroupConfig(jsonList[i]))
    }
    this.widgetRenderingService.createWidget(fieldConfigList);
  }

}
