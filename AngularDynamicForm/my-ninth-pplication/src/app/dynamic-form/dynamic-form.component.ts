import { Component, OnInit, Input, OnChanges,Output,EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { GroupConfig } from '../shared/model/group-config/GroupConfig';
import { WidgetRenderingService } from '../shared/service/widgetRendering/widget-rendering.service';
import { FieldConfig } from '../shared/model/field-config/FieldConfig';

@Component({
  exportAs: 'dynamicForm',
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.css']
})
export class DynamicFormComponent implements OnInit, OnChanges {
  form: FormGroup;
  groupConfigList: GroupConfig[] = [];
  @Output()submit: EventEmitter<any> = new EventEmitter<any>();
  constructor(private fb: FormBuilder, private widgetRenderingService: WidgetRenderingService) { }

  ngOnInit() {
    this.form = this.createGroup();
    this.widgetRenderingService.jsonAdded.subscribe(
      (response) => {
        this.groupConfigList = response;
        this.form = this.createGroup();
      }
    )
  }

  ngOnChanges() {
  }
  createGroup() {
    const group = this.fb.group({});
    for(var i=0; i<this.innerGroups.length;i++){
      this.createInnerGroup(this.innerGroups[i],group)
    }
    return group;
  }
  createInnerGroup(childGroup: GroupConfig, parentGroup: FormGroup):FormGroup {
    const groupName = childGroup.name;
    const childFormGroup = this.fb.group({});
    parentGroup.addControl(childGroup.name,childFormGroup);
    if(childGroup.fieldConfigList){
      childGroup.fieldConfigList.filter(({type}) => type !== 'button').forEach(control => childFormGroup.addControl(control.name, this.createControl(control)));      
    } 
    if(childGroup.groupConfigList){
      childGroup.groupConfigList.forEach(innerGroup => 
        {
          this.createInnerGroup(innerGroup,childFormGroup);
        });
    }
    return childFormGroup;
  }

  createControl(config: FieldConfig) {
    const { disabled, validation, value } = config;
    return this.fb.control({ disabled, value });
  }
  

  handleSubmit(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    this.submit.emit(this.value);
  }
  get innerGroups() { return this.groupConfigList; }
  //get controls() { return this.configList.filter(({type}) => type !== 'button'); }
  get changes() { return this.form.valueChanges; }
  get valid() { return this.form.valid; }
  get value() { return this.form.value; }
}
