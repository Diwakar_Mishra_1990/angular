import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormComponent } from './dynamic-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DynamicFormGroupDirective } from '../shared/dynamicForm/dynamic-form-group.directive';
import { FormButtonComponent } from './dynamic-fields/form-button/form-button.component';
import { FormInputComponent } from './dynamic-fields/form-input/form-input.component';
import { FormSelectComponent } from './dynamic-fields/form-select/form-select.component';
import { FormGroupComponent } from './dynamic-fields/form-group/form-group.component';
import { DynamicTemplateGroupDirective } from '../shared/dynamicTemplate/dynamic-template-group.directive';
import { DynamicTemplateFieldDirective } from '../shared/dynamicTemplate/dynamic-template-field.directive';

@NgModule({
  declarations: [
    DynamicFormComponent,
    DynamicFormGroupDirective,
    FormButtonComponent,
    FormInputComponent,
    FormSelectComponent,
    FormGroupComponent,
    DynamicTemplateGroupDirective,
    DynamicTemplateFieldDirective
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    FormButtonComponent,
    FormInputComponent,
    FormSelectComponent,
    FormGroupComponent
  ],
  exports:[DynamicFormComponent]
})
export class DynamicFormModule { 
   
}
