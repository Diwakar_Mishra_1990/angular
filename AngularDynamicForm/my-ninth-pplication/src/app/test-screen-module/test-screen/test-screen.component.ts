import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormControl } from '@angular/forms';
import { WidgetRenderingService } from 'src/app/shared/service/widgetRendering/widget-rendering.service';
import { GroupConfig } from 'src/app/shared/model/group-config/GroupConfig';


@Component({
  selector: 'app-test-screen',
  templateUrl: './test-screen.component.html',
  styleUrls: ['./test-screen.component.css']
})
export class TestScreenComponent implements OnInit {
  
  constructor(private widgetRenderingService:WidgetRenderingService){

  }
  

  ngOnInit(): void {
  }

  submit(value: {[name: string]: any}) {
    console.log(value);
  }
}
  
  
  


