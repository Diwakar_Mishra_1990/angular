import { GroupConfig } from '../../model/group-config/GroupConfig';
import { FieldConfigList, GroupConfigList } from './LoadField02';

export const FinalGroupList :GroupConfig[]=[
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'a'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'b'
  })
]