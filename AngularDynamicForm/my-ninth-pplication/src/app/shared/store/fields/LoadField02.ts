import {GroupConfig} from '../../model/group-config/GroupConfig';
import { FieldConfig } from '../../model/field-config/FieldConfig';


export const FieldConfigList:FieldConfig[] = [
    new FieldConfig(
      {
        type: 'input',
        label: 'Full name',
        name: 'name1',
        placeholder: 'Enter your name',
        validation: '',
        value:'val1'
      }
    ),
    new FieldConfig({
      type: 'select',
      label: 'Favourite Food',
      name: 'food1',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'Coffee'],
      placeholder: 'Select an option',
      validation: '',
      value:'Pizza'
    }),

    new FieldConfig(
      {
        label: 'Submit',
        name: 'submit9',
        type: 'button'
      }
    )
  ];

export const GroupConfigList:GroupConfig[] = [
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'aa'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    name:'ab'
  })
]

export const FinalGroupList :GroupConfig[]=[
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'a'
  }),
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'b'
  })
]