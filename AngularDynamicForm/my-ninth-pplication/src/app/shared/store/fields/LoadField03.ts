import {GroupConfig} from '../../model/group-config/GroupConfig';
import { FieldConfig } from '../../model/field-config/FieldConfig';


export const FieldConfigList:FieldConfig[] = [
    new FieldConfig(
      {
        type: 'input',
        label: 'Full name',
        name: 'name1',
        placeholder: 'Enter your name',
        validation: '',
        value:'val1'
      }
    ),
    new FieldConfig({
      type: 'select',
      label: 'Favourite Food',
      name: 'food1',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'Coffee'],
      placeholder: 'Select an option',
      validation: '',
      value:'Pizza'
    })

  ];

export const GroupConfigList:GroupConfig[] = [
  new GroupConfig({
    fieldConfigList:[
      new FieldConfig(
        {
          type: 'input',
          label: 'Full name',
          name: 'test',
          placeholder: 'Enter your name',
          validation: '',
          value:'test1'
        }
      ),
      new FieldConfig({
        type: 'select',
        label: 'Favourite Food',
        name: 'testChoose',
        options: ['Test1', 'Test2', 'Test3', 'Test4'],
        placeholder: 'Select an option',
        validation: '',
        value:'Test1'
      })
    ],
    name:'aa'
  })
]

export const FinalGroupList :GroupConfig[]=[
  new GroupConfig({
    fieldConfigList:FieldConfigList,
    groupConfigList:GroupConfigList,
    name:'a'
  })
]