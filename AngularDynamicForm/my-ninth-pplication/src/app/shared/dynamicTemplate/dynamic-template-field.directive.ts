import { Directive, Input, ComponentRef, SimpleChanges, ComponentFactoryResolver, ViewContainerRef, Type } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { GroupConfig } from '../model/group-config/GroupConfig';
import { Group } from '../model/group/group.interface';
import { FormGroupComponent } from 'src/app/dynamic-form/dynamic-fields/form-group/form-group.component';
import { Field } from '../model/field/field.interface';
import { FormButtonComponent } from 'src/app/dynamic-form/dynamic-fields/form-button/form-button.component';
import { FormInputComponent } from 'src/app/dynamic-form/dynamic-fields/form-input/form-input.component';
import { FormSelectComponent } from 'src/app/dynamic-form/dynamic-fields/form-select/form-select.component';
import { FieldConfig } from '../model/field-config/FieldConfig';

const groupComponents: { [type: string]: Type<Group> } = {
  group: FormGroupComponent
};

const fieldComponents: { [type: string]: Type<Field> } = {
  button: FormButtonComponent,
  input: FormInputComponent,
  select: FormSelectComponent
};
@Directive({
  selector: '[appDynamicTemplateField]'
})
export class DynamicTemplateFieldDirective {
  @Input() fieldConfig: FieldConfig;
  @Input() groupConfig: GroupConfig;
  component: ComponentRef<Field>;
  @Input() group: FormGroup;

  ngOnInit(): void {
    this.createField(this.fieldConfig, this.groupConfig.name, this.group);
  }


  createField(fieldConfig: FieldConfig, formGroupName: String, formGroup: FormGroup) {
    if (!fieldComponents[fieldConfig.type]) {
      const supportedTypes = Object.keys(fieldComponents).join(', ');
      throw new Error(
        `Trying to use an unsupported type (${fieldConfig.type}).
      Supported types: ${supportedTypes}`
      );
    }
    const component = this.resolver.resolveComponentFactory<Field>(fieldComponents[fieldConfig.type]);
    this.component = this.container.createComponent(component);
    this.component.instance.fieldConfig = fieldConfig;
    this.component.instance.group = formGroup;
    this.component.instance.formGroupName = formGroupName;
  }
  ngOnChanges(changes: SimpleChanges): void {
    
    if (this.component) {
      this.component.instance.fieldConfig = this.fieldConfig;
      this.component.instance.group = this.group;
    }
    
  }

  constructor(
    private resolver: ComponentFactoryResolver,
    private container: ViewContainerRef
  ) { }

}
