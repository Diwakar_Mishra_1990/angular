import { Directive, Input, ComponentRef, SimpleChanges, ComponentFactoryResolver, ViewContainerRef, Type, OnInit, OnChanges } from '@angular/core';
import { GroupConfig } from '../model/group-config/GroupConfig';
import { Group } from '../model/group/group.interface';
import { FormGroup } from '@angular/forms';
import { FormGroupComponent } from 'src/app/dynamic-form/dynamic-fields/form-group/form-group.component';
import { Field } from '../model/field/field.interface';
import { FormButtonComponent } from 'src/app/dynamic-form/dynamic-fields/form-button/form-button.component';
import { FormInputComponent } from 'src/app/dynamic-form/dynamic-fields/form-input/form-input.component';
import { FormSelectComponent } from 'src/app/dynamic-form/dynamic-fields/form-select/form-select.component';

const groupComponents: { [type: string]: Type<Group> } = {
  group: FormGroupComponent
};

const fieldComponents: { [type: string]: Type<Field> } = {
  button: FormButtonComponent,
  input: FormInputComponent,
  select: FormSelectComponent
};

@Directive({
  selector: '[appDynamicTemplateGroup]'
})
export class DynamicTemplateGroupDirective implements OnInit,OnChanges{

  @Input() groupConfig: GroupConfig;
  component: ComponentRef<Group>;
  @Input() group: FormGroup;

  ngOnInit(): void {
    this.createGroupHeaders(this.groupConfig, this.group);
  }

  createGroupHeaders(config: GroupConfig, formGroup: FormGroup) {
    const component = this.resolver.resolveComponentFactory<Group>(groupComponents['group']);
    this.component = this.container.createComponent(component);
    this.component.instance.groupConfig = config;
    this.component.instance.group = formGroup;

  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.component) {
      this.component.instance.groupConfig = this.groupConfig;
      this.component.instance.group = this.group;
    }
  }

  constructor(
    private resolver: ComponentFactoryResolver,
    private container: ViewContainerRef
  ) { }

}
