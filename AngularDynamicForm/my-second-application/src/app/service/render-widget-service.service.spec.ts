import { TestBed } from '@angular/core/testing';

import { RenderWidgetServiceService } from './render-widget-service.service';

describe('RenderWidgetServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RenderWidgetServiceService = TestBed.get(RenderWidgetServiceService);
    expect(service).toBeTruthy();
  });
});
