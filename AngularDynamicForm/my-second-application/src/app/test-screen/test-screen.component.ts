import { Component, OnInit, ComponentFactoryResolver, ViewContainerRef, ViewChild } from '@angular/core';
import { FormArray, FormGroup, FormControl } from '@angular/forms';

import { RenderWidgetServiceService } from '../service/render-widget-service.service';
import { DateComponent } from '../widgets/date/date.component';
import { REGISTRY } from '../registry.service';

@Component({
  selector: 'app-test-screen',
  templateUrl: './test-screen.component.html',
  styleUrls: ['./test-screen.component.css']
})
export class TestScreenComponent implements OnInit {
  @ViewChild('container',{read:ViewContainerRef}) container;
  widget:any;
  widgetRenderer;
  ngAfterContentInit(): void {
    
  }
  
  
  constructor(private resolver:ComponentFactoryResolver,private widgetService:RenderWidgetServiceService,
   ) { }


  ngOnInit(): void {
    this.widgetRenderer='<app-date></app-date>';
    this.widgetService.jsonAdded.subscribe(
      (jsonMessage:string)=>{
        
        var json = JSON.parse(jsonMessage);
        console.log(json);
        this.initForm(json);
      }
  );
    
  }
initForm(widget){
  console.log(widget.length);
  
  for (let i = 0 ;i<widget.widget.length;i++){
     
    //console.log(REGISTRY.get('DateComponent'));
    //console.log(this.registryService.getallType('DateComponent'))   
    //const dateComponent = this.resolver.resolveComponentFactory(DateComponent);    
    //const dateComponent = this.resolver.resolveComponentFactory(REGISTRY.get('DateComponent')); 
    console.log(widget.widget[i].component);
    const dateComponent = this.resolver.resolveComponentFactory(REGISTRY.get(widget.widget[i].component)); 
    this.container.createComponent(dateComponent);    
   
} }

  
  
  

}