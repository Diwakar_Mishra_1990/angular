import { Component, OnInit } from '@angular/core';
import { RenderWidgetServiceService } from '../service/render-widget-service.service';

@Component({
  selector: 'app-json-box',
  templateUrl: './json-box.component.html',
  styleUrls: ['./json-box.component.css']
})
export class JsonBoxComponent implements OnInit {
  jsonMessage="";
  constructor(private renderwidgetService:RenderWidgetServiceService) { }
  
  ngOnInit() {
  }

  onSubmitJson(){
    this.renderwidgetService.createWidget(this.jsonMessage);
  }

}
