import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { DateComponent } from './widgets/date/date.component';
import { TextboxComponent } from './widgets/textbox/textbox.component';
import { JsonBoxComponent } from './json-box/json-box.component';
import { TestScreenComponent } from './test-screen/test-screen.component';


@NgModule({
  declarations: [
    AppComponent,
    DateComponent,
    TextboxComponent,
    JsonBoxComponent,
    TestScreenComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule
  ],
  entryComponents:[DateComponent,
    TextboxComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
