import { DateComponent } from './widgets/date/date.component';
import { TextboxComponent } from './widgets/textbox/textbox.component';

export const REGISTRY = new Map<string, any>();
REGISTRY.set(DateComponent.name, DateComponent);
REGISTRY.set(TextboxComponent.name, TextboxComponent);
  

