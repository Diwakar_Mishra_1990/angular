import { Directive, Input, Type, OnInit, OnChanges, SimpleChanges, ComponentFactoryResolver, ViewContainerRef, ComponentRef } from '@angular/core';
import { GroupConfig } from '../model/group/group-config/GroupConfig';
import { FormGroup } from '@angular/forms';
import { Field } from '../model/field/field.interface';
import { FormButtonComponent } from 'src/app/dynamic-form/dynamic-fields/form-button/form-button.component';
import { FormInputComponent } from 'src/app/dynamic-form/dynamic-fields/form-input/form-input.component';
import { FormSelectComponent } from 'src/app/dynamic-form/dynamic-fields/form-select/form-select.component';
import { FormGroupComponent } from 'src/app/dynamic-form/dynamic-fields/form-group/form-group.component';
import { Group } from '../model/group/group.interface';
import { FieldConfig } from '../model/field/field-config/FieldConfig';

const groupComponents: { [type: string]: Type<Group> } = {
  group: FormGroupComponent
};

const fieldComponents: { [type: string]: Type<Field> } = {
    button: FormButtonComponent,
    input: FormInputComponent,
    select: FormSelectComponent  
};


@Directive({
  selector: '[appDynamicFormGroup]'
})
export class DynamicFormGroupDirective implements Group, OnChanges, OnInit {
  @Input() groupConfig: GroupConfig;
  component: ComponentRef<Group>;
  @Input() group: FormGroup;

  ngOnInit(): void {
    this.createGroupHeaders(this.groupConfig,this.group);    
  }
/*
  createGroupHeaders(config: GroupConfig ,formGroup:FormGroup) {
    const component = this.resolver.resolveComponentFactory<Group>(groupComponents['group']);
    this.component = this.container.createComponent(component);
    this.component.instance.groupConfig = config;
    this.component.instance.group = this.group;
    if (config.fieldConfigList) {
      for (var i = 0; i < config.fieldConfigList.length; i++) {
        this.createField(config.fieldConfigList[i],config.name);
      }
    }
    this.component.instance.group = (<FormGroup>(this.group.controls[config.name])); 
    if (config.groupConfigList) {
      for (var i = 0; i < config.groupConfigList.length; i++) {
        this.createGroupHeaders(config.groupConfigList[i]);
      }
    }
  }
  */

 createGroupHeaders(config: GroupConfig ,formGroup:FormGroup) {
  const component = this.resolver.resolveComponentFactory<Group>(groupComponents['group']);
  this.component = this.container.createComponent(component);
  this.component.instance.groupConfig = config;
  this.component.instance.group = formGroup;
  if (config.fieldConfigList) {
    for (var i = 0; i < config.fieldConfigList.length; i++) {
      this.createField(config.fieldConfigList[i],config.name, formGroup);
    }
  }
  var childGroup = (<FormGroup>(formGroup.controls[config.name])); 
  if (config.groupConfigList) {
    for (var i = 0; i < config.groupConfigList.length; i++) {
      this.createGroupHeaders(config.groupConfigList[i],childGroup);
    }
  }
 }/*
  createField(fieldConfig: FieldConfig, formGroupName:String) {
    if (!fieldComponents[fieldConfig.type]) {
      const supportedTypes = Object.keys(fieldComponents).join(', ');
      throw new Error(
        `Trying to use an unsupported type (${fieldConfig.type}).
        Supported types: ${supportedTypes}`
      );
    }
    const component = this.resolver.resolveComponentFactory<Field>(fieldComponents[fieldConfig.type]);
    var fieldcomponent = this.container.createComponent(component);
    fieldcomponent.instance.fieldConfig = fieldConfig;
    fieldcomponent.instance.group = this.group;
    fieldcomponent.instance.formGroupName = formGroupName;  
    
  }
*/

createField(fieldConfig: FieldConfig, formGroupName:String, formGroup:FormGroup) {
  if (!fieldComponents[fieldConfig.type]) {
    const supportedTypes = Object.keys(fieldComponents).join(', ');
    throw new Error(
      `Trying to use an unsupported type (${fieldConfig.type}).
      Supported types: ${supportedTypes}`
    );
  }
  const component = this.resolver.resolveComponentFactory<Field>(fieldComponents[fieldConfig.type]);
  var fieldcomponent = this.container.createComponent(component);
  fieldcomponent.instance.fieldConfig = fieldConfig;
  fieldcomponent.instance.group = formGroup;
  fieldcomponent.instance.formGroupName = formGroupName;  
  
}
  ngOnChanges(changes: SimpleChanges): void {
    if (this.component) {
      this.component.instance.groupConfig = this.groupConfig;
      this.component.instance.group = this.group;
    }
  }

  constructor(
    private resolver: ComponentFactoryResolver,
    private container: ViewContainerRef
  ) { }
}
