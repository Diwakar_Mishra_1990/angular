import { GroupConfig } from './group-config/GroupConfig';
import { FormGroup } from '@angular/forms';

export interface Group {
  group: FormGroup,
  groupConfig: GroupConfig,
}
