import { GroupConfig } from '../group-config/GroupConfig';
import { FieldLayoutModel } from '../../field/field-layout/field-layout.model';
export class GroupLayoutModel {
    fieldLayout: FieldLayoutModel[]
    groupLayout: GroupLayoutModel[];

    constructor(value) {
        this.fieldLayout = value.fieldLayout;
        this.groupLayout = value.groupLayout;
    }

    getValue() {
        return {
            'fieldLayout': this.fieldLayout,
            'groupLayout': this.groupLayout
        }
    }
}



