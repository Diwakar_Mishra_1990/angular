import { FieldConfig } from '../field-config/FieldConfig';
export class FieldLayoutModel {
  disabled: boolean = false;
  orientation: string;
  validation: string;
  fieldConfig: FieldConfig[];

  constructor(value) {
    this.disabled = value.disabled;
    this.orientation = value.orientation;
    this.validation = value.validation;
    this.fieldConfig = value.fieldConfig;
  }

  getValue() {
    return {
      'disabled': this.disabled,
      'orientation': this.orientation,
      'validation': this.validation,
      'fieldConfig': this.fieldConfig
    }
  }
}



