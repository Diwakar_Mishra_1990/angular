import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Field } from 'src/app/shared/model/field/field.interface';
import { FieldConfig } from 'src/app/shared/model/field/field-config/FieldConfig';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.css']
})
export class FormInputComponent implements OnInit ,Field{
  fieldConfig: FieldConfig;
  group: FormGroup;
  formGroupName:string;
  constructor() { }

  ngOnInit() {
  }

}
