import { Component, OnInit } from '@angular/core';
import { FieldConfig } from 'src/app/shared/model/field/field-config/FieldConfig';
import { FormGroup } from '@angular/forms';
import { GroupConfig } from 'src/app/shared/model/group/group-config/GroupConfig';
import { Group } from 'src/app/shared/model/group/group.interface';

@Component({
  selector: 'app-form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.css']
})
export class FormGroupComponent implements Group {
  groupConfig: GroupConfig;
  group: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
