import { Component, OnInit } from '@angular/core';
import { Field } from 'src/app/shared/model/field/field.interface';
import { GroupConfig } from 'src/app/shared/model/group/group-config/GroupConfig';
import { FormGroup } from '@angular/forms';
import { FieldConfig } from 'src/app/shared/model/field/field-config/FieldConfig';

@Component({
  selector: 'app-form-button',
  templateUrl: './form-button.component.html',
  styleUrls: ['./form-button.component.css']
})
export class FormButtonComponent implements OnInit,Field {
  fieldcomponent: String;
  fieldConfig: FieldConfig;
  group: FormGroup;
  formGroupName:String;
  constructor() { }

  ngOnInit() {
  }

}
