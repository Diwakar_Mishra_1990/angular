import { Input } from '@angular/core';
import { FormGroup, FormGroupName, FormControlName } from '@angular/forms';

export class WidgetComponent {
    @Input('labelName') labelName: String;
    @Input('validation') validation: String;
    @Input('formControlName') formControlName: String;
    @Input('group') group: FormGroup;
    @Input('name') name: String;
    @Input('groupName') groupName: String;
    @Input('ctrlName') ctrlName: FormControlName;
    @Input('fieldType') fieldType: String;
    @Input('options') options: String[];
    constructor() { }

    onChangeVal(event){
        var customVal = this.group.get(this.groupName + '.' + this.ctrlName).value;
        customVal.value =  event.target.value;
        console.log(event);   
        this.group.get(this.groupName + '.' + this.ctrlName).patchValue(customVal);
      }
        
}