import { Component, OnInit } from '@angular/core';
import { WidgetComponent } from '../widget.component';

@Component({
  selector: 'app-customized-checkbox',
  templateUrl: './customized-checkbox.component.html',
  styleUrls: ['./customized-checkbox.component.css']
})
export class CustomizedCheckboxComponent extends WidgetComponent implements OnInit {

  constructor() {super(); }

  ngOnInit() {
  }
  
  onChangeVal(event){
    var customVal = this.group.get(this.groupName + '.' + this.ctrlName).value;
    if(customVal.value){
      customVal.value.push(event.target.value);
    }else{
      customVal.value=[];
      customVal.value.push(event.target.value);      
    }
          
    this.group.get(this.groupName + '.' + this.ctrlName).patchValue(customVal);
  }
  
}
