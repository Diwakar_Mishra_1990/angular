import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomizedWidgetComponent } from './customized-widget.component';

describe('CustomizedWidgetComponent', () => {
  let component: CustomizedWidgetComponent;
  let fixture: ComponentFixture<CustomizedWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomizedWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomizedWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
