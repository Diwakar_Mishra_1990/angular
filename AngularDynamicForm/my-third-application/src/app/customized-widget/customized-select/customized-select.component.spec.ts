import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomizedSelectComponent } from './customized-select.component';

describe('CustomizedSelectComponent', () => {
  let component: CustomizedSelectComponent;
  let fixture: ComponentFixture<CustomizedSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomizedSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomizedSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
