import { Component, OnInit } from '@angular/core';
import { WidgetComponent } from '../widget.component';

@Component({
  selector: 'app-customized-select',
  templateUrl: './customized-select.component.html',
  styleUrls: ['./customized-select.component.css']
})
export class CustomizedSelectComponent extends WidgetComponent implements OnInit {

  constructor() { super()}

  ngOnInit() {
  }

}
