import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomizedRadioComponent } from './customized-radio.component';

describe('CustomizedRadioComponent', () => {
  let component: CustomizedRadioComponent;
  let fixture: ComponentFixture<CustomizedRadioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomizedRadioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomizedRadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
