import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormGroupName, FormControlName } from '@angular/forms';

@Component({
  selector: 'app-customized-widget',
  templateUrl: './customized-widget.component.html',
  styleUrls: ['./customized-widget.component.css']
})
export class CustomizedWidgetComponent implements OnInit {
  @Input('labelName') labelName: String;
  @Input('validation') validation: String;
  @Input('formControlName') formControlName: String;
  @Input('group') group: FormGroup;
  @Input('name') name: String;
  @Input('groupName') groupName: String;
  @Input('ctrlName') ctrlName: FormControlName;
  @Input('fieldType') fieldType: String;
  @Input('options') options: String[];
  constructor() { }

  ngOnInit() {
  }

  
}
