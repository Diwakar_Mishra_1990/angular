import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomizedTextboxComponent } from './customized-textbox.component';

describe('CustomizedTextboxComponent', () => {
  let component: CustomizedTextboxComponent;
  let fixture: ComponentFixture<CustomizedTextboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomizedTextboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomizedTextboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
