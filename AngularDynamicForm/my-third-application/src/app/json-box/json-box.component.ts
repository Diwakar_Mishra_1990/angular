import { Component, OnInit } from '@angular/core';
import { RenderWidgetServiceService } from '../service/render-widget-service.service';

@Component({
  selector: 'app-json-box',
  templateUrl: './json-box.component.html',
  styleUrls: ['./json-box.component.css']
})
export class JsonBoxComponent implements OnInit {
  jsonMessage="";
  isNewFieldAdded=false;
  isEditField=false;
  fieldTypes=["textBox","checkbox","options","dropdown"];
  options=['InvoiceDetailRequest','InvoiceDetailOrder'];
  fields=['InvoiceDetailRequest.InvoiceNumber','InvoiceDetailRequest.CustomerId','InvoiceDetailOrder.orderNumber','InvoiceDetailOrder.SupplierorderId'];
  visibleOptions=['Yes','No','Custom'];
  editableOptions=['Yes','No','Custom'];
  constructor(private renderwidgetService:RenderWidgetServiceService) { }
  
  ngOnInit() {
  }

  onSubmitJson(){
    this.renderwidgetService.createWidget(this.jsonMessage);
  }
  addField(){
    this.isNewFieldAdded=true;
    this.isEditField=false;
  }

  editField(){
    this.isNewFieldAdded = false;
    this.isEditField=true;

  }

  onVisibleOptionSelect(){
    //this.isNewFieldAdded = true;
  }
}
