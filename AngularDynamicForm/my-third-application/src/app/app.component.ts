import { Component, OnInit, AfterContentInit, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterContentInit{
  
  ngAfterContentInit(): void {
    
  }
  
  constructor(private resolver:ComponentFactoryResolver){}
  ngOnInit(){
    
  }

  onSubmit(){
    //
  }
}
