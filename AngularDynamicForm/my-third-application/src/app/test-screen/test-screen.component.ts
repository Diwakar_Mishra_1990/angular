import { Component, OnInit, ComponentFactoryResolver, ViewContainerRef, ViewChild } from '@angular/core';
import { FormArray, FormGroup, FormControl } from '@angular/forms';
import { RenderWidgetServiceService } from '../service/render-widget-service.service';

@Component({
  selector: 'app-test-screen',
  templateUrl: './test-screen.component.html',
  styleUrls: ['./test-screen.component.css']
})
export class TestScreenComponent implements OnInit {
  //@ViewChild('container',{read:ViewContainerRef}) container;
  widget: any;
  InvoiceDetailRequest: FormGroup;

  ngAfterContentInit(): void {

  }

  constructor(private resolver: ComponentFactoryResolver, private widgetService: RenderWidgetServiceService,
  ) { }

  ngOnInit(): void {
    this.InvoiceDetailRequest = new FormGroup({
      'InvoiceDetailRequestHeader':new FormGroup({
        'invoiceNumber': new FormControl(null),
        'customerID': new FormControl(null),
        'customizedComponent': new FormArray([
          new FormControl({
            labelName: 'LABEL1',
            validation:'',
            name:'name1',
            fieldType:'app-customized-textbox',
          }),
          new FormControl({
            labelName: 'LABEL2',
            fieldType:'app-customized-radio',
            validation:'',
            name:'name2',
            options:['LEVEL_2_RADIO_A','LEVEL_2_RADIO_B','LEVEL_2_RADIO_C']
          }),
          new FormControl({
            labelName: 'LABEL3',
            fieldType:'app-customized-select',
            validation:'',
            name:'name3',
            options:['LEVEL_3_SELECT_A','LEVEL_3_SELECT_B','LEVEL_3_SELECT_C']
          })
        ])
      }),
      'InvoiceDetailOrder':new FormGroup({
        'InvoiceDetailOrderInfo': new FormGroup({
          'OrderIDInfo': new FormControl(null),
          'SupplierOrderInfo': new FormControl(null),
          'customizedComponent': new FormArray([
            new FormControl({
              labelName: 'LABEL4',
              validation:'',
              fieldType:'app-customized-textbox',
              name:'name4'
            }),
            new FormControl({
              labelName: 'LABEL5',
              validation:'',
              fieldType:'app-customized-checkbox',
              name:'name5',
              options:['LEVEL_5_CHECKBOX_A','LEVEL_6_CHECKBOX_B','LEVEL_6_CHECKBOX_C']
            }),
            new FormControl({
              labelName: 'LABEL6',
              fieldType:'app-customized-radio',
              validation:'',
              name:'name6',
              options:['LEVEL_6_RADIO_A','LEVEL_6_RADIO_B','LEVEL_6_RADIO_C']
            })
          ])
        })
      }),
    });
    //console.log(this.InvoiceDetailRequest.get('InvoiceDetailOrder.InvoiceDetailOrderInfo'));
    //console.log(this.InvoiceDetailRequest);
    /*
    this.widgetService.jsonAdded.subscribe(
      (jsonMessage: string) => {

        var json = JSON.parse(jsonMessage);
        console.log(json);
        this.initForm(json);
      }
    );
*/
  }
  initForm(widget) {
    console.log(widget.length);

    for (let i = 0; i < widget.widget.length; i++) {



    }
  }
  onSubmit(){
    console.log(this.InvoiceDetailRequest);
  }


}