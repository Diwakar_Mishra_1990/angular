import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { JsonBoxComponent } from './json-box/json-box.component';
import { TestScreenComponent } from './test-screen/test-screen.component';
import { CustomizedWidgetComponent } from './customized-widget/customized-widget.component';
import { CustomizedTextboxComponent } from './customized-widget/customized-textbox/customized-textbox.component';
import { DropdownDirective } from './shared/directive/dropdown.directive';
import { CustomizedCheckboxComponent } from './customized-widget/customized-checkbox/customized-checkbox.component';
import { CustomizedRadioComponent } from './customized-widget/customized-radio/customized-radio.component';
import { CustomizedSelectComponent } from './customized-widget/customized-select/customized-select.component';



@NgModule({
  declarations: [
    AppComponent,
    JsonBoxComponent,
    TestScreenComponent,
    CustomizedWidgetComponent,
    CustomizedTextboxComponent,
    DropdownDirective,
    CustomizedCheckboxComponent,
    CustomizedRadioComponent,
    CustomizedSelectComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
