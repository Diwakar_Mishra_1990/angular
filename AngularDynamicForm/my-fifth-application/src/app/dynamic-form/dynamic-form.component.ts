import { Component, OnInit, Input, OnChanges,Output,EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FieldConfig } from '../shared/model/field-config/fieldConfig';
import { WidgetRenderingService } from '../shared/service/widgetRendering/widget-rendering.service';

@Component({
  exportAs: 'dynamicForm',
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.css']
})
export class DynamicFormComponent implements OnInit, OnChanges {
  form: FormGroup;
  configList: FieldConfig[] = [];
  @Output()submit: EventEmitter<any> = new EventEmitter<any>();
  constructor(private fb: FormBuilder, private widgetRenderingService: WidgetRenderingService) { }

  ngOnInit() {
    this.form = this.createGroup();
    this.widgetRenderingService.jsonAdded.subscribe(
      (response) => {
        this.configList = response;
        this.form = this.createGroup();
      }
    )
  }

  ngOnChanges() {
  }
  createGroup() {
    const group = this.fb.group({});
    this.controls.forEach(control => group.addControl(control.name, this.createControl(control)));
    //console.log(group);
    return group;
  }

  createControl(config: FieldConfig) {
    const { disabled, validation, value } = config;
    return this.fb.control({ disabled, value });
  }


  handleSubmit(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    console.log(event);
    this.submit.emit(this.value);
  }

  get controls() { return this.configList.filter(({type}) => type !== 'button'); }
  get changes() { return this.form.valueChanges; }
  get valid() { return this.form.valid; }
  get value() { return this.form.value; }
}
