import { Component, OnInit } from '@angular/core';
import { FieldConfig } from 'src/app/shared/model/field-config/fieldConfig';
import { FormGroup } from '@angular/forms';
import { Field } from 'src/app/shared/model/field/field.interface';

@Component({
  selector: 'app-form-select',
  templateUrl: './form-select.component.html',
  styleUrls: ['./form-select.component.css']
})
export class FormSelectComponent implements OnInit ,Field{
  config: FieldConfig;
  group: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
