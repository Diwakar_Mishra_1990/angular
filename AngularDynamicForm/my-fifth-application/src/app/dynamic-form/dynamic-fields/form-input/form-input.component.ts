import { Component, OnInit } from '@angular/core';
import { FieldConfig } from 'src/app/shared/model/field-config/fieldConfig';
import { FormGroup } from '@angular/forms';
import { Field } from 'src/app/shared/model/field/field.interface';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.css']
})
export class FormInputComponent implements OnInit ,Field{
  config: FieldConfig;
  group: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
