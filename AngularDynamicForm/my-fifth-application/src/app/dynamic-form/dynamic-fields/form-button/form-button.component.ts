import { Component, OnInit } from '@angular/core';
import { Field } from 'src/app/shared/model/field/field.interface';
import { FieldConfig } from 'src/app/shared/model/field-config/fieldConfig';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-button',
  templateUrl: './form-button.component.html',
  styleUrls: ['./form-button.component.css']
})
export class FormButtonComponent implements OnInit,Field {
  config: FieldConfig;
  group: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
