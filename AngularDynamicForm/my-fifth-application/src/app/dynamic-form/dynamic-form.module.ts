import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormComponent } from './dynamic-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DynamicFormDirective } from './../shared/dynamicForm/dynamic-form.directive';
import { FormButtonComponent } from './dynamic-fields/form-button/form-button.component';
import { FormInputComponent } from './dynamic-fields/form-input/form-input.component';
import { FormSelectComponent } from './dynamic-fields/form-select/form-select.component';

@NgModule({
  declarations: [
    DynamicFormComponent,
    DynamicFormDirective,
    FormButtonComponent,
    FormInputComponent,
    FormSelectComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    FormButtonComponent,
    FormInputComponent,
    FormSelectComponent
  ],
  exports:[DynamicFormComponent]
})
export class DynamicFormModule { 
   
}
