import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DynamicFormModule } from './dynamic-form/dynamic-form.module';
import { DynamicFormDirective } from './shared/dynamicForm/dynamic-form.directive';
import { JsonModuleModule } from './json-module/json-module.module';
import { TestScreenModuleModule } from './test-screen-module/test-screen-module.module';
import { WidgetRenderingService } from './shared/service/widgetRendering/widget-rendering.service';


@NgModule({
  declarations: [
    AppComponent 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    DynamicFormModule,
    JsonModuleModule,
    TestScreenModuleModule
  ],
  providers: [WidgetRenderingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
