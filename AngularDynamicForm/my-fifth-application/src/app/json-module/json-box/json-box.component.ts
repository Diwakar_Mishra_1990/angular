import { Component, OnInit } from '@angular/core';
import { WidgetRenderingService } from 'src/app/shared/service/widgetRendering/widget-rendering.service';
import { FieldConfigList } from 'src/app/shared/store/fields/field';
import { FieldConfig } from 'src/app/shared/model/field-config/fieldConfig';

@Component({
  exportAs: 'jsonBox',
  selector: 'app-json-box',
  templateUrl: './json-box.component.html',
  styleUrls: ['./json-box.component.css']
})
export class JsonBoxComponent implements OnInit {
  jsonMessage="";
  constructor(private widgetRenderingService:WidgetRenderingService) { }

  ngOnInit() {
    let list = FieldConfigList;
    this.jsonMessage="[";
    for(var i=0;i<list.length;i++){
      this.jsonMessage = this.jsonMessage +  JSON.stringify(list[i].getValue())+",";
    }
    this.jsonMessage=this.jsonMessage.slice(0, -1)+"]";
  }
  onSubmitJson(){
    var fieldConfigList:FieldConfig[]=[];
    var jsonList= JSON.parse(this.jsonMessage);
    for(var i=0;i<jsonList.length;i++){
      fieldConfigList.push(new FieldConfig(jsonList[i]))
    }
    this.widgetRenderingService.createWidget(fieldConfigList);
  }

}
