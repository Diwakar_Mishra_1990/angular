import {FieldConfig} from '../../model/field-config/fieldConfig';

export const FieldConfigList:FieldConfig[] = [
    new FieldConfig(
      {
        type: 'input',
        label: 'Full name',
        name: 'name',
        placeholder: 'Enter your name',
        validation: ''
      }
    ),
    new FieldConfig({
      type: 'select',
      label: 'Favourite Food',
      name: 'food',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'Coffee'],
      placeholder: 'Select an option',
      validation: ''
    }),
    new FieldConfig(
      {
        label: 'Submit',
        name: 'submit',
        type: 'button'
      }
    )
  ];
