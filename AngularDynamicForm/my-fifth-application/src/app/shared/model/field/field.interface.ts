import { FieldConfig } from '../field-config/fieldConfig';
import { FormGroup } from '@angular/forms';

export interface Field {
    config: FieldConfig,
    group: FormGroup
  }
  