import { Injectable, EventEmitter } from '@angular/core';
import { FieldConfig } from '../../model/field-config/fieldConfig';
import { Subject } from 'rxjs';

@Injectable()
export class WidgetRenderingService {
  jsonAdded = new Subject<FieldConfig[]>();


  constructor() { }

  createWidget(fieldConfigList:FieldConfig[]){
    this.jsonAdded.next(fieldConfigList);  
  }
}
