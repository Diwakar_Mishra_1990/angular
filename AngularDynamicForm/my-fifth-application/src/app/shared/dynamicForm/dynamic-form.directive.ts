import { Directive, Input, Type, OnInit, OnChanges ,SimpleChanges, ComponentFactoryResolver, ViewContainerRef, ComponentRef} from '@angular/core';
import { FieldConfig } from '../model/field-config/fieldConfig';
import { FormGroup } from '@angular/forms';
import { Field } from '../model/field/field.interface';
import { FormButtonComponent } from 'src/app/dynamic-form/dynamic-fields/form-button/form-button.component';
import { FormInputComponent } from 'src/app/dynamic-form/dynamic-fields/form-input/form-input.component';
import { FormSelectComponent } from 'src/app/dynamic-form/dynamic-fields/form-select/form-select.component';


const components: {[type: string]: Type<Field>} = {
  button: FormButtonComponent,
  input: FormInputComponent,
  select: FormSelectComponent
};


@Directive({
  selector: '[appDynamicForm]'
})
export class DynamicFormDirective implements Field, OnChanges, OnInit{
  ngOnInit(): void {
    if (!components[this.config.type]) {
      const supportedTypes = Object.keys(components).join(', ');
      throw new Error(
        `Trying to use an unsupported type (${this.config.type}).
        Supported types: ${supportedTypes}`
      );
    }
    const component = this.resolver.resolveComponentFactory<Field>(components[this.config.type]);
    this.component = this.container.createComponent(component);
    this.component.instance.config = this.config;
    this.component.instance.group = this.group;
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.component) {
      this.component.instance.config = this.config;
      this.component.instance.group = this.group;
    }
  }
  @Input()
  config: FieldConfig;
  component: ComponentRef<Field>;
  @Input() group: FormGroup;
  constructor(
    private resolver: ComponentFactoryResolver,
    private container: ViewContainerRef
  ) {}

}
