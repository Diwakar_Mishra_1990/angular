import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestScreenComponent } from './test-screen/test-screen.component';
import { DynamicFormModule } from '../dynamic-form/dynamic-form.module';

@NgModule({
  declarations: [TestScreenComponent],
  imports: [
    CommonModule,
    DynamicFormModule
  ],
  exports:[
    TestScreenComponent,  
    DynamicFormModule  
  ]
})
export class TestScreenModuleModule { }
