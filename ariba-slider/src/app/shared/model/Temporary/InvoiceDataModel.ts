import { AddressDataModel } from './AddressDataModel';
import { LineItemDataModel } from './LineItemDataModel';

export class InvoiceDataModel {
    public invoiceId: String;
    public billTo: AddressDataModel;
    public billFrom: AddressDataModel;
    public lineItemDataModel: LineItemDataModel[];

    constructor(invoiceId: String, billTo: AddressDataModel, billFrom: AddressDataModel, lineItemDataModel: LineItemDataModel[]) {
        this.invoiceId = invoiceId;
        this.billTo = billTo;
        this.billFrom = billFrom;
        this.lineItemDataModel = lineItemDataModel;
    }

}