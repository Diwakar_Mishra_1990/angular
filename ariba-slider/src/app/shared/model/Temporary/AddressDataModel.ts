export class AddressDataModel {
    public addressLine1: string;
    public addressLine2: string;
    public addressLine3: string;
    public municipalty: string;
    public city: string;
    public state: string;
    public country: string;

    constructor(addressLine1: string, addressLine2: string, addressLine3: string, municipalty: string,
        city: string, state: string, country: string) {
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressLine3 = addressLine3;
        this.municipalty = municipalty;
        this.city = city;
        this.state = state;
        this.country = country;
    }
   

}