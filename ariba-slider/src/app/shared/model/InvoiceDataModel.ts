interface RootObject {
    InvoiceDetailRequest: InvoiceDetailRequest;
}
interface InvoiceDetailRequest {
    'xmlns:cxml2': string;
    'xmlns:xsi': string;
    'xsi:schemaLocation': string;
    InvoiceDetailRequestHeader: InvoiceDetailRequestHeader;
    InvoiceDetailOrder: InvoiceDetailOrder;
    InvoiceDetailHeaderOrder: InvoiceDetailHeaderOrder;
    InvoiceDetailSummary: InvoiceDetailSummary;
}
interface InvoiceDetailRequestHeader {
    invoiceID: string;
    isInformationOnly: string;
    purpose: string;
    operation: string;
    invoiceDate: string;
    invoiceOrigin: string;
    InvoiceDetailHeaderIndicator: InvoiceDetailHeaderIndicator;
    InvoiceDetailLineIndicator: InvoiceDetailLineIndicator;
    InvoicePartner: InvoicePartner;
    remitTo: RemitTo;
    billTo: BillTo;
    from: From;
    billFrom: BillFrom;
    requester: Requester;
    fieldEngineer: FieldEngineer;
    fieldContractor: FieldContractor;
    soldTo: SoldTo;
    taxRepresentative: TaxRepresentative;
    customerTaxRepresentative: CustomerTaxRepresentative;
    serviceBroker: ServiceBroker;
    subsequentBuyer: SubsequentBuyer;
    receivingBank: ReceivingBank;
    wireReceivingBank: WireReceivingBank;
    receivingCorrespondentBank: ReceivingCorrespondentBank;
    DocumentReference: DocumentReference;
    InvoiceIDInfo: InvoiceIDInfo;
    PaymentProposalIDInfo: PaymentProposalIDInfo;
    InvoiceDetailShipping: InvoiceDetailShipping;
    ShipNoticeIDInfo: ShipNoticeIDInfo;
    InvoiceDetailNetPaymentTerm: InvoiceDetailNetPaymentTerm;
    NetPaymentTerm: NetPaymentTerm;
    InvoiceDetailPaymentTerm: InvoiceDetailPaymentTerm;
    Period: Period;
    Comments: Comments;
    InvoiceDisclaimerComments: InvoiceDisclaimerComments;
    CreditDisclaimerComments: CreditDisclaimerComments;
    supplierReference: SupplierReference;
    discountInformation: DiscountInformation;
    penaltyInformation: PenaltyInformation;
    netTermInformation: NetTermInformation;
    PaymentTermsExplanation: PaymentTermsExplanation;
    taxInvoice: TaxInvoice;
    invoiceTitle: InvoiceTitle;
    customerReference: CustomerReference;
    DocumentType: DocumentType;
    PaymentService: PaymentService;
    PaymentProcessor: PaymentProcessor;
    Transaction: Transaction;
    IsShippingTaxExempt: IsShippingTaxExempt;
    buyerVatID: BuyerVatID;
    supplierVatID: SupplierVatID;
    buyerTaxID: BuyerTaxID;
    supplierCommercialRegisterCourt: SupplierCommercialRegisterCourt;
    VATonDebits: VATonDebits;
    invoiceSourceDocument: InvoiceSourceDocument;
    invoiceSubmissionMethod: InvoiceSubmissionMethod;
    paymentMethod: PaymentMethod;
    bankDetails: BankDetails;
    paymentNote: PaymentNote;
    invoiceType: InvoiceType;
    isAutoflip: IsAutoflip;
    dynamicDiscountingCreditMemo: DynamicDiscountingCreditMemo;
    punchinContractInvoice: PunchinContractInvoice;
    isNFSe: IsNFSe;
    saoPauloServiceCode: SaoPauloServiceCode;
    rpsType: RpsType;
    rpsSeries: RpsSeries;
    rpsNumber: RpsNumber;
    verificationCode: VerificationCode;
    isServiceBrokerUsed: IsServiceBrokerUsed;
    CEICode: CEICode;
    supplierCommercialIdentifier: SupplierCommercialIdentifier;
    legalCapital: LegalCapital;
    shareHolderType: ShareHolderType;
    supplierLiquidationState: SupplierLiquidationState;
    supplierTaxRegime: SupplierTaxRegime;
    projectCode: ProjectCode;
    tenderCode: TenderCode;
    CNAECode: CNAECode;
    rioServiceCode: RioServiceCode;
    regionalEngineerAssociationNum: RegionalEngineerAssociationNum;
    culturalIncentive: CulturalIncentive;
    simpleNational: SimpleNational;
    specialTaxRegime: SpecialTaxRegime;
    taxBenefitCode: TaxBenefitCode;
    useOfCFDICode: UseOfCFDICode;
    placeOfSupply: PlaceOfSupply;
    Extrinsic: Extrinsic;
    invoiceDisplayUrl: InvoiceDisplayUrl;
    exchangeRate: ExchangeRate;
    supplierCommercialRegistration: SupplierCommercialRegistration;
    taxInvoiceNumber: TaxInvoiceNumber;
    typeOfSupply: TypeOfSupply;
    externalPurpose: ExternalPurpose;
    referenceTaxInvoiceNumber: ReferenceTaxInvoiceNumber;
    referenceExternalPurpose: ReferenceExternalPurpose;
    resolutionNumber: ResolutionNumber;
    resolutionDate: ResolutionDate;
    taxDeterminationDate: TaxDeterminationDate;
    economicActivityCode: EconomicActivityCode;
    economicActivityDescription: EconomicActivityDescription;
    fiscalRegimeType: FiscalRegimeType;
    specialRegimeStatus: SpecialRegimeStatus;
    taxpayerType: TaxpayerType;
    autoDeductionStatus: AutoDeductionStatus;
    buyerEconomicActivityDescription: BuyerEconomicActivityDescription;
    creditMemoReason: CreditMemoReason;
    debitMemoReason: DebitMemoReason;
    supplierLegalDetails: SupplierLegalDetails;
    convertedInvoiceData: ConvertedInvoiceData;
    initiatePaymentAction: InitiatePaymentAction;
    serviceDescription: ServiceDescription;
    isFinalAutoSES: IsFinalAutoSES;
    supplierSalesTaxID: SupplierSalesTaxID;
    buyerSalesTaxID: BuyerSalesTaxID;
    supplierServiceTaxID: SupplierServiceTaxID;
    buyerServiceTaxID: BuyerServiceTaxID;
}
interface InvoiceDetailHeaderIndicator {
    isHeaderInvoice: string;
    isVatRecoverable: string;
    isPriceBasedLineLevelCreditMemo: string;
}
interface InvoiceDetailLineIndicator {
    isTaxInLine: string;
    isSpecialHandlingInLine: string;
    isShippingInLine: string;
    isDiscountInLine: string;
    isAccountingInLine: string;
    isPriceAdjustmentInLine: string;
}
interface InvoicePartner {
    Contact: Contact;
    IdReference: IdReference;
}
interface Contact {
    role: string;
    addressID: string;
    isRemoved: boolean;
    isNew: boolean;
    orgName: string;
    Name: Name;
    PostalAddress: PostalAddress;
    Email: Email;
    Phone: Phone;
    Fax: Fax;
    URL: URL;
    OldContact: OldContact;
    departmentName: DepartmentName;
    gstID: GstID;
    pstID: PstID;
    qstID: QstID;
    IdReference: IdReference;
    isFactoring: IsFactoring;
    LegalStatus: LegalStatus;
    LegalCapital: LegalCapital;
    Extrinsic: Extrinsic;
}
interface Name {
    'xml:lang': string;
    content: string;
}
interface PostalAddress {
    name: string;
    DeliverTo: string;
    Street: string;
    City: City;
    Municipality: Municipality;
    State: State;
    PostalCode: string;
    Country: Country;
}
interface City {
    cityCode: string;
    content: string;
}
interface Municipality {
    municipalityCode: string;
    content: string;
}
interface State {
    isoStateCode: string;
    content: string;
}
interface Country {
    isoCountryCode: string;
    content: string;
}
interface Email {
    name: string;
    content: string;
}
interface Phone {
    name: string;
    TelephoneNumber: TelephoneNumber;
}
interface TelephoneNumber {
    CountryCode: CountryCode;
    AreaOrCityCode: string;
    Number: string;
    Extension: string;
}
interface CountryCode {
    isoCountryCode: string;
    content: string;
}
interface Fax {
    name: string;
    TelephoneNumber?: TelephoneNumber;
    URL?: URL;
    Email?: Email;
}
interface URL {
    name: string;
    content: string;
}
interface OldContact {
    role: string;
    addressID: string;
    Name: Name;
    PostalAddress: PostalAddress;
    Email: Email;
    Phone: Phone;
    Fax: Fax;
    URL: URL;
}
interface DepartmentName {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface Creator {
    'xml:lang': string;
    content: string;
}
interface Description {
    'xml:lang': string;
    type: string;
    ShortName: string;
}
interface GstID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface PstID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface QstID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface IdReference {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface IsFactoring {
    name: string;
    schemaMappedType: string;
    content: boolean;
}
interface LegalStatus {
    name: string;
}
interface LegalCapital {
    name: string;
    schemaMappedType: string;
    Money: Money;
    content?: string;
}
interface Money {
    currency: string;
    alternateAmount: number;
    alternateCurrency: string;
    content: number;
}
interface Extrinsic {
    name: string;
}
interface RemitTo {
    Contact: Contact;
    IdReference: IdReference;
    accountId: AccountId;
    supplierTaxID: SupplierTaxID;
    accountReceivableID: AccountReceivableID;
}
interface AccountId {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface SupplierTaxID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface AccountReceivableID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface BillTo {
    Contact: Contact;
    IdReference: IdReference;
}
interface From {
    Contact: Contact;
    IdReference: IdReference;
    provincialTaxID: ProvincialTaxID;
    workRegistrationID: WorkRegistrationID;
}
interface ProvincialTaxID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface WorkRegistrationID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface BillFrom {
    Contact: Contact;
    IdReference: IdReference;
}
interface Requester {
    Contact: Contact;
    IdReference: IdReference;
}
interface FieldEngineer {
    Contact: Contact;
    IdReference: IdReference;
}
interface FieldContractor {
    Contact: Contact;
    IdReference: IdReference;
}
interface SoldTo {
    Contact: Contact;
    IdReference: IdReference;
    provincialTaxID: ProvincialTaxID;
    stateTaxID: StateTaxID;
}
interface StateTaxID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface TaxRepresentative {
    Contact: Contact;
    IdReference: IdReference;
    vatID: VatID;
}
interface VatID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface CustomerTaxRepresentative {
    Contact: Contact;
    IdReference: IdReference;
    vatID: VatID;
}
interface ServiceBroker {
    Contact: Contact;
    IdReference: IdReference;
    vatID: VatID;
    provincialTaxID: ProvincialTaxID;
}
interface SubsequentBuyer {
    Contact: Contact;
    IdReference: IdReference;
}
interface ReceivingBank {
    Contact: Contact;
    IdReference: IdReference;
    accountID: AccountID;
    bankRoutingID: BankRoutingID;
    swiftID: SwiftID;
    ibanID: IbanID;
    accountName: AccountName;
    accountType: AccountType;
    bankIdType: BankIdType;
}
interface AccountID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface BankRoutingID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface SwiftID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface IbanID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface AccountName {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface AccountType {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface BankIdType {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface WireReceivingBank {
    Contact: Contact;
    IdReference: IdReference;
    accountID: AccountID;
    bankRoutingID: BankRoutingID;
    swiftID: SwiftID;
    ibanID: IbanID;
    accountName: AccountName;
    accountType: AccountType;
    bankIdType: BankIdType;
}
interface ReceivingCorrespondentBank {
    Contact: Contact;
    IdReference: IdReference;
    accountID: AccountID;
    bankRoutingID: BankRoutingID;
    swiftID: SwiftID;
    ibanID: IbanID;
    accountName: AccountName;
    accountType: AccountType;
    bankIdType: BankIdType;
}
interface DocumentReference {
    payloadID: string;
}
interface InvoiceIDInfo {
    invoiceID: string;
    invoiceDate: string;
}
interface PaymentProposalIDInfo {
    paymentProposalID: string;
}
interface InvoiceDetailShipping {
    shippingDate: string;
    shipFrom: ShipFrom;
    shipTo: ShipTo;
    Contact: Contact;
    CarrierIdentifier: CarrierIdentifier;
    ShipmentIdentifier: string;
    DocumentReference: DocumentReference;
}
interface ShipFrom {
    role: string;
    addressID: string;
    isRemoved: boolean;
    isNew: boolean;
    orgName: string;
    Name: Name;
    PostalAddress: PostalAddress;
    Email: Email;
    Phone: Phone;
    Fax: Fax;
    URL: URL;
    OldContact: OldContact;
    departmentName: DepartmentName;
    gstID: GstID;
    pstID: PstID;
    qstID: QstID;
    IdReference: IdReference;
    isFactoring: IsFactoring;
    LegalStatus: LegalStatus;
    LegalCapital: LegalCapital;
    Extrinsic: Extrinsic;
}
interface ShipTo {
    role: string;
    addressID: string;
    isRemoved: boolean;
    isNew: boolean;
    orgName: string;
    Name: Name;
    PostalAddress: PostalAddress;
    Email: Email;
    Phone: Phone;
    Fax: Fax;
    URL: URL;
    OldContact: OldContact;
    departmentName: DepartmentName;
    gstID: GstID;
    pstID: PstID;
    qstID: QstID;
    IdReference: IdReference;
    isFactoring: IsFactoring;
    LegalStatus: LegalStatus;
    LegalCapital: LegalCapital;
    Extrinsic: Extrinsic;
}
interface CarrierIdentifier {
    domain: string;
    content: string;
}
interface ShipNoticeIDInfo {
    shipNoticeID: string;
    shipNoticeDate: string;
    oldshipNoticeID: string;
    oldshipNoticeDate: string;
    IdReference: IdReference;
    deliveryNoteID: DeliveryNoteID;
    deliveryNoteDate: DeliveryNoteDate;
    deliveryNoteLineItemNo: DeliveryNoteLineItemNo;
    dispatchAdviceID: DispatchAdviceID;
    receivingAdviceID: ReceivingAdviceID;
    receivingAdviceDate: ReceivingAdviceDate;
    transportDocumentID: TransportDocumentID;
    proofOfDeliveryID: ProofOfDeliveryID;
    proofOfDeliveryDate: ProofOfDeliveryDate;
    actualDeliveryDate: ActualDeliveryDate;
    goodsPositioningDate: GoodsPositioningDate;
    goodsPositioningStartDate: GoodsPositioningStartDate;
    goodsPositioningEndDate: GoodsPositioningEndDate;
}
interface DeliveryNoteID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface DeliveryNoteDate {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface DeliveryNoteLineItemNo {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface DispatchAdviceID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface ReceivingAdviceID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface ReceivingAdviceDate {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface TransportDocumentID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface ProofOfDeliveryID {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface ProofOfDeliveryDate {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface ActualDeliveryDate {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface GoodsPositioningDate {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface GoodsPositioningStartDate {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface GoodsPositioningEndDate {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface InvoiceDetailNetPaymentTerm {
    payInNumberOfDays: number;
    percentageRate: number;
}
interface NetPaymentTerm {
    payInNumberOfDays: number;
}
interface InvoiceDetailPaymentTerm {
    payInNumberOfDays: number;
    percentageRate: number;
}
interface Period {
    startDate: string;
    endDate: string;
}
interface Comments {
    'xml:lang': string;
    Attachment: Attachment;
}
interface Attachment {
    URL: URL;
}
interface InvoiceDisclaimerComments {
    'xml:lang': string;
    Attachment: Attachment;
}
interface CreditDisclaimerComments {
    'xml:lang': string;
    Attachment: Attachment;
}
interface SupplierReference {
    identifier: string;
    domain: string;
    Creator: Creator;
    Description: Description;
}
interface DiscountInformation {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface PenaltyInformation {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface NetTermInformation {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface PaymentTermsExplanation {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface TaxInvoice {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface InvoiceTitle {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface CustomerReference {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface DocumentType {
    schemaMappedType: string;
    content: string;
}
interface PaymentService {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface PaymentProcessor {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface Transaction {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface IsShippingTaxExempt {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface BuyerVatID {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface SupplierVatID {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface BuyerTaxID {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface SupplierCommercialRegisterCourt {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface VATonDebits {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface InvoiceSourceDocument {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface InvoiceSubmissionMethod {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface PaymentMethod {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface BankDetails {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface PaymentNote {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface InvoiceType {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface IsAutoflip {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface DynamicDiscountingCreditMemo {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface PunchinContractInvoice {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface IsNFSe {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface SaoPauloServiceCode {
    name: string;
    schemaMappedType: string;
    Classification: Classification;
}
interface Classification {
    domain: string;
    content: string;
}
interface RpsType {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface RpsSeries {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface RpsNumber {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface VerificationCode {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface IsServiceBrokerUsed {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface CEICode {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface SupplierCommercialIdentifier {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface ShareHolderType {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface SupplierLiquidationState {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface SupplierTaxRegime {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface ProjectCode {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface TenderCode {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface CNAECode {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface RioServiceCode {
    name: string;
    schemaMappedType: string;
    Classification: Classification;
}
interface RegionalEngineerAssociationNum {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface CulturalIncentive {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface SimpleNational {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface SpecialTaxRegime {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface TaxBenefitCode {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface UseOfCFDICode {
    name: string;
    schemaMappedType: string;
    Classification: Classification;
}
interface PlaceOfSupply {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface InvoiceDisplayUrl {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface ExchangeRate {
    name: string;
    schemaMappedType: string;
    content: number;
}
interface SupplierCommercialRegistration {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface TaxInvoiceNumber {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface TypeOfSupply {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface ExternalPurpose {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface ReferenceTaxInvoiceNumber {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface ReferenceExternalPurpose {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface ResolutionNumber {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface ResolutionDate {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface TaxDeterminationDate {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface EconomicActivityCode {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface EconomicActivityDescription {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface FiscalRegimeType {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface SpecialRegimeStatus {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface TaxpayerType {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface AutoDeductionStatus {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface BuyerEconomicActivityDescription {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface CreditMemoReason {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface DebitMemoReason {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface SupplierLegalDetails {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface ConvertedInvoiceData {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface InitiatePaymentAction {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface ServiceDescription {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface IsFinalAutoSES {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface SupplierSalesTaxID {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface BuyerSalesTaxID {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface SupplierServiceTaxID {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface BuyerServiceTaxID {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface InvoiceDetailOrder {
    InvoiceDetailOrderInfo: InvoiceDetailOrderInfo;
    InvoiceDetailReceiptInfo: InvoiceDetailReceiptInfo;
    InvoiceDetailShipNoticeInfo: InvoiceDetailShipNoticeInfo;
    InvoiceDetailServiceItem: InvoiceDetailServiceItem[];
    pureShippingLine: PureShippingLine;
    pureSpecialHandlingLine: PureSpecialHandlingLine;
}
interface InvoiceDetailOrderInfo {
    OrderReference: OrderReference;
    MasterAgreementReference: MasterAgreementReference;
    MasterAgreementIDInfo: MasterAgreementIDInfo;
    OrderIDInfo: OrderIDInfo;
    SupplierOrderInfo: SupplierOrderInfo;
}
interface OrderReference {
    orderID: string;
    orderDate: string;
    DocumentReference: DocumentReference;
}
interface MasterAgreementReference {
    agreementID: string;
    agreementDate: string;
    agreementType: string;
    DocumentReference: DocumentReference;
}
interface MasterAgreementIDInfo {
    agreementID: string;
    agreementDate: string;
    agreementType: string;
}
interface OrderIDInfo {
    orderID: string;
    orderDate: string;
}
interface SupplierOrderInfo {
    orderID: string;
    orderDate: string;
}
interface InvoiceDetailReceiptInfo {
    ReceiptReference: ReceiptReference;
    ReceiptIDInfo: ReceiptIDInfo;
}
interface ReceiptReference {
    receiptID: string;
    receiptDate: string;
    DocumentReference: DocumentReference;
}
interface ReceiptIDInfo {
    receiptID: string;
    receiptDate: string;
}
interface InvoiceDetailShipNoticeInfo {
    ShipNoticeReference: ShipNoticeReference;
    ShipNoticeIDInfo: ShipNoticeIDInfo;
}
interface ShipNoticeReference {
    shipNoticeID: string;
    shipNoticeDate: string;
    oldshipNoticeID: string;
    oldshipNoticeDate: string;
    DocumentReference: DocumentReference;
}
interface InvoiceDetailServiceItem {
    invoiceLineNumber: number;
    parentInvoiceLineNumber: number;
    quantity: number;
    inspectionDate: string;
    referenceDate: string;
    isAdHoc: string;
    InvoiceDetailItemReference: InvoiceDetailItemReference;
    InvoiceDetailServiceItemReference: InvoiceDetailServiceItemReference;
    ServiceEntryItemIDInfo: ServiceEntryItemIDInfo;
    SubtotalAmount: SubtotalAmount;
    Period: Period;
    UnitRate: UnitRate;
    Tax: Tax;
    POTax: POTax;
    RelatedInvoiceTax: RelatedInvoiceTax;
    InvoiceDetailLineSpecialHandling: InvoiceDetailLineSpecialHandling;
    InvoiceDetailLineShipping: InvoiceDetailLineShipping;
    ShipNoticeIDInfo: ShipNoticeIDInfo;
    GrossAmount: GrossAmount;
    InvoiceDetailDiscount: InvoiceDetailDiscount;
    InvoiceItemModifications: InvoiceItemModifications;
    TotalCharges: TotalCharges;
    TotalAllowances: TotalAllowances;
    TotalAmountWithoutTax: TotalAmountWithoutTax;
    TotalTaxOnModifications: TotalTaxOnModifications;
    NetAmount: NetAmount;
    distribution: Distribution;
    Distribution: Distribution;
    Comments: Comments;
    InvoiceLaborDetail: InvoiceLaborDetail;
    punchinItemFromCatalog: PunchinItemFromCatalog;
    Extrinsic: Extrinsic;
    IsShippingServiceItem: IsShippingServiceItem;
    IsSpecialHandlingServiceItem: IsSpecialHandlingServiceItem;
    isIncluded: boolean;
    isFullyInvoiced: boolean;
    isLineFromPO: IsLineFromPO;
    parentPOLineNumber: ParentPOLineNumber;
    extLineNumber: ExtLineNumber;
    parentExtLineNumber: ParentExtLineNumber;
}
interface InvoiceDetailItemReference {
    lineNumber: number;
    adhocLineNumber: number;
    serialNumber: string;
    lineCopyTax: string;
    ItemID: ItemID;
    Description: Description;
    Classification: Classification;
    ManufacturerPartID: string;
    ManufacturerName: ManufacturerName;
    Country: Country;
    SerialNumber: string;
    InvoiceDetailItemReferenceIndustry: InvoiceDetailItemReferenceIndustry;
}
interface ItemID {
    SupplierPartID: string;
    SupplierPartAuxiliaryID: string;
    OldSupplierPartID: string;
    OldSupplierPartAuxiliaryID: string;
    BuyerPartID: string;
    OldBuyerPartID: string;
}
interface ManufacturerName {
    'xml:lang': string;
    content: string;
}
interface InvoiceDetailItemReferenceIndustry {
    InvoiceDetailItemReferenceRetail: InvoiceDetailItemReferenceRetail;
}
interface InvoiceDetailItemReferenceRetail {
    EANID: string;
    EuropeanWasteCatalogID: string;
    Characteristic: Characteristic;
    color: Color;
    colorCode: ColorCode;
    size: Size;
    sizeCode: SizeCode;
    quality: Quality;
    qualityCode: QualityCode;
    grade: Grade;
    gradeCode: GradeCode;
}
interface Characteristic {
    domain: string;
    value: string;
    code: string;
}
interface Color {
    domain: string;
    value: string;
    code: string;
}
interface ColorCode {
    domain: string;
    value: string;
    code: string;
}
interface Size {
    domain: string;
    value: string;
    code: string;
}
interface SizeCode {
    domain: string;
    value: string;
    code: string;
}
interface Quality {
    domain: string;
    value: string;
    code: string;
}
interface QualityCode {
    domain: string;
    value: string;
    code: string;
}
interface Grade {
    domain: string;
    value: string;
    code: string;
}
interface GradeCode {
    domain: string;
    value: string;
    code: string;
}
interface InvoiceDetailServiceItemReference {
    lineNumber: number;
    adhocLineNumber: number;
    Classification: Classification;
    ItemID: ItemID;
    Description: Description;
}
interface ServiceEntryItemIDInfo {
    serviceLineNumber: number;
    serviceEntryID: string;
    serviceEntryDate: string;
    IdReference: IdReference;
}
interface SubtotalAmount {
    Money: Money;
}
interface UnitRate {
    Money: Money;
    UnitOfMeasure: string;
    PriceBasisQuantity: PriceBasisQuantity;
    TermReference: TermReference;
}
interface PriceBasisQuantity {
    quantity: number;
    oldquantity: number;
    conversionFactor: number;
    oldconversionFactor: number;
    UnitOfMeasure: string;
    OldUnitOfMeasure: string;
    Description: Description;
}
interface TermReference {
    termName: string;
    term: string;
}
interface Tax {
    Money: Money;
    Description: Description;
    TaxDetail: TaxDetail;
    OldMoney: OldMoney;
    DeletedMoney: DeletedMoney;
    taxationType: TaxationType;
    isISSRetention: IsISSRetention;
    isServiceBrokerISSRetention: IsServiceBrokerISSRetention;
    issRetentionValue: IssRetentionValue;
    Extrinsic: Extrinsic;
    withholdingTaxTotal: WithholdingTaxTotal;
    taxTotal: TaxTotal;
}
interface TaxDetail {
    purpose: string;
    category: string;
    percentageRate: number;
    taxRateType: string;
    isVatRecoverable: string;
    isWithholdingTax: string;
    taxPointDate: string;
    paymentDate: string;
    isTriangularTransaction: string;
    exemptDetail: string;
    TaxableAmount: TaxableAmount;
    TaxAmount: TaxAmount;
    TaxLocation: TaxLocation;
    Description: Description;
    TriangularTransactionLawReference: TriangularTransactionLawReference;
    TaxRegime: string;
    TaxExemption: TaxExemption;
    withholdingTaxType: WithholdingTaxType;
    taxPointDateCode: TaxPointDateCode;
    Extrinsic: Extrinsic;
}
interface TaxableAmount {
    Money: Money;
}
interface TaxAmount {
    Money: Money;
}
interface TaxLocation {
    'xml:lang': string;
    content: string;
}
interface TriangularTransactionLawReference {
    'xml:lang': string;
    content: string;
}
interface TaxExemption {
    exemptCode: string;
    ExemptReason: ExemptReason;
}
interface ExemptReason {
    'xml:lang': string;
    content: string;
}
interface WithholdingTaxType {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface TaxPointDateCode {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface OldMoney {
    currency: string;
    alternateAmount: number;
    alternateCurrency: string;
    content: number;
}
interface DeletedMoney {
    currency: string;
    alternateAmount: number;
    alternateCurrency: string;
    content: number;
}
interface TaxationType {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface IsISSRetention {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface IsServiceBrokerISSRetention {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface IssRetentionValue {
    name: string;
    schemaMappedType: string;
    Money: Money;
}
interface WithholdingTaxTotal {
    name: string;
    schemaMappedType: string;
    Money: Money;
}
interface TaxTotal {
    name: string;
    schemaMappedType: string;
    Money: Money;
}
interface POTax {
    Money: Money;
    Description: Description;
    TaxDetail: TaxDetail;
    OldMoney: OldMoney;
    DeletedMoney: DeletedMoney;
    taxationType: TaxationType;
    isISSRetention: IsISSRetention;
    isServiceBrokerISSRetention: IsServiceBrokerISSRetention;
    issRetentionValue: IssRetentionValue;
    Extrinsic: Extrinsic;
    withholdingTaxTotal: WithholdingTaxTotal;
    taxTotal: TaxTotal;
}
interface RelatedInvoiceTax {
    Money: Money;
    Description: Description;
    TaxDetail: TaxDetail;
    OldMoney: OldMoney;
    DeletedMoney: DeletedMoney;
    taxationType: TaxationType;
    isISSRetention: IsISSRetention;
    isServiceBrokerISSRetention: IsServiceBrokerISSRetention;
    issRetentionValue: IssRetentionValue;
    Extrinsic: Extrinsic;
    withholdingTaxTotal: WithholdingTaxTotal;
    taxTotal: TaxTotal;
}
interface InvoiceDetailLineSpecialHandling {
    Description: Description;
    Money: Money;
}
interface InvoiceDetailLineShipping {
    InvoiceDetailShipping: InvoiceDetailShipping;
    Money: Money;
}
interface GrossAmount {
    Money: Money;
}
interface InvoiceDetailDiscount {
    percentageRate: number;
    Money: Money;
}
interface InvoiceItemModifications {
    Modification: Modification;
}
interface Modification {
    level: number;
    OriginalPrice: OriginalPrice;
    AdditionalCost: AdditionalCost;
    Tax: Tax;
    ModificationDetail: ModificationDetail;
}
interface OriginalPrice {
    Money: Money;
}
interface AdditionalCost {
    Percentage?: Percentage;
    Money?: Money;
}
interface Percentage {
    percent: number;
}
interface ModificationDetail {
    name: string;
    code: string;
    startDate: string;
    endDate: string;
    Description: Description;
    Extrinsic: Extrinsic;
}
interface TotalCharges {
    Money: Money;
}
interface TotalAllowances {
    Money: Money;
}
interface TotalAmountWithoutTax {
    Money: Money;
}
interface TotalTaxOnModifications {
    Money: Money;
}
interface NetAmount {
    Money: Money;
}
interface Distribution {
    Accounting: Accounting;
    Charge: Charge;
}
interface Accounting {
    name: string;
    AccountingSegment?: AccountingSegment;
    Segment?: Segment;
}
interface AccountingSegment {
    id: string;
    Name: Name;
    Description: Description;
}
interface Charge {
    Money: Money;
}
interface Segment {
    type: string;
    id: string;
    description: string;
}
interface InvoiceLaborDetail {
    Contractor: Contractor;
    JobDescription: JobDescription;
    Supervisor: Supervisor;
    WorkLocation: WorkLocation;
    InvoiceTimeCardDetail: InvoiceTimeCardDetail;
}
interface Contractor {
    ContractorIdentifier: ContractorIdentifier;
    Contact: Contact;
}
interface ContractorIdentifier {
    domain: string;
    content: string;
}
interface JobDescription {
    Description: Description;
}
interface Supervisor {
    Contact: Contact;
}
interface WorkLocation {
    Address: Address;
}
interface Address {
    isoCountryCode: string;
    addressID: string;
    addressIDDomain: string;
    Name: Name;
    PostalAddress: PostalAddress;
    Email: Email;
    Phone: Phone;
    Fax: Fax;
    URL: URL;
}
interface InvoiceTimeCardDetail {
    TimeCardIDInfo: TimeCardIDInfo;
    TimeCardReference?: TimeCardReference;
}
interface TimeCardIDInfo {
    timeCardID: string;
}
interface PunchinItemFromCatalog {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface IsShippingServiceItem {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface IsSpecialHandlingServiceItem {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface IsLineFromPO {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface ParentPOLineNumber {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface ExtLineNumber {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface ParentExtLineNumber {
    name: string;
    schemaMappedType: string;
    content: string;
}
interface PureShippingLine {
    invoiceLineNumber: number;
    parentInvoiceLineNumber: number;
    quantity: number;
    inspectionDate: string;
    referenceDate: string;
    isAdHoc: string;
    InvoiceDetailItemReference: InvoiceDetailItemReference;
    InvoiceDetailServiceItemReference: InvoiceDetailServiceItemReference;
    ServiceEntryItemReference: ServiceEntryItemReference;
    SubtotalAmount: SubtotalAmount;
    Period: Period;
    UnitOfMeasure: string;
    UnitPrice: UnitPrice;
    OriginalUnitPrice: OriginalUnitPrice;
    PriceBasisQuantity: PriceBasisQuantity;
    Tax: Tax;
    POTax: POTax;
    RelatedInvoiceTax: RelatedInvoiceTax;
    InvoiceDetailLineSpecialHandling: InvoiceDetailLineSpecialHandling;
    InvoiceDetailLineShipping: InvoiceDetailLineShipping;
    ShipNoticeIDInfo: ShipNoticeIDInfo;
    GrossAmount: GrossAmount;
    InvoiceDetailDiscount: InvoiceDetailDiscount;
    InvoiceItemModifications: InvoiceItemModifications;
    TotalCharges: TotalCharges;
    TotalAllowances: TotalAllowances;
    TotalAmountWithoutTax: TotalAmountWithoutTax;
    TotalTaxOnModifications: TotalTaxOnModifications;
    NetAmount: NetAmount;
    distribution: Distribution;
    Distribution: Distribution;
    Comments: Comments;
    InvoiceLaborDetail: InvoiceLaborDetail;
    punchinItemFromCatalog: PunchinItemFromCatalog;
    Extrinsic: Extrinsic;
    IsShippingServiceItem: IsShippingServiceItem;
    IsSpecialHandlingServiceItem: IsSpecialHandlingServiceItem;
    isIncluded: boolean;
    isFullyInvoiced: boolean;
    isLineFromPO: IsLineFromPO;
    parentPOLineNumber: ParentPOLineNumber;
    extLineNumber: ExtLineNumber;
    parentExtLineNumber: ParentExtLineNumber;
}
interface ServiceEntryItemReference {
    serviceLineNumber: number;
    serviceEntryID: string;
    serviceEntryDate: string;
    DocumentReference: DocumentReference;
}
interface UnitPrice {
    Money: Money;
    OldMoney: OldMoney;
    Modifications: Modifications;
}
interface Modifications {
    Modification: Modification;
}
interface OriginalUnitPrice {
    Money: Money;
}
interface PureSpecialHandlingLine {
    invoiceLineNumber: number;
    parentInvoiceLineNumber: number;
    quantity: number;
    inspectionDate: string;
    referenceDate: string;
    isAdHoc: string;
    InvoiceDetailItemReference: InvoiceDetailItemReference;
    InvoiceDetailServiceItemReference: InvoiceDetailServiceItemReference;
    ServiceEntryItemIDInfo: ServiceEntryItemIDInfo;
    SubtotalAmount: SubtotalAmount;
    Period: Period;
    UnitRate: UnitRate;
    Tax: Tax;
    POTax: POTax;
    RelatedInvoiceTax: RelatedInvoiceTax;
    InvoiceDetailLineSpecialHandling: InvoiceDetailLineSpecialHandling;
    InvoiceDetailLineShipping: InvoiceDetailLineShipping;
    ShipNoticeIDInfo: ShipNoticeIDInfo;
    GrossAmount: GrossAmount;
    InvoiceDetailDiscount: InvoiceDetailDiscount;
    InvoiceItemModifications: InvoiceItemModifications;
    TotalCharges: TotalCharges;
    TotalAllowances: TotalAllowances;
    TotalAmountWithoutTax: TotalAmountWithoutTax;
    TotalTaxOnModifications: TotalTaxOnModifications;
    NetAmount: NetAmount;
    distribution: Distribution;
    Distribution: Distribution;
    Comments: Comments;
    InvoiceLaborDetail: InvoiceLaborDetail;
    punchinItemFromCatalog: PunchinItemFromCatalog;
    Extrinsic: Extrinsic;
    IsShippingServiceItem: IsShippingServiceItem;
    IsSpecialHandlingServiceItem: IsSpecialHandlingServiceItem;
    isIncluded: boolean;
    isFullyInvoiced: boolean;
    isLineFromPO: IsLineFromPO;
    parentPOLineNumber: ParentPOLineNumber;
    extLineNumber: ExtLineNumber;
    parentExtLineNumber: ParentExtLineNumber;
}
interface TimeCardReference {
    timeCardID: string;
    DocumentReference: DocumentReference;
}
interface InvoiceDetailHeaderOrder {
    InvoiceDetailOrderInfo: InvoiceDetailOrderInfo;
    InvoiceDetailOrderSummary: InvoiceDetailOrderSummary;
}
interface InvoiceDetailOrderSummary {
    invoiceLineNumber: number;
    SubtotalAmount: SubtotalAmount;
    Period: Period;
    Tax: Tax;
    InvoiceDetailLineSpecialHandling: InvoiceDetailLineSpecialHandling;
    InvoiceDetailLineShipping: InvoiceDetailLineShipping;
    GrossAmount: GrossAmount;
    InvoiceDetailDiscount: InvoiceDetailDiscount;
    NetAmount: NetAmount;
    Comments: Comments;
    Extrinsic: Extrinsic;
}
interface InvoiceDetailSummary {
    SubtotalAmount: SubtotalAmount;
    Tax: Tax;
    shippingTax: ShippingTax;
    SpecialHandlingAmount: SpecialHandlingAmount;
    ShippingAmount: ShippingAmount;
    GrossAmount: GrossAmount;
    InvoiceDetailDiscount: InvoiceDetailDiscount;
    InvoiceHeaderModifications: InvoiceHeaderModifications;
    InvoiceDetailSummaryLineItemModifications: InvoiceDetailSummaryLineItemModifications;
    TotalCharges: TotalCharges;
    TotalAllowances: TotalAllowances;
    TotalAmountWithoutTax: TotalAmountWithoutTax;
    NetAmount: NetAmount;
    DepositAmount: DepositAmount;
    DueAmount: DueAmount;
    InvoiceDetailSummaryIndustry: InvoiceDetailSummaryIndustry;
}
interface ShippingTax {
    Money: Money;
    Description: Description;
    TaxDetail: TaxDetail;
    OldMoney: OldMoney;
    DeletedMoney: DeletedMoney;
    taxationType: TaxationType;
    isISSRetention: IsISSRetention;
    isServiceBrokerISSRetention: IsServiceBrokerISSRetention;
    issRetentionValue: IssRetentionValue;
    Extrinsic: Extrinsic;
    withholdingTaxTotal: WithholdingTaxTotal;
    taxTotal: TaxTotal;
}
interface SpecialHandlingAmount {
    Money: Money;
    Description: Description;
}
interface ShippingAmount {
    Money: Money;
}
interface InvoiceHeaderModifications {
    Modification: Modification;
}
interface InvoiceDetailSummaryLineItemModifications {
    Modification: Modification;
}
interface DepositAmount {
    Money: Money;
}
interface DueAmount {
    Money: Money;
}
interface InvoiceDetailSummaryIndustry {
    InvoiceDetailSummaryRetail: InvoiceDetailSummaryRetail;
}
interface InvoiceDetailSummaryRetail {
    AdditionalAmounts: AdditionalAmounts;
}
interface AdditionalAmounts {
    TotalRetailAmount: TotalRetailAmount;
    InformationalAmount: InformationalAmount;
    GrossProgressPaymentAmount: GrossProgressPaymentAmount;
    TotalReturnableItemsDepositAmount: TotalReturnableItemsDepositAmount;
    GoodsAndServiceAmount: GoodsAndServiceAmount;
    ExactAmount: ExactAmount;
}
interface TotalRetailAmount {
    Money: Money;
}
interface InformationalAmount {
    Money: Money;
}
interface GrossProgressPaymentAmount {
    Money: Money;
}
interface TotalReturnableItemsDepositAmount {
    Money: Money;
}
interface GoodsAndServiceAmount {
    Money: Money;
}
interface ExactAmount {
    Money: Money;
}
