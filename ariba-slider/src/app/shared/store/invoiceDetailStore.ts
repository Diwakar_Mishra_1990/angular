export const invoiceDetailRequest = 
{
    "InvoiceDetailRequest": {
        "xmlns:cxml2": "http://www.cxml.org/ns/cXML/cXML2",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "xsi:schemaLocation": "http://www.cxml.org/ns/cXML/cXML2 file:/Users/i306174/Documents/takeitfromhere/flatten/Invoice.xsd",
        "InvoiceDetailRequestHeader": {
            "invoiceID": "invoiceID0",
            "isInformationOnly": "yes",
            "purpose": "standard",
            "operation": "new",
            "invoiceDate": "2006-05-04T18:13:51.0",
            "invoiceOrigin": "supplier",
            "InvoiceDetailHeaderIndicator": {
                "isHeaderInvoice": "yes",
                "isVatRecoverable": "yes",
                "isPriceBasedLineLevelCreditMemo": "yes"
            },
            "InvoiceDetailLineIndicator": {
                "isTaxInLine": "yes",
                "isSpecialHandlingInLine": "yes",
                "isShippingInLine": "yes",
                "isDiscountInLine": "yes",
                "isAccountingInLine": "yes",
                "isPriceAdjustmentInLine": "yes"
            },
            "InvoicePartner": {
                "Contact": {
                    "role": "role0",
                    "addressID": "addressID0",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName0",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name0"
                    },
                    "PostalAddress": {
                        "name": "name0",
                        "DeliverTo": "DeliverTo0",
                        "Street": "Street0",
                        "City": {
                            "cityCode": "cityCode0",
                            "content": "City0"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode0",
                            "content": "Municipality0"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode0",
                            "content": "State0"
                        },
                        "PostalCode": "PostalCode0",
                        "Country": {
                            "isoCountryCode": "isoCountryCode0",
                            "content": "Country0"
                        }
                    },
                    "Email": {
                        "name": "name1",
                        "content": "Email0"
                    },
                    "Phone": {
                        "name": "name2",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode1",
                                "content": "CountryCode0"
                            },
                            "AreaOrCityCode": "AreaOrCityCode0",
                            "Number": "Number0",
                            "Extension": "Extension0"
                        }
                    },
                    "Fax": {
                        "name": "name3",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode2",
                                "content": "CountryCode1"
                            },
                            "AreaOrCityCode": "AreaOrCityCode1",
                            "Number": "Number1",
                            "Extension": "Extension1"
                        }
                    },
                    "URL": {
                        "name": "name4",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role1",
                        "addressID": "addressID1",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name1"
                        },
                        "PostalAddress": {
                            "name": "name5",
                            "DeliverTo": "DeliverTo1",
                            "Street": "Street1",
                            "City": {
                                "cityCode": "cityCode1",
                                "content": "City1"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode1",
                                "content": "Municipality1"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode1",
                                "content": "State1"
                            },
                            "PostalCode": "PostalCode1",
                            "Country": {
                                "isoCountryCode": "isoCountryCode3",
                                "content": "Country1"
                            }
                        },
                        "Email": {
                            "name": "name6",
                            "content": "Email1"
                        },
                        "Phone": {
                            "name": "name7",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode4",
                                    "content": "CountryCode2"
                                },
                                "AreaOrCityCode": "AreaOrCityCode2",
                                "Number": "Number2",
                                "Extension": "Extension2"
                            }
                        },
                        "Fax": {
                            "name": "name8",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode5",
                                    "content": "CountryCode3"
                                },
                                "AreaOrCityCode": "AreaOrCityCode3",
                                "Number": "Number3",
                                "Extension": "Extension3"
                            }
                        },
                        "URL": {
                            "name": "name9",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier0",
                        "domain": "domain0",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator0"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type0",
                            "ShortName": "ShortName0"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier1",
                        "domain": "domain1",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator1"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type1",
                            "ShortName": "ShortName1"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier2",
                        "domain": "domain2",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator2"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type2",
                            "ShortName": "ShortName2"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier3",
                        "domain": "domain3",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator3"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type3",
                            "ShortName": "ShortName3"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier4",
                        "domain": "domain4",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator4"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type4",
                            "ShortName": "ShortName4"
                        }
                    },
                    "isFactoring": {
                        "name": "name10",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name11"},
                    "LegalCapital": {
                        "name": "name12",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency0",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency0",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name13"}
                },
                "IdReference": {
                    "identifier": "identifier5",
                    "domain": "domain5",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator5"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type5",
                        "ShortName": "ShortName5"
                    }
                }
            },
            "remitTo": {
                "Contact": {
                    "role": "role2",
                    "addressID": "addressID2",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName1",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name2"
                    },
                    "PostalAddress": {
                        "name": "name14",
                        "DeliverTo": "DeliverTo2",
                        "Street": "Street2",
                        "City": {
                            "cityCode": "cityCode2",
                            "content": "City2"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode2",
                            "content": "Municipality2"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode2",
                            "content": "State2"
                        },
                        "PostalCode": "PostalCode2",
                        "Country": {
                            "isoCountryCode": "isoCountryCode6",
                            "content": "Country2"
                        }
                    },
                    "Email": {
                        "name": "name15",
                        "content": "Email2"
                    },
                    "Phone": {
                        "name": "name16",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode7",
                                "content": "CountryCode4"
                            },
                            "AreaOrCityCode": "AreaOrCityCode4",
                            "Number": "Number4",
                            "Extension": "Extension4"
                        }
                    },
                    "Fax": {
                        "name": "name17",
                        "URL": {
                            "name": "name18",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "URL": {
                        "name": "name19",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role3",
                        "addressID": "addressID3",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name3"
                        },
                        "PostalAddress": {
                            "name": "name20",
                            "DeliverTo": "DeliverTo3",
                            "Street": "Street3",
                            "City": {
                                "cityCode": "cityCode3",
                                "content": "City3"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode3",
                                "content": "Municipality3"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode3",
                                "content": "State3"
                            },
                            "PostalCode": "PostalCode3",
                            "Country": {
                                "isoCountryCode": "isoCountryCode8",
                                "content": "Country3"
                            }
                        },
                        "Email": {
                            "name": "name21",
                            "content": "Email3"
                        },
                        "Phone": {
                            "name": "name22",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode9",
                                    "content": "CountryCode5"
                                },
                                "AreaOrCityCode": "AreaOrCityCode5",
                                "Number": "Number5",
                                "Extension": "Extension5"
                            }
                        },
                        "Fax": {
                            "name": "name23",
                            "Email": {
                                "name": "name24",
                                "content": "Email4"
                            }
                        },
                        "URL": {
                            "name": "name25",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier6",
                        "domain": "domain6",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator6"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type6",
                            "ShortName": "ShortName6"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier7",
                        "domain": "domain7",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator7"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type7",
                            "ShortName": "ShortName7"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier8",
                        "domain": "domain8",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator8"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type8",
                            "ShortName": "ShortName8"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier9",
                        "domain": "domain9",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator9"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type9",
                            "ShortName": "ShortName9"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier10",
                        "domain": "domain10",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator10"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type10",
                            "ShortName": "ShortName10"
                        }
                    },
                    "isFactoring": {
                        "name": "name26",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name27"},
                    "LegalCapital": {
                        "name": "name28",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency1",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency1",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name29"}
                },
                "IdReference": {
                    "identifier": "identifier11",
                    "domain": "domain11",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator11"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type11",
                        "ShortName": "ShortName11"
                    }
                },
                "accountId": {
                    "identifier": "identifier12",
                    "domain": "domain12",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator12"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type12",
                        "ShortName": "ShortName12"
                    }
                },
                "supplierTaxID": {
                    "identifier": "identifier13",
                    "domain": "domain13",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator13"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type13",
                        "ShortName": "ShortName13"
                    }
                },
                "accountReceivableID": {
                    "identifier": "identifier14",
                    "domain": "domain14",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator14"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type14",
                        "ShortName": "ShortName14"
                    }
                }
            },
            "billTo": {
                "Contact": {
                    "role": "role4",
                    "addressID": "addressID4",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName2",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name4"
                    },
                    "PostalAddress": {
                        "name": "name30",
                        "DeliverTo": "DeliverTo4",
                        "Street": "Street4",
                        "City": {
                            "cityCode": "cityCode4",
                            "content": "City4"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode4",
                            "content": "Municipality4"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode4",
                            "content": "State4"
                        },
                        "PostalCode": "PostalCode4",
                        "Country": {
                            "isoCountryCode": "isoCountryCode10",
                            "content": "Country4"
                        }
                    },
                    "Email": {
                        "name": "name31",
                        "content": "Email5"
                    },
                    "Phone": {
                        "name": "name32",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode11",
                                "content": "CountryCode6"
                            },
                            "AreaOrCityCode": "AreaOrCityCode6",
                            "Number": "Number6",
                            "Extension": "Extension6"
                        }
                    },
                    "Fax": {
                        "name": "name33",
                        "Email": {
                            "name": "name34",
                            "content": "Email6"
                        }
                    },
                    "URL": {
                        "name": "name35",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role5",
                        "addressID": "addressID5",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name5"
                        },
                        "PostalAddress": {
                            "name": "name36",
                            "DeliverTo": "DeliverTo5",
                            "Street": "Street5",
                            "City": {
                                "cityCode": "cityCode5",
                                "content": "City5"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode5",
                                "content": "Municipality5"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode5",
                                "content": "State5"
                            },
                            "PostalCode": "PostalCode5",
                            "Country": {
                                "isoCountryCode": "isoCountryCode12",
                                "content": "Country5"
                            }
                        },
                        "Email": {
                            "name": "name37",
                            "content": "Email7"
                        },
                        "Phone": {
                            "name": "name38",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode13",
                                    "content": "CountryCode7"
                                },
                                "AreaOrCityCode": "AreaOrCityCode7",
                                "Number": "Number7",
                                "Extension": "Extension7"
                            }
                        },
                        "Fax": {
                            "name": "name39",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode14",
                                    "content": "CountryCode8"
                                },
                                "AreaOrCityCode": "AreaOrCityCode8",
                                "Number": "Number8",
                                "Extension": "Extension8"
                            }
                        },
                        "URL": {
                            "name": "name40",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier15",
                        "domain": "domain15",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator15"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type15",
                            "ShortName": "ShortName15"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier16",
                        "domain": "domain16",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator16"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type16",
                            "ShortName": "ShortName16"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier17",
                        "domain": "domain17",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator17"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type17",
                            "ShortName": "ShortName17"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier18",
                        "domain": "domain18",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator18"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type18",
                            "ShortName": "ShortName18"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier19",
                        "domain": "domain19",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator19"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type19",
                            "ShortName": "ShortName19"
                        }
                    },
                    "isFactoring": {
                        "name": "name41",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name42"},
                    "LegalCapital": {
                        "name": "name43",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency2",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency2",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name44"}
                },
                "IdReference": {
                    "identifier": "identifier20",
                    "domain": "domain20",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator20"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type20",
                        "ShortName": "ShortName20"
                    }
                }
            },
            "from": {
                "Contact": {
                    "role": "role6",
                    "addressID": "addressID6",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName3",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name6"
                    },
                    "PostalAddress": {
                        "name": "name45",
                        "DeliverTo": "DeliverTo6",
                        "Street": "Street6",
                        "City": {
                            "cityCode": "cityCode6",
                            "content": "City6"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode6",
                            "content": "Municipality6"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode6",
                            "content": "State6"
                        },
                        "PostalCode": "PostalCode6",
                        "Country": {
                            "isoCountryCode": "isoCountryCode15",
                            "content": "Country6"
                        }
                    },
                    "Email": {
                        "name": "name46",
                        "content": "Email8"
                    },
                    "Phone": {
                        "name": "name47",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode16",
                                "content": "CountryCode9"
                            },
                            "AreaOrCityCode": "AreaOrCityCode9",
                            "Number": "Number9",
                            "Extension": "Extension9"
                        }
                    },
                    "Fax": {
                        "name": "name48",
                        "URL": {
                            "name": "name49",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "URL": {
                        "name": "name50",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role7",
                        "addressID": "addressID7",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name7"
                        },
                        "PostalAddress": {
                            "name": "name51",
                            "DeliverTo": "DeliverTo7",
                            "Street": "Street7",
                            "City": {
                                "cityCode": "cityCode7",
                                "content": "City7"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode7",
                                "content": "Municipality7"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode7",
                                "content": "State7"
                            },
                            "PostalCode": "PostalCode7",
                            "Country": {
                                "isoCountryCode": "isoCountryCode17",
                                "content": "Country7"
                            }
                        },
                        "Email": {
                            "name": "name52",
                            "content": "Email9"
                        },
                        "Phone": {
                            "name": "name53",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode18",
                                    "content": "CountryCode10"
                                },
                                "AreaOrCityCode": "AreaOrCityCode10",
                                "Number": "Number10",
                                "Extension": "Extension10"
                            }
                        },
                        "Fax": {
                            "name": "name54",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode19",
                                    "content": "CountryCode11"
                                },
                                "AreaOrCityCode": "AreaOrCityCode11",
                                "Number": "Number11",
                                "Extension": "Extension11"
                            }
                        },
                        "URL": {
                            "name": "name55",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier21",
                        "domain": "domain21",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator21"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type21",
                            "ShortName": "ShortName21"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier22",
                        "domain": "domain22",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator22"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type22",
                            "ShortName": "ShortName22"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier23",
                        "domain": "domain23",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator23"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type23",
                            "ShortName": "ShortName23"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier24",
                        "domain": "domain24",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator24"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type24",
                            "ShortName": "ShortName24"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier25",
                        "domain": "domain25",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator25"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type25",
                            "ShortName": "ShortName25"
                        }
                    },
                    "isFactoring": {
                        "name": "name56",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name57"},
                    "LegalCapital": {
                        "name": "name58",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency3",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency3",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name59"}
                },
                "IdReference": {
                    "identifier": "identifier26",
                    "domain": "domain26",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator26"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type26",
                        "ShortName": "ShortName26"
                    }
                },
                "provincialTaxID": {
                    "identifier": "identifier27",
                    "domain": "domain27",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator27"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type27",
                        "ShortName": "ShortName27"
                    }
                },
                "workRegistrationID": {
                    "identifier": "identifier28",
                    "domain": "domain28",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator28"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type28",
                        "ShortName": "ShortName28"
                    }
                }
            },
            "billFrom": {
                "Contact": {
                    "role": "role8",
                    "addressID": "addressID8",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName4",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name8"
                    },
                    "PostalAddress": {
                        "name": "name60",
                        "DeliverTo": "DeliverTo8",
                        "Street": "Street8",
                        "City": {
                            "cityCode": "cityCode8",
                            "content": "City8"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode8",
                            "content": "Municipality8"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode8",
                            "content": "State8"
                        },
                        "PostalCode": "PostalCode8",
                        "Country": {
                            "isoCountryCode": "isoCountryCode20",
                            "content": "Country8"
                        }
                    },
                    "Email": {
                        "name": "name61",
                        "content": "Email10"
                    },
                    "Phone": {
                        "name": "name62",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode21",
                                "content": "CountryCode12"
                            },
                            "AreaOrCityCode": "AreaOrCityCode12",
                            "Number": "Number12",
                            "Extension": "Extension12"
                        }
                    },
                    "Fax": {
                        "name": "name63",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode22",
                                "content": "CountryCode13"
                            },
                            "AreaOrCityCode": "AreaOrCityCode13",
                            "Number": "Number13",
                            "Extension": "Extension13"
                        }
                    },
                    "URL": {
                        "name": "name64",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role9",
                        "addressID": "addressID9",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name9"
                        },
                        "PostalAddress": {
                            "name": "name65",
                            "DeliverTo": "DeliverTo9",
                            "Street": "Street9",
                            "City": {
                                "cityCode": "cityCode9",
                                "content": "City9"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode9",
                                "content": "Municipality9"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode9",
                                "content": "State9"
                            },
                            "PostalCode": "PostalCode9",
                            "Country": {
                                "isoCountryCode": "isoCountryCode23",
                                "content": "Country9"
                            }
                        },
                        "Email": {
                            "name": "name66",
                            "content": "Email11"
                        },
                        "Phone": {
                            "name": "name67",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode24",
                                    "content": "CountryCode14"
                                },
                                "AreaOrCityCode": "AreaOrCityCode14",
                                "Number": "Number14",
                                "Extension": "Extension14"
                            }
                        },
                        "Fax": {
                            "name": "name68",
                            "URL": {
                                "name": "name69",
                                "content": "http://www.oxygenxml.com/"
                            }
                        },
                        "URL": {
                            "name": "name70",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier29",
                        "domain": "domain29",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator29"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type29",
                            "ShortName": "ShortName29"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier30",
                        "domain": "domain30",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator30"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type30",
                            "ShortName": "ShortName30"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier31",
                        "domain": "domain31",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator31"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type31",
                            "ShortName": "ShortName31"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier32",
                        "domain": "domain32",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator32"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type32",
                            "ShortName": "ShortName32"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier33",
                        "domain": "domain33",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator33"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type33",
                            "ShortName": "ShortName33"
                        }
                    },
                    "isFactoring": {
                        "name": "name71",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name72"},
                    "LegalCapital": {
                        "name": "name73",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency4",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency4",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name74"}
                },
                "IdReference": {
                    "identifier": "identifier34",
                    "domain": "domain34",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator34"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type34",
                        "ShortName": "ShortName34"
                    }
                }
            },
            "requester": {
                "Contact": {
                    "role": "role10",
                    "addressID": "addressID10",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName5",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name10"
                    },
                    "PostalAddress": {
                        "name": "name75",
                        "DeliverTo": "DeliverTo10",
                        "Street": "Street10",
                        "City": {
                            "cityCode": "cityCode10",
                            "content": "City10"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode10",
                            "content": "Municipality10"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode10",
                            "content": "State10"
                        },
                        "PostalCode": "PostalCode10",
                        "Country": {
                            "isoCountryCode": "isoCountryCode25",
                            "content": "Country10"
                        }
                    },
                    "Email": {
                        "name": "name76",
                        "content": "Email12"
                    },
                    "Phone": {
                        "name": "name77",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode26",
                                "content": "CountryCode15"
                            },
                            "AreaOrCityCode": "AreaOrCityCode15",
                            "Number": "Number15",
                            "Extension": "Extension15"
                        }
                    },
                    "Fax": {
                        "name": "name78",
                        "URL": {
                            "name": "name79",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "URL": {
                        "name": "name80",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role11",
                        "addressID": "addressID11",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name11"
                        },
                        "PostalAddress": {
                            "name": "name81",
                            "DeliverTo": "DeliverTo11",
                            "Street": "Street11",
                            "City": {
                                "cityCode": "cityCode11",
                                "content": "City11"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode11",
                                "content": "Municipality11"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode11",
                                "content": "State11"
                            },
                            "PostalCode": "PostalCode11",
                            "Country": {
                                "isoCountryCode": "isoCountryCode27",
                                "content": "Country11"
                            }
                        },
                        "Email": {
                            "name": "name82",
                            "content": "Email13"
                        },
                        "Phone": {
                            "name": "name83",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode28",
                                    "content": "CountryCode16"
                                },
                                "AreaOrCityCode": "AreaOrCityCode16",
                                "Number": "Number16",
                                "Extension": "Extension16"
                            }
                        },
                        "Fax": {
                            "name": "name84",
                            "Email": {
                                "name": "name85",
                                "content": "Email14"
                            }
                        },
                        "URL": {
                            "name": "name86",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier35",
                        "domain": "domain35",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator35"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type35",
                            "ShortName": "ShortName35"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier36",
                        "domain": "domain36",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator36"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type36",
                            "ShortName": "ShortName36"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier37",
                        "domain": "domain37",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator37"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type37",
                            "ShortName": "ShortName37"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier38",
                        "domain": "domain38",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator38"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type38",
                            "ShortName": "ShortName38"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier39",
                        "domain": "domain39",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator39"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type39",
                            "ShortName": "ShortName39"
                        }
                    },
                    "isFactoring": {
                        "name": "name87",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name88"},
                    "LegalCapital": {
                        "name": "name89",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency5",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency5",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name90"}
                },
                "IdReference": {
                    "identifier": "identifier40",
                    "domain": "domain40",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator40"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type40",
                        "ShortName": "ShortName40"
                    }
                }
            },
            "fieldEngineer": {
                "Contact": {
                    "role": "role12",
                    "addressID": "addressID12",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName6",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name12"
                    },
                    "PostalAddress": {
                        "name": "name91",
                        "DeliverTo": "DeliverTo12",
                        "Street": "Street12",
                        "City": {
                            "cityCode": "cityCode12",
                            "content": "City12"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode12",
                            "content": "Municipality12"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode12",
                            "content": "State12"
                        },
                        "PostalCode": "PostalCode12",
                        "Country": {
                            "isoCountryCode": "isoCountryCode29",
                            "content": "Country12"
                        }
                    },
                    "Email": {
                        "name": "name92",
                        "content": "Email15"
                    },
                    "Phone": {
                        "name": "name93",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode30",
                                "content": "CountryCode17"
                            },
                            "AreaOrCityCode": "AreaOrCityCode17",
                            "Number": "Number17",
                            "Extension": "Extension17"
                        }
                    },
                    "Fax": {
                        "name": "name94",
                        "Email": {
                            "name": "name95",
                            "content": "Email16"
                        }
                    },
                    "URL": {
                        "name": "name96",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role13",
                        "addressID": "addressID13",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name13"
                        },
                        "PostalAddress": {
                            "name": "name97",
                            "DeliverTo": "DeliverTo13",
                            "Street": "Street13",
                            "City": {
                                "cityCode": "cityCode13",
                                "content": "City13"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode13",
                                "content": "Municipality13"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode13",
                                "content": "State13"
                            },
                            "PostalCode": "PostalCode13",
                            "Country": {
                                "isoCountryCode": "isoCountryCode31",
                                "content": "Country13"
                            }
                        },
                        "Email": {
                            "name": "name98",
                            "content": "Email17"
                        },
                        "Phone": {
                            "name": "name99",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode32",
                                    "content": "CountryCode18"
                                },
                                "AreaOrCityCode": "AreaOrCityCode18",
                                "Number": "Number18",
                                "Extension": "Extension18"
                            }
                        },
                        "Fax": {
                            "name": "name100",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode33",
                                    "content": "CountryCode19"
                                },
                                "AreaOrCityCode": "AreaOrCityCode19",
                                "Number": "Number19",
                                "Extension": "Extension19"
                            }
                        },
                        "URL": {
                            "name": "name101",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier41",
                        "domain": "domain41",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator41"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type41",
                            "ShortName": "ShortName41"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier42",
                        "domain": "domain42",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator42"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type42",
                            "ShortName": "ShortName42"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier43",
                        "domain": "domain43",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator43"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type43",
                            "ShortName": "ShortName43"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier44",
                        "domain": "domain44",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator44"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type44",
                            "ShortName": "ShortName44"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier45",
                        "domain": "domain45",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator45"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type45",
                            "ShortName": "ShortName45"
                        }
                    },
                    "isFactoring": {
                        "name": "name102",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name103"},
                    "LegalCapital": {
                        "name": "name104",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency6",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency6",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name105"}
                },
                "IdReference": {
                    "identifier": "identifier46",
                    "domain": "domain46",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator46"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type46",
                        "ShortName": "ShortName46"
                    }
                }
            },
            "fieldContractor": {
                "Contact": {
                    "role": "role14",
                    "addressID": "addressID14",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName7",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name14"
                    },
                    "PostalAddress": {
                        "name": "name106",
                        "DeliverTo": "DeliverTo14",
                        "Street": "Street14",
                        "City": {
                            "cityCode": "cityCode14",
                            "content": "City14"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode14",
                            "content": "Municipality14"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode14",
                            "content": "State14"
                        },
                        "PostalCode": "PostalCode14",
                        "Country": {
                            "isoCountryCode": "isoCountryCode34",
                            "content": "Country14"
                        }
                    },
                    "Email": {
                        "name": "name107",
                        "content": "Email18"
                    },
                    "Phone": {
                        "name": "name108",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode35",
                                "content": "CountryCode20"
                            },
                            "AreaOrCityCode": "AreaOrCityCode20",
                            "Number": "Number20",
                            "Extension": "Extension20"
                        }
                    },
                    "Fax": {
                        "name": "name109",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode36",
                                "content": "CountryCode21"
                            },
                            "AreaOrCityCode": "AreaOrCityCode21",
                            "Number": "Number21",
                            "Extension": "Extension21"
                        }
                    },
                    "URL": {
                        "name": "name110",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role15",
                        "addressID": "addressID15",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name15"
                        },
                        "PostalAddress": {
                            "name": "name111",
                            "DeliverTo": "DeliverTo15",
                            "Street": "Street15",
                            "City": {
                                "cityCode": "cityCode15",
                                "content": "City15"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode15",
                                "content": "Municipality15"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode15",
                                "content": "State15"
                            },
                            "PostalCode": "PostalCode15",
                            "Country": {
                                "isoCountryCode": "isoCountryCode37",
                                "content": "Country15"
                            }
                        },
                        "Email": {
                            "name": "name112",
                            "content": "Email19"
                        },
                        "Phone": {
                            "name": "name113",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode38",
                                    "content": "CountryCode22"
                                },
                                "AreaOrCityCode": "AreaOrCityCode22",
                                "Number": "Number22",
                                "Extension": "Extension22"
                            }
                        },
                        "Fax": {
                            "name": "name114",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode39",
                                    "content": "CountryCode23"
                                },
                                "AreaOrCityCode": "AreaOrCityCode23",
                                "Number": "Number23",
                                "Extension": "Extension23"
                            }
                        },
                        "URL": {
                            "name": "name115",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier47",
                        "domain": "domain47",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator47"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type47",
                            "ShortName": "ShortName47"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier48",
                        "domain": "domain48",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator48"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type48",
                            "ShortName": "ShortName48"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier49",
                        "domain": "domain49",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator49"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type49",
                            "ShortName": "ShortName49"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier50",
                        "domain": "domain50",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator50"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type50",
                            "ShortName": "ShortName50"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier51",
                        "domain": "domain51",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator51"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type51",
                            "ShortName": "ShortName51"
                        }
                    },
                    "isFactoring": {
                        "name": "name116",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name117"},
                    "LegalCapital": {
                        "name": "name118",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency7",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency7",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name119"}
                },
                "IdReference": {
                    "identifier": "identifier52",
                    "domain": "domain52",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator52"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type52",
                        "ShortName": "ShortName52"
                    }
                }
            },
            "soldTo": {
                "Contact": {
                    "role": "role16",
                    "addressID": "addressID16",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName8",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name16"
                    },
                    "PostalAddress": {
                        "name": "name120",
                        "DeliverTo": "DeliverTo16",
                        "Street": "Street16",
                        "City": {
                            "cityCode": "cityCode16",
                            "content": "City16"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode16",
                            "content": "Municipality16"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode16",
                            "content": "State16"
                        },
                        "PostalCode": "PostalCode16",
                        "Country": {
                            "isoCountryCode": "isoCountryCode40",
                            "content": "Country16"
                        }
                    },
                    "Email": {
                        "name": "name121",
                        "content": "Email20"
                    },
                    "Phone": {
                        "name": "name122",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode41",
                                "content": "CountryCode24"
                            },
                            "AreaOrCityCode": "AreaOrCityCode24",
                            "Number": "Number24",
                            "Extension": "Extension24"
                        }
                    },
                    "Fax": {
                        "name": "name123",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode42",
                                "content": "CountryCode25"
                            },
                            "AreaOrCityCode": "AreaOrCityCode25",
                            "Number": "Number25",
                            "Extension": "Extension25"
                        }
                    },
                    "URL": {
                        "name": "name124",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role17",
                        "addressID": "addressID17",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name17"
                        },
                        "PostalAddress": {
                            "name": "name125",
                            "DeliverTo": "DeliverTo17",
                            "Street": "Street17",
                            "City": {
                                "cityCode": "cityCode17",
                                "content": "City17"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode17",
                                "content": "Municipality17"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode17",
                                "content": "State17"
                            },
                            "PostalCode": "PostalCode17",
                            "Country": {
                                "isoCountryCode": "isoCountryCode43",
                                "content": "Country17"
                            }
                        },
                        "Email": {
                            "name": "name126",
                            "content": "Email21"
                        },
                        "Phone": {
                            "name": "name127",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode44",
                                    "content": "CountryCode26"
                                },
                                "AreaOrCityCode": "AreaOrCityCode26",
                                "Number": "Number26",
                                "Extension": "Extension26"
                            }
                        },
                        "Fax": {
                            "name": "name128",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode45",
                                    "content": "CountryCode27"
                                },
                                "AreaOrCityCode": "AreaOrCityCode27",
                                "Number": "Number27",
                                "Extension": "Extension27"
                            }
                        },
                        "URL": {
                            "name": "name129",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier53",
                        "domain": "domain53",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator53"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type53",
                            "ShortName": "ShortName53"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier54",
                        "domain": "domain54",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator54"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type54",
                            "ShortName": "ShortName54"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier55",
                        "domain": "domain55",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator55"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type55",
                            "ShortName": "ShortName55"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier56",
                        "domain": "domain56",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator56"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type56",
                            "ShortName": "ShortName56"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier57",
                        "domain": "domain57",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator57"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type57",
                            "ShortName": "ShortName57"
                        }
                    },
                    "isFactoring": {
                        "name": "name130",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name131"},
                    "LegalCapital": {
                        "name": "name132",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency8",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency8",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name133"}
                },
                "IdReference": {
                    "identifier": "identifier58",
                    "domain": "domain58",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator58"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type58",
                        "ShortName": "ShortName58"
                    }
                },
                "provincialTaxID": {
                    "identifier": "identifier59",
                    "domain": "domain59",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator59"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type59",
                        "ShortName": "ShortName59"
                    }
                },
                "stateTaxID": {
                    "identifier": "identifier60",
                    "domain": "domain60",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator60"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type60",
                        "ShortName": "ShortName60"
                    }
                }
            },
            "taxRepresentative": {
                "Contact": {
                    "role": "role18",
                    "addressID": "addressID18",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName9",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name18"
                    },
                    "PostalAddress": {
                        "name": "name134",
                        "DeliverTo": "DeliverTo18",
                        "Street": "Street18",
                        "City": {
                            "cityCode": "cityCode18",
                            "content": "City18"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode18",
                            "content": "Municipality18"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode18",
                            "content": "State18"
                        },
                        "PostalCode": "PostalCode18",
                        "Country": {
                            "isoCountryCode": "isoCountryCode46",
                            "content": "Country18"
                        }
                    },
                    "Email": {
                        "name": "name135",
                        "content": "Email22"
                    },
                    "Phone": {
                        "name": "name136",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode47",
                                "content": "CountryCode28"
                            },
                            "AreaOrCityCode": "AreaOrCityCode28",
                            "Number": "Number28",
                            "Extension": "Extension28"
                        }
                    },
                    "Fax": {
                        "name": "name137",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode48",
                                "content": "CountryCode29"
                            },
                            "AreaOrCityCode": "AreaOrCityCode29",
                            "Number": "Number29",
                            "Extension": "Extension29"
                        }
                    },
                    "URL": {
                        "name": "name138",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role19",
                        "addressID": "addressID19",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name19"
                        },
                        "PostalAddress": {
                            "name": "name139",
                            "DeliverTo": "DeliverTo19",
                            "Street": "Street19",
                            "City": {
                                "cityCode": "cityCode19",
                                "content": "City19"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode19",
                                "content": "Municipality19"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode19",
                                "content": "State19"
                            },
                            "PostalCode": "PostalCode19",
                            "Country": {
                                "isoCountryCode": "isoCountryCode49",
                                "content": "Country19"
                            }
                        },
                        "Email": {
                            "name": "name140",
                            "content": "Email23"
                        },
                        "Phone": {
                            "name": "name141",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode50",
                                    "content": "CountryCode30"
                                },
                                "AreaOrCityCode": "AreaOrCityCode30",
                                "Number": "Number30",
                                "Extension": "Extension30"
                            }
                        },
                        "Fax": {
                            "name": "name142",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode51",
                                    "content": "CountryCode31"
                                },
                                "AreaOrCityCode": "AreaOrCityCode31",
                                "Number": "Number31",
                                "Extension": "Extension31"
                            }
                        },
                        "URL": {
                            "name": "name143",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier61",
                        "domain": "domain61",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator61"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type61",
                            "ShortName": "ShortName61"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier62",
                        "domain": "domain62",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator62"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type62",
                            "ShortName": "ShortName62"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier63",
                        "domain": "domain63",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator63"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type63",
                            "ShortName": "ShortName63"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier64",
                        "domain": "domain64",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator64"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type64",
                            "ShortName": "ShortName64"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier65",
                        "domain": "domain65",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator65"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type65",
                            "ShortName": "ShortName65"
                        }
                    },
                    "isFactoring": {
                        "name": "name144",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name145"},
                    "LegalCapital": {
                        "name": "name146",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency9",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency9",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name147"}
                },
                "IdReference": {
                    "identifier": "identifier66",
                    "domain": "domain66",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator66"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type66",
                        "ShortName": "ShortName66"
                    }
                },
                "vatID": {
                    "identifier": "identifier67",
                    "domain": "domain67",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator67"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type67",
                        "ShortName": "ShortName67"
                    }
                }
            },
            "customerTaxRepresentative": {
                "Contact": {
                    "role": "role20",
                    "addressID": "addressID20",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName10",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name20"
                    },
                    "PostalAddress": {
                        "name": "name148",
                        "DeliverTo": "DeliverTo20",
                        "Street": "Street20",
                        "City": {
                            "cityCode": "cityCode20",
                            "content": "City20"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode20",
                            "content": "Municipality20"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode20",
                            "content": "State20"
                        },
                        "PostalCode": "PostalCode20",
                        "Country": {
                            "isoCountryCode": "isoCountryCode52",
                            "content": "Country20"
                        }
                    },
                    "Email": {
                        "name": "name149",
                        "content": "Email24"
                    },
                    "Phone": {
                        "name": "name150",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode53",
                                "content": "CountryCode32"
                            },
                            "AreaOrCityCode": "AreaOrCityCode32",
                            "Number": "Number32",
                            "Extension": "Extension32"
                        }
                    },
                    "Fax": {
                        "name": "name151",
                        "Email": {
                            "name": "name152",
                            "content": "Email25"
                        }
                    },
                    "URL": {
                        "name": "name153",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role21",
                        "addressID": "addressID21",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name21"
                        },
                        "PostalAddress": {
                            "name": "name154",
                            "DeliverTo": "DeliverTo21",
                            "Street": "Street21",
                            "City": {
                                "cityCode": "cityCode21",
                                "content": "City21"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode21",
                                "content": "Municipality21"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode21",
                                "content": "State21"
                            },
                            "PostalCode": "PostalCode21",
                            "Country": {
                                "isoCountryCode": "isoCountryCode54",
                                "content": "Country21"
                            }
                        },
                        "Email": {
                            "name": "name155",
                            "content": "Email26"
                        },
                        "Phone": {
                            "name": "name156",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode55",
                                    "content": "CountryCode33"
                                },
                                "AreaOrCityCode": "AreaOrCityCode33",
                                "Number": "Number33",
                                "Extension": "Extension33"
                            }
                        },
                        "Fax": {
                            "name": "name157",
                            "Email": {
                                "name": "name158",
                                "content": "Email27"
                            }
                        },
                        "URL": {
                            "name": "name159",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier68",
                        "domain": "domain68",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator68"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type68",
                            "ShortName": "ShortName68"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier69",
                        "domain": "domain69",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator69"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type69",
                            "ShortName": "ShortName69"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier70",
                        "domain": "domain70",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator70"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type70",
                            "ShortName": "ShortName70"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier71",
                        "domain": "domain71",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator71"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type71",
                            "ShortName": "ShortName71"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier72",
                        "domain": "domain72",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator72"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type72",
                            "ShortName": "ShortName72"
                        }
                    },
                    "isFactoring": {
                        "name": "name160",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name161"},
                    "LegalCapital": {
                        "name": "name162",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency10",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency10",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name163"}
                },
                "IdReference": {
                    "identifier": "identifier73",
                    "domain": "domain73",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator73"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type73",
                        "ShortName": "ShortName73"
                    }
                },
                "vatID": {
                    "identifier": "identifier74",
                    "domain": "domain74",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator74"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type74",
                        "ShortName": "ShortName74"
                    }
                }
            },
            "serviceBroker": {
                "Contact": {
                    "role": "role22",
                    "addressID": "addressID22",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName11",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name22"
                    },
                    "PostalAddress": {
                        "name": "name164",
                        "DeliverTo": "DeliverTo22",
                        "Street": "Street22",
                        "City": {
                            "cityCode": "cityCode22",
                            "content": "City22"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode22",
                            "content": "Municipality22"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode22",
                            "content": "State22"
                        },
                        "PostalCode": "PostalCode22",
                        "Country": {
                            "isoCountryCode": "isoCountryCode56",
                            "content": "Country22"
                        }
                    },
                    "Email": {
                        "name": "name165",
                        "content": "Email28"
                    },
                    "Phone": {
                        "name": "name166",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode57",
                                "content": "CountryCode34"
                            },
                            "AreaOrCityCode": "AreaOrCityCode34",
                            "Number": "Number34",
                            "Extension": "Extension34"
                        }
                    },
                    "Fax": {
                        "name": "name167",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode58",
                                "content": "CountryCode35"
                            },
                            "AreaOrCityCode": "AreaOrCityCode35",
                            "Number": "Number35",
                            "Extension": "Extension35"
                        }
                    },
                    "URL": {
                        "name": "name168",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role23",
                        "addressID": "addressID23",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name23"
                        },
                        "PostalAddress": {
                            "name": "name169",
                            "DeliverTo": "DeliverTo23",
                            "Street": "Street23",
                            "City": {
                                "cityCode": "cityCode23",
                                "content": "City23"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode23",
                                "content": "Municipality23"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode23",
                                "content": "State23"
                            },
                            "PostalCode": "PostalCode23",
                            "Country": {
                                "isoCountryCode": "isoCountryCode59",
                                "content": "Country23"
                            }
                        },
                        "Email": {
                            "name": "name170",
                            "content": "Email29"
                        },
                        "Phone": {
                            "name": "name171",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode60",
                                    "content": "CountryCode36"
                                },
                                "AreaOrCityCode": "AreaOrCityCode36",
                                "Number": "Number36",
                                "Extension": "Extension36"
                            }
                        },
                        "Fax": {
                            "name": "name172",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode61",
                                    "content": "CountryCode37"
                                },
                                "AreaOrCityCode": "AreaOrCityCode37",
                                "Number": "Number37",
                                "Extension": "Extension37"
                            }
                        },
                        "URL": {
                            "name": "name173",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier75",
                        "domain": "domain75",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator75"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type75",
                            "ShortName": "ShortName75"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier76",
                        "domain": "domain76",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator76"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type76",
                            "ShortName": "ShortName76"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier77",
                        "domain": "domain77",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator77"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type77",
                            "ShortName": "ShortName77"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier78",
                        "domain": "domain78",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator78"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type78",
                            "ShortName": "ShortName78"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier79",
                        "domain": "domain79",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator79"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type79",
                            "ShortName": "ShortName79"
                        }
                    },
                    "isFactoring": {
                        "name": "name174",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name175"},
                    "LegalCapital": {
                        "name": "name176",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency11",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency11",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name177"}
                },
                "IdReference": {
                    "identifier": "identifier80",
                    "domain": "domain80",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator80"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type80",
                        "ShortName": "ShortName80"
                    }
                },
                "vatID": {
                    "identifier": "identifier81",
                    "domain": "domain81",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator81"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type81",
                        "ShortName": "ShortName81"
                    }
                },
                "provincialTaxID": {
                    "identifier": "identifier82",
                    "domain": "domain82",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator82"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type82",
                        "ShortName": "ShortName82"
                    }
                }
            },
            "subsequentBuyer": {
                "Contact": {
                    "role": "role24",
                    "addressID": "addressID24",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName12",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name24"
                    },
                    "PostalAddress": {
                        "name": "name178",
                        "DeliverTo": "DeliverTo24",
                        "Street": "Street24",
                        "City": {
                            "cityCode": "cityCode24",
                            "content": "City24"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode24",
                            "content": "Municipality24"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode24",
                            "content": "State24"
                        },
                        "PostalCode": "PostalCode24",
                        "Country": {
                            "isoCountryCode": "isoCountryCode62",
                            "content": "Country24"
                        }
                    },
                    "Email": {
                        "name": "name179",
                        "content": "Email30"
                    },
                    "Phone": {
                        "name": "name180",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode63",
                                "content": "CountryCode38"
                            },
                            "AreaOrCityCode": "AreaOrCityCode38",
                            "Number": "Number38",
                            "Extension": "Extension38"
                        }
                    },
                    "Fax": {
                        "name": "name181",
                        "Email": {
                            "name": "name182",
                            "content": "Email31"
                        }
                    },
                    "URL": {
                        "name": "name183",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role25",
                        "addressID": "addressID25",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name25"
                        },
                        "PostalAddress": {
                            "name": "name184",
                            "DeliverTo": "DeliverTo25",
                            "Street": "Street25",
                            "City": {
                                "cityCode": "cityCode25",
                                "content": "City25"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode25",
                                "content": "Municipality25"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode25",
                                "content": "State25"
                            },
                            "PostalCode": "PostalCode25",
                            "Country": {
                                "isoCountryCode": "isoCountryCode64",
                                "content": "Country25"
                            }
                        },
                        "Email": {
                            "name": "name185",
                            "content": "Email32"
                        },
                        "Phone": {
                            "name": "name186",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode65",
                                    "content": "CountryCode39"
                                },
                                "AreaOrCityCode": "AreaOrCityCode39",
                                "Number": "Number39",
                                "Extension": "Extension39"
                            }
                        },
                        "Fax": {
                            "name": "name187",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode66",
                                    "content": "CountryCode40"
                                },
                                "AreaOrCityCode": "AreaOrCityCode40",
                                "Number": "Number40",
                                "Extension": "Extension40"
                            }
                        },
                        "URL": {
                            "name": "name188",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier83",
                        "domain": "domain83",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator83"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type83",
                            "ShortName": "ShortName83"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier84",
                        "domain": "domain84",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator84"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type84",
                            "ShortName": "ShortName84"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier85",
                        "domain": "domain85",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator85"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type85",
                            "ShortName": "ShortName85"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier86",
                        "domain": "domain86",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator86"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type86",
                            "ShortName": "ShortName86"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier87",
                        "domain": "domain87",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator87"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type87",
                            "ShortName": "ShortName87"
                        }
                    },
                    "isFactoring": {
                        "name": "name189",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name190"},
                    "LegalCapital": {
                        "name": "name191",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency12",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency12",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name192"}
                },
                "IdReference": {
                    "identifier": "identifier88",
                    "domain": "domain88",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator88"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type88",
                        "ShortName": "ShortName88"
                    }
                }
            },
            "receivingBank": {
                "Contact": {
                    "role": "role26",
                    "addressID": "addressID26",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName13",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name26"
                    },
                    "PostalAddress": {
                        "name": "name193",
                        "DeliverTo": "DeliverTo26",
                        "Street": "Street26",
                        "City": {
                            "cityCode": "cityCode26",
                            "content": "City26"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode26",
                            "content": "Municipality26"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode26",
                            "content": "State26"
                        },
                        "PostalCode": "PostalCode26",
                        "Country": {
                            "isoCountryCode": "isoCountryCode67",
                            "content": "Country26"
                        }
                    },
                    "Email": {
                        "name": "name194",
                        "content": "Email33"
                    },
                    "Phone": {
                        "name": "name195",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode68",
                                "content": "CountryCode41"
                            },
                            "AreaOrCityCode": "AreaOrCityCode41",
                            "Number": "Number41",
                            "Extension": "Extension41"
                        }
                    },
                    "Fax": {
                        "name": "name196",
                        "Email": {
                            "name": "name197",
                            "content": "Email34"
                        }
                    },
                    "URL": {
                        "name": "name198",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role27",
                        "addressID": "addressID27",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name27"
                        },
                        "PostalAddress": {
                            "name": "name199",
                            "DeliverTo": "DeliverTo27",
                            "Street": "Street27",
                            "City": {
                                "cityCode": "cityCode27",
                                "content": "City27"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode27",
                                "content": "Municipality27"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode27",
                                "content": "State27"
                            },
                            "PostalCode": "PostalCode27",
                            "Country": {
                                "isoCountryCode": "isoCountryCode69",
                                "content": "Country27"
                            }
                        },
                        "Email": {
                            "name": "name200",
                            "content": "Email35"
                        },
                        "Phone": {
                            "name": "name201",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode70",
                                    "content": "CountryCode42"
                                },
                                "AreaOrCityCode": "AreaOrCityCode42",
                                "Number": "Number42",
                                "Extension": "Extension42"
                            }
                        },
                        "Fax": {
                            "name": "name202",
                            "URL": {
                                "name": "name203",
                                "content": "http://www.oxygenxml.com/"
                            }
                        },
                        "URL": {
                            "name": "name204",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier89",
                        "domain": "domain89",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator89"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type89",
                            "ShortName": "ShortName89"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier90",
                        "domain": "domain90",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator90"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type90",
                            "ShortName": "ShortName90"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier91",
                        "domain": "domain91",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator91"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type91",
                            "ShortName": "ShortName91"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier92",
                        "domain": "domain92",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator92"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type92",
                            "ShortName": "ShortName92"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier93",
                        "domain": "domain93",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator93"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type93",
                            "ShortName": "ShortName93"
                        }
                    },
                    "isFactoring": {
                        "name": "name205",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name206"},
                    "LegalCapital": {
                        "name": "name207",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency13",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency13",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name208"}
                },
                "IdReference": {
                    "identifier": "identifier94",
                    "domain": "domain94",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator94"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type94",
                        "ShortName": "ShortName94"
                    }
                },
                "accountID": {
                    "identifier": "identifier95",
                    "domain": "domain95",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator95"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type95",
                        "ShortName": "ShortName95"
                    }
                },
                "bankRoutingID": {
                    "identifier": "identifier96",
                    "domain": "domain96",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator96"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type96",
                        "ShortName": "ShortName96"
                    }
                },
                "swiftID": {
                    "identifier": "identifier97",
                    "domain": "domain97",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator97"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type97",
                        "ShortName": "ShortName97"
                    }
                },
                "ibanID": {
                    "identifier": "identifier98",
                    "domain": "domain98",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator98"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type98",
                        "ShortName": "ShortName98"
                    }
                },
                "accountName": {
                    "identifier": "identifier99",
                    "domain": "domain99",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator99"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type99",
                        "ShortName": "ShortName99"
                    }
                },
                "accountType": {
                    "identifier": "identifier100",
                    "domain": "domain100",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator100"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type100",
                        "ShortName": "ShortName100"
                    }
                },
                "bankIdType": {
                    "identifier": "identifier101",
                    "domain": "domain101",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator101"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type101",
                        "ShortName": "ShortName101"
                    }
                }
            },
            "wireReceivingBank": {
                "Contact": {
                    "role": "role28",
                    "addressID": "addressID28",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName14",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name28"
                    },
                    "PostalAddress": {
                        "name": "name209",
                        "DeliverTo": "DeliverTo28",
                        "Street": "Street28",
                        "City": {
                            "cityCode": "cityCode28",
                            "content": "City28"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode28",
                            "content": "Municipality28"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode28",
                            "content": "State28"
                        },
                        "PostalCode": "PostalCode28",
                        "Country": {
                            "isoCountryCode": "isoCountryCode71",
                            "content": "Country28"
                        }
                    },
                    "Email": {
                        "name": "name210",
                        "content": "Email36"
                    },
                    "Phone": {
                        "name": "name211",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode72",
                                "content": "CountryCode43"
                            },
                            "AreaOrCityCode": "AreaOrCityCode43",
                            "Number": "Number43",
                            "Extension": "Extension43"
                        }
                    },
                    "Fax": {
                        "name": "name212",
                        "URL": {
                            "name": "name213",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "URL": {
                        "name": "name214",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role29",
                        "addressID": "addressID29",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name29"
                        },
                        "PostalAddress": {
                            "name": "name215",
                            "DeliverTo": "DeliverTo29",
                            "Street": "Street29",
                            "City": {
                                "cityCode": "cityCode29",
                                "content": "City29"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode29",
                                "content": "Municipality29"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode29",
                                "content": "State29"
                            },
                            "PostalCode": "PostalCode29",
                            "Country": {
                                "isoCountryCode": "isoCountryCode73",
                                "content": "Country29"
                            }
                        },
                        "Email": {
                            "name": "name216",
                            "content": "Email37"
                        },
                        "Phone": {
                            "name": "name217",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode74",
                                    "content": "CountryCode44"
                                },
                                "AreaOrCityCode": "AreaOrCityCode44",
                                "Number": "Number44",
                                "Extension": "Extension44"
                            }
                        },
                        "Fax": {
                            "name": "name218",
                            "URL": {
                                "name": "name219",
                                "content": "http://www.oxygenxml.com/"
                            }
                        },
                        "URL": {
                            "name": "name220",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier102",
                        "domain": "domain102",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator102"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type102",
                            "ShortName": "ShortName102"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier103",
                        "domain": "domain103",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator103"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type103",
                            "ShortName": "ShortName103"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier104",
                        "domain": "domain104",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator104"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type104",
                            "ShortName": "ShortName104"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier105",
                        "domain": "domain105",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator105"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type105",
                            "ShortName": "ShortName105"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier106",
                        "domain": "domain106",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator106"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type106",
                            "ShortName": "ShortName106"
                        }
                    },
                    "isFactoring": {
                        "name": "name221",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name222"},
                    "LegalCapital": {
                        "name": "name223",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency14",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency14",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name224"}
                },
                "IdReference": {
                    "identifier": "identifier107",
                    "domain": "domain107",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator107"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type107",
                        "ShortName": "ShortName107"
                    }
                },
                "accountID": {
                    "identifier": "identifier108",
                    "domain": "domain108",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator108"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type108",
                        "ShortName": "ShortName108"
                    }
                },
                "bankRoutingID": {
                    "identifier": "identifier109",
                    "domain": "domain109",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator109"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type109",
                        "ShortName": "ShortName109"
                    }
                },
                "swiftID": {
                    "identifier": "identifier110",
                    "domain": "domain110",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator110"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type110",
                        "ShortName": "ShortName110"
                    }
                },
                "ibanID": {
                    "identifier": "identifier111",
                    "domain": "domain111",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator111"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type111",
                        "ShortName": "ShortName111"
                    }
                },
                "accountName": {
                    "identifier": "identifier112",
                    "domain": "domain112",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator112"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type112",
                        "ShortName": "ShortName112"
                    }
                },
                "accountType": {
                    "identifier": "identifier113",
                    "domain": "domain113",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator113"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type113",
                        "ShortName": "ShortName113"
                    }
                },
                "bankIdType": {
                    "identifier": "identifier114",
                    "domain": "domain114",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator114"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type114",
                        "ShortName": "ShortName114"
                    }
                }
            },
            "receivingCorrespondentBank": {
                "Contact": {
                    "role": "role30",
                    "addressID": "addressID30",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName15",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name30"
                    },
                    "PostalAddress": {
                        "name": "name225",
                        "DeliverTo": "DeliverTo30",
                        "Street": "Street30",
                        "City": {
                            "cityCode": "cityCode30",
                            "content": "City30"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode30",
                            "content": "Municipality30"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode30",
                            "content": "State30"
                        },
                        "PostalCode": "PostalCode30",
                        "Country": {
                            "isoCountryCode": "isoCountryCode75",
                            "content": "Country30"
                        }
                    },
                    "Email": {
                        "name": "name226",
                        "content": "Email38"
                    },
                    "Phone": {
                        "name": "name227",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode76",
                                "content": "CountryCode45"
                            },
                            "AreaOrCityCode": "AreaOrCityCode45",
                            "Number": "Number45",
                            "Extension": "Extension45"
                        }
                    },
                    "Fax": {
                        "name": "name228",
                        "URL": {
                            "name": "name229",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "URL": {
                        "name": "name230",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role31",
                        "addressID": "addressID31",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name31"
                        },
                        "PostalAddress": {
                            "name": "name231",
                            "DeliverTo": "DeliverTo31",
                            "Street": "Street31",
                            "City": {
                                "cityCode": "cityCode31",
                                "content": "City31"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode31",
                                "content": "Municipality31"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode31",
                                "content": "State31"
                            },
                            "PostalCode": "PostalCode31",
                            "Country": {
                                "isoCountryCode": "isoCountryCode77",
                                "content": "Country31"
                            }
                        },
                        "Email": {
                            "name": "name232",
                            "content": "Email39"
                        },
                        "Phone": {
                            "name": "name233",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode78",
                                    "content": "CountryCode46"
                                },
                                "AreaOrCityCode": "AreaOrCityCode46",
                                "Number": "Number46",
                                "Extension": "Extension46"
                            }
                        },
                        "Fax": {
                            "name": "name234",
                            "URL": {
                                "name": "name235",
                                "content": "http://www.oxygenxml.com/"
                            }
                        },
                        "URL": {
                            "name": "name236",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier115",
                        "domain": "domain115",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator115"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type115",
                            "ShortName": "ShortName115"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier116",
                        "domain": "domain116",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator116"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type116",
                            "ShortName": "ShortName116"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier117",
                        "domain": "domain117",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator117"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type117",
                            "ShortName": "ShortName117"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier118",
                        "domain": "domain118",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator118"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type118",
                            "ShortName": "ShortName118"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier119",
                        "domain": "domain119",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator119"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type119",
                            "ShortName": "ShortName119"
                        }
                    },
                    "isFactoring": {
                        "name": "name237",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name238"},
                    "LegalCapital": {
                        "name": "name239",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency15",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency15",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name240"}
                },
                "IdReference": {
                    "identifier": "identifier120",
                    "domain": "domain120",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator120"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type120",
                        "ShortName": "ShortName120"
                    }
                },
                "accountID": {
                    "identifier": "identifier121",
                    "domain": "domain121",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator121"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type121",
                        "ShortName": "ShortName121"
                    }
                },
                "bankRoutingID": {
                    "identifier": "identifier122",
                    "domain": "domain122",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator122"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type122",
                        "ShortName": "ShortName122"
                    }
                },
                "swiftID": {
                    "identifier": "identifier123",
                    "domain": "domain123",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator123"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type123",
                        "ShortName": "ShortName123"
                    }
                },
                "ibanID": {
                    "identifier": "identifier124",
                    "domain": "domain124",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator124"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type124",
                        "ShortName": "ShortName124"
                    }
                },
                "accountName": {
                    "identifier": "identifier125",
                    "domain": "domain125",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator125"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type125",
                        "ShortName": "ShortName125"
                    }
                },
                "accountType": {
                    "identifier": "identifier126",
                    "domain": "domain126",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator126"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type126",
                        "ShortName": "ShortName126"
                    }
                },
                "bankIdType": {
                    "identifier": "identifier127",
                    "domain": "domain127",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator127"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type127",
                        "ShortName": "ShortName127"
                    }
                }
            },
            "DocumentReference": {"payloadID": "payloadID0"},
            "InvoiceIDInfo": {
                "invoiceID": "invoiceID1",
                "invoiceDate": "2006-05-04T18:13:51.0"
            },
            "PaymentProposalIDInfo": {"paymentProposalID": "paymentProposalID0"},
            "InvoiceDetailShipping": {
                "shippingDate": "2006-05-04T18:13:51.0",
                "shipFrom": {
                    "role": "role32",
                    "addressID": "addressID32",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName16",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name32"
                    },
                    "PostalAddress": {
                        "name": "name241",
                        "DeliverTo": "DeliverTo32",
                        "Street": "Street32",
                        "City": {
                            "cityCode": "cityCode32",
                            "content": "City32"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode32",
                            "content": "Municipality32"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode32",
                            "content": "State32"
                        },
                        "PostalCode": "PostalCode32",
                        "Country": {
                            "isoCountryCode": "isoCountryCode79",
                            "content": "Country32"
                        }
                    },
                    "Email": {
                        "name": "name242",
                        "content": "Email40"
                    },
                    "Phone": {
                        "name": "name243",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode80",
                                "content": "CountryCode47"
                            },
                            "AreaOrCityCode": "AreaOrCityCode47",
                            "Number": "Number47",
                            "Extension": "Extension47"
                        }
                    },
                    "Fax": {
                        "name": "name244",
                        "URL": {
                            "name": "name245",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "URL": {
                        "name": "name246",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role33",
                        "addressID": "addressID33",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name33"
                        },
                        "PostalAddress": {
                            "name": "name247",
                            "DeliverTo": "DeliverTo33",
                            "Street": "Street33",
                            "City": {
                                "cityCode": "cityCode33",
                                "content": "City33"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode33",
                                "content": "Municipality33"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode33",
                                "content": "State33"
                            },
                            "PostalCode": "PostalCode33",
                            "Country": {
                                "isoCountryCode": "isoCountryCode81",
                                "content": "Country33"
                            }
                        },
                        "Email": {
                            "name": "name248",
                            "content": "Email41"
                        },
                        "Phone": {
                            "name": "name249",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode82",
                                    "content": "CountryCode48"
                                },
                                "AreaOrCityCode": "AreaOrCityCode48",
                                "Number": "Number48",
                                "Extension": "Extension48"
                            }
                        },
                        "Fax": {
                            "name": "name250",
                            "Email": {
                                "name": "name251",
                                "content": "Email42"
                            }
                        },
                        "URL": {
                            "name": "name252",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier128",
                        "domain": "domain128",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator128"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type128",
                            "ShortName": "ShortName128"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier129",
                        "domain": "domain129",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator129"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type129",
                            "ShortName": "ShortName129"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier130",
                        "domain": "domain130",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator130"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type130",
                            "ShortName": "ShortName130"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier131",
                        "domain": "domain131",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator131"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type131",
                            "ShortName": "ShortName131"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier132",
                        "domain": "domain132",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator132"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type132",
                            "ShortName": "ShortName132"
                        }
                    },
                    "isFactoring": {
                        "name": "name253",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name254"},
                    "LegalCapital": {
                        "name": "name255",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency16",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency16",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name256"}
                },
                "shipTo": {
                    "role": "role34",
                    "addressID": "addressID34",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName17",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name34"
                    },
                    "PostalAddress": {
                        "name": "name257",
                        "DeliverTo": "DeliverTo34",
                        "Street": "Street34",
                        "City": {
                            "cityCode": "cityCode34",
                            "content": "City34"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode34",
                            "content": "Municipality34"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode34",
                            "content": "State34"
                        },
                        "PostalCode": "PostalCode34",
                        "Country": {
                            "isoCountryCode": "isoCountryCode83",
                            "content": "Country34"
                        }
                    },
                    "Email": {
                        "name": "name258",
                        "content": "Email43"
                    },
                    "Phone": {
                        "name": "name259",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode84",
                                "content": "CountryCode49"
                            },
                            "AreaOrCityCode": "AreaOrCityCode49",
                            "Number": "Number49",
                            "Extension": "Extension49"
                        }
                    },
                    "Fax": {
                        "name": "name260",
                        "Email": {
                            "name": "name261",
                            "content": "Email44"
                        }
                    },
                    "URL": {
                        "name": "name262",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role35",
                        "addressID": "addressID35",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name35"
                        },
                        "PostalAddress": {
                            "name": "name263",
                            "DeliverTo": "DeliverTo35",
                            "Street": "Street35",
                            "City": {
                                "cityCode": "cityCode35",
                                "content": "City35"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode35",
                                "content": "Municipality35"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode35",
                                "content": "State35"
                            },
                            "PostalCode": "PostalCode35",
                            "Country": {
                                "isoCountryCode": "isoCountryCode85",
                                "content": "Country35"
                            }
                        },
                        "Email": {
                            "name": "name264",
                            "content": "Email45"
                        },
                        "Phone": {
                            "name": "name265",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode86",
                                    "content": "CountryCode50"
                                },
                                "AreaOrCityCode": "AreaOrCityCode50",
                                "Number": "Number50",
                                "Extension": "Extension50"
                            }
                        },
                        "Fax": {
                            "name": "name266",
                            "Email": {
                                "name": "name267",
                                "content": "Email46"
                            }
                        },
                        "URL": {
                            "name": "name268",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier133",
                        "domain": "domain133",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator133"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type133",
                            "ShortName": "ShortName133"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier134",
                        "domain": "domain134",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator134"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type134",
                            "ShortName": "ShortName134"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier135",
                        "domain": "domain135",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator135"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type135",
                            "ShortName": "ShortName135"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier136",
                        "domain": "domain136",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator136"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type136",
                            "ShortName": "ShortName136"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier137",
                        "domain": "domain137",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator137"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type137",
                            "ShortName": "ShortName137"
                        }
                    },
                    "isFactoring": {
                        "name": "name269",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name270"},
                    "LegalCapital": {
                        "name": "name271",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency17",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency17",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name272"}
                },
                "Contact": {
                    "role": "role36",
                    "addressID": "addressID36",
                    "isRemoved": false,
                    "isNew": false,
                    "orgName": "orgName18",
                    "Name": {
                        "xml:lang": "en-US",
                        "content": "Name36"
                    },
                    "PostalAddress": {
                        "name": "name273",
                        "DeliverTo": "DeliverTo36",
                        "Street": "Street36",
                        "City": {
                            "cityCode": "cityCode36",
                            "content": "City36"
                        },
                        "Municipality": {
                            "municipalityCode": "municipalityCode36",
                            "content": "Municipality36"
                        },
                        "State": {
                            "isoStateCode": "isoStateCode36",
                            "content": "State36"
                        },
                        "PostalCode": "PostalCode36",
                        "Country": {
                            "isoCountryCode": "isoCountryCode87",
                            "content": "Country36"
                        }
                    },
                    "Email": {
                        "name": "name274",
                        "content": "Email47"
                    },
                    "Phone": {
                        "name": "name275",
                        "TelephoneNumber": {
                            "CountryCode": {
                                "isoCountryCode": "isoCountryCode88",
                                "content": "CountryCode51"
                            },
                            "AreaOrCityCode": "AreaOrCityCode51",
                            "Number": "Number51",
                            "Extension": "Extension51"
                        }
                    },
                    "Fax": {
                        "name": "name276",
                        "Email": {
                            "name": "name277",
                            "content": "Email48"
                        }
                    },
                    "URL": {
                        "name": "name278",
                        "content": "http://www.oxygenxml.com/"
                    },
                    "OldContact": {
                        "role": "role37",
                        "addressID": "addressID37",
                        "Name": {
                            "xml:lang": "en-US",
                            "content": "Name37"
                        },
                        "PostalAddress": {
                            "name": "name279",
                            "DeliverTo": "DeliverTo37",
                            "Street": "Street37",
                            "City": {
                                "cityCode": "cityCode37",
                                "content": "City37"
                            },
                            "Municipality": {
                                "municipalityCode": "municipalityCode37",
                                "content": "Municipality37"
                            },
                            "State": {
                                "isoStateCode": "isoStateCode37",
                                "content": "State37"
                            },
                            "PostalCode": "PostalCode37",
                            "Country": {
                                "isoCountryCode": "isoCountryCode89",
                                "content": "Country37"
                            }
                        },
                        "Email": {
                            "name": "name280",
                            "content": "Email49"
                        },
                        "Phone": {
                            "name": "name281",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode90",
                                    "content": "CountryCode52"
                                },
                                "AreaOrCityCode": "AreaOrCityCode52",
                                "Number": "Number52",
                                "Extension": "Extension52"
                            }
                        },
                        "Fax": {
                            "name": "name282",
                            "TelephoneNumber": {
                                "CountryCode": {
                                    "isoCountryCode": "isoCountryCode91",
                                    "content": "CountryCode53"
                                },
                                "AreaOrCityCode": "AreaOrCityCode53",
                                "Number": "Number53",
                                "Extension": "Extension53"
                            }
                        },
                        "URL": {
                            "name": "name283",
                            "content": "http://www.oxygenxml.com/"
                        }
                    },
                    "departmentName": {
                        "identifier": "identifier138",
                        "domain": "domain138",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator138"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type138",
                            "ShortName": "ShortName138"
                        }
                    },
                    "gstID": {
                        "identifier": "identifier139",
                        "domain": "domain139",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator139"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type139",
                            "ShortName": "ShortName139"
                        }
                    },
                    "pstID": {
                        "identifier": "identifier140",
                        "domain": "domain140",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator140"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type140",
                            "ShortName": "ShortName140"
                        }
                    },
                    "qstID": {
                        "identifier": "identifier141",
                        "domain": "domain141",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator141"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type141",
                            "ShortName": "ShortName141"
                        }
                    },
                    "IdReference": {
                        "identifier": "identifier142",
                        "domain": "domain142",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator142"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type142",
                            "ShortName": "ShortName142"
                        }
                    },
                    "isFactoring": {
                        "name": "name284",
                        "schemaMappedType": "Extrinsic",
                        "content": false
                    },
                    "LegalStatus": {"name": "name285"},
                    "LegalCapital": {
                        "name": "name286",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency18",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency18",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name287"}
                },
                "CarrierIdentifier": {
                    "domain": "domain143",
                    "content": "CarrierIdentifier0"
                },
                "ShipmentIdentifier": "ShipmentIdentifier0",
                "DocumentReference": {"payloadID": "payloadID1"}
            },
            "ShipNoticeIDInfo": {
                "shipNoticeID": "shipNoticeID0",
                "shipNoticeDate": "2006-05-04T18:13:51.0",
                "oldshipNoticeID": "oldshipNoticeID0",
                "oldshipNoticeDate": "2006-05-04T18:13:51.0",
                "IdReference": {
                    "identifier": "identifier143",
                    "domain": "domain144",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator143"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type143",
                        "ShortName": "ShortName143"
                    }
                },
                "deliveryNoteID": {
                    "identifier": "identifier144",
                    "domain": "domain145",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator144"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type144",
                        "ShortName": "ShortName144"
                    }
                },
                "deliveryNoteDate": {
                    "identifier": "identifier145",
                    "domain": "domain146",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator145"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type145",
                        "ShortName": "ShortName145"
                    }
                },
                "deliveryNoteLineItemNo": {
                    "identifier": "identifier146",
                    "domain": "domain147",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator146"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type146",
                        "ShortName": "ShortName146"
                    }
                },
                "dispatchAdviceID": {
                    "identifier": "identifier147",
                    "domain": "domain148",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator147"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type147",
                        "ShortName": "ShortName147"
                    }
                },
                "receivingAdviceID": {
                    "identifier": "identifier148",
                    "domain": "domain149",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator148"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type148",
                        "ShortName": "ShortName148"
                    }
                },
                "receivingAdviceDate": {
                    "identifier": "identifier149",
                    "domain": "domain150",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator149"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type149",
                        "ShortName": "ShortName149"
                    }
                },
                "transportDocumentID": {
                    "identifier": "identifier150",
                    "domain": "domain151",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator150"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type150",
                        "ShortName": "ShortName150"
                    }
                },
                "proofOfDeliveryID": {
                    "identifier": "identifier151",
                    "domain": "domain152",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator151"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type151",
                        "ShortName": "ShortName151"
                    }
                },
                "proofOfDeliveryDate": {
                    "identifier": "identifier152",
                    "domain": "domain153",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator152"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type152",
                        "ShortName": "ShortName152"
                    }
                },
                "actualDeliveryDate": {
                    "identifier": "identifier153",
                    "domain": "domain154",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator153"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type153",
                        "ShortName": "ShortName153"
                    }
                },
                "goodsPositioningDate": {
                    "identifier": "identifier154",
                    "domain": "domain155",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator154"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type154",
                        "ShortName": "ShortName154"
                    }
                },
                "goodsPositioningStartDate": {
                    "identifier": "identifier155",
                    "domain": "domain156",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator155"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type155",
                        "ShortName": "ShortName155"
                    }
                },
                "goodsPositioningEndDate": {
                    "identifier": "identifier156",
                    "domain": "domain157",
                    "Creator": {
                        "xml:lang": "en-US",
                        "content": "Creator156"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type156",
                        "ShortName": "ShortName156"
                    }
                }
            },
            "InvoiceDetailNetPaymentTerm": {
                "payInNumberOfDays": 50,
                "percentageRate": 0
            },
            "NetPaymentTerm": {"payInNumberOfDays": 0},
            "InvoiceDetailPaymentTerm": {
                "payInNumberOfDays": 50,
                "percentageRate": 0
            },
            "Period": {
                "startDate": "2006-05-04T18:13:51.0",
                "endDate": "2006-05-04T18:13:51.0"
            },
            "Comments": {
                "xml:lang": "en-US",
                "Attachment": {
                    "URL": {
                        "name": "name288",
                        "content": "http://www.oxygenxml.com/"
                    }
                }
            },
            "InvoiceDisclaimerComments": {
                "xml:lang": "en-US",
                "Attachment": {
                    "URL": {
                        "name": "name289",
                        "content": "http://www.oxygenxml.com/"
                    }
                }
            },
            "CreditDisclaimerComments": {
                "xml:lang": "en-US",
                "Attachment": {
                    "URL": {
                        "name": "name290",
                        "content": "http://www.oxygenxml.com/"
                    }
                }
            },
            "supplierReference": {
                "identifier": "identifier157",
                "domain": "domain158",
                "Creator": {
                    "xml:lang": "en-US",
                    "content": "Creator157"
                },
                "Description": {
                    "xml:lang": "en-US",
                    "type": "type157",
                    "ShortName": "ShortName157"
                }
            },
            "discountInformation": {
                "name": "name291",
                "schemaMappedType": "Extrinsic",
                "content": "discountInformation0"
            },
            "penaltyInformation": {
                "name": "name292",
                "schemaMappedType": "Extrinsic",
                "content": "penaltyInformation0"
            },
            "netTermInformation": {
                "name": "name293",
                "schemaMappedType": "Extrinsic",
                "content": "netTermInformation0"
            },
            "PaymentTermsExplanation": {
                "name": "AribaNetwork.PaymentTermsExplanation",
                "schemaMappedType": "Extrinsic",
                "content": "PaymentTermsExplanation0"
            },
            "taxInvoice": {
                "name": "TaxInvoice",
                "schemaMappedType": "Extrinsic",
                "content": "taxInvoice0"
            },
            "invoiceTitle": {
                "name": "InvoiceTitle",
                "schemaMappedType": "Extrinsic",
                "content": "invoiceTitle0"
            },
            "customerReference": {
                "name": "Customer Reference",
                "schemaMappedType": "Extrinsic",
                "content": "customerReference0"
            },
            "DocumentType": {
                "name": "DocumentType",
                "schemaMappedType": "Extrinsic",
                "content": "DocumentType0"
            },
            "PaymentService": {
                "name": "PaymentService",
                "schemaMappedType": "Extrinsic",
                "content": "PaymentService0"
            },
            "PaymentProcessor": {
                "name": "PaymentProcessor",
                "schemaMappedType": "Extrinsic",
                "content": "PaymentProcessor0"
            },
            "Transaction": {
                "name": "Transaction",
                "schemaMappedType": "Extrinsic",
                "content": "Transaction0"
            },
            "IsShippingTaxExempt": {
                "name": "IsShippingTaxExempt",
                "schemaMappedType": "Extrinsic",
                "content": "IsShippingTaxExempt0"
            },
            "buyerVatID": {
                "name": "name303",
                "schemaMappedType": "Extrinsic",
                "content": "buyerVatID0"
            },
            "supplierVatID": {
                "name": "name304",
                "schemaMappedType": "Extrinsic",
                "content": "supplierVatID0"
            },
            "buyerTaxID": {
                "name": "name305",
                "schemaMappedType": "Extrinsic",
                "content": "buyerTaxID0"
            },
            "supplierCommercialRegisterCourt": {
                "name": "name306",
                "schemaMappedType": "Extrinsic",
                "content": "supplierCommercialRegisterCourt0"
            },
            "VATonDebits": {
                "name": "VATonDebits",
                "schemaMappedType": "Extrinsic",
                "content": "VATonDebits0"
            },
            "invoiceSourceDocument": {
                "name": "name308",
                "schemaMappedType": "Extrinsic",
                "content": "invoiceSourceDocument0"
            },
            "invoiceSubmissionMethod": {
                "name": "name309",
                "schemaMappedType": "Extrinsic",
                "content": "invoiceSubmissionMethod0"
            },
            "paymentMethod": {
                "name": "name310",
                "schemaMappedType": "Extrinsic",
                "content": "paymentMethod0"
            },
            "bankDetails": {
                "name": "name311",
                "schemaMappedType": "Extrinsic",
                "content": "bankDetails0"
            },
            "paymentNote": {
                "name": "name312",
                "schemaMappedType": "Extrinsic",
                "content": "paymentNote0"
            },
            "invoiceType": {
                "name": "name313",
                "schemaMappedType": "Extrinsic",
                "content": "invoiceType0"
            },
            "isAutoflip": {
                "name": "name314",
                "schemaMappedType": "Extrinsic",
                "content": "isAutoflip0"
            },
            "dynamicDiscountingCreditMemo": {
                "name": "name315",
                "schemaMappedType": "Extrinsic",
                "content": "dynamicDiscountingCreditMemo0"
            },
            "punchinContractInvoice": {
                "name": "name316",
                "schemaMappedType": "Extrinsic",
                "content": "punchinContractInvoice0"
            },
            "isNFSe": {
                "name": "name317",
                "schemaMappedType": "Extrinsic",
                "content": "isNFSe0"
            },
            "saoPauloServiceCode": {
                "name": "name318",
                "schemaMappedType": "Extrinsic",
                "Classification": {
                    "domain": "domain159",
                    "content": "Classification0"
                }
            },
            "rpsType": {
                "name": "name319",
                "schemaMappedType": "Extrinsic",
                "content": "rpsType0"
            },
            "rpsSeries": {
                "name": "name320",
                "schemaMappedType": "Extrinsic",
                "content": "rpsSeries0"
            },
            "rpsNumber": {
                "name": "name321",
                "schemaMappedType": "Extrinsic",
                "content": "rpsNumber0"
            },
            "verificationCode": {
                "name": "name322",
                "schemaMappedType": "Extrinsic",
                "content": "verificationCode0"
            },
            "isServiceBrokerUsed": {
                "name": "name323",
                "schemaMappedType": "Extrinsic",
                "content": "isServiceBrokerUsed0"
            },
            "CEICode": {
                "name": "name324",
                "schemaMappedType": "Extrinsic",
                "content": "CEICode0"
            },
            "supplierCommercialIdentifier": {
                "name": "name325",
                "schemaMappedType": "Extrinsic",
                "content": "supplierCommercialIdentifier0"
            },
            "legalCapital": {
                "name": "name326",
                "schemaMappedType": "Extrinsic",
                "content": "legalCapital0"
            },
            "shareHolderType": {
                "name": "name327",
                "schemaMappedType": "Extrinsic",
                "content": "shareHolderType0"
            },
            "supplierLiquidationState": {
                "name": "name328",
                "schemaMappedType": "Extrinsic",
                "content": "supplierLiquidationState0"
            },
            "supplierTaxRegime": {
                "name": "name329",
                "schemaMappedType": "Extrinsic",
                "content": "supplierTaxRegime0"
            },
            "projectCode": {
                "name": "name330",
                "schemaMappedType": "Extrinsic",
                "content": "projectCode0"
            },
            "tenderCode": {
                "name": "name331",
                "schemaMappedType": "Extrinsic",
                "content": "tenderCode0"
            },
            "CNAECode": {
                "name": "name332",
                "schemaMappedType": "Extrinsic",
                "content": "CNAECode0"
            },
            "rioServiceCode": {
                "name": "name333",
                "schemaMappedType": "Extrinsic",
                "Classification": {
                    "domain": "domain160",
                    "content": "Classification1"
                }
            },
            "regionalEngineerAssociationNum": {
                "name": "name334",
                "schemaMappedType": "Extrinsic",
                "content": "regionalEngineerAssociationNum0"
            },
            "culturalIncentive": {
                "name": "name335",
                "schemaMappedType": "Extrinsic",
                "content": "culturalIncentive0"
            },
            "simpleNational": {
                "name": "name336",
                "schemaMappedType": "Extrinsic",
                "content": "simpleNational0"
            },
            "specialTaxRegime": {
                "name": "name337",
                "schemaMappedType": "Extrinsic",
                "content": "specialTaxRegime0"
            },
            "taxBenefitCode": {
                "name": "name338",
                "schemaMappedType": "Extrinsic",
                "content": "taxBenefitCode0"
            },
            "useOfCFDICode": {
                "name": "name339",
                "schemaMappedType": "Extrinsic",
                "Classification": {
                    "domain": "domain161",
                    "content": "Classification2"
                }
            },
            "placeOfSupply": {
                "name": "name340",
                "schemaMappedType": "Extrinsic",
                "content": "placeOfSupply0"
            },
            "Extrinsic": {"name": "name341"},
            "invoiceDisplayUrl": {
                "name": "AribaNetwork.InvoiceDisplay",
                "schemaMappedType": "Extrinsic",
                "content": "invoiceDisplayUrl0"
            },
            "exchangeRate": {
                "name": "name343",
                "schemaMappedType": "Extrinsic",
                "content": 0
            },
            "supplierCommercialRegistration": {
                "name": "name344",
                "schemaMappedType": "Extrinsic",
                "content": "supplierCommercialRegistration0"
            },
            "taxInvoiceNumber": {
                "name": "name345",
                "schemaMappedType": "Extrinsic",
                "content": "taxInvoiceNumber0"
            },
            "typeOfSupply": {
                "name": "name346",
                "schemaMappedType": "Extrinsic",
                "content": "typeOfSupply0"
            },
            "externalPurpose": {
                "name": "name347",
                "schemaMappedType": "Extrinsic",
                "content": "externalPurpose0"
            },
            "referenceTaxInvoiceNumber": {
                "name": "name348",
                "schemaMappedType": "Extrinsic",
                "content": "referenceTaxInvoiceNumber0"
            },
            "referenceExternalPurpose": {
                "name": "name349",
                "schemaMappedType": "Extrinsic",
                "content": "referenceExternalPurpose0"
            },
            "resolutionNumber": {
                "name": "name350",
                "schemaMappedType": "Extrinsic",
                "content": "resolutionNumber0"
            },
            "resolutionDate": {
                "name": "name351",
                "schemaMappedType": "Extrinsic",
                "content": "2006-05-04"
            },
            "taxDeterminationDate": {
                "name": "name352",
                "schemaMappedType": "Extrinsic",
                "content": "2006-05-04"
            },
            "economicActivityCode": {
                "name": "name353",
                "schemaMappedType": "Extrinsic",
                "content": "economicActivityCode0"
            },
            "economicActivityDescription": {
                "name": "name354",
                "schemaMappedType": "Extrinsic",
                "content": "economicActivityDescription0"
            },
            "fiscalRegimeType": {
                "name": "name355",
                "schemaMappedType": "Extrinsic",
                "content": "fiscalRegimeType0"
            },
            "specialRegimeStatus": {
                "name": "name356",
                "schemaMappedType": "Extrinsic",
                "content": "specialRegimeStatus0"
            },
            "taxpayerType": {
                "name": "name357",
                "schemaMappedType": "Extrinsic",
                "content": "taxpayerType0"
            },
            "autoDeductionStatus": {
                "name": "name358",
                "schemaMappedType": "Extrinsic",
                "content": "autoDeductionStatus0"
            },
            "buyerEconomicActivityDescription": {
                "name": "name359",
                "schemaMappedType": "Extrinsic",
                "content": "buyerEconomicActivityDescription0"
            },
            "creditMemoReason": {
                "name": "name360",
                "schemaMappedType": "Extrinsic",
                "content": "creditMemoReason0"
            },
            "debitMemoReason": {
                "name": "name361",
                "schemaMappedType": "Extrinsic",
                "content": "debitMemoReason0"
            },
            "supplierLegalDetails": {
                "name": "name362",
                "schemaMappedType": "Extrinsic",
                "content": "supplierLegalDetails0"
            },
            "convertedInvoiceData": {
                "name": "name363",
                "schemaMappedType": "Extrinsic",
                "content": "convertedInvoiceData0"
            },
            "initiatePaymentAction": {
                "name": "name364",
                "schemaMappedType": "Extrinsic",
                "content": "initiatePaymentAction0"
            },
            "serviceDescription": {
                "name": "name365",
                "schemaMappedType": "Extrinsic",
                "content": "serviceDescription0"
            },
            "isFinalAutoSES": {
                "name": "name366",
                "schemaMappedType": "Extrinsic",
                "content": "isFinalAutoSES0"
            },
            "supplierSalesTaxID": {
                "name": "name367",
                "schemaMappedType": "Extrinsic",
                "content": "supplierSalesTaxID0"
            },
            "buyerSalesTaxID": {
                "name": "name368",
                "schemaMappedType": "Extrinsic",
                "content": "buyerSalesTaxID0"
            },
            "supplierServiceTaxID": {
                "name": "name369",
                "schemaMappedType": "Extrinsic",
                "content": "supplierServiceTaxID0"
            },
            "buyerServiceTaxID": {
                "name": "name370",
                "schemaMappedType": "Extrinsic",
                "content": "buyerServiceTaxID0"
            }
        },
        "InvoiceDetailOrder": {
            "InvoiceDetailOrderInfo": {
                "OrderReference": {
                    "orderID": "orderID0",
                    "orderDate": "2006-05-04T18:13:51.0",
                    "DocumentReference": {"payloadID": "payloadID2"}
                },
                "MasterAgreementReference": {
                    "agreementID": "agreementID0",
                    "agreementDate": "2006-05-04T18:13:51.0",
                    "agreementType": "agreementType0",
                    "DocumentReference": {"payloadID": "payloadID3"}
                },
                "MasterAgreementIDInfo": {
                    "agreementID": "agreementID1",
                    "agreementDate": "2006-05-04T18:13:51.0",
                    "agreementType": "agreementType1"
                },
                "OrderIDInfo": {
                    "orderID": "orderID1",
                    "orderDate": "2006-05-04T18:13:51.0"
                },
                "SupplierOrderInfo": {
                    "orderID": "orderID2",
                    "orderDate": "2006-05-04T18:13:51.0"
                }
            },
            "InvoiceDetailReceiptInfo": {
                "ReceiptReference": {
                    "receiptID": "receiptID0",
                    "receiptDate": "2006-05-04T18:13:51.0",
                    "DocumentReference": {"payloadID": "payloadID4"}
                },
                "ReceiptIDInfo": {
                    "receiptID": "receiptID1",
                    "receiptDate": "2006-05-04T18:13:51.0"
                }
            },
            "InvoiceDetailShipNoticeInfo": {
                "ShipNoticeReference": {
                    "shipNoticeID": "shipNoticeID1",
                    "shipNoticeDate": "2006-05-04T18:13:51.0",
                    "oldshipNoticeID": "oldshipNoticeID1",
                    "oldshipNoticeDate": "2006-05-04T18:13:51.0",
                    "DocumentReference": {"payloadID": "payloadID5"}
                },
                "ShipNoticeIDInfo": {
                    "shipNoticeID": "shipNoticeID2",
                    "shipNoticeDate": "2006-05-04T18:13:51.0",
                    "oldshipNoticeID": "oldshipNoticeID2",
                    "oldshipNoticeDate": "2006-05-04T18:13:51.0",
                    "IdReference": {
                        "identifier": "identifier158",
                        "domain": "domain162",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator158"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type158",
                            "ShortName": "ShortName158"
                        }
                    },
                    "deliveryNoteID": {
                        "identifier": "identifier159",
                        "domain": "domain163",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator159"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type159",
                            "ShortName": "ShortName159"
                        }
                    },
                    "deliveryNoteDate": {
                        "identifier": "identifier160",
                        "domain": "domain164",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator160"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type160",
                            "ShortName": "ShortName160"
                        }
                    },
                    "deliveryNoteLineItemNo": {
                        "identifier": "identifier161",
                        "domain": "domain165",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator161"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type161",
                            "ShortName": "ShortName161"
                        }
                    },
                    "dispatchAdviceID": {
                        "identifier": "identifier162",
                        "domain": "domain166",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator162"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type162",
                            "ShortName": "ShortName162"
                        }
                    },
                    "receivingAdviceID": {
                        "identifier": "identifier163",
                        "domain": "domain167",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator163"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type163",
                            "ShortName": "ShortName163"
                        }
                    },
                    "receivingAdviceDate": {
                        "identifier": "identifier164",
                        "domain": "domain168",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator164"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type164",
                            "ShortName": "ShortName164"
                        }
                    },
                    "transportDocumentID": {
                        "identifier": "identifier165",
                        "domain": "domain169",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator165"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type165",
                            "ShortName": "ShortName165"
                        }
                    },
                    "proofOfDeliveryID": {
                        "identifier": "identifier166",
                        "domain": "domain170",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator166"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type166",
                            "ShortName": "ShortName166"
                        }
                    },
                    "proofOfDeliveryDate": {
                        "identifier": "identifier167",
                        "domain": "domain171",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator167"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type167",
                            "ShortName": "ShortName167"
                        }
                    },
                    "actualDeliveryDate": {
                        "identifier": "identifier168",
                        "domain": "domain172",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator168"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type168",
                            "ShortName": "ShortName168"
                        }
                    },
                    "goodsPositioningDate": {
                        "identifier": "identifier169",
                        "domain": "domain173",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator169"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type169",
                            "ShortName": "ShortName169"
                        }
                    },
                    "goodsPositioningStartDate": {
                        "identifier": "identifier170",
                        "domain": "domain174",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator170"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type170",
                            "ShortName": "ShortName170"
                        }
                    },
                    "goodsPositioningEndDate": {
                        "identifier": "identifier171",
                        "domain": "domain175",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator171"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type171",
                            "ShortName": "ShortName171"
                        }
                    }
                }
            },
            "InvoiceDetailServiceItem": {
                "invoiceLineNumber": 0,
                "parentInvoiceLineNumber": 0,
                "quantity": 0,
                "inspectionDate": "2006-05-04T18:13:51.0",
                "referenceDate": "2006-05-04T18:13:51.0",
                "isAdHoc": "isAdHoc0",
                "InvoiceDetailItemReference": {
                    "lineNumber": 0,
                    "adhocLineNumber": 0,
                    "serialNumber": "serialNumber0",
                    "lineCopyTax": "lineCopyTax0",
                    "ItemID": {
                        "SupplierPartID": "SupplierPartID0",
                        "SupplierPartAuxiliaryID": "",
                        "OldSupplierPartID": "OldSupplierPartID0",
                        "OldSupplierPartAuxiliaryID": "",
                        "BuyerPartID": "BuyerPartID0",
                        "OldBuyerPartID": "OldBuyerPartID0"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type172",
                        "ShortName": "ShortName172"
                    },
                    "Classification": {
                        "domain": "domain176",
                        "content": "Classification3"
                    },
                    "ManufacturerPartID": "ManufacturerPartID0",
                    "ManufacturerName": {
                        "xml:lang": "en-US",
                        "content": "ManufacturerName0"
                    },
                    "Country": {
                        "isoCountryCode": "isoCountryCode92",
                        "content": "Country38"
                    },
                    "SerialNumber": "SerialNumber0",
                    "InvoiceDetailItemReferenceIndustry": {
                        "InvoiceDetailItemReferenceRetail": {
                            "EANID": "EANID0",
                            "EuropeanWasteCatalogID": "EuropeanWasteCatalogID0",
                            "Characteristic": {
                                "domain": "domain177",
                                "value": "value0",
                                "code": "code0"
                            },
                            "color": {
                                "domain": "domain178",
                                "value": "value1",
                                "code": "code1"
                            },
                            "colorCode": {
                                "domain": "domain179",
                                "value": "value2",
                                "code": "code2"
                            },
                            "size": {
                                "domain": "domain180",
                                "value": "value3",
                                "code": "code3"
                            },
                            "sizeCode": {
                                "domain": "domain181",
                                "value": "value4",
                                "code": "code4"
                            },
                            "quality": {
                                "domain": "domain182",
                                "value": "value5",
                                "code": "code5"
                            },
                            "qualityCode": {
                                "domain": "domain183",
                                "value": "value6",
                                "code": "code6"
                            },
                            "grade": {
                                "domain": "domain184",
                                "value": "value7",
                                "code": "code7"
                            },
                            "gradeCode": {
                                "domain": "domain185",
                                "value": "value8",
                                "code": "code8"
                            }
                        }
                    }
                },
                "InvoiceDetailServiceItemReference": {
                    "lineNumber": 0,
                    "adhocLineNumber": 0,
                    "Classification": {
                        "domain": "domain186",
                        "content": "Classification4"
                    },
                    "ItemID": {
                        "SupplierPartID": "SupplierPartID1",
                        "SupplierPartAuxiliaryID": "",
                        "OldSupplierPartID": "OldSupplierPartID1",
                        "OldSupplierPartAuxiliaryID": "",
                        "BuyerPartID": "BuyerPartID1",
                        "OldBuyerPartID": "OldBuyerPartID1"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type173",
                        "ShortName": "ShortName173"
                    }
                },
                "ServiceEntryItemIDInfo": {
                    "serviceLineNumber": 0,
                    "serviceEntryID": "serviceEntryID0",
                    "serviceEntryDate": "2006-05-04T18:13:51.0",
                    "IdReference": {
                        "identifier": "identifier172",
                        "domain": "domain187",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator172"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type174",
                            "ShortName": "ShortName174"
                        }
                    }
                },
                "SubtotalAmount": {
                    "Money": {
                        "currency": "currency19",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency19",
                        "content": 0
                    }
                },
                "Period": {
                    "startDate": "2006-05-04T18:13:51.0",
                    "endDate": "2006-05-04T18:13:51.0"
                },
                "UnitRate": {
                    "Money": {
                        "currency": "currency20",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency20",
                        "content": 0
                    },
                    "UnitOfMeasure": "UnitOfMeasure0",
                    "PriceBasisQuantity": {
                        "quantity": 0,
                        "oldquantity": 0,
                        "conversionFactor": 0,
                        "oldconversionFactor": 0,
                        "UnitOfMeasure": "UnitOfMeasure1",
                        "OldUnitOfMeasure": "OldUnitOfMeasure0",
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type175",
                            "ShortName": "ShortName175"
                        }
                    },
                    "TermReference": {
                        "termName": "#any",
                        "term": "#any"
                    }
                },
                "Tax": {
                    "Money": {
                        "currency": "currency21",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency21",
                        "content": 0
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type176",
                        "ShortName": "ShortName176"
                    },
                    "TaxDetail": {
                        "purpose": "purpose1",
                        "category": "category0",
                        "percentageRate": 0,
                        "taxRateType": "taxRateType0",
                        "isVatRecoverable": "yes",
                        "isWithholdingTax": "yes",
                        "taxPointDate": "2006-05-04T18:13:51.0",
                        "paymentDate": "2006-05-04T18:13:51.0",
                        "isTriangularTransaction": "yes",
                        "exemptDetail": "zeroRated",
                        "TaxableAmount": {
                            "Money": {
                                "currency": "currency22",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency22",
                                "content": 0
                            }
                        },
                        "TaxAmount": {
                            "Money": {
                                "currency": "currency23",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency23",
                                "content": 0
                            }
                        },
                        "TaxLocation": {
                            "xml:lang": "en-US",
                            "content": "TaxLocation0"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type177",
                            "ShortName": "ShortName177"
                        },
                        "TriangularTransactionLawReference": {
                            "xml:lang": "en-US",
                            "content": "TriangularTransactionLawReference0"
                        },
                        "TaxRegime": "TaxRegime0",
                        "TaxExemption": {
                            "exemptCode": "exemptCode0",
                            "ExemptReason": {
                                "xml:lang": "en-US",
                                "content": "ExemptReason0"
                            }
                        },
                        "withholdingTaxType": {
                            "name": "name371",
                            "schemaMappedType": "Extrinsic",
                            "content": "withholdingTaxType0"
                        },
                        "taxPointDateCode": {
                            "name": "name372",
                            "schemaMappedType": "Extrinsic",
                            "content": "taxPointDateCode0"
                        },
                        "Extrinsic": {"name": "name373"}
                    },
                    "OldMoney": {
                        "currency": "currency24",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency24",
                        "content": 0
                    },
                    "DeletedMoney": {
                        "currency": "currency25",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency25",
                        "content": 0
                    },
                    "taxationType": {
                        "name": "name374",
                        "schemaMappedType": "Extrinsic",
                        "content": "taxationType0"
                    },
                    "isISSRetention": {
                        "name": "name375",
                        "schemaMappedType": "Extrinsic",
                        "content": "isISSRetention0"
                    },
                    "isServiceBrokerISSRetention": {
                        "name": "name376",
                        "schemaMappedType": "Extrinsic",
                        "content": "isServiceBrokerISSRetention0"
                    },
                    "issRetentionValue": {
                        "name": "name377",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency26",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency26",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name378"},
                    "withholdingTaxTotal": {
                        "name": "name379",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency27",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency27",
                            "content": 0
                        }
                    },
                    "taxTotal": {
                        "name": "name380",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency28",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency28",
                            "content": 0
                        }
                    }
                },
                "POTax": {
                    "Money": {
                        "currency": "currency29",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency29",
                        "content": 0
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type178",
                        "ShortName": "ShortName178"
                    },
                    "TaxDetail": {
                        "purpose": "purpose2",
                        "category": "category1",
                        "percentageRate": 0,
                        "taxRateType": "taxRateType1",
                        "isVatRecoverable": "yes",
                        "isWithholdingTax": "yes",
                        "taxPointDate": "2006-05-04T18:13:51.0",
                        "paymentDate": "2006-05-04T18:13:51.0",
                        "isTriangularTransaction": "yes",
                        "exemptDetail": "zeroRated",
                        "TaxableAmount": {
                            "Money": {
                                "currency": "currency30",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency30",
                                "content": 0
                            }
                        },
                        "TaxAmount": {
                            "Money": {
                                "currency": "currency31",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency31",
                                "content": 0
                            }
                        },
                        "TaxLocation": {
                            "xml:lang": "en-US",
                            "content": "TaxLocation1"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type179",
                            "ShortName": "ShortName179"
                        },
                        "TriangularTransactionLawReference": {
                            "xml:lang": "en-US",
                            "content": "TriangularTransactionLawReference1"
                        },
                        "TaxRegime": "TaxRegime1",
                        "TaxExemption": {
                            "exemptCode": "exemptCode1",
                            "ExemptReason": {
                                "xml:lang": "en-US",
                                "content": "ExemptReason1"
                            }
                        },
                        "withholdingTaxType": {
                            "name": "name381",
                            "schemaMappedType": "Extrinsic",
                            "content": "withholdingTaxType1"
                        },
                        "taxPointDateCode": {
                            "name": "name382",
                            "schemaMappedType": "Extrinsic",
                            "content": "taxPointDateCode1"
                        },
                        "Extrinsic": {"name": "name383"}
                    },
                    "OldMoney": {
                        "currency": "currency32",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency32",
                        "content": 0
                    },
                    "DeletedMoney": {
                        "currency": "currency33",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency33",
                        "content": 0
                    },
                    "taxationType": {
                        "name": "name384",
                        "schemaMappedType": "Extrinsic",
                        "content": "taxationType1"
                    },
                    "isISSRetention": {
                        "name": "name385",
                        "schemaMappedType": "Extrinsic",
                        "content": "isISSRetention1"
                    },
                    "isServiceBrokerISSRetention": {
                        "name": "name386",
                        "schemaMappedType": "Extrinsic",
                        "content": "isServiceBrokerISSRetention1"
                    },
                    "issRetentionValue": {
                        "name": "name387",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency34",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency34",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name388"},
                    "withholdingTaxTotal": {
                        "name": "name389",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency35",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency35",
                            "content": 0
                        }
                    },
                    "taxTotal": {
                        "name": "name390",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency36",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency36",
                            "content": 0
                        }
                    }
                },
                "RelatedInvoiceTax": {
                    "Money": {
                        "currency": "currency37",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency37",
                        "content": 0
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type180",
                        "ShortName": "ShortName180"
                    },
                    "TaxDetail": {
                        "purpose": "purpose3",
                        "category": "category2",
                        "percentageRate": 0,
                        "taxRateType": "taxRateType2",
                        "isVatRecoverable": "yes",
                        "isWithholdingTax": "yes",
                        "taxPointDate": "2006-05-04T18:13:51.0",
                        "paymentDate": "2006-05-04T18:13:51.0",
                        "isTriangularTransaction": "yes",
                        "exemptDetail": "zeroRated",
                        "TaxableAmount": {
                            "Money": {
                                "currency": "currency38",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency38",
                                "content": 0
                            }
                        },
                        "TaxAmount": {
                            "Money": {
                                "currency": "currency39",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency39",
                                "content": 0
                            }
                        },
                        "TaxLocation": {
                            "xml:lang": "en-US",
                            "content": "TaxLocation2"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type181",
                            "ShortName": "ShortName181"
                        },
                        "TriangularTransactionLawReference": {
                            "xml:lang": "en-US",
                            "content": "TriangularTransactionLawReference2"
                        },
                        "TaxRegime": "TaxRegime2",
                        "TaxExemption": {
                            "exemptCode": "exemptCode2",
                            "ExemptReason": {
                                "xml:lang": "en-US",
                                "content": "ExemptReason2"
                            }
                        },
                        "withholdingTaxType": {
                            "name": "name391",
                            "schemaMappedType": "Extrinsic",
                            "content": "withholdingTaxType2"
                        },
                        "taxPointDateCode": {
                            "name": "name392",
                            "schemaMappedType": "Extrinsic",
                            "content": "taxPointDateCode2"
                        },
                        "Extrinsic": {"name": "name393"}
                    },
                    "OldMoney": {
                        "currency": "currency40",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency40",
                        "content": 0
                    },
                    "DeletedMoney": {
                        "currency": "currency41",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency41",
                        "content": 0
                    },
                    "taxationType": {
                        "name": "name394",
                        "schemaMappedType": "Extrinsic",
                        "content": "taxationType2"
                    },
                    "isISSRetention": {
                        "name": "name395",
                        "schemaMappedType": "Extrinsic",
                        "content": "isISSRetention2"
                    },
                    "isServiceBrokerISSRetention": {
                        "name": "name396",
                        "schemaMappedType": "Extrinsic",
                        "content": "isServiceBrokerISSRetention2"
                    },
                    "issRetentionValue": {
                        "name": "name397",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency42",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency42",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name398"},
                    "withholdingTaxTotal": {
                        "name": "name399",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency43",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency43",
                            "content": 0
                        }
                    },
                    "taxTotal": {
                        "name": "name400",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency44",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency44",
                            "content": 0
                        }
                    }
                },
                "InvoiceDetailLineSpecialHandling": {
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type182",
                        "ShortName": "ShortName182"
                    },
                    "Money": {
                        "currency": "currency45",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency45",
                        "content": 0
                    }
                },
                "InvoiceDetailLineShipping": {
                    "InvoiceDetailShipping": {
                        "shippingDate": "2006-05-04T18:13:51.0",
                        "shipFrom": {
                            "role": "role38",
                            "addressID": "addressID38",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName19",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name38"
                            },
                            "PostalAddress": {
                                "name": "name401",
                                "DeliverTo": "DeliverTo38",
                                "Street": "Street38",
                                "City": {
                                    "cityCode": "cityCode38",
                                    "content": "City38"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode38",
                                    "content": "Municipality38"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode38",
                                    "content": "State38"
                                },
                                "PostalCode": "PostalCode38",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode93",
                                    "content": "Country39"
                                }
                            },
                            "Email": {
                                "name": "name402",
                                "content": "Email50"
                            },
                            "Phone": {
                                "name": "name403",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode94",
                                        "content": "CountryCode54"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode54",
                                    "Number": "Number54",
                                    "Extension": "Extension54"
                                }
                            },
                            "Fax": {
                                "name": "name404",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode95",
                                        "content": "CountryCode55"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode55",
                                    "Number": "Number55",
                                    "Extension": "Extension55"
                                }
                            },
                            "URL": {
                                "name": "name405",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role39",
                                "addressID": "addressID39",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name39"
                                },
                                "PostalAddress": {
                                    "name": "name406",
                                    "DeliverTo": "DeliverTo39",
                                    "Street": "Street39",
                                    "City": {
                                        "cityCode": "cityCode39",
                                        "content": "City39"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode39",
                                        "content": "Municipality39"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode39",
                                        "content": "State39"
                                    },
                                    "PostalCode": "PostalCode39",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode96",
                                        "content": "Country40"
                                    }
                                },
                                "Email": {
                                    "name": "name407",
                                    "content": "Email51"
                                },
                                "Phone": {
                                    "name": "name408",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode97",
                                            "content": "CountryCode56"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode56",
                                        "Number": "Number56",
                                        "Extension": "Extension56"
                                    }
                                },
                                "Fax": {
                                    "name": "name409",
                                    "Email": {
                                        "name": "name410",
                                        "content": "Email52"
                                    }
                                },
                                "URL": {
                                    "name": "name411",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier173",
                                "domain": "domain188",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator173"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type183",
                                    "ShortName": "ShortName183"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier174",
                                "domain": "domain189",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator174"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type184",
                                    "ShortName": "ShortName184"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier175",
                                "domain": "domain190",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator175"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type185",
                                    "ShortName": "ShortName185"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier176",
                                "domain": "domain191",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator176"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type186",
                                    "ShortName": "ShortName186"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier177",
                                "domain": "domain192",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator177"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type187",
                                    "ShortName": "ShortName187"
                                }
                            },
                            "isFactoring": {
                                "name": "name412",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name413"},
                            "LegalCapital": {
                                "name": "name414",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency46",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency46",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name415"}
                        },
                        "shipTo": {
                            "role": "role40",
                            "addressID": "addressID40",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName20",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name40"
                            },
                            "PostalAddress": {
                                "name": "name416",
                                "DeliverTo": "DeliverTo40",
                                "Street": "Street40",
                                "City": {
                                    "cityCode": "cityCode40",
                                    "content": "City40"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode40",
                                    "content": "Municipality40"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode40",
                                    "content": "State40"
                                },
                                "PostalCode": "PostalCode40",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode98",
                                    "content": "Country41"
                                }
                            },
                            "Email": {
                                "name": "name417",
                                "content": "Email53"
                            },
                            "Phone": {
                                "name": "name418",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode99",
                                        "content": "CountryCode57"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode57",
                                    "Number": "Number57",
                                    "Extension": "Extension57"
                                }
                            },
                            "Fax": {
                                "name": "name419",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode100",
                                        "content": "CountryCode58"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode58",
                                    "Number": "Number58",
                                    "Extension": "Extension58"
                                }
                            },
                            "URL": {
                                "name": "name420",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role41",
                                "addressID": "addressID41",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name41"
                                },
                                "PostalAddress": {
                                    "name": "name421",
                                    "DeliverTo": "DeliverTo41",
                                    "Street": "Street41",
                                    "City": {
                                        "cityCode": "cityCode41",
                                        "content": "City41"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode41",
                                        "content": "Municipality41"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode41",
                                        "content": "State41"
                                    },
                                    "PostalCode": "PostalCode41",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode101",
                                        "content": "Country42"
                                    }
                                },
                                "Email": {
                                    "name": "name422",
                                    "content": "Email54"
                                },
                                "Phone": {
                                    "name": "name423",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode102",
                                            "content": "CountryCode59"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode59",
                                        "Number": "Number59",
                                        "Extension": "Extension59"
                                    }
                                },
                                "Fax": {
                                    "name": "name424",
                                    "Email": {
                                        "name": "name425",
                                        "content": "Email55"
                                    }
                                },
                                "URL": {
                                    "name": "name426",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier178",
                                "domain": "domain193",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator178"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type188",
                                    "ShortName": "ShortName188"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier179",
                                "domain": "domain194",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator179"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type189",
                                    "ShortName": "ShortName189"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier180",
                                "domain": "domain195",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator180"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type190",
                                    "ShortName": "ShortName190"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier181",
                                "domain": "domain196",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator181"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type191",
                                    "ShortName": "ShortName191"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier182",
                                "domain": "domain197",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator182"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type192",
                                    "ShortName": "ShortName192"
                                }
                            },
                            "isFactoring": {
                                "name": "name427",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name428"},
                            "LegalCapital": {
                                "name": "name429",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency47",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency47",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name430"}
                        },
                        "Contact": {
                            "role": "role42",
                            "addressID": "addressID42",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName21",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name42"
                            },
                            "PostalAddress": {
                                "name": "name431",
                                "DeliverTo": "DeliverTo42",
                                "Street": "Street42",
                                "City": {
                                    "cityCode": "cityCode42",
                                    "content": "City42"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode42",
                                    "content": "Municipality42"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode42",
                                    "content": "State42"
                                },
                                "PostalCode": "PostalCode42",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode103",
                                    "content": "Country43"
                                }
                            },
                            "Email": {
                                "name": "name432",
                                "content": "Email56"
                            },
                            "Phone": {
                                "name": "name433",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode104",
                                        "content": "CountryCode60"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode60",
                                    "Number": "Number60",
                                    "Extension": "Extension60"
                                }
                            },
                            "Fax": {
                                "name": "name434",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode105",
                                        "content": "CountryCode61"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode61",
                                    "Number": "Number61",
                                    "Extension": "Extension61"
                                }
                            },
                            "URL": {
                                "name": "name435",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role43",
                                "addressID": "addressID43",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name43"
                                },
                                "PostalAddress": {
                                    "name": "name436",
                                    "DeliverTo": "DeliverTo43",
                                    "Street": "Street43",
                                    "City": {
                                        "cityCode": "cityCode43",
                                        "content": "City43"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode43",
                                        "content": "Municipality43"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode43",
                                        "content": "State43"
                                    },
                                    "PostalCode": "PostalCode43",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode106",
                                        "content": "Country44"
                                    }
                                },
                                "Email": {
                                    "name": "name437",
                                    "content": "Email57"
                                },
                                "Phone": {
                                    "name": "name438",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode107",
                                            "content": "CountryCode62"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode62",
                                        "Number": "Number62",
                                        "Extension": "Extension62"
                                    }
                                },
                                "Fax": {
                                    "name": "name439",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode108",
                                            "content": "CountryCode63"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode63",
                                        "Number": "Number63",
                                        "Extension": "Extension63"
                                    }
                                },
                                "URL": {
                                    "name": "name440",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier183",
                                "domain": "domain198",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator183"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type193",
                                    "ShortName": "ShortName193"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier184",
                                "domain": "domain199",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator184"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type194",
                                    "ShortName": "ShortName194"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier185",
                                "domain": "domain200",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator185"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type195",
                                    "ShortName": "ShortName195"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier186",
                                "domain": "domain201",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator186"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type196",
                                    "ShortName": "ShortName196"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier187",
                                "domain": "domain202",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator187"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type197",
                                    "ShortName": "ShortName197"
                                }
                            },
                            "isFactoring": {
                                "name": "name441",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name442"},
                            "LegalCapital": {
                                "name": "name443",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency48",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency48",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name444"}
                        },
                        "CarrierIdentifier": {
                            "domain": "domain203",
                            "content": "CarrierIdentifier1"
                        },
                        "ShipmentIdentifier": "ShipmentIdentifier1",
                        "DocumentReference": {"payloadID": "payloadID6"}
                    },
                    "Money": {
                        "currency": "currency49",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency49",
                        "content": 0
                    }
                },
                "ShipNoticeIDInfo": {
                    "shipNoticeID": "shipNoticeID3",
                    "shipNoticeDate": "2006-05-04T18:13:51.0",
                    "oldshipNoticeID": "oldshipNoticeID3",
                    "oldshipNoticeDate": "2006-05-04T18:13:51.0",
                    "IdReference": {
                        "identifier": "identifier188",
                        "domain": "domain204",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator188"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type198",
                            "ShortName": "ShortName198"
                        }
                    },
                    "deliveryNoteID": {
                        "identifier": "identifier189",
                        "domain": "domain205",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator189"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type199",
                            "ShortName": "ShortName199"
                        }
                    },
                    "deliveryNoteDate": {
                        "identifier": "identifier190",
                        "domain": "domain206",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator190"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type200",
                            "ShortName": "ShortName200"
                        }
                    },
                    "deliveryNoteLineItemNo": {
                        "identifier": "identifier191",
                        "domain": "domain207",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator191"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type201",
                            "ShortName": "ShortName201"
                        }
                    },
                    "dispatchAdviceID": {
                        "identifier": "identifier192",
                        "domain": "domain208",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator192"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type202",
                            "ShortName": "ShortName202"
                        }
                    },
                    "receivingAdviceID": {
                        "identifier": "identifier193",
                        "domain": "domain209",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator193"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type203",
                            "ShortName": "ShortName203"
                        }
                    },
                    "receivingAdviceDate": {
                        "identifier": "identifier194",
                        "domain": "domain210",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator194"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type204",
                            "ShortName": "ShortName204"
                        }
                    },
                    "transportDocumentID": {
                        "identifier": "identifier195",
                        "domain": "domain211",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator195"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type205",
                            "ShortName": "ShortName205"
                        }
                    },
                    "proofOfDeliveryID": {
                        "identifier": "identifier196",
                        "domain": "domain212",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator196"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type206",
                            "ShortName": "ShortName206"
                        }
                    },
                    "proofOfDeliveryDate": {
                        "identifier": "identifier197",
                        "domain": "domain213",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator197"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type207",
                            "ShortName": "ShortName207"
                        }
                    },
                    "actualDeliveryDate": {
                        "identifier": "identifier198",
                        "domain": "domain214",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator198"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type208",
                            "ShortName": "ShortName208"
                        }
                    },
                    "goodsPositioningDate": {
                        "identifier": "identifier199",
                        "domain": "domain215",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator199"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type209",
                            "ShortName": "ShortName209"
                        }
                    },
                    "goodsPositioningStartDate": {
                        "identifier": "identifier200",
                        "domain": "domain216",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator200"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type210",
                            "ShortName": "ShortName210"
                        }
                    },
                    "goodsPositioningEndDate": {
                        "identifier": "identifier201",
                        "domain": "domain217",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator201"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type211",
                            "ShortName": "ShortName211"
                        }
                    }
                },
                "GrossAmount": {
                    "Money": {
                        "currency": "currency50",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency50",
                        "content": 0
                    }
                },
                "InvoiceDetailDiscount": {
                    "percentageRate": 0,
                    "Money": {
                        "currency": "currency51",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency51",
                        "content": 0
                    }
                },
                "InvoiceItemModifications": {
                    "Modification": {
                        "level": 0,
                        "OriginalPrice": {
                            "Money": {
                                "currency": "currency52",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency52",
                                "content": 0
                            }
                        },
                        "AdditionalCost": {
                            "Percentage": {"percent": 0}
                        },
                        "Tax": {
                            "Money": {
                                "currency": "currency53",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency53",
                                "content": 0
                            },
                            "Description": {
                                "xml:lang": "en-US",
                                "type": "type212",
                                "ShortName": "ShortName212"
                            },
                            "TaxDetail": {
                                "purpose": "purpose4",
                                "category": "category3",
                                "percentageRate": 0,
                                "taxRateType": "taxRateType3",
                                "isVatRecoverable": "yes",
                                "isWithholdingTax": "yes",
                                "taxPointDate": "2006-05-04T18:13:51.0",
                                "paymentDate": "2006-05-04T18:13:51.0",
                                "isTriangularTransaction": "yes",
                                "exemptDetail": "zeroRated",
                                "TaxableAmount": {
                                    "Money": {
                                        "currency": "currency54",
                                        "alternateAmount": 0,
                                        "alternateCurrency": "alternateCurrency54",
                                        "content": 0
                                    }
                                },
                                "TaxAmount": {
                                    "Money": {
                                        "currency": "currency55",
                                        "alternateAmount": 0,
                                        "alternateCurrency": "alternateCurrency55",
                                        "content": 0
                                    }
                                },
                                "TaxLocation": {
                                    "xml:lang": "en-US",
                                    "content": "TaxLocation3"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type213",
                                    "ShortName": "ShortName213"
                                },
                                "TriangularTransactionLawReference": {
                                    "xml:lang": "en-US",
                                    "content": "TriangularTransactionLawReference3"
                                },
                                "TaxRegime": "TaxRegime3",
                                "TaxExemption": {
                                    "exemptCode": "exemptCode3",
                                    "ExemptReason": {
                                        "xml:lang": "en-US",
                                        "content": "ExemptReason3"
                                    }
                                },
                                "withholdingTaxType": {
                                    "name": "name445",
                                    "schemaMappedType": "Extrinsic",
                                    "content": "withholdingTaxType3"
                                },
                                "taxPointDateCode": {
                                    "name": "name446",
                                    "schemaMappedType": "Extrinsic",
                                    "content": "taxPointDateCode3"
                                },
                                "Extrinsic": {"name": "name447"}
                            },
                            "OldMoney": {
                                "currency": "currency56",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency56",
                                "content": 0
                            },
                            "DeletedMoney": {
                                "currency": "currency57",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency57",
                                "content": 0
                            },
                            "taxationType": {
                                "name": "name448",
                                "schemaMappedType": "Extrinsic",
                                "content": "taxationType3"
                            },
                            "isISSRetention": {
                                "name": "name449",
                                "schemaMappedType": "Extrinsic",
                                "content": "isISSRetention3"
                            },
                            "isServiceBrokerISSRetention": {
                                "name": "name450",
                                "schemaMappedType": "Extrinsic",
                                "content": "isServiceBrokerISSRetention3"
                            },
                            "issRetentionValue": {
                                "name": "name451",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency58",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency58",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name452"},
                            "withholdingTaxTotal": {
                                "name": "name453",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency59",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency59",
                                    "content": 0
                                }
                            },
                            "taxTotal": {
                                "name": "name454",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency60",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency60",
                                    "content": 0
                                }
                            }
                        },
                        "ModificationDetail": {
                            "name": "name455",
                            "code": "code9",
                            "startDate": "2006-05-04T18:13:51.0",
                            "endDate": "2006-05-04T18:13:51.0",
                            "Description": {
                                "xml:lang": "en-US",
                                "type": "type214",
                                "ShortName": "ShortName214"
                            },
                            "Extrinsic": {"name": "name456"}
                        }
                    }
                },
                "TotalCharges": {
                    "Money": {
                        "currency": "currency61",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency61",
                        "content": 0
                    }
                },
                "TotalAllowances": {
                    "Money": {
                        "currency": "currency62",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency62",
                        "content": 0
                    }
                },
                "TotalAmountWithoutTax": {
                    "Money": {
                        "currency": "currency63",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency63",
                        "content": 0
                    }
                },
                "TotalTaxOnModifications": {
                    "Money": {
                        "currency": "currency64",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency64",
                        "content": 0
                    }
                },
                "NetAmount": {
                    "Money": {
                        "currency": "currency65",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency65",
                        "content": 0
                    }
                },
                "distribution": {
                    "Accounting": {
                        "name": "name457",
                        "AccountingSegment": {
                            "id": "id0",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name44"
                            },
                            "Description": {
                                "xml:lang": "en-US",
                                "type": "type215",
                                "ShortName": "ShortName215"
                            }
                        }
                    },
                    "Charge": {
                        "Money": {
                            "currency": "currency66",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency66",
                            "content": 0
                        }
                    }
                },
                "Distribution": {
                    "Accounting": {
                        "name": "name458",
                        "Segment": {
                            "type": "type216",
                            "id": "id1",
                            "description": "description0"
                        }
                    },
                    "Charge": {
                        "Money": {
                            "currency": "currency67",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency67",
                            "content": 0
                        }
                    }
                },
                "Comments": {
                    "xml:lang": "en-US",
                    "Attachment": {
                        "URL": {
                            "name": "name459",
                            "content": "http://www.oxygenxml.com/"
                        }
                    }
                },
                "InvoiceLaborDetail": {
                    "Contractor": {
                        "ContractorIdentifier": {
                            "domain": "supplierReferenceID",
                            "content": "ContractorIdentifier0"
                        },
                        "Contact": {
                            "role": "role44",
                            "addressID": "addressID44",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName22",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name45"
                            },
                            "PostalAddress": {
                                "name": "name460",
                                "DeliverTo": "DeliverTo44",
                                "Street": "Street44",
                                "City": {
                                    "cityCode": "cityCode44",
                                    "content": "City44"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode44",
                                    "content": "Municipality44"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode44",
                                    "content": "State44"
                                },
                                "PostalCode": "PostalCode44",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode109",
                                    "content": "Country45"
                                }
                            },
                            "Email": {
                                "name": "name461",
                                "content": "Email58"
                            },
                            "Phone": {
                                "name": "name462",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode110",
                                        "content": "CountryCode64"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode64",
                                    "Number": "Number64",
                                    "Extension": "Extension64"
                                }
                            },
                            "Fax": {
                                "name": "name463",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode111",
                                        "content": "CountryCode65"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode65",
                                    "Number": "Number65",
                                    "Extension": "Extension65"
                                }
                            },
                            "URL": {
                                "name": "name464",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role45",
                                "addressID": "addressID45",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name46"
                                },
                                "PostalAddress": {
                                    "name": "name465",
                                    "DeliverTo": "DeliverTo45",
                                    "Street": "Street45",
                                    "City": {
                                        "cityCode": "cityCode45",
                                        "content": "City45"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode45",
                                        "content": "Municipality45"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode45",
                                        "content": "State45"
                                    },
                                    "PostalCode": "PostalCode45",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode112",
                                        "content": "Country46"
                                    }
                                },
                                "Email": {
                                    "name": "name466",
                                    "content": "Email59"
                                },
                                "Phone": {
                                    "name": "name467",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode113",
                                            "content": "CountryCode66"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode66",
                                        "Number": "Number66",
                                        "Extension": "Extension66"
                                    }
                                },
                                "Fax": {
                                    "name": "name468",
                                    "Email": {
                                        "name": "name469",
                                        "content": "Email60"
                                    }
                                },
                                "URL": {
                                    "name": "name470",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier202",
                                "domain": "domain219",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator202"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type217",
                                    "ShortName": "ShortName216"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier203",
                                "domain": "domain220",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator203"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type218",
                                    "ShortName": "ShortName217"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier204",
                                "domain": "domain221",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator204"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type219",
                                    "ShortName": "ShortName218"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier205",
                                "domain": "domain222",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator205"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type220",
                                    "ShortName": "ShortName219"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier206",
                                "domain": "domain223",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator206"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type221",
                                    "ShortName": "ShortName220"
                                }
                            },
                            "isFactoring": {
                                "name": "name471",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name472"},
                            "LegalCapital": {
                                "name": "name473",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency68",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency68",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name474"}
                        }
                    },
                    "JobDescription": {
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type222",
                            "ShortName": "ShortName221"
                        }
                    },
                    "Supervisor": {
                        "Contact": {
                            "role": "role46",
                            "addressID": "addressID46",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName23",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name47"
                            },
                            "PostalAddress": {
                                "name": "name475",
                                "DeliverTo": "DeliverTo46",
                                "Street": "Street46",
                                "City": {
                                    "cityCode": "cityCode46",
                                    "content": "City46"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode46",
                                    "content": "Municipality46"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode46",
                                    "content": "State46"
                                },
                                "PostalCode": "PostalCode46",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode114",
                                    "content": "Country47"
                                }
                            },
                            "Email": {
                                "name": "name476",
                                "content": "Email61"
                            },
                            "Phone": {
                                "name": "name477",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode115",
                                        "content": "CountryCode67"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode67",
                                    "Number": "Number67",
                                    "Extension": "Extension67"
                                }
                            },
                            "Fax": {
                                "name": "name478",
                                "Email": {
                                    "name": "name479",
                                    "content": "Email62"
                                }
                            },
                            "URL": {
                                "name": "name480",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role47",
                                "addressID": "addressID47",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name48"
                                },
                                "PostalAddress": {
                                    "name": "name481",
                                    "DeliverTo": "DeliverTo47",
                                    "Street": "Street47",
                                    "City": {
                                        "cityCode": "cityCode47",
                                        "content": "City47"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode47",
                                        "content": "Municipality47"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode47",
                                        "content": "State47"
                                    },
                                    "PostalCode": "PostalCode47",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode116",
                                        "content": "Country48"
                                    }
                                },
                                "Email": {
                                    "name": "name482",
                                    "content": "Email63"
                                },
                                "Phone": {
                                    "name": "name483",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode117",
                                            "content": "CountryCode68"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode68",
                                        "Number": "Number68",
                                        "Extension": "Extension68"
                                    }
                                },
                                "Fax": {
                                    "name": "name484",
                                    "Email": {
                                        "name": "name485",
                                        "content": "Email64"
                                    }
                                },
                                "URL": {
                                    "name": "name486",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier207",
                                "domain": "domain224",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator207"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type223",
                                    "ShortName": "ShortName222"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier208",
                                "domain": "domain225",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator208"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type224",
                                    "ShortName": "ShortName223"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier209",
                                "domain": "domain226",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator209"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type225",
                                    "ShortName": "ShortName224"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier210",
                                "domain": "domain227",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator210"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type226",
                                    "ShortName": "ShortName225"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier211",
                                "domain": "domain228",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator211"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type227",
                                    "ShortName": "ShortName226"
                                }
                            },
                            "isFactoring": {
                                "name": "name487",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name488"},
                            "LegalCapital": {
                                "name": "name489",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency69",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency69",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name490"}
                        }
                    },
                    "WorkLocation": {
                        "Address": {
                            "isoCountryCode": "isoCountryCode118",
                            "addressID": "addressID48",
                            "addressIDDomain": "addressIDDomain0",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name49"
                            },
                            "PostalAddress": {
                                "name": "name491",
                                "DeliverTo": "DeliverTo48",
                                "Street": "Street48",
                                "City": {
                                    "cityCode": "cityCode48",
                                    "content": "City48"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode48",
                                    "content": "Municipality48"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode48",
                                    "content": "State48"
                                },
                                "PostalCode": "PostalCode48",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode119",
                                    "content": "Country49"
                                }
                            },
                            "Email": {
                                "name": "name492",
                                "content": "Email65"
                            },
                            "Phone": {
                                "name": "name493",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode120",
                                        "content": "CountryCode69"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode69",
                                    "Number": "Number69",
                                    "Extension": "Extension69"
                                }
                            },
                            "Fax": {
                                "name": "name494",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode121",
                                        "content": "CountryCode70"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode70",
                                    "Number": "Number70",
                                    "Extension": "Extension70"
                                }
                            },
                            "URL": {
                                "name": "name495",
                                "content": "http://www.oxygenxml.com/"
                            }
                        }
                    },
                    "InvoiceTimeCardDetail": {
                        "TimeCardIDInfo": {"timeCardID": "timeCardID0"}
                    }
                },
                "punchinItemFromCatalog": {
                    "name": "name496",
                    "schemaMappedType": "Extrinsic",
                    "content": "punchinItemFromCatalog0"
                },
                "Extrinsic": {"name": "name497"},
                "IsShippingServiceItem": {
                    "name": "name498",
                    "schemaMappedType": "Extrinsic",
                    "content": "IsShippingServiceItem0"
                },
                "IsSpecialHandlingServiceItem": {
                    "name": "name499",
                    "schemaMappedType": "Extrinsic",
                    "content": "IsSpecialHandlingServiceItem0"
                },
                "isIncluded": false,
                "isFullyInvoiced": false,
                "isLineFromPO": {
                    "name": "name500",
                    "schemaMappedType": "Extrinsic",
                    "content": "isLineFromPO0"
                },
                "parentPOLineNumber": {
                    "name": "parentPOLineNumber",
                    "schemaMappedType": "Extrinsic",
                    "content": "parentPOLineNumber0"
                },
                "extLineNumber": {
                    "name": "extLineNumber",
                    "schemaMappedType": "Extrinsic",
                    "content": "extLineNumber0"
                },
                "parentExtLineNumber": {
                    "name": "parentExtLineNumber",
                    "schemaMappedType": "Extrinsic",
                    "content": "parentExtLineNumber0"
                }
            },
            "pureShippingLine": {
                "invoiceLineNumber": 0,
                "parentInvoiceLineNumber": 0,
                "quantity": 0,
                "inspectionDate": "2006-05-04T18:13:51.0",
                "referenceDate": "2006-05-04T18:13:51.0",
                "isAdHoc": "isAdHoc1",
                "InvoiceDetailItemReference": {
                    "lineNumber": 0,
                    "adhocLineNumber": 0,
                    "serialNumber": "serialNumber1",
                    "lineCopyTax": "lineCopyTax1",
                    "ItemID": {
                        "SupplierPartID": "SupplierPartID2",
                        "SupplierPartAuxiliaryID": "",
                        "OldSupplierPartID": "OldSupplierPartID2",
                        "OldSupplierPartAuxiliaryID": "",
                        "BuyerPartID": "BuyerPartID2",
                        "OldBuyerPartID": "OldBuyerPartID2"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type228",
                        "ShortName": "ShortName227"
                    },
                    "Classification": {
                        "domain": "domain229",
                        "content": "Classification5"
                    },
                    "ManufacturerPartID": "ManufacturerPartID1",
                    "ManufacturerName": {
                        "xml:lang": "en-US",
                        "content": "ManufacturerName1"
                    },
                    "Country": {
                        "isoCountryCode": "isoCountryCode122",
                        "content": "Country50"
                    },
                    "SerialNumber": "SerialNumber1",
                    "InvoiceDetailItemReferenceIndustry": {
                        "InvoiceDetailItemReferenceRetail": {
                            "EANID": "EANID1",
                            "EuropeanWasteCatalogID": "EuropeanWasteCatalogID1",
                            "Characteristic": {
                                "domain": "domain230",
                                "value": "value9",
                                "code": "code10"
                            },
                            "color": {
                                "domain": "domain231",
                                "value": "value10",
                                "code": "code11"
                            },
                            "colorCode": {
                                "domain": "domain232",
                                "value": "value11",
                                "code": "code12"
                            },
                            "size": {
                                "domain": "domain233",
                                "value": "value12",
                                "code": "code13"
                            },
                            "sizeCode": {
                                "domain": "domain234",
                                "value": "value13",
                                "code": "code14"
                            },
                            "quality": {
                                "domain": "domain235",
                                "value": "value14",
                                "code": "code15"
                            },
                            "qualityCode": {
                                "domain": "domain236",
                                "value": "value15",
                                "code": "code16"
                            },
                            "grade": {
                                "domain": "domain237",
                                "value": "value16",
                                "code": "code17"
                            },
                            "gradeCode": {
                                "domain": "domain238",
                                "value": "value17",
                                "code": "code18"
                            }
                        }
                    }
                },
                "InvoiceDetailServiceItemReference": {
                    "lineNumber": 0,
                    "adhocLineNumber": 0,
                    "Classification": {
                        "domain": "domain239",
                        "content": "Classification6"
                    },
                    "ItemID": {
                        "SupplierPartID": "SupplierPartID3",
                        "SupplierPartAuxiliaryID": "",
                        "OldSupplierPartID": "OldSupplierPartID3",
                        "OldSupplierPartAuxiliaryID": "",
                        "BuyerPartID": "BuyerPartID3",
                        "OldBuyerPartID": "OldBuyerPartID3"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type229",
                        "ShortName": "ShortName228"
                    }
                },
                "ServiceEntryItemReference": {
                    "serviceLineNumber": 0,
                    "serviceEntryID": "serviceEntryID1",
                    "serviceEntryDate": "2006-05-04T18:13:51.0",
                    "DocumentReference": {"payloadID": "payloadID7"}
                },
                "SubtotalAmount": {
                    "Money": {
                        "currency": "currency70",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency70",
                        "content": 0
                    }
                },
                "Period": {
                    "startDate": "2006-05-04T18:13:51.0",
                    "endDate": "2006-05-04T18:13:51.0"
                },
                "UnitOfMeasure": "UnitOfMeasure2",
                "UnitPrice": {
                    "Money": {
                        "currency": "currency71",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency71",
                        "content": 0
                    },
                    "OldMoney": {
                        "currency": "currency72",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency72",
                        "content": 0
                    },
                    "Modifications": {
                        "Modification": {
                            "level": 0,
                            "OriginalPrice": {
                                "Money": {
                                    "currency": "currency73",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency73",
                                    "content": 0
                                }
                            },
                            "AdditionalCost": {
                                "Money": {
                                    "currency": "currency74",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency74",
                                    "content": 0
                                }
                            },
                            "Tax": {
                                "Money": {
                                    "currency": "currency75",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency75",
                                    "content": 0
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type230",
                                    "ShortName": "ShortName229"
                                },
                                "TaxDetail": {
                                    "purpose": "purpose5",
                                    "category": "category4",
                                    "percentageRate": 0,
                                    "taxRateType": "taxRateType4",
                                    "isVatRecoverable": "yes",
                                    "isWithholdingTax": "yes",
                                    "taxPointDate": "2006-05-04T18:13:51.0",
                                    "paymentDate": "2006-05-04T18:13:51.0",
                                    "isTriangularTransaction": "yes",
                                    "exemptDetail": "zeroRated",
                                    "TaxableAmount": {
                                        "Money": {
                                            "currency": "currency76",
                                            "alternateAmount": 0,
                                            "alternateCurrency": "alternateCurrency76",
                                            "content": 0
                                        }
                                    },
                                    "TaxAmount": {
                                        "Money": {
                                            "currency": "currency77",
                                            "alternateAmount": 0,
                                            "alternateCurrency": "alternateCurrency77",
                                            "content": 0
                                        }
                                    },
                                    "TaxLocation": {
                                        "xml:lang": "en-US",
                                        "content": "TaxLocation4"
                                    },
                                    "Description": {
                                        "xml:lang": "en-US",
                                        "type": "type231",
                                        "ShortName": "ShortName230"
                                    },
                                    "TriangularTransactionLawReference": {
                                        "xml:lang": "en-US",
                                        "content": "TriangularTransactionLawReference4"
                                    },
                                    "TaxRegime": "TaxRegime4",
                                    "TaxExemption": {
                                        "exemptCode": "exemptCode4",
                                        "ExemptReason": {
                                            "xml:lang": "en-US",
                                            "content": "ExemptReason4"
                                        }
                                    },
                                    "withholdingTaxType": {
                                        "name": "name504",
                                        "schemaMappedType": "Extrinsic",
                                        "content": "withholdingTaxType4"
                                    },
                                    "taxPointDateCode": {
                                        "name": "name505",
                                        "schemaMappedType": "Extrinsic",
                                        "content": "taxPointDateCode4"
                                    },
                                    "Extrinsic": {"name": "name506"}
                                },
                                "OldMoney": {
                                    "currency": "currency78",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency78",
                                    "content": 0
                                },
                                "DeletedMoney": {
                                    "currency": "currency79",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency79",
                                    "content": 0
                                },
                                "taxationType": {
                                    "name": "name507",
                                    "schemaMappedType": "Extrinsic",
                                    "content": "taxationType4"
                                },
                                "isISSRetention": {
                                    "name": "name508",
                                    "schemaMappedType": "Extrinsic",
                                    "content": "isISSRetention4"
                                },
                                "isServiceBrokerISSRetention": {
                                    "name": "name509",
                                    "schemaMappedType": "Extrinsic",
                                    "content": "isServiceBrokerISSRetention4"
                                },
                                "issRetentionValue": {
                                    "name": "name510",
                                    "schemaMappedType": "Extrinsic",
                                    "Money": {
                                        "currency": "currency80",
                                        "alternateAmount": 0,
                                        "alternateCurrency": "alternateCurrency80",
                                        "content": 0
                                    }
                                },
                                "Extrinsic": {"name": "name511"},
                                "withholdingTaxTotal": {
                                    "name": "name512",
                                    "schemaMappedType": "Extrinsic",
                                    "Money": {
                                        "currency": "currency81",
                                        "alternateAmount": 0,
                                        "alternateCurrency": "alternateCurrency81",
                                        "content": 0
                                    }
                                },
                                "taxTotal": {
                                    "name": "name513",
                                    "schemaMappedType": "Extrinsic",
                                    "Money": {
                                        "currency": "currency82",
                                        "alternateAmount": 0,
                                        "alternateCurrency": "alternateCurrency82",
                                        "content": 0
                                    }
                                }
                            },
                            "ModificationDetail": {
                                "name": "name514",
                                "code": "code19",
                                "startDate": "2006-05-04T18:13:51.0",
                                "endDate": "2006-05-04T18:13:51.0",
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type232",
                                    "ShortName": "ShortName231"
                                },
                                "Extrinsic": {"name": "name515"}
                            }
                        }
                    }
                },
                "OriginalUnitPrice": {
                    "Money": {
                        "currency": "currency83",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency83",
                        "content": 0
                    }
                },
                "PriceBasisQuantity": {
                    "quantity": 0,
                    "oldquantity": 0,
                    "conversionFactor": 0,
                    "oldconversionFactor": 0,
                    "UnitOfMeasure": "UnitOfMeasure3",
                    "OldUnitOfMeasure": "OldUnitOfMeasure1",
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type233",
                        "ShortName": "ShortName232"
                    }
                },
                "Tax": {
                    "Money": {
                        "currency": "currency84",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency84",
                        "content": 0
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type234",
                        "ShortName": "ShortName233"
                    },
                    "TaxDetail": {
                        "purpose": "purpose6",
                        "category": "category5",
                        "percentageRate": 0,
                        "taxRateType": "taxRateType5",
                        "isVatRecoverable": "yes",
                        "isWithholdingTax": "yes",
                        "taxPointDate": "2006-05-04T18:13:51.0",
                        "paymentDate": "2006-05-04T18:13:51.0",
                        "isTriangularTransaction": "yes",
                        "exemptDetail": "zeroRated",
                        "TaxableAmount": {
                            "Money": {
                                "currency": "currency85",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency85",
                                "content": 0
                            }
                        },
                        "TaxAmount": {
                            "Money": {
                                "currency": "currency86",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency86",
                                "content": 0
                            }
                        },
                        "TaxLocation": {
                            "xml:lang": "en-US",
                            "content": "TaxLocation5"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type235",
                            "ShortName": "ShortName234"
                        },
                        "TriangularTransactionLawReference": {
                            "xml:lang": "en-US",
                            "content": "TriangularTransactionLawReference5"
                        },
                        "TaxRegime": "TaxRegime5",
                        "TaxExemption": {
                            "exemptCode": "exemptCode5",
                            "ExemptReason": {
                                "xml:lang": "en-US",
                                "content": "ExemptReason5"
                            }
                        },
                        "withholdingTaxType": {
                            "name": "name516",
                            "schemaMappedType": "Extrinsic",
                            "content": "withholdingTaxType5"
                        },
                        "taxPointDateCode": {
                            "name": "name517",
                            "schemaMappedType": "Extrinsic",
                            "content": "taxPointDateCode5"
                        },
                        "Extrinsic": {"name": "name518"}
                    },
                    "OldMoney": {
                        "currency": "currency87",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency87",
                        "content": 0
                    },
                    "DeletedMoney": {
                        "currency": "currency88",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency88",
                        "content": 0
                    },
                    "taxationType": {
                        "name": "name519",
                        "schemaMappedType": "Extrinsic",
                        "content": "taxationType5"
                    },
                    "isISSRetention": {
                        "name": "name520",
                        "schemaMappedType": "Extrinsic",
                        "content": "isISSRetention5"
                    },
                    "isServiceBrokerISSRetention": {
                        "name": "name521",
                        "schemaMappedType": "Extrinsic",
                        "content": "isServiceBrokerISSRetention5"
                    },
                    "issRetentionValue": {
                        "name": "name522",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency89",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency89",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name523"},
                    "withholdingTaxTotal": {
                        "name": "name524",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency90",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency90",
                            "content": 0
                        }
                    },
                    "taxTotal": {
                        "name": "name525",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency91",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency91",
                            "content": 0
                        }
                    }
                },
                "POTax": {
                    "Money": {
                        "currency": "currency92",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency92",
                        "content": 0
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type236",
                        "ShortName": "ShortName235"
                    },
                    "TaxDetail": {
                        "purpose": "purpose7",
                        "category": "category6",
                        "percentageRate": 0,
                        "taxRateType": "taxRateType6",
                        "isVatRecoverable": "yes",
                        "isWithholdingTax": "yes",
                        "taxPointDate": "2006-05-04T18:13:51.0",
                        "paymentDate": "2006-05-04T18:13:51.0",
                        "isTriangularTransaction": "yes",
                        "exemptDetail": "zeroRated",
                        "TaxableAmount": {
                            "Money": {
                                "currency": "currency93",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency93",
                                "content": 0
                            }
                        },
                        "TaxAmount": {
                            "Money": {
                                "currency": "currency94",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency94",
                                "content": 0
                            }
                        },
                        "TaxLocation": {
                            "xml:lang": "en-US",
                            "content": "TaxLocation6"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type237",
                            "ShortName": "ShortName236"
                        },
                        "TriangularTransactionLawReference": {
                            "xml:lang": "en-US",
                            "content": "TriangularTransactionLawReference6"
                        },
                        "TaxRegime": "TaxRegime6",
                        "TaxExemption": {
                            "exemptCode": "exemptCode6",
                            "ExemptReason": {
                                "xml:lang": "en-US",
                                "content": "ExemptReason6"
                            }
                        },
                        "withholdingTaxType": {
                            "name": "name526",
                            "schemaMappedType": "Extrinsic",
                            "content": "withholdingTaxType6"
                        },
                        "taxPointDateCode": {
                            "name": "name527",
                            "schemaMappedType": "Extrinsic",
                            "content": "taxPointDateCode6"
                        },
                        "Extrinsic": {"name": "name528"}
                    },
                    "OldMoney": {
                        "currency": "currency95",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency95",
                        "content": 0
                    },
                    "DeletedMoney": {
                        "currency": "currency96",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency96",
                        "content": 0
                    },
                    "taxationType": {
                        "name": "name529",
                        "schemaMappedType": "Extrinsic",
                        "content": "taxationType6"
                    },
                    "isISSRetention": {
                        "name": "name530",
                        "schemaMappedType": "Extrinsic",
                        "content": "isISSRetention6"
                    },
                    "isServiceBrokerISSRetention": {
                        "name": "name531",
                        "schemaMappedType": "Extrinsic",
                        "content": "isServiceBrokerISSRetention6"
                    },
                    "issRetentionValue": {
                        "name": "name532",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency97",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency97",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name533"},
                    "withholdingTaxTotal": {
                        "name": "name534",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency98",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency98",
                            "content": 0
                        }
                    },
                    "taxTotal": {
                        "name": "name535",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency99",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency99",
                            "content": 0
                        }
                    }
                },
                "RelatedInvoiceTax": {
                    "Money": {
                        "currency": "currency100",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency100",
                        "content": 0
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type238",
                        "ShortName": "ShortName237"
                    },
                    "TaxDetail": {
                        "purpose": "purpose8",
                        "category": "category7",
                        "percentageRate": 0,
                        "taxRateType": "taxRateType7",
                        "isVatRecoverable": "yes",
                        "isWithholdingTax": "yes",
                        "taxPointDate": "2006-05-04T18:13:51.0",
                        "paymentDate": "2006-05-04T18:13:51.0",
                        "isTriangularTransaction": "yes",
                        "exemptDetail": "zeroRated",
                        "TaxableAmount": {
                            "Money": {
                                "currency": "currency101",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency101",
                                "content": 0
                            }
                        },
                        "TaxAmount": {
                            "Money": {
                                "currency": "currency102",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency102",
                                "content": 0
                            }
                        },
                        "TaxLocation": {
                            "xml:lang": "en-US",
                            "content": "TaxLocation7"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type239",
                            "ShortName": "ShortName238"
                        },
                        "TriangularTransactionLawReference": {
                            "xml:lang": "en-US",
                            "content": "TriangularTransactionLawReference7"
                        },
                        "TaxRegime": "TaxRegime7",
                        "TaxExemption": {
                            "exemptCode": "exemptCode7",
                            "ExemptReason": {
                                "xml:lang": "en-US",
                                "content": "ExemptReason7"
                            }
                        },
                        "withholdingTaxType": {
                            "name": "name536",
                            "schemaMappedType": "Extrinsic",
                            "content": "withholdingTaxType7"
                        },
                        "taxPointDateCode": {
                            "name": "name537",
                            "schemaMappedType": "Extrinsic",
                            "content": "taxPointDateCode7"
                        },
                        "Extrinsic": {"name": "name538"}
                    },
                    "OldMoney": {
                        "currency": "currency103",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency103",
                        "content": 0
                    },
                    "DeletedMoney": {
                        "currency": "currency104",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency104",
                        "content": 0
                    },
                    "taxationType": {
                        "name": "name539",
                        "schemaMappedType": "Extrinsic",
                        "content": "taxationType7"
                    },
                    "isISSRetention": {
                        "name": "name540",
                        "schemaMappedType": "Extrinsic",
                        "content": "isISSRetention7"
                    },
                    "isServiceBrokerISSRetention": {
                        "name": "name541",
                        "schemaMappedType": "Extrinsic",
                        "content": "isServiceBrokerISSRetention7"
                    },
                    "issRetentionValue": {
                        "name": "name542",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency105",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency105",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name543"},
                    "withholdingTaxTotal": {
                        "name": "name544",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency106",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency106",
                            "content": 0
                        }
                    },
                    "taxTotal": {
                        "name": "name545",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency107",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency107",
                            "content": 0
                        }
                    }
                },
                "InvoiceDetailLineSpecialHandling": {
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type240",
                        "ShortName": "ShortName239"
                    },
                    "Money": {
                        "currency": "currency108",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency108",
                        "content": 0
                    }
                },
                "InvoiceDetailLineShipping": {
                    "InvoiceDetailShipping": {
                        "shippingDate": "2006-05-04T18:13:51.0",
                        "shipFrom": {
                            "role": "role48",
                            "addressID": "addressID49",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName24",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name50"
                            },
                            "PostalAddress": {
                                "name": "name546",
                                "DeliverTo": "DeliverTo49",
                                "Street": "Street49",
                                "City": {
                                    "cityCode": "cityCode49",
                                    "content": "City49"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode49",
                                    "content": "Municipality49"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode49",
                                    "content": "State49"
                                },
                                "PostalCode": "PostalCode49",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode123",
                                    "content": "Country51"
                                }
                            },
                            "Email": {
                                "name": "name547",
                                "content": "Email66"
                            },
                            "Phone": {
                                "name": "name548",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode124",
                                        "content": "CountryCode71"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode71",
                                    "Number": "Number71",
                                    "Extension": "Extension71"
                                }
                            },
                            "Fax": {
                                "name": "name549",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode125",
                                        "content": "CountryCode72"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode72",
                                    "Number": "Number72",
                                    "Extension": "Extension72"
                                }
                            },
                            "URL": {
                                "name": "name550",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role49",
                                "addressID": "addressID50",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name51"
                                },
                                "PostalAddress": {
                                    "name": "name551",
                                    "DeliverTo": "DeliverTo50",
                                    "Street": "Street50",
                                    "City": {
                                        "cityCode": "cityCode50",
                                        "content": "City50"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode50",
                                        "content": "Municipality50"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode50",
                                        "content": "State50"
                                    },
                                    "PostalCode": "PostalCode50",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode126",
                                        "content": "Country52"
                                    }
                                },
                                "Email": {
                                    "name": "name552",
                                    "content": "Email67"
                                },
                                "Phone": {
                                    "name": "name553",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode127",
                                            "content": "CountryCode73"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode73",
                                        "Number": "Number73",
                                        "Extension": "Extension73"
                                    }
                                },
                                "Fax": {
                                    "name": "name554",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode128",
                                            "content": "CountryCode74"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode74",
                                        "Number": "Number74",
                                        "Extension": "Extension74"
                                    }
                                },
                                "URL": {
                                    "name": "name555",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier212",
                                "domain": "domain240",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator212"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type241",
                                    "ShortName": "ShortName240"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier213",
                                "domain": "domain241",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator213"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type242",
                                    "ShortName": "ShortName241"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier214",
                                "domain": "domain242",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator214"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type243",
                                    "ShortName": "ShortName242"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier215",
                                "domain": "domain243",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator215"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type244",
                                    "ShortName": "ShortName243"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier216",
                                "domain": "domain244",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator216"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type245",
                                    "ShortName": "ShortName244"
                                }
                            },
                            "isFactoring": {
                                "name": "name556",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name557"},
                            "LegalCapital": {
                                "name": "name558",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency109",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency109",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name559"}
                        },
                        "shipTo": {
                            "role": "role50",
                            "addressID": "addressID51",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName25",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name52"
                            },
                            "PostalAddress": {
                                "name": "name560",
                                "DeliverTo": "DeliverTo51",
                                "Street": "Street51",
                                "City": {
                                    "cityCode": "cityCode51",
                                    "content": "City51"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode51",
                                    "content": "Municipality51"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode51",
                                    "content": "State51"
                                },
                                "PostalCode": "PostalCode51",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode129",
                                    "content": "Country53"
                                }
                            },
                            "Email": {
                                "name": "name561",
                                "content": "Email68"
                            },
                            "Phone": {
                                "name": "name562",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode130",
                                        "content": "CountryCode75"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode75",
                                    "Number": "Number75",
                                    "Extension": "Extension75"
                                }
                            },
                            "Fax": {
                                "name": "name563",
                                "Email": {
                                    "name": "name564",
                                    "content": "Email69"
                                }
                            },
                            "URL": {
                                "name": "name565",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role51",
                                "addressID": "addressID52",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name53"
                                },
                                "PostalAddress": {
                                    "name": "name566",
                                    "DeliverTo": "DeliverTo52",
                                    "Street": "Street52",
                                    "City": {
                                        "cityCode": "cityCode52",
                                        "content": "City52"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode52",
                                        "content": "Municipality52"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode52",
                                        "content": "State52"
                                    },
                                    "PostalCode": "PostalCode52",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode131",
                                        "content": "Country54"
                                    }
                                },
                                "Email": {
                                    "name": "name567",
                                    "content": "Email70"
                                },
                                "Phone": {
                                    "name": "name568",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode132",
                                            "content": "CountryCode76"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode76",
                                        "Number": "Number76",
                                        "Extension": "Extension76"
                                    }
                                },
                                "Fax": {
                                    "name": "name569",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode133",
                                            "content": "CountryCode77"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode77",
                                        "Number": "Number77",
                                        "Extension": "Extension77"
                                    }
                                },
                                "URL": {
                                    "name": "name570",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier217",
                                "domain": "domain245",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator217"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type246",
                                    "ShortName": "ShortName245"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier218",
                                "domain": "domain246",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator218"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type247",
                                    "ShortName": "ShortName246"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier219",
                                "domain": "domain247",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator219"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type248",
                                    "ShortName": "ShortName247"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier220",
                                "domain": "domain248",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator220"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type249",
                                    "ShortName": "ShortName248"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier221",
                                "domain": "domain249",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator221"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type250",
                                    "ShortName": "ShortName249"
                                }
                            },
                            "isFactoring": {
                                "name": "name571",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name572"},
                            "LegalCapital": {
                                "name": "name573",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency110",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency110",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name574"}
                        },
                        "Contact": {
                            "role": "role52",
                            "addressID": "addressID53",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName26",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name54"
                            },
                            "PostalAddress": {
                                "name": "name575",
                                "DeliverTo": "DeliverTo53",
                                "Street": "Street53",
                                "City": {
                                    "cityCode": "cityCode53",
                                    "content": "City53"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode53",
                                    "content": "Municipality53"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode53",
                                    "content": "State53"
                                },
                                "PostalCode": "PostalCode53",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode134",
                                    "content": "Country55"
                                }
                            },
                            "Email": {
                                "name": "name576",
                                "content": "Email71"
                            },
                            "Phone": {
                                "name": "name577",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode135",
                                        "content": "CountryCode78"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode78",
                                    "Number": "Number78",
                                    "Extension": "Extension78"
                                }
                            },
                            "Fax": {
                                "name": "name578",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode136",
                                        "content": "CountryCode79"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode79",
                                    "Number": "Number79",
                                    "Extension": "Extension79"
                                }
                            },
                            "URL": {
                                "name": "name579",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role53",
                                "addressID": "addressID54",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name55"
                                },
                                "PostalAddress": {
                                    "name": "name580",
                                    "DeliverTo": "DeliverTo54",
                                    "Street": "Street54",
                                    "City": {
                                        "cityCode": "cityCode54",
                                        "content": "City54"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode54",
                                        "content": "Municipality54"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode54",
                                        "content": "State54"
                                    },
                                    "PostalCode": "PostalCode54",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode137",
                                        "content": "Country56"
                                    }
                                },
                                "Email": {
                                    "name": "name581",
                                    "content": "Email72"
                                },
                                "Phone": {
                                    "name": "name582",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode138",
                                            "content": "CountryCode80"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode80",
                                        "Number": "Number80",
                                        "Extension": "Extension80"
                                    }
                                },
                                "Fax": {
                                    "name": "name583",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode139",
                                            "content": "CountryCode81"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode81",
                                        "Number": "Number81",
                                        "Extension": "Extension81"
                                    }
                                },
                                "URL": {
                                    "name": "name584",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier222",
                                "domain": "domain250",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator222"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type251",
                                    "ShortName": "ShortName250"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier223",
                                "domain": "domain251",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator223"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type252",
                                    "ShortName": "ShortName251"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier224",
                                "domain": "domain252",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator224"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type253",
                                    "ShortName": "ShortName252"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier225",
                                "domain": "domain253",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator225"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type254",
                                    "ShortName": "ShortName253"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier226",
                                "domain": "domain254",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator226"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type255",
                                    "ShortName": "ShortName254"
                                }
                            },
                            "isFactoring": {
                                "name": "name585",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name586"},
                            "LegalCapital": {
                                "name": "name587",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency111",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency111",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name588"}
                        },
                        "CarrierIdentifier": {
                            "domain": "domain255",
                            "content": "CarrierIdentifier2"
                        },
                        "ShipmentIdentifier": "ShipmentIdentifier2",
                        "DocumentReference": {"payloadID": "payloadID8"}
                    },
                    "Money": {
                        "currency": "currency112",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency112",
                        "content": 0
                    }
                },
                "ShipNoticeIDInfo": {
                    "shipNoticeID": "shipNoticeID4",
                    "shipNoticeDate": "2006-05-04T18:13:51.0",
                    "oldshipNoticeID": "oldshipNoticeID4",
                    "oldshipNoticeDate": "2006-05-04T18:13:51.0",
                    "IdReference": {
                        "identifier": "identifier227",
                        "domain": "domain256",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator227"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type256",
                            "ShortName": "ShortName255"
                        }
                    },
                    "deliveryNoteID": {
                        "identifier": "identifier228",
                        "domain": "domain257",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator228"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type257",
                            "ShortName": "ShortName256"
                        }
                    },
                    "deliveryNoteDate": {
                        "identifier": "identifier229",
                        "domain": "domain258",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator229"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type258",
                            "ShortName": "ShortName257"
                        }
                    },
                    "deliveryNoteLineItemNo": {
                        "identifier": "identifier230",
                        "domain": "domain259",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator230"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type259",
                            "ShortName": "ShortName258"
                        }
                    },
                    "dispatchAdviceID": {
                        "identifier": "identifier231",
                        "domain": "domain260",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator231"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type260",
                            "ShortName": "ShortName259"
                        }
                    },
                    "receivingAdviceID": {
                        "identifier": "identifier232",
                        "domain": "domain261",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator232"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type261",
                            "ShortName": "ShortName260"
                        }
                    },
                    "receivingAdviceDate": {
                        "identifier": "identifier233",
                        "domain": "domain262",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator233"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type262",
                            "ShortName": "ShortName261"
                        }
                    },
                    "transportDocumentID": {
                        "identifier": "identifier234",
                        "domain": "domain263",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator234"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type263",
                            "ShortName": "ShortName262"
                        }
                    },
                    "proofOfDeliveryID": {
                        "identifier": "identifier235",
                        "domain": "domain264",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator235"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type264",
                            "ShortName": "ShortName263"
                        }
                    },
                    "proofOfDeliveryDate": {
                        "identifier": "identifier236",
                        "domain": "domain265",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator236"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type265",
                            "ShortName": "ShortName264"
                        }
                    },
                    "actualDeliveryDate": {
                        "identifier": "identifier237",
                        "domain": "domain266",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator237"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type266",
                            "ShortName": "ShortName265"
                        }
                    },
                    "goodsPositioningDate": {
                        "identifier": "identifier238",
                        "domain": "domain267",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator238"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type267",
                            "ShortName": "ShortName266"
                        }
                    },
                    "goodsPositioningStartDate": {
                        "identifier": "identifier239",
                        "domain": "domain268",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator239"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type268",
                            "ShortName": "ShortName267"
                        }
                    },
                    "goodsPositioningEndDate": {
                        "identifier": "identifier240",
                        "domain": "domain269",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator240"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type269",
                            "ShortName": "ShortName268"
                        }
                    }
                },
                "GrossAmount": {
                    "Money": {
                        "currency": "currency113",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency113",
                        "content": 0
                    }
                },
                "InvoiceDetailDiscount": {
                    "percentageRate": 0,
                    "Money": {
                        "currency": "currency114",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency114",
                        "content": 0
                    }
                },
                "InvoiceItemModifications": {
                    "Modification": {
                        "level": 0,
                        "OriginalPrice": {
                            "Money": {
                                "currency": "currency115",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency115",
                                "content": 0
                            }
                        },
                        "AdditionalCost": {
                            "Money": {
                                "currency": "currency116",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency116",
                                "content": 0
                            }
                        },
                        "Tax": {
                            "Money": {
                                "currency": "currency117",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency117",
                                "content": 0
                            },
                            "Description": {
                                "xml:lang": "en-US",
                                "type": "type270",
                                "ShortName": "ShortName269"
                            },
                            "TaxDetail": {
                                "purpose": "purpose9",
                                "category": "category8",
                                "percentageRate": 0,
                                "taxRateType": "taxRateType8",
                                "isVatRecoverable": "yes",
                                "isWithholdingTax": "yes",
                                "taxPointDate": "2006-05-04T18:13:51.0",
                                "paymentDate": "2006-05-04T18:13:51.0",
                                "isTriangularTransaction": "yes",
                                "exemptDetail": "zeroRated",
                                "TaxableAmount": {
                                    "Money": {
                                        "currency": "currency118",
                                        "alternateAmount": 0,
                                        "alternateCurrency": "alternateCurrency118",
                                        "content": 0
                                    }
                                },
                                "TaxAmount": {
                                    "Money": {
                                        "currency": "currency119",
                                        "alternateAmount": 0,
                                        "alternateCurrency": "alternateCurrency119",
                                        "content": 0
                                    }
                                },
                                "TaxLocation": {
                                    "xml:lang": "en-US",
                                    "content": "TaxLocation8"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type271",
                                    "ShortName": "ShortName270"
                                },
                                "TriangularTransactionLawReference": {
                                    "xml:lang": "en-US",
                                    "content": "TriangularTransactionLawReference8"
                                },
                                "TaxRegime": "TaxRegime8",
                                "TaxExemption": {
                                    "exemptCode": "exemptCode8",
                                    "ExemptReason": {
                                        "xml:lang": "en-US",
                                        "content": "ExemptReason8"
                                    }
                                },
                                "withholdingTaxType": {
                                    "name": "name589",
                                    "schemaMappedType": "Extrinsic",
                                    "content": "withholdingTaxType8"
                                },
                                "taxPointDateCode": {
                                    "name": "name590",
                                    "schemaMappedType": "Extrinsic",
                                    "content": "taxPointDateCode8"
                                },
                                "Extrinsic": {"name": "name591"}
                            },
                            "OldMoney": {
                                "currency": "currency120",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency120",
                                "content": 0
                            },
                            "DeletedMoney": {
                                "currency": "currency121",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency121",
                                "content": 0
                            },
                            "taxationType": {
                                "name": "name592",
                                "schemaMappedType": "Extrinsic",
                                "content": "taxationType8"
                            },
                            "isISSRetention": {
                                "name": "name593",
                                "schemaMappedType": "Extrinsic",
                                "content": "isISSRetention8"
                            },
                            "isServiceBrokerISSRetention": {
                                "name": "name594",
                                "schemaMappedType": "Extrinsic",
                                "content": "isServiceBrokerISSRetention8"
                            },
                            "issRetentionValue": {
                                "name": "name595",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency122",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency122",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name596"},
                            "withholdingTaxTotal": {
                                "name": "name597",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency123",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency123",
                                    "content": 0
                                }
                            },
                            "taxTotal": {
                                "name": "name598",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency124",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency124",
                                    "content": 0
                                }
                            }
                        },
                        "ModificationDetail": {
                            "name": "name599",
                            "code": "code20",
                            "startDate": "2006-05-04T18:13:51.0",
                            "endDate": "2006-05-04T18:13:51.0",
                            "Description": {
                                "xml:lang": "en-US",
                                "type": "type272",
                                "ShortName": "ShortName271"
                            },
                            "Extrinsic": {"name": "name600"}
                        }
                    }
                },
                "TotalCharges": {
                    "Money": {
                        "currency": "currency125",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency125",
                        "content": 0
                    }
                },
                "TotalAllowances": {
                    "Money": {
                        "currency": "currency126",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency126",
                        "content": 0
                    }
                },
                "TotalAmountWithoutTax": {
                    "Money": {
                        "currency": "currency127",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency127",
                        "content": 0
                    }
                },
                "TotalTaxOnModifications": {
                    "Money": {
                        "currency": "currency128",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency128",
                        "content": 0
                    }
                },
                "NetAmount": {
                    "Money": {
                        "currency": "currency129",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency129",
                        "content": 0
                    }
                },
                "distribution": {
                    "Accounting": {
                        "name": "name601",
                        "AccountingSegment": {
                            "id": "id2",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name56"
                            },
                            "Description": {
                                "xml:lang": "en-US",
                                "type": "type273",
                                "ShortName": "ShortName272"
                            }
                        }
                    },
                    "Charge": {
                        "Money": {
                            "currency": "currency130",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency130",
                            "content": 0
                        }
                    }
                },
                "Distribution": {
                    "Accounting": {
                        "name": "name602",
                        "Segment": {
                            "type": "type274",
                            "id": "id3",
                            "description": "description1"
                        }
                    },
                    "Charge": {
                        "Money": {
                            "currency": "currency131",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency131",
                            "content": 0
                        }
                    }
                },
                "Comments": {
                    "xml:lang": "en-US",
                    "Attachment": {
                        "URL": {
                            "name": "name603",
                            "content": "http://www.oxygenxml.com/"
                        }
                    }
                },
                "InvoiceLaborDetail": {
                    "Contractor": {
                        "ContractorIdentifier": {
                            "domain": "supplierReferenceID",
                            "content": "ContractorIdentifier1"
                        },
                        "Contact": {
                            "role": "role54",
                            "addressID": "addressID55",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName27",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name57"
                            },
                            "PostalAddress": {
                                "name": "name604",
                                "DeliverTo": "DeliverTo55",
                                "Street": "Street55",
                                "City": {
                                    "cityCode": "cityCode55",
                                    "content": "City55"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode55",
                                    "content": "Municipality55"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode55",
                                    "content": "State55"
                                },
                                "PostalCode": "PostalCode55",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode140",
                                    "content": "Country57"
                                }
                            },
                            "Email": {
                                "name": "name605",
                                "content": "Email73"
                            },
                            "Phone": {
                                "name": "name606",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode141",
                                        "content": "CountryCode82"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode82",
                                    "Number": "Number82",
                                    "Extension": "Extension82"
                                }
                            },
                            "Fax": {
                                "name": "name607",
                                "Email": {
                                    "name": "name608",
                                    "content": "Email74"
                                }
                            },
                            "URL": {
                                "name": "name609",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role55",
                                "addressID": "addressID56",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name58"
                                },
                                "PostalAddress": {
                                    "name": "name610",
                                    "DeliverTo": "DeliverTo56",
                                    "Street": "Street56",
                                    "City": {
                                        "cityCode": "cityCode56",
                                        "content": "City56"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode56",
                                        "content": "Municipality56"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode56",
                                        "content": "State56"
                                    },
                                    "PostalCode": "PostalCode56",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode142",
                                        "content": "Country58"
                                    }
                                },
                                "Email": {
                                    "name": "name611",
                                    "content": "Email75"
                                },
                                "Phone": {
                                    "name": "name612",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode143",
                                            "content": "CountryCode83"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode83",
                                        "Number": "Number83",
                                        "Extension": "Extension83"
                                    }
                                },
                                "Fax": {
                                    "name": "name613",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode144",
                                            "content": "CountryCode84"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode84",
                                        "Number": "Number84",
                                        "Extension": "Extension84"
                                    }
                                },
                                "URL": {
                                    "name": "name614",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier241",
                                "domain": "domain271",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator241"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type275",
                                    "ShortName": "ShortName273"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier242",
                                "domain": "domain272",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator242"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type276",
                                    "ShortName": "ShortName274"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier243",
                                "domain": "domain273",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator243"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type277",
                                    "ShortName": "ShortName275"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier244",
                                "domain": "domain274",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator244"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type278",
                                    "ShortName": "ShortName276"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier245",
                                "domain": "domain275",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator245"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type279",
                                    "ShortName": "ShortName277"
                                }
                            },
                            "isFactoring": {
                                "name": "name615",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name616"},
                            "LegalCapital": {
                                "name": "name617",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency132",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency132",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name618"}
                        }
                    },
                    "JobDescription": {
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type280",
                            "ShortName": "ShortName278"
                        }
                    },
                    "Supervisor": {
                        "Contact": {
                            "role": "role56",
                            "addressID": "addressID57",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName28",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name59"
                            },
                            "PostalAddress": {
                                "name": "name619",
                                "DeliverTo": "DeliverTo57",
                                "Street": "Street57",
                                "City": {
                                    "cityCode": "cityCode57",
                                    "content": "City57"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode57",
                                    "content": "Municipality57"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode57",
                                    "content": "State57"
                                },
                                "PostalCode": "PostalCode57",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode145",
                                    "content": "Country59"
                                }
                            },
                            "Email": {
                                "name": "name620",
                                "content": "Email76"
                            },
                            "Phone": {
                                "name": "name621",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode146",
                                        "content": "CountryCode85"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode85",
                                    "Number": "Number85",
                                    "Extension": "Extension85"
                                }
                            },
                            "Fax": {
                                "name": "name622",
                                "Email": {
                                    "name": "name623",
                                    "content": "Email77"
                                }
                            },
                            "URL": {
                                "name": "name624",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role57",
                                "addressID": "addressID58",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name60"
                                },
                                "PostalAddress": {
                                    "name": "name625",
                                    "DeliverTo": "DeliverTo58",
                                    "Street": "Street58",
                                    "City": {
                                        "cityCode": "cityCode58",
                                        "content": "City58"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode58",
                                        "content": "Municipality58"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode58",
                                        "content": "State58"
                                    },
                                    "PostalCode": "PostalCode58",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode147",
                                        "content": "Country60"
                                    }
                                },
                                "Email": {
                                    "name": "name626",
                                    "content": "Email78"
                                },
                                "Phone": {
                                    "name": "name627",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode148",
                                            "content": "CountryCode86"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode86",
                                        "Number": "Number86",
                                        "Extension": "Extension86"
                                    }
                                },
                                "Fax": {
                                    "name": "name628",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode149",
                                            "content": "CountryCode87"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode87",
                                        "Number": "Number87",
                                        "Extension": "Extension87"
                                    }
                                },
                                "URL": {
                                    "name": "name629",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier246",
                                "domain": "domain276",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator246"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type281",
                                    "ShortName": "ShortName279"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier247",
                                "domain": "domain277",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator247"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type282",
                                    "ShortName": "ShortName280"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier248",
                                "domain": "domain278",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator248"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type283",
                                    "ShortName": "ShortName281"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier249",
                                "domain": "domain279",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator249"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type284",
                                    "ShortName": "ShortName282"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier250",
                                "domain": "domain280",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator250"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type285",
                                    "ShortName": "ShortName283"
                                }
                            },
                            "isFactoring": {
                                "name": "name630",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name631"},
                            "LegalCapital": {
                                "name": "name632",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency133",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency133",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name633"}
                        }
                    },
                    "WorkLocation": {
                        "Address": {
                            "isoCountryCode": "isoCountryCode150",
                            "addressID": "addressID59",
                            "addressIDDomain": "addressIDDomain1",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name61"
                            },
                            "PostalAddress": {
                                "name": "name634",
                                "DeliverTo": "DeliverTo59",
                                "Street": "Street59",
                                "City": {
                                    "cityCode": "cityCode59",
                                    "content": "City59"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode59",
                                    "content": "Municipality59"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode59",
                                    "content": "State59"
                                },
                                "PostalCode": "PostalCode59",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode151",
                                    "content": "Country61"
                                }
                            },
                            "Email": {
                                "name": "name635",
                                "content": "Email79"
                            },
                            "Phone": {
                                "name": "name636",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode152",
                                        "content": "CountryCode88"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode88",
                                    "Number": "Number88",
                                    "Extension": "Extension88"
                                }
                            },
                            "Fax": {
                                "name": "name637",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode153",
                                        "content": "CountryCode89"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode89",
                                    "Number": "Number89",
                                    "Extension": "Extension89"
                                }
                            },
                            "URL": {
                                "name": "name638",
                                "content": "http://www.oxygenxml.com/"
                            }
                        }
                    },
                    "InvoiceTimeCardDetail": {
                        "TimeCardIDInfo": {"timeCardID": "timeCardID1"}
                    }
                },
                "punchinItemFromCatalog": {
                    "name": "name639",
                    "schemaMappedType": "Extrinsic",
                    "content": "punchinItemFromCatalog1"
                },
                "Extrinsic": {"name": "name640"},
                "IsShippingServiceItem": {
                    "name": "name641",
                    "schemaMappedType": "Extrinsic",
                    "content": "IsShippingServiceItem1"
                },
                "IsSpecialHandlingServiceItem": {
                    "name": "name642",
                    "schemaMappedType": "Extrinsic",
                    "content": "IsSpecialHandlingServiceItem1"
                },
                "isIncluded": false,
                "isFullyInvoiced": false,
                "isLineFromPO": {
                    "name": "name643",
                    "schemaMappedType": "Extrinsic",
                    "content": "isLineFromPO1"
                },
                "parentPOLineNumber": {
                    "name": "parentPOLineNumber",
                    "schemaMappedType": "Extrinsic",
                    "content": "parentPOLineNumber1"
                },
                "extLineNumber": {
                    "name": "extLineNumber",
                    "schemaMappedType": "Extrinsic",
                    "content": "extLineNumber1"
                },
                "parentExtLineNumber": {
                    "name": "parentExtLineNumber",
                    "schemaMappedType": "Extrinsic",
                    "content": "parentExtLineNumber1"
                }
            },
            "pureSpecialHandlingLine": {
                "invoiceLineNumber": 0,
                "parentInvoiceLineNumber": 0,
                "quantity": 0,
                "inspectionDate": "2006-05-04T18:13:51.0",
                "referenceDate": "2006-05-04T18:13:51.0",
                "isAdHoc": "isAdHoc2",
                "InvoiceDetailItemReference": {
                    "lineNumber": 0,
                    "adhocLineNumber": 0,
                    "serialNumber": "serialNumber2",
                    "lineCopyTax": "lineCopyTax2",
                    "ItemID": {
                        "SupplierPartID": "SupplierPartID4",
                        "SupplierPartAuxiliaryID": "",
                        "OldSupplierPartID": "OldSupplierPartID4",
                        "OldSupplierPartAuxiliaryID": "",
                        "BuyerPartID": "BuyerPartID4",
                        "OldBuyerPartID": "OldBuyerPartID4"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type286",
                        "ShortName": "ShortName284"
                    },
                    "Classification": {
                        "domain": "domain281",
                        "content": "Classification7"
                    },
                    "ManufacturerPartID": "ManufacturerPartID2",
                    "ManufacturerName": {
                        "xml:lang": "en-US",
                        "content": "ManufacturerName2"
                    },
                    "Country": {
                        "isoCountryCode": "isoCountryCode154",
                        "content": "Country62"
                    },
                    "SerialNumber": "SerialNumber2",
                    "InvoiceDetailItemReferenceIndustry": {
                        "InvoiceDetailItemReferenceRetail": {
                            "EANID": "EANID2",
                            "EuropeanWasteCatalogID": "EuropeanWasteCatalogID2",
                            "Characteristic": {
                                "domain": "domain282",
                                "value": "value18",
                                "code": "code21"
                            },
                            "color": {
                                "domain": "domain283",
                                "value": "value19",
                                "code": "code22"
                            },
                            "colorCode": {
                                "domain": "domain284",
                                "value": "value20",
                                "code": "code23"
                            },
                            "size": {
                                "domain": "domain285",
                                "value": "value21",
                                "code": "code24"
                            },
                            "sizeCode": {
                                "domain": "domain286",
                                "value": "value22",
                                "code": "code25"
                            },
                            "quality": {
                                "domain": "domain287",
                                "value": "value23",
                                "code": "code26"
                            },
                            "qualityCode": {
                                "domain": "domain288",
                                "value": "value24",
                                "code": "code27"
                            },
                            "grade": {
                                "domain": "domain289",
                                "value": "value25",
                                "code": "code28"
                            },
                            "gradeCode": {
                                "domain": "domain290",
                                "value": "value26",
                                "code": "code29"
                            }
                        }
                    }
                },
                "InvoiceDetailServiceItemReference": {
                    "lineNumber": 0,
                    "adhocLineNumber": 0,
                    "Classification": {
                        "domain": "domain291",
                        "content": "Classification8"
                    },
                    "ItemID": {
                        "SupplierPartID": "SupplierPartID5",
                        "SupplierPartAuxiliaryID": "",
                        "OldSupplierPartID": "OldSupplierPartID5",
                        "OldSupplierPartAuxiliaryID": "",
                        "BuyerPartID": "BuyerPartID5",
                        "OldBuyerPartID": "OldBuyerPartID5"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type287",
                        "ShortName": "ShortName285"
                    }
                },
                "ServiceEntryItemIDInfo": {
                    "serviceLineNumber": 0,
                    "serviceEntryID": "serviceEntryID2",
                    "serviceEntryDate": "2006-05-04T18:13:51.0",
                    "IdReference": {
                        "identifier": "identifier251",
                        "domain": "domain292",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator251"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type288",
                            "ShortName": "ShortName286"
                        }
                    }
                },
                "SubtotalAmount": {
                    "Money": {
                        "currency": "currency134",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency134",
                        "content": 0
                    }
                },
                "Period": {
                    "startDate": "2006-05-04T18:13:51.0",
                    "endDate": "2006-05-04T18:13:51.0"
                },
                "UnitRate": {
                    "Money": {
                        "currency": "currency135",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency135",
                        "content": 0
                    },
                    "UnitOfMeasure": "UnitOfMeasure4",
                    "PriceBasisQuantity": {
                        "quantity": 0,
                        "oldquantity": 0,
                        "conversionFactor": 0,
                        "oldconversionFactor": 0,
                        "UnitOfMeasure": "UnitOfMeasure5",
                        "OldUnitOfMeasure": "OldUnitOfMeasure2",
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type289",
                            "ShortName": "ShortName287"
                        }
                    },
                    "TermReference": {
                        "termName": "#any",
                        "term": "#any"
                    }
                },
                "Tax": {
                    "Money": {
                        "currency": "currency136",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency136",
                        "content": 0
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type290",
                        "ShortName": "ShortName288"
                    },
                    "TaxDetail": {
                        "purpose": "purpose10",
                        "category": "category9",
                        "percentageRate": 0,
                        "taxRateType": "taxRateType9",
                        "isVatRecoverable": "yes",
                        "isWithholdingTax": "yes",
                        "taxPointDate": "2006-05-04T18:13:51.0",
                        "paymentDate": "2006-05-04T18:13:51.0",
                        "isTriangularTransaction": "yes",
                        "exemptDetail": "zeroRated",
                        "TaxableAmount": {
                            "Money": {
                                "currency": "currency137",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency137",
                                "content": 0
                            }
                        },
                        "TaxAmount": {
                            "Money": {
                                "currency": "currency138",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency138",
                                "content": 0
                            }
                        },
                        "TaxLocation": {
                            "xml:lang": "en-US",
                            "content": "TaxLocation9"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type291",
                            "ShortName": "ShortName289"
                        },
                        "TriangularTransactionLawReference": {
                            "xml:lang": "en-US",
                            "content": "TriangularTransactionLawReference9"
                        },
                        "TaxRegime": "TaxRegime9",
                        "TaxExemption": {
                            "exemptCode": "exemptCode9",
                            "ExemptReason": {
                                "xml:lang": "en-US",
                                "content": "ExemptReason9"
                            }
                        },
                        "withholdingTaxType": {
                            "name": "name647",
                            "schemaMappedType": "Extrinsic",
                            "content": "withholdingTaxType9"
                        },
                        "taxPointDateCode": {
                            "name": "name648",
                            "schemaMappedType": "Extrinsic",
                            "content": "taxPointDateCode9"
                        },
                        "Extrinsic": {"name": "name649"}
                    },
                    "OldMoney": {
                        "currency": "currency139",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency139",
                        "content": 0
                    },
                    "DeletedMoney": {
                        "currency": "currency140",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency140",
                        "content": 0
                    },
                    "taxationType": {
                        "name": "name650",
                        "schemaMappedType": "Extrinsic",
                        "content": "taxationType9"
                    },
                    "isISSRetention": {
                        "name": "name651",
                        "schemaMappedType": "Extrinsic",
                        "content": "isISSRetention9"
                    },
                    "isServiceBrokerISSRetention": {
                        "name": "name652",
                        "schemaMappedType": "Extrinsic",
                        "content": "isServiceBrokerISSRetention9"
                    },
                    "issRetentionValue": {
                        "name": "name653",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency141",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency141",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name654"},
                    "withholdingTaxTotal": {
                        "name": "name655",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency142",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency142",
                            "content": 0
                        }
                    },
                    "taxTotal": {
                        "name": "name656",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency143",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency143",
                            "content": 0
                        }
                    }
                },
                "POTax": {
                    "Money": {
                        "currency": "currency144",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency144",
                        "content": 0
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type292",
                        "ShortName": "ShortName290"
                    },
                    "TaxDetail": {
                        "purpose": "purpose11",
                        "category": "category10",
                        "percentageRate": 0,
                        "taxRateType": "taxRateType10",
                        "isVatRecoverable": "yes",
                        "isWithholdingTax": "yes",
                        "taxPointDate": "2006-05-04T18:13:51.0",
                        "paymentDate": "2006-05-04T18:13:51.0",
                        "isTriangularTransaction": "yes",
                        "exemptDetail": "zeroRated",
                        "TaxableAmount": {
                            "Money": {
                                "currency": "currency145",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency145",
                                "content": 0
                            }
                        },
                        "TaxAmount": {
                            "Money": {
                                "currency": "currency146",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency146",
                                "content": 0
                            }
                        },
                        "TaxLocation": {
                            "xml:lang": "en-US",
                            "content": "TaxLocation10"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type293",
                            "ShortName": "ShortName291"
                        },
                        "TriangularTransactionLawReference": {
                            "xml:lang": "en-US",
                            "content": "TriangularTransactionLawReference10"
                        },
                        "TaxRegime": "TaxRegime10",
                        "TaxExemption": {
                            "exemptCode": "exemptCode10",
                            "ExemptReason": {
                                "xml:lang": "en-US",
                                "content": "ExemptReason10"
                            }
                        },
                        "withholdingTaxType": {
                            "name": "name657",
                            "schemaMappedType": "Extrinsic",
                            "content": "withholdingTaxType10"
                        },
                        "taxPointDateCode": {
                            "name": "name658",
                            "schemaMappedType": "Extrinsic",
                            "content": "taxPointDateCode10"
                        },
                        "Extrinsic": {"name": "name659"}
                    },
                    "OldMoney": {
                        "currency": "currency147",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency147",
                        "content": 0
                    },
                    "DeletedMoney": {
                        "currency": "currency148",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency148",
                        "content": 0
                    },
                    "taxationType": {
                        "name": "name660",
                        "schemaMappedType": "Extrinsic",
                        "content": "taxationType10"
                    },
                    "isISSRetention": {
                        "name": "name661",
                        "schemaMappedType": "Extrinsic",
                        "content": "isISSRetention10"
                    },
                    "isServiceBrokerISSRetention": {
                        "name": "name662",
                        "schemaMappedType": "Extrinsic",
                        "content": "isServiceBrokerISSRetention10"
                    },
                    "issRetentionValue": {
                        "name": "name663",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency149",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency149",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name664"},
                    "withholdingTaxTotal": {
                        "name": "name665",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency150",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency150",
                            "content": 0
                        }
                    },
                    "taxTotal": {
                        "name": "name666",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency151",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency151",
                            "content": 0
                        }
                    }
                },
                "RelatedInvoiceTax": {
                    "Money": {
                        "currency": "currency152",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency152",
                        "content": 0
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type294",
                        "ShortName": "ShortName292"
                    },
                    "TaxDetail": {
                        "purpose": "purpose12",
                        "category": "category11",
                        "percentageRate": 0,
                        "taxRateType": "taxRateType11",
                        "isVatRecoverable": "yes",
                        "isWithholdingTax": "yes",
                        "taxPointDate": "2006-05-04T18:13:51.0",
                        "paymentDate": "2006-05-04T18:13:51.0",
                        "isTriangularTransaction": "yes",
                        "exemptDetail": "zeroRated",
                        "TaxableAmount": {
                            "Money": {
                                "currency": "currency153",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency153",
                                "content": 0
                            }
                        },
                        "TaxAmount": {
                            "Money": {
                                "currency": "currency154",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency154",
                                "content": 0
                            }
                        },
                        "TaxLocation": {
                            "xml:lang": "en-US",
                            "content": "TaxLocation11"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type295",
                            "ShortName": "ShortName293"
                        },
                        "TriangularTransactionLawReference": {
                            "xml:lang": "en-US",
                            "content": "TriangularTransactionLawReference11"
                        },
                        "TaxRegime": "TaxRegime11",
                        "TaxExemption": {
                            "exemptCode": "exemptCode11",
                            "ExemptReason": {
                                "xml:lang": "en-US",
                                "content": "ExemptReason11"
                            }
                        },
                        "withholdingTaxType": {
                            "name": "name667",
                            "schemaMappedType": "Extrinsic",
                            "content": "withholdingTaxType11"
                        },
                        "taxPointDateCode": {
                            "name": "name668",
                            "schemaMappedType": "Extrinsic",
                            "content": "taxPointDateCode11"
                        },
                        "Extrinsic": {"name": "name669"}
                    },
                    "OldMoney": {
                        "currency": "currency155",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency155",
                        "content": 0
                    },
                    "DeletedMoney": {
                        "currency": "currency156",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency156",
                        "content": 0
                    },
                    "taxationType": {
                        "name": "name670",
                        "schemaMappedType": "Extrinsic",
                        "content": "taxationType11"
                    },
                    "isISSRetention": {
                        "name": "name671",
                        "schemaMappedType": "Extrinsic",
                        "content": "isISSRetention11"
                    },
                    "isServiceBrokerISSRetention": {
                        "name": "name672",
                        "schemaMappedType": "Extrinsic",
                        "content": "isServiceBrokerISSRetention11"
                    },
                    "issRetentionValue": {
                        "name": "name673",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency157",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency157",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name674"},
                    "withholdingTaxTotal": {
                        "name": "name675",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency158",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency158",
                            "content": 0
                        }
                    },
                    "taxTotal": {
                        "name": "name676",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency159",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency159",
                            "content": 0
                        }
                    }
                },
                "InvoiceDetailLineSpecialHandling": {
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type296",
                        "ShortName": "ShortName294"
                    },
                    "Money": {
                        "currency": "currency160",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency160",
                        "content": 0
                    }
                },
                "InvoiceDetailLineShipping": {
                    "InvoiceDetailShipping": {
                        "shippingDate": "2006-05-04T18:13:51.0",
                        "shipFrom": {
                            "role": "role58",
                            "addressID": "addressID60",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName29",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name62"
                            },
                            "PostalAddress": {
                                "name": "name677",
                                "DeliverTo": "DeliverTo60",
                                "Street": "Street60",
                                "City": {
                                    "cityCode": "cityCode60",
                                    "content": "City60"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode60",
                                    "content": "Municipality60"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode60",
                                    "content": "State60"
                                },
                                "PostalCode": "PostalCode60",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode155",
                                    "content": "Country63"
                                }
                            },
                            "Email": {
                                "name": "name678",
                                "content": "Email80"
                            },
                            "Phone": {
                                "name": "name679",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode156",
                                        "content": "CountryCode90"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode90",
                                    "Number": "Number90",
                                    "Extension": "Extension90"
                                }
                            },
                            "Fax": {
                                "name": "name680",
                                "URL": {
                                    "name": "name681",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "URL": {
                                "name": "name682",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role59",
                                "addressID": "addressID61",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name63"
                                },
                                "PostalAddress": {
                                    "name": "name683",
                                    "DeliverTo": "DeliverTo61",
                                    "Street": "Street61",
                                    "City": {
                                        "cityCode": "cityCode61",
                                        "content": "City61"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode61",
                                        "content": "Municipality61"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode61",
                                        "content": "State61"
                                    },
                                    "PostalCode": "PostalCode61",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode157",
                                        "content": "Country64"
                                    }
                                },
                                "Email": {
                                    "name": "name684",
                                    "content": "Email81"
                                },
                                "Phone": {
                                    "name": "name685",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode158",
                                            "content": "CountryCode91"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode91",
                                        "Number": "Number91",
                                        "Extension": "Extension91"
                                    }
                                },
                                "Fax": {
                                    "name": "name686",
                                    "Email": {
                                        "name": "name687",
                                        "content": "Email82"
                                    }
                                },
                                "URL": {
                                    "name": "name688",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier252",
                                "domain": "domain293",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator252"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type297",
                                    "ShortName": "ShortName295"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier253",
                                "domain": "domain294",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator253"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type298",
                                    "ShortName": "ShortName296"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier254",
                                "domain": "domain295",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator254"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type299",
                                    "ShortName": "ShortName297"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier255",
                                "domain": "domain296",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator255"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type300",
                                    "ShortName": "ShortName298"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier256",
                                "domain": "domain297",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator256"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type301",
                                    "ShortName": "ShortName299"
                                }
                            },
                            "isFactoring": {
                                "name": "name689",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name690"},
                            "LegalCapital": {
                                "name": "name691",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency161",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency161",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name692"}
                        },
                        "shipTo": {
                            "role": "role60",
                            "addressID": "addressID62",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName30",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name64"
                            },
                            "PostalAddress": {
                                "name": "name693",
                                "DeliverTo": "DeliverTo62",
                                "Street": "Street62",
                                "City": {
                                    "cityCode": "cityCode62",
                                    "content": "City62"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode62",
                                    "content": "Municipality62"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode62",
                                    "content": "State62"
                                },
                                "PostalCode": "PostalCode62",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode159",
                                    "content": "Country65"
                                }
                            },
                            "Email": {
                                "name": "name694",
                                "content": "Email83"
                            },
                            "Phone": {
                                "name": "name695",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode160",
                                        "content": "CountryCode92"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode92",
                                    "Number": "Number92",
                                    "Extension": "Extension92"
                                }
                            },
                            "Fax": {
                                "name": "name696",
                                "URL": {
                                    "name": "name697",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "URL": {
                                "name": "name698",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role61",
                                "addressID": "addressID63",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name65"
                                },
                                "PostalAddress": {
                                    "name": "name699",
                                    "DeliverTo": "DeliverTo63",
                                    "Street": "Street63",
                                    "City": {
                                        "cityCode": "cityCode63",
                                        "content": "City63"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode63",
                                        "content": "Municipality63"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode63",
                                        "content": "State63"
                                    },
                                    "PostalCode": "PostalCode63",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode161",
                                        "content": "Country66"
                                    }
                                },
                                "Email": {
                                    "name": "name700",
                                    "content": "Email84"
                                },
                                "Phone": {
                                    "name": "name701",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode162",
                                            "content": "CountryCode93"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode93",
                                        "Number": "Number93",
                                        "Extension": "Extension93"
                                    }
                                },
                                "Fax": {
                                    "name": "name702",
                                    "URL": {
                                        "name": "name703",
                                        "content": "http://www.oxygenxml.com/"
                                    }
                                },
                                "URL": {
                                    "name": "name704",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier257",
                                "domain": "domain298",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator257"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type302",
                                    "ShortName": "ShortName300"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier258",
                                "domain": "domain299",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator258"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type303",
                                    "ShortName": "ShortName301"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier259",
                                "domain": "domain300",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator259"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type304",
                                    "ShortName": "ShortName302"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier260",
                                "domain": "domain301",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator260"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type305",
                                    "ShortName": "ShortName303"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier261",
                                "domain": "domain302",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator261"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type306",
                                    "ShortName": "ShortName304"
                                }
                            },
                            "isFactoring": {
                                "name": "name705",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name706"},
                            "LegalCapital": {
                                "name": "name707",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency162",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency162",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name708"}
                        },
                        "Contact": {
                            "role": "role62",
                            "addressID": "addressID64",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName31",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name66"
                            },
                            "PostalAddress": {
                                "name": "name709",
                                "DeliverTo": "DeliverTo64",
                                "Street": "Street64",
                                "City": {
                                    "cityCode": "cityCode64",
                                    "content": "City64"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode64",
                                    "content": "Municipality64"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode64",
                                    "content": "State64"
                                },
                                "PostalCode": "PostalCode64",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode163",
                                    "content": "Country67"
                                }
                            },
                            "Email": {
                                "name": "name710",
                                "content": "Email85"
                            },
                            "Phone": {
                                "name": "name711",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode164",
                                        "content": "CountryCode94"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode94",
                                    "Number": "Number94",
                                    "Extension": "Extension94"
                                }
                            },
                            "Fax": {
                                "name": "name712",
                                "URL": {
                                    "name": "name713",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "URL": {
                                "name": "name714",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role63",
                                "addressID": "addressID65",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name67"
                                },
                                "PostalAddress": {
                                    "name": "name715",
                                    "DeliverTo": "DeliverTo65",
                                    "Street": "Street65",
                                    "City": {
                                        "cityCode": "cityCode65",
                                        "content": "City65"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode65",
                                        "content": "Municipality65"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode65",
                                        "content": "State65"
                                    },
                                    "PostalCode": "PostalCode65",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode165",
                                        "content": "Country68"
                                    }
                                },
                                "Email": {
                                    "name": "name716",
                                    "content": "Email86"
                                },
                                "Phone": {
                                    "name": "name717",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode166",
                                            "content": "CountryCode95"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode95",
                                        "Number": "Number95",
                                        "Extension": "Extension95"
                                    }
                                },
                                "Fax": {
                                    "name": "name718",
                                    "Email": {
                                        "name": "name719",
                                        "content": "Email87"
                                    }
                                },
                                "URL": {
                                    "name": "name720",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier262",
                                "domain": "domain303",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator262"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type307",
                                    "ShortName": "ShortName305"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier263",
                                "domain": "domain304",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator263"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type308",
                                    "ShortName": "ShortName306"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier264",
                                "domain": "domain305",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator264"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type309",
                                    "ShortName": "ShortName307"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier265",
                                "domain": "domain306",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator265"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type310",
                                    "ShortName": "ShortName308"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier266",
                                "domain": "domain307",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator266"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type311",
                                    "ShortName": "ShortName309"
                                }
                            },
                            "isFactoring": {
                                "name": "name721",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name722"},
                            "LegalCapital": {
                                "name": "name723",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency163",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency163",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name724"}
                        },
                        "CarrierIdentifier": {
                            "domain": "domain308",
                            "content": "CarrierIdentifier3"
                        },
                        "ShipmentIdentifier": "ShipmentIdentifier3",
                        "DocumentReference": {"payloadID": "payloadID9"}
                    },
                    "Money": {
                        "currency": "currency164",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency164",
                        "content": 0
                    }
                },
                "ShipNoticeIDInfo": {
                    "shipNoticeID": "shipNoticeID5",
                    "shipNoticeDate": "2006-05-04T18:13:51.0",
                    "oldshipNoticeID": "oldshipNoticeID5",
                    "oldshipNoticeDate": "2006-05-04T18:13:51.0",
                    "IdReference": {
                        "identifier": "identifier267",
                        "domain": "domain309",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator267"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type312",
                            "ShortName": "ShortName310"
                        }
                    },
                    "deliveryNoteID": {
                        "identifier": "identifier268",
                        "domain": "domain310",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator268"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type313",
                            "ShortName": "ShortName311"
                        }
                    },
                    "deliveryNoteDate": {
                        "identifier": "identifier269",
                        "domain": "domain311",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator269"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type314",
                            "ShortName": "ShortName312"
                        }
                    },
                    "deliveryNoteLineItemNo": {
                        "identifier": "identifier270",
                        "domain": "domain312",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator270"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type315",
                            "ShortName": "ShortName313"
                        }
                    },
                    "dispatchAdviceID": {
                        "identifier": "identifier271",
                        "domain": "domain313",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator271"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type316",
                            "ShortName": "ShortName314"
                        }
                    },
                    "receivingAdviceID": {
                        "identifier": "identifier272",
                        "domain": "domain314",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator272"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type317",
                            "ShortName": "ShortName315"
                        }
                    },
                    "receivingAdviceDate": {
                        "identifier": "identifier273",
                        "domain": "domain315",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator273"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type318",
                            "ShortName": "ShortName316"
                        }
                    },
                    "transportDocumentID": {
                        "identifier": "identifier274",
                        "domain": "domain316",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator274"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type319",
                            "ShortName": "ShortName317"
                        }
                    },
                    "proofOfDeliveryID": {
                        "identifier": "identifier275",
                        "domain": "domain317",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator275"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type320",
                            "ShortName": "ShortName318"
                        }
                    },
                    "proofOfDeliveryDate": {
                        "identifier": "identifier276",
                        "domain": "domain318",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator276"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type321",
                            "ShortName": "ShortName319"
                        }
                    },
                    "actualDeliveryDate": {
                        "identifier": "identifier277",
                        "domain": "domain319",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator277"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type322",
                            "ShortName": "ShortName320"
                        }
                    },
                    "goodsPositioningDate": {
                        "identifier": "identifier278",
                        "domain": "domain320",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator278"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type323",
                            "ShortName": "ShortName321"
                        }
                    },
                    "goodsPositioningStartDate": {
                        "identifier": "identifier279",
                        "domain": "domain321",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator279"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type324",
                            "ShortName": "ShortName322"
                        }
                    },
                    "goodsPositioningEndDate": {
                        "identifier": "identifier280",
                        "domain": "domain322",
                        "Creator": {
                            "xml:lang": "en-US",
                            "content": "Creator280"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type325",
                            "ShortName": "ShortName323"
                        }
                    }
                },
                "GrossAmount": {
                    "Money": {
                        "currency": "currency165",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency165",
                        "content": 0
                    }
                },
                "InvoiceDetailDiscount": {
                    "percentageRate": 0,
                    "Money": {
                        "currency": "currency166",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency166",
                        "content": 0
                    }
                },
                "InvoiceItemModifications": {
                    "Modification": {
                        "level": 0,
                        "OriginalPrice": {
                            "Money": {
                                "currency": "currency167",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency167",
                                "content": 0
                            }
                        },
                        "AdditionalCost": {
                            "Percentage": {"percent": 0}
                        },
                        "Tax": {
                            "Money": {
                                "currency": "currency168",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency168",
                                "content": 0
                            },
                            "Description": {
                                "xml:lang": "en-US",
                                "type": "type326",
                                "ShortName": "ShortName324"
                            },
                            "TaxDetail": {
                                "purpose": "purpose13",
                                "category": "category12",
                                "percentageRate": 0,
                                "taxRateType": "taxRateType12",
                                "isVatRecoverable": "yes",
                                "isWithholdingTax": "yes",
                                "taxPointDate": "2006-05-04T18:13:51.0",
                                "paymentDate": "2006-05-04T18:13:51.0",
                                "isTriangularTransaction": "yes",
                                "exemptDetail": "zeroRated",
                                "TaxableAmount": {
                                    "Money": {
                                        "currency": "currency169",
                                        "alternateAmount": 0,
                                        "alternateCurrency": "alternateCurrency169",
                                        "content": 0
                                    }
                                },
                                "TaxAmount": {
                                    "Money": {
                                        "currency": "currency170",
                                        "alternateAmount": 0,
                                        "alternateCurrency": "alternateCurrency170",
                                        "content": 0
                                    }
                                },
                                "TaxLocation": {
                                    "xml:lang": "en-US",
                                    "content": "TaxLocation12"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type327",
                                    "ShortName": "ShortName325"
                                },
                                "TriangularTransactionLawReference": {
                                    "xml:lang": "en-US",
                                    "content": "TriangularTransactionLawReference12"
                                },
                                "TaxRegime": "TaxRegime12",
                                "TaxExemption": {
                                    "exemptCode": "exemptCode12",
                                    "ExemptReason": {
                                        "xml:lang": "en-US",
                                        "content": "ExemptReason12"
                                    }
                                },
                                "withholdingTaxType": {
                                    "name": "name725",
                                    "schemaMappedType": "Extrinsic",
                                    "content": "withholdingTaxType12"
                                },
                                "taxPointDateCode": {
                                    "name": "name726",
                                    "schemaMappedType": "Extrinsic",
                                    "content": "taxPointDateCode12"
                                },
                                "Extrinsic": {"name": "name727"}
                            },
                            "OldMoney": {
                                "currency": "currency171",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency171",
                                "content": 0
                            },
                            "DeletedMoney": {
                                "currency": "currency172",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency172",
                                "content": 0
                            },
                            "taxationType": {
                                "name": "name728",
                                "schemaMappedType": "Extrinsic",
                                "content": "taxationType12"
                            },
                            "isISSRetention": {
                                "name": "name729",
                                "schemaMappedType": "Extrinsic",
                                "content": "isISSRetention12"
                            },
                            "isServiceBrokerISSRetention": {
                                "name": "name730",
                                "schemaMappedType": "Extrinsic",
                                "content": "isServiceBrokerISSRetention12"
                            },
                            "issRetentionValue": {
                                "name": "name731",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency173",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency173",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name732"},
                            "withholdingTaxTotal": {
                                "name": "name733",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency174",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency174",
                                    "content": 0
                                }
                            },
                            "taxTotal": {
                                "name": "name734",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency175",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency175",
                                    "content": 0
                                }
                            }
                        },
                        "ModificationDetail": {
                            "name": "name735",
                            "code": "code30",
                            "startDate": "2006-05-04T18:13:51.0",
                            "endDate": "2006-05-04T18:13:51.0",
                            "Description": {
                                "xml:lang": "en-US",
                                "type": "type328",
                                "ShortName": "ShortName326"
                            },
                            "Extrinsic": {"name": "name736"}
                        }
                    }
                },
                "TotalCharges": {
                    "Money": {
                        "currency": "currency176",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency176",
                        "content": 0
                    }
                },
                "TotalAllowances": {
                    "Money": {
                        "currency": "currency177",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency177",
                        "content": 0
                    }
                },
                "TotalAmountWithoutTax": {
                    "Money": {
                        "currency": "currency178",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency178",
                        "content": 0
                    }
                },
                "TotalTaxOnModifications": {
                    "Money": {
                        "currency": "currency179",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency179",
                        "content": 0
                    }
                },
                "NetAmount": {
                    "Money": {
                        "currency": "currency180",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency180",
                        "content": 0
                    }
                },
                "distribution": {
                    "Accounting": {
                        "name": "name737",
                        "AccountingSegment": {
                            "id": "id4",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name68"
                            },
                            "Description": {
                                "xml:lang": "en-US",
                                "type": "type329",
                                "ShortName": "ShortName327"
                            }
                        }
                    },
                    "Charge": {
                        "Money": {
                            "currency": "currency181",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency181",
                            "content": 0
                        }
                    }
                },
                "Distribution": {
                    "Accounting": {
                        "name": "name738",
                        "Segment": {
                            "type": "type330",
                            "id": "id5",
                            "description": "description2"
                        }
                    },
                    "Charge": {
                        "Money": {
                            "currency": "currency182",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency182",
                            "content": 0
                        }
                    }
                },
                "Comments": {
                    "xml:lang": "en-US",
                    "Attachment": {
                        "URL": {
                            "name": "name739",
                            "content": "http://www.oxygenxml.com/"
                        }
                    }
                },
                "InvoiceLaborDetail": {
                    "Contractor": {
                        "ContractorIdentifier": {
                            "domain": "supplierReferenceID",
                            "content": "ContractorIdentifier2"
                        },
                        "Contact": {
                            "role": "role64",
                            "addressID": "addressID66",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName32",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name69"
                            },
                            "PostalAddress": {
                                "name": "name740",
                                "DeliverTo": "DeliverTo66",
                                "Street": "Street66",
                                "City": {
                                    "cityCode": "cityCode66",
                                    "content": "City66"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode66",
                                    "content": "Municipality66"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode66",
                                    "content": "State66"
                                },
                                "PostalCode": "PostalCode66",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode167",
                                    "content": "Country69"
                                }
                            },
                            "Email": {
                                "name": "name741",
                                "content": "Email88"
                            },
                            "Phone": {
                                "name": "name742",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode168",
                                        "content": "CountryCode96"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode96",
                                    "Number": "Number96",
                                    "Extension": "Extension96"
                                }
                            },
                            "Fax": {
                                "name": "name743",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode169",
                                        "content": "CountryCode97"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode97",
                                    "Number": "Number97",
                                    "Extension": "Extension97"
                                }
                            },
                            "URL": {
                                "name": "name744",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role65",
                                "addressID": "addressID67",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name70"
                                },
                                "PostalAddress": {
                                    "name": "name745",
                                    "DeliverTo": "DeliverTo67",
                                    "Street": "Street67",
                                    "City": {
                                        "cityCode": "cityCode67",
                                        "content": "City67"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode67",
                                        "content": "Municipality67"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode67",
                                        "content": "State67"
                                    },
                                    "PostalCode": "PostalCode67",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode170",
                                        "content": "Country70"
                                    }
                                },
                                "Email": {
                                    "name": "name746",
                                    "content": "Email89"
                                },
                                "Phone": {
                                    "name": "name747",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode171",
                                            "content": "CountryCode98"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode98",
                                        "Number": "Number98",
                                        "Extension": "Extension98"
                                    }
                                },
                                "Fax": {
                                    "name": "name748",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode172",
                                            "content": "CountryCode99"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode99",
                                        "Number": "Number99",
                                        "Extension": "Extension99"
                                    }
                                },
                                "URL": {
                                    "name": "name749",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier281",
                                "domain": "domain324",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator281"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type331",
                                    "ShortName": "ShortName328"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier282",
                                "domain": "domain325",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator282"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type332",
                                    "ShortName": "ShortName329"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier283",
                                "domain": "domain326",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator283"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type333",
                                    "ShortName": "ShortName330"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier284",
                                "domain": "domain327",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator284"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type334",
                                    "ShortName": "ShortName331"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier285",
                                "domain": "domain328",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator285"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type335",
                                    "ShortName": "ShortName332"
                                }
                            },
                            "isFactoring": {
                                "name": "name750",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name751"},
                            "LegalCapital": {
                                "name": "name752",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency183",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency183",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name753"}
                        }
                    },
                    "JobDescription": {
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type336",
                            "ShortName": "ShortName333"
                        }
                    },
                    "Supervisor": {
                        "Contact": {
                            "role": "role66",
                            "addressID": "addressID68",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName33",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name71"
                            },
                            "PostalAddress": {
                                "name": "name754",
                                "DeliverTo": "DeliverTo68",
                                "Street": "Street68",
                                "City": {
                                    "cityCode": "cityCode68",
                                    "content": "City68"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode68",
                                    "content": "Municipality68"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode68",
                                    "content": "State68"
                                },
                                "PostalCode": "PostalCode68",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode173",
                                    "content": "Country71"
                                }
                            },
                            "Email": {
                                "name": "name755",
                                "content": "Email90"
                            },
                            "Phone": {
                                "name": "name756",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode174",
                                        "content": "CountryCode100"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode100",
                                    "Number": "Number100",
                                    "Extension": "Extension100"
                                }
                            },
                            "Fax": {
                                "name": "name757",
                                "URL": {
                                    "name": "name758",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "URL": {
                                "name": "name759",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role67",
                                "addressID": "addressID69",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name72"
                                },
                                "PostalAddress": {
                                    "name": "name760",
                                    "DeliverTo": "DeliverTo69",
                                    "Street": "Street69",
                                    "City": {
                                        "cityCode": "cityCode69",
                                        "content": "City69"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode69",
                                        "content": "Municipality69"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode69",
                                        "content": "State69"
                                    },
                                    "PostalCode": "PostalCode69",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode175",
                                        "content": "Country72"
                                    }
                                },
                                "Email": {
                                    "name": "name761",
                                    "content": "Email91"
                                },
                                "Phone": {
                                    "name": "name762",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode176",
                                            "content": "CountryCode101"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode101",
                                        "Number": "Number101",
                                        "Extension": "Extension101"
                                    }
                                },
                                "Fax": {
                                    "name": "name763",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode177",
                                            "content": "CountryCode102"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode102",
                                        "Number": "Number102",
                                        "Extension": "Extension102"
                                    }
                                },
                                "URL": {
                                    "name": "name764",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier286",
                                "domain": "domain329",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator286"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type337",
                                    "ShortName": "ShortName334"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier287",
                                "domain": "domain330",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator287"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type338",
                                    "ShortName": "ShortName335"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier288",
                                "domain": "domain331",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator288"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type339",
                                    "ShortName": "ShortName336"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier289",
                                "domain": "domain332",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator289"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type340",
                                    "ShortName": "ShortName337"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier290",
                                "domain": "domain333",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator290"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type341",
                                    "ShortName": "ShortName338"
                                }
                            },
                            "isFactoring": {
                                "name": "name765",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name766"},
                            "LegalCapital": {
                                "name": "name767",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency184",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency184",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name768"}
                        }
                    },
                    "WorkLocation": {
                        "Address": {
                            "isoCountryCode": "isoCountryCode178",
                            "addressID": "addressID70",
                            "addressIDDomain": "addressIDDomain2",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name73"
                            },
                            "PostalAddress": {
                                "name": "name769",
                                "DeliverTo": "DeliverTo70",
                                "Street": "Street70",
                                "City": {
                                    "cityCode": "cityCode70",
                                    "content": "City70"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode70",
                                    "content": "Municipality70"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode70",
                                    "content": "State70"
                                },
                                "PostalCode": "PostalCode70",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode179",
                                    "content": "Country73"
                                }
                            },
                            "Email": {
                                "name": "name770",
                                "content": "Email92"
                            },
                            "Phone": {
                                "name": "name771",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode180",
                                        "content": "CountryCode103"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode103",
                                    "Number": "Number103",
                                    "Extension": "Extension103"
                                }
                            },
                            "Fax": {
                                "name": "name772",
                                "URL": {
                                    "name": "name773",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "URL": {
                                "name": "name774",
                                "content": "http://www.oxygenxml.com/"
                            }
                        }
                    },
                    "InvoiceTimeCardDetail": {
                        "TimeCardReference": {
                            "timeCardID": "timeCardID2",
                            "DocumentReference": {"payloadID": "payloadID10"}
                        }
                    }
                },
                "punchinItemFromCatalog": {
                    "name": "name775",
                    "schemaMappedType": "Extrinsic",
                    "content": "punchinItemFromCatalog2"
                },
                "Extrinsic": {"name": "name776"},
                "IsShippingServiceItem": {
                    "name": "name777",
                    "schemaMappedType": "Extrinsic",
                    "content": "IsShippingServiceItem2"
                },
                "IsSpecialHandlingServiceItem": {
                    "name": "name778",
                    "schemaMappedType": "Extrinsic",
                    "content": "IsSpecialHandlingServiceItem2"
                },
                "isIncluded": false,
                "isFullyInvoiced": false,
                "isLineFromPO": {
                    "name": "name779",
                    "schemaMappedType": "Extrinsic",
                    "content": "isLineFromPO2"
                },
                "parentPOLineNumber": {
                    "name": "parentPOLineNumber",
                    "schemaMappedType": "Extrinsic",
                    "content": "parentPOLineNumber2"
                },
                "extLineNumber": {
                    "name": "extLineNumber",
                    "schemaMappedType": "Extrinsic",
                    "content": "extLineNumber2"
                },
                "parentExtLineNumber": {
                    "name": "parentExtLineNumber",
                    "schemaMappedType": "Extrinsic",
                    "content": "parentExtLineNumber2"
                }
            }
        },
        "InvoiceDetailHeaderOrder": {
            "InvoiceDetailOrderInfo": {
                "OrderReference": {
                    "orderID": "orderID3",
                    "orderDate": "2006-05-04T18:13:51.0",
                    "DocumentReference": {"payloadID": "payloadID11"}
                },
                "MasterAgreementReference": {
                    "agreementID": "agreementID2",
                    "agreementDate": "2006-05-04T18:13:51.0",
                    "agreementType": "agreementType2",
                    "DocumentReference": {"payloadID": "payloadID12"}
                },
                "MasterAgreementIDInfo": {
                    "agreementID": "agreementID3",
                    "agreementDate": "2006-05-04T18:13:51.0",
                    "agreementType": "agreementType3"
                },
                "OrderIDInfo": {
                    "orderID": "orderID4",
                    "orderDate": "2006-05-04T18:13:51.0"
                },
                "SupplierOrderInfo": {
                    "orderID": "orderID5",
                    "orderDate": "2006-05-04T18:13:51.0"
                }
            },
            "InvoiceDetailOrderSummary": {
                "invoiceLineNumber": 0,
                "SubtotalAmount": {
                    "Money": {
                        "currency": "currency185",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency185",
                        "content": 0
                    }
                },
                "Period": {
                    "startDate": "2006-05-04T18:13:51.0",
                    "endDate": "2006-05-04T18:13:51.0"
                },
                "Tax": {
                    "Money": {
                        "currency": "currency186",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency186",
                        "content": 0
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type342",
                        "ShortName": "ShortName339"
                    },
                    "TaxDetail": {
                        "purpose": "purpose14",
                        "category": "category13",
                        "percentageRate": 0,
                        "taxRateType": "taxRateType13",
                        "isVatRecoverable": "yes",
                        "isWithholdingTax": "yes",
                        "taxPointDate": "2006-05-04T18:13:51.0",
                        "paymentDate": "2006-05-04T18:13:51.0",
                        "isTriangularTransaction": "yes",
                        "exemptDetail": "zeroRated",
                        "TaxableAmount": {
                            "Money": {
                                "currency": "currency187",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency187",
                                "content": 0
                            }
                        },
                        "TaxAmount": {
                            "Money": {
                                "currency": "currency188",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency188",
                                "content": 0
                            }
                        },
                        "TaxLocation": {
                            "xml:lang": "en-US",
                            "content": "TaxLocation13"
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type343",
                            "ShortName": "ShortName340"
                        },
                        "TriangularTransactionLawReference": {
                            "xml:lang": "en-US",
                            "content": "TriangularTransactionLawReference13"
                        },
                        "TaxRegime": "TaxRegime13",
                        "TaxExemption": {
                            "exemptCode": "exemptCode13",
                            "ExemptReason": {
                                "xml:lang": "en-US",
                                "content": "ExemptReason13"
                            }
                        },
                        "withholdingTaxType": {
                            "name": "name783",
                            "schemaMappedType": "Extrinsic",
                            "content": "withholdingTaxType13"
                        },
                        "taxPointDateCode": {
                            "name": "name784",
                            "schemaMappedType": "Extrinsic",
                            "content": "taxPointDateCode13"
                        },
                        "Extrinsic": {"name": "name785"}
                    },
                    "OldMoney": {
                        "currency": "currency189",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency189",
                        "content": 0
                    },
                    "DeletedMoney": {
                        "currency": "currency190",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency190",
                        "content": 0
                    },
                    "taxationType": {
                        "name": "name786",
                        "schemaMappedType": "Extrinsic",
                        "content": "taxationType13"
                    },
                    "isISSRetention": {
                        "name": "name787",
                        "schemaMappedType": "Extrinsic",
                        "content": "isISSRetention13"
                    },
                    "isServiceBrokerISSRetention": {
                        "name": "name788",
                        "schemaMappedType": "Extrinsic",
                        "content": "isServiceBrokerISSRetention13"
                    },
                    "issRetentionValue": {
                        "name": "name789",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency191",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency191",
                            "content": 0
                        }
                    },
                    "Extrinsic": {"name": "name790"},
                    "withholdingTaxTotal": {
                        "name": "name791",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency192",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency192",
                            "content": 0
                        }
                    },
                    "taxTotal": {
                        "name": "name792",
                        "schemaMappedType": "Extrinsic",
                        "Money": {
                            "currency": "currency193",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency193",
                            "content": 0
                        }
                    }
                },
                "InvoiceDetailLineSpecialHandling": {
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type344",
                        "ShortName": "ShortName341"
                    },
                    "Money": {
                        "currency": "currency194",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency194",
                        "content": 0
                    }
                },
                "InvoiceDetailLineShipping": {
                    "InvoiceDetailShipping": {
                        "shippingDate": "2006-05-04T18:13:51.0",
                        "shipFrom": {
                            "role": "role68",
                            "addressID": "addressID71",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName34",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name74"
                            },
                            "PostalAddress": {
                                "name": "name793",
                                "DeliverTo": "DeliverTo71",
                                "Street": "Street71",
                                "City": {
                                    "cityCode": "cityCode71",
                                    "content": "City71"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode71",
                                    "content": "Municipality71"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode71",
                                    "content": "State71"
                                },
                                "PostalCode": "PostalCode71",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode181",
                                    "content": "Country74"
                                }
                            },
                            "Email": {
                                "name": "name794",
                                "content": "Email93"
                            },
                            "Phone": {
                                "name": "name795",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode182",
                                        "content": "CountryCode104"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode104",
                                    "Number": "Number104",
                                    "Extension": "Extension104"
                                }
                            },
                            "Fax": {
                                "name": "name796",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode183",
                                        "content": "CountryCode105"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode105",
                                    "Number": "Number105",
                                    "Extension": "Extension105"
                                }
                            },
                            "URL": {
                                "name": "name797",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role69",
                                "addressID": "addressID72",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name75"
                                },
                                "PostalAddress": {
                                    "name": "name798",
                                    "DeliverTo": "DeliverTo72",
                                    "Street": "Street72",
                                    "City": {
                                        "cityCode": "cityCode72",
                                        "content": "City72"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode72",
                                        "content": "Municipality72"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode72",
                                        "content": "State72"
                                    },
                                    "PostalCode": "PostalCode72",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode184",
                                        "content": "Country75"
                                    }
                                },
                                "Email": {
                                    "name": "name799",
                                    "content": "Email94"
                                },
                                "Phone": {
                                    "name": "name800",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode185",
                                            "content": "CountryCode106"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode106",
                                        "Number": "Number106",
                                        "Extension": "Extension106"
                                    }
                                },
                                "Fax": {
                                    "name": "name801",
                                    "Email": {
                                        "name": "name802",
                                        "content": "Email95"
                                    }
                                },
                                "URL": {
                                    "name": "name803",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier291",
                                "domain": "domain334",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator291"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type345",
                                    "ShortName": "ShortName342"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier292",
                                "domain": "domain335",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator292"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type346",
                                    "ShortName": "ShortName343"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier293",
                                "domain": "domain336",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator293"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type347",
                                    "ShortName": "ShortName344"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier294",
                                "domain": "domain337",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator294"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type348",
                                    "ShortName": "ShortName345"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier295",
                                "domain": "domain338",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator295"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type349",
                                    "ShortName": "ShortName346"
                                }
                            },
                            "isFactoring": {
                                "name": "name804",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name805"},
                            "LegalCapital": {
                                "name": "name806",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency195",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency195",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name807"}
                        },
                        "shipTo": {
                            "role": "role70",
                            "addressID": "addressID73",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName35",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name76"
                            },
                            "PostalAddress": {
                                "name": "name808",
                                "DeliverTo": "DeliverTo73",
                                "Street": "Street73",
                                "City": {
                                    "cityCode": "cityCode73",
                                    "content": "City73"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode73",
                                    "content": "Municipality73"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode73",
                                    "content": "State73"
                                },
                                "PostalCode": "PostalCode73",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode186",
                                    "content": "Country76"
                                }
                            },
                            "Email": {
                                "name": "name809",
                                "content": "Email96"
                            },
                            "Phone": {
                                "name": "name810",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode187",
                                        "content": "CountryCode107"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode107",
                                    "Number": "Number107",
                                    "Extension": "Extension107"
                                }
                            },
                            "Fax": {
                                "name": "name811",
                                "Email": {
                                    "name": "name812",
                                    "content": "Email97"
                                }
                            },
                            "URL": {
                                "name": "name813",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role71",
                                "addressID": "addressID74",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name77"
                                },
                                "PostalAddress": {
                                    "name": "name814",
                                    "DeliverTo": "DeliverTo74",
                                    "Street": "Street74",
                                    "City": {
                                        "cityCode": "cityCode74",
                                        "content": "City74"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode74",
                                        "content": "Municipality74"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode74",
                                        "content": "State74"
                                    },
                                    "PostalCode": "PostalCode74",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode188",
                                        "content": "Country77"
                                    }
                                },
                                "Email": {
                                    "name": "name815",
                                    "content": "Email98"
                                },
                                "Phone": {
                                    "name": "name816",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode189",
                                            "content": "CountryCode108"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode108",
                                        "Number": "Number108",
                                        "Extension": "Extension108"
                                    }
                                },
                                "Fax": {
                                    "name": "name817",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode190",
                                            "content": "CountryCode109"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode109",
                                        "Number": "Number109",
                                        "Extension": "Extension109"
                                    }
                                },
                                "URL": {
                                    "name": "name818",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier296",
                                "domain": "domain339",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator296"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type350",
                                    "ShortName": "ShortName347"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier297",
                                "domain": "domain340",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator297"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type351",
                                    "ShortName": "ShortName348"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier298",
                                "domain": "domain341",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator298"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type352",
                                    "ShortName": "ShortName349"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier299",
                                "domain": "domain342",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator299"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type353",
                                    "ShortName": "ShortName350"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier300",
                                "domain": "domain343",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator300"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type354",
                                    "ShortName": "ShortName351"
                                }
                            },
                            "isFactoring": {
                                "name": "name819",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name820"},
                            "LegalCapital": {
                                "name": "name821",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency196",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency196",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name822"}
                        },
                        "Contact": {
                            "role": "role72",
                            "addressID": "addressID75",
                            "isRemoved": false,
                            "isNew": false,
                            "orgName": "orgName36",
                            "Name": {
                                "xml:lang": "en-US",
                                "content": "Name78"
                            },
                            "PostalAddress": {
                                "name": "name823",
                                "DeliverTo": "DeliverTo75",
                                "Street": "Street75",
                                "City": {
                                    "cityCode": "cityCode75",
                                    "content": "City75"
                                },
                                "Municipality": {
                                    "municipalityCode": "municipalityCode75",
                                    "content": "Municipality75"
                                },
                                "State": {
                                    "isoStateCode": "isoStateCode75",
                                    "content": "State75"
                                },
                                "PostalCode": "PostalCode75",
                                "Country": {
                                    "isoCountryCode": "isoCountryCode191",
                                    "content": "Country78"
                                }
                            },
                            "Email": {
                                "name": "name824",
                                "content": "Email99"
                            },
                            "Phone": {
                                "name": "name825",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode192",
                                        "content": "CountryCode110"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode110",
                                    "Number": "Number110",
                                    "Extension": "Extension110"
                                }
                            },
                            "Fax": {
                                "name": "name826",
                                "TelephoneNumber": {
                                    "CountryCode": {
                                        "isoCountryCode": "isoCountryCode193",
                                        "content": "CountryCode111"
                                    },
                                    "AreaOrCityCode": "AreaOrCityCode111",
                                    "Number": "Number111",
                                    "Extension": "Extension111"
                                }
                            },
                            "URL": {
                                "name": "name827",
                                "content": "http://www.oxygenxml.com/"
                            },
                            "OldContact": {
                                "role": "role73",
                                "addressID": "addressID76",
                                "Name": {
                                    "xml:lang": "en-US",
                                    "content": "Name79"
                                },
                                "PostalAddress": {
                                    "name": "name828",
                                    "DeliverTo": "DeliverTo76",
                                    "Street": "Street76",
                                    "City": {
                                        "cityCode": "cityCode76",
                                        "content": "City76"
                                    },
                                    "Municipality": {
                                        "municipalityCode": "municipalityCode76",
                                        "content": "Municipality76"
                                    },
                                    "State": {
                                        "isoStateCode": "isoStateCode76",
                                        "content": "State76"
                                    },
                                    "PostalCode": "PostalCode76",
                                    "Country": {
                                        "isoCountryCode": "isoCountryCode194",
                                        "content": "Country79"
                                    }
                                },
                                "Email": {
                                    "name": "name829",
                                    "content": "Email100"
                                },
                                "Phone": {
                                    "name": "name830",
                                    "TelephoneNumber": {
                                        "CountryCode": {
                                            "isoCountryCode": "isoCountryCode195",
                                            "content": "CountryCode112"
                                        },
                                        "AreaOrCityCode": "AreaOrCityCode112",
                                        "Number": "Number112",
                                        "Extension": "Extension112"
                                    }
                                },
                                "Fax": {
                                    "name": "name831",
                                    "URL": {
                                        "name": "name832",
                                        "content": "http://www.oxygenxml.com/"
                                    }
                                },
                                "URL": {
                                    "name": "name833",
                                    "content": "http://www.oxygenxml.com/"
                                }
                            },
                            "departmentName": {
                                "identifier": "identifier301",
                                "domain": "domain344",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator301"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type355",
                                    "ShortName": "ShortName352"
                                }
                            },
                            "gstID": {
                                "identifier": "identifier302",
                                "domain": "domain345",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator302"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type356",
                                    "ShortName": "ShortName353"
                                }
                            },
                            "pstID": {
                                "identifier": "identifier303",
                                "domain": "domain346",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator303"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type357",
                                    "ShortName": "ShortName354"
                                }
                            },
                            "qstID": {
                                "identifier": "identifier304",
                                "domain": "domain347",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator304"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type358",
                                    "ShortName": "ShortName355"
                                }
                            },
                            "IdReference": {
                                "identifier": "identifier305",
                                "domain": "domain348",
                                "Creator": {
                                    "xml:lang": "en-US",
                                    "content": "Creator305"
                                },
                                "Description": {
                                    "xml:lang": "en-US",
                                    "type": "type359",
                                    "ShortName": "ShortName356"
                                }
                            },
                            "isFactoring": {
                                "name": "name834",
                                "schemaMappedType": "Extrinsic",
                                "content": false
                            },
                            "LegalStatus": {"name": "name835"},
                            "LegalCapital": {
                                "name": "name836",
                                "schemaMappedType": "Extrinsic",
                                "Money": {
                                    "currency": "currency197",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency197",
                                    "content": 0
                                }
                            },
                            "Extrinsic": {"name": "name837"}
                        },
                        "CarrierIdentifier": {
                            "domain": "domain349",
                            "content": "CarrierIdentifier4"
                        },
                        "ShipmentIdentifier": "ShipmentIdentifier4",
                        "DocumentReference": {"payloadID": "payloadID13"}
                    },
                    "Money": {
                        "currency": "currency198",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency198",
                        "content": 0
                    }
                },
                "GrossAmount": {
                    "Money": {
                        "currency": "currency199",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency199",
                        "content": 0
                    }
                },
                "InvoiceDetailDiscount": {
                    "percentageRate": 0,
                    "Money": {
                        "currency": "currency200",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency200",
                        "content": 0
                    }
                },
                "NetAmount": {
                    "Money": {
                        "currency": "currency201",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency201",
                        "content": 0
                    }
                },
                "Comments": {
                    "xml:lang": "en-US",
                    "Attachment": {
                        "URL": {
                            "name": "name838",
                            "content": "http://www.oxygenxml.com/"
                        }
                    }
                },
                "Extrinsic": {"name": "name839"}
            }
        },
        "InvoiceDetailSummary": {
            "SubtotalAmount": {
                "Money": {
                    "currency": "currency202",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency202",
                    "content": 0
                }
            },
            "Tax": {
                "Money": {
                    "currency": "currency203",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency203",
                    "content": 0
                },
                "Description": {
                    "xml:lang": "en-US",
                    "type": "type360",
                    "ShortName": "ShortName357"
                },
                "TaxDetail": {
                    "purpose": "purpose15",
                    "category": "category14",
                    "percentageRate": 0,
                    "taxRateType": "taxRateType14",
                    "isVatRecoverable": "yes",
                    "isWithholdingTax": "yes",
                    "taxPointDate": "2006-05-04T18:13:51.0",
                    "paymentDate": "2006-05-04T18:13:51.0",
                    "isTriangularTransaction": "yes",
                    "exemptDetail": "zeroRated",
                    "TaxableAmount": {
                        "Money": {
                            "currency": "currency204",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency204",
                            "content": 0
                        }
                    },
                    "TaxAmount": {
                        "Money": {
                            "currency": "currency205",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency205",
                            "content": 0
                        }
                    },
                    "TaxLocation": {
                        "xml:lang": "en-US",
                        "content": "TaxLocation14"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type361",
                        "ShortName": "ShortName358"
                    },
                    "TriangularTransactionLawReference": {
                        "xml:lang": "en-US",
                        "content": "TriangularTransactionLawReference14"
                    },
                    "TaxRegime": "TaxRegime14",
                    "TaxExemption": {
                        "exemptCode": "exemptCode14",
                        "ExemptReason": {
                            "xml:lang": "en-US",
                            "content": "ExemptReason14"
                        }
                    },
                    "withholdingTaxType": {
                        "name": "name840",
                        "schemaMappedType": "Extrinsic",
                        "content": "withholdingTaxType14"
                    },
                    "taxPointDateCode": {
                        "name": "name841",
                        "schemaMappedType": "Extrinsic",
                        "content": "taxPointDateCode14"
                    },
                    "Extrinsic": {"name": "name842"}
                },
                "OldMoney": {
                    "currency": "currency206",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency206",
                    "content": 0
                },
                "DeletedMoney": {
                    "currency": "currency207",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency207",
                    "content": 0
                },
                "taxationType": {
                    "name": "name843",
                    "schemaMappedType": "Extrinsic",
                    "content": "taxationType14"
                },
                "isISSRetention": {
                    "name": "name844",
                    "schemaMappedType": "Extrinsic",
                    "content": "isISSRetention14"
                },
                "isServiceBrokerISSRetention": {
                    "name": "name845",
                    "schemaMappedType": "Extrinsic",
                    "content": "isServiceBrokerISSRetention14"
                },
                "issRetentionValue": {
                    "name": "name846",
                    "schemaMappedType": "Extrinsic",
                    "Money": {
                        "currency": "currency208",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency208",
                        "content": 0
                    }
                },
                "Extrinsic": {"name": "name847"},
                "withholdingTaxTotal": {
                    "name": "name848",
                    "schemaMappedType": "Extrinsic",
                    "Money": {
                        "currency": "currency209",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency209",
                        "content": 0
                    }
                },
                "taxTotal": {
                    "name": "name849",
                    "schemaMappedType": "Extrinsic",
                    "Money": {
                        "currency": "currency210",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency210",
                        "content": 0
                    }
                }
            },
            "shippingTax": {
                "Money": {
                    "currency": "currency211",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency211",
                    "content": 0
                },
                "Description": {
                    "xml:lang": "en-US",
                    "type": "type362",
                    "ShortName": "ShortName359"
                },
                "TaxDetail": {
                    "purpose": "purpose16",
                    "category": "category15",
                    "percentageRate": 0,
                    "taxRateType": "taxRateType15",
                    "isVatRecoverable": "yes",
                    "isWithholdingTax": "yes",
                    "taxPointDate": "2006-05-04T18:13:51.0",
                    "paymentDate": "2006-05-04T18:13:51.0",
                    "isTriangularTransaction": "yes",
                    "exemptDetail": "zeroRated",
                    "TaxableAmount": {
                        "Money": {
                            "currency": "currency212",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency212",
                            "content": 0
                        }
                    },
                    "TaxAmount": {
                        "Money": {
                            "currency": "currency213",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency213",
                            "content": 0
                        }
                    },
                    "TaxLocation": {
                        "xml:lang": "en-US",
                        "content": "TaxLocation15"
                    },
                    "Description": {
                        "xml:lang": "en-US",
                        "type": "type363",
                        "ShortName": "ShortName360"
                    },
                    "TriangularTransactionLawReference": {
                        "xml:lang": "en-US",
                        "content": "TriangularTransactionLawReference15"
                    },
                    "TaxRegime": "TaxRegime15",
                    "TaxExemption": {
                        "exemptCode": "exemptCode15",
                        "ExemptReason": {
                            "xml:lang": "en-US",
                            "content": "ExemptReason15"
                        }
                    },
                    "withholdingTaxType": {
                        "name": "name850",
                        "schemaMappedType": "Extrinsic",
                        "content": "withholdingTaxType15"
                    },
                    "taxPointDateCode": {
                        "name": "name851",
                        "schemaMappedType": "Extrinsic",
                        "content": "taxPointDateCode15"
                    },
                    "Extrinsic": {"name": "name852"}
                },
                "OldMoney": {
                    "currency": "currency214",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency214",
                    "content": 0
                },
                "DeletedMoney": {
                    "currency": "currency215",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency215",
                    "content": 0
                },
                "taxationType": {
                    "name": "name853",
                    "schemaMappedType": "Extrinsic",
                    "content": "taxationType15"
                },
                "isISSRetention": {
                    "name": "name854",
                    "schemaMappedType": "Extrinsic",
                    "content": "isISSRetention15"
                },
                "isServiceBrokerISSRetention": {
                    "name": "name855",
                    "schemaMappedType": "Extrinsic",
                    "content": "isServiceBrokerISSRetention15"
                },
                "issRetentionValue": {
                    "name": "name856",
                    "schemaMappedType": "Extrinsic",
                    "Money": {
                        "currency": "currency216",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency216",
                        "content": 0
                    }
                },
                "Extrinsic": {"name": "name857"},
                "withholdingTaxTotal": {
                    "name": "name858",
                    "schemaMappedType": "Extrinsic",
                    "Money": {
                        "currency": "currency217",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency217",
                        "content": 0
                    }
                },
                "taxTotal": {
                    "name": "name859",
                    "schemaMappedType": "Extrinsic",
                    "Money": {
                        "currency": "currency218",
                        "alternateAmount": 0,
                        "alternateCurrency": "alternateCurrency218",
                        "content": 0
                    }
                }
            },
            "SpecialHandlingAmount": {
                "Money": {
                    "currency": "currency219",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency219",
                    "content": 0
                },
                "Description": {
                    "xml:lang": "en-US",
                    "type": "type364",
                    "ShortName": "ShortName361"
                }
            },
            "ShippingAmount": {
                "Money": {
                    "currency": "currency220",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency220",
                    "content": 0
                }
            },
            "GrossAmount": {
                "Money": {
                    "currency": "currency221",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency221",
                    "content": 0
                }
            },
            "InvoiceDetailDiscount": {
                "percentageRate": 0,
                "Money": {
                    "currency": "currency222",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency222",
                    "content": 0
                }
            },
            "InvoiceHeaderModifications": {
                "Modification": {
                    "level": 0,
                    "OriginalPrice": {
                        "Money": {
                            "currency": "currency223",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency223",
                            "content": 0
                        }
                    },
                    "AdditionalCost": {
                        "Percentage": {"percent": 0}
                    },
                    "Tax": {
                        "Money": {
                            "currency": "currency224",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency224",
                            "content": 0
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type365",
                            "ShortName": "ShortName362"
                        },
                        "TaxDetail": {
                            "purpose": "purpose17",
                            "category": "category16",
                            "percentageRate": 0,
                            "taxRateType": "taxRateType16",
                            "isVatRecoverable": "yes",
                            "isWithholdingTax": "yes",
                            "taxPointDate": "2006-05-04T18:13:51.0",
                            "paymentDate": "2006-05-04T18:13:51.0",
                            "isTriangularTransaction": "yes",
                            "exemptDetail": "zeroRated",
                            "TaxableAmount": {
                                "Money": {
                                    "currency": "currency225",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency225",
                                    "content": 0
                                }
                            },
                            "TaxAmount": {
                                "Money": {
                                    "currency": "currency226",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency226",
                                    "content": 0
                                }
                            },
                            "TaxLocation": {
                                "xml:lang": "en-US",
                                "content": "TaxLocation16"
                            },
                            "Description": {
                                "xml:lang": "en-US",
                                "type": "type366",
                                "ShortName": "ShortName363"
                            },
                            "TriangularTransactionLawReference": {
                                "xml:lang": "en-US",
                                "content": "TriangularTransactionLawReference16"
                            },
                            "TaxRegime": "TaxRegime16",
                            "TaxExemption": {
                                "exemptCode": "exemptCode16",
                                "ExemptReason": {
                                    "xml:lang": "en-US",
                                    "content": "ExemptReason16"
                                }
                            },
                            "withholdingTaxType": {
                                "name": "name860",
                                "schemaMappedType": "Extrinsic",
                                "content": "withholdingTaxType16"
                            },
                            "taxPointDateCode": {
                                "name": "name861",
                                "schemaMappedType": "Extrinsic",
                                "content": "taxPointDateCode16"
                            },
                            "Extrinsic": {"name": "name862"}
                        },
                        "OldMoney": {
                            "currency": "currency227",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency227",
                            "content": 0
                        },
                        "DeletedMoney": {
                            "currency": "currency228",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency228",
                            "content": 0
                        },
                        "taxationType": {
                            "name": "name863",
                            "schemaMappedType": "Extrinsic",
                            "content": "taxationType16"
                        },
                        "isISSRetention": {
                            "name": "name864",
                            "schemaMappedType": "Extrinsic",
                            "content": "isISSRetention16"
                        },
                        "isServiceBrokerISSRetention": {
                            "name": "name865",
                            "schemaMappedType": "Extrinsic",
                            "content": "isServiceBrokerISSRetention16"
                        },
                        "issRetentionValue": {
                            "name": "name866",
                            "schemaMappedType": "Extrinsic",
                            "Money": {
                                "currency": "currency229",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency229",
                                "content": 0
                            }
                        },
                        "Extrinsic": {"name": "name867"},
                        "withholdingTaxTotal": {
                            "name": "name868",
                            "schemaMappedType": "Extrinsic",
                            "Money": {
                                "currency": "currency230",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency230",
                                "content": 0
                            }
                        },
                        "taxTotal": {
                            "name": "name869",
                            "schemaMappedType": "Extrinsic",
                            "Money": {
                                "currency": "currency231",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency231",
                                "content": 0
                            }
                        }
                    },
                    "ModificationDetail": {
                        "name": "name870",
                        "code": "code31",
                        "startDate": "2006-05-04T18:13:51.0",
                        "endDate": "2006-05-04T18:13:51.0",
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type367",
                            "ShortName": "ShortName364"
                        },
                        "Extrinsic": {"name": "name871"}
                    }
                }
            },
            "InvoiceDetailSummaryLineItemModifications": {
                "Modification": {
                    "level": 0,
                    "OriginalPrice": {
                        "Money": {
                            "currency": "currency232",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency232",
                            "content": 0
                        }
                    },
                    "AdditionalCost": {
                        "Percentage": {"percent": 0}
                    },
                    "Tax": {
                        "Money": {
                            "currency": "currency233",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency233",
                            "content": 0
                        },
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type368",
                            "ShortName": "ShortName365"
                        },
                        "TaxDetail": {
                            "purpose": "purpose18",
                            "category": "category17",
                            "percentageRate": 0,
                            "taxRateType": "taxRateType17",
                            "isVatRecoverable": "yes",
                            "isWithholdingTax": "yes",
                            "taxPointDate": "2006-05-04T18:13:51.0",
                            "paymentDate": "2006-05-04T18:13:51.0",
                            "isTriangularTransaction": "yes",
                            "exemptDetail": "zeroRated",
                            "TaxableAmount": {
                                "Money": {
                                    "currency": "currency234",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency234",
                                    "content": 0
                                }
                            },
                            "TaxAmount": {
                                "Money": {
                                    "currency": "currency235",
                                    "alternateAmount": 0,
                                    "alternateCurrency": "alternateCurrency235",
                                    "content": 0
                                }
                            },
                            "TaxLocation": {
                                "xml:lang": "en-US",
                                "content": "TaxLocation17"
                            },
                            "Description": {
                                "xml:lang": "en-US",
                                "type": "type369",
                                "ShortName": "ShortName366"
                            },
                            "TriangularTransactionLawReference": {
                                "xml:lang": "en-US",
                                "content": "TriangularTransactionLawReference17"
                            },
                            "TaxRegime": "TaxRegime17",
                            "TaxExemption": {
                                "exemptCode": "exemptCode17",
                                "ExemptReason": {
                                    "xml:lang": "en-US",
                                    "content": "ExemptReason17"
                                }
                            },
                            "withholdingTaxType": {
                                "name": "name872",
                                "schemaMappedType": "Extrinsic",
                                "content": "withholdingTaxType17"
                            },
                            "taxPointDateCode": {
                                "name": "name873",
                                "schemaMappedType": "Extrinsic",
                                "content": "taxPointDateCode17"
                            },
                            "Extrinsic": {"name": "name874"}
                        },
                        "OldMoney": {
                            "currency": "currency236",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency236",
                            "content": 0
                        },
                        "DeletedMoney": {
                            "currency": "currency237",
                            "alternateAmount": 0,
                            "alternateCurrency": "alternateCurrency237",
                            "content": 0
                        },
                        "taxationType": {
                            "name": "name875",
                            "schemaMappedType": "Extrinsic",
                            "content": "taxationType17"
                        },
                        "isISSRetention": {
                            "name": "name876",
                            "schemaMappedType": "Extrinsic",
                            "content": "isISSRetention17"
                        },
                        "isServiceBrokerISSRetention": {
                            "name": "name877",
                            "schemaMappedType": "Extrinsic",
                            "content": "isServiceBrokerISSRetention17"
                        },
                        "issRetentionValue": {
                            "name": "name878",
                            "schemaMappedType": "Extrinsic",
                            "Money": {
                                "currency": "currency238",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency238",
                                "content": 0
                            }
                        },
                        "Extrinsic": {"name": "name879"},
                        "withholdingTaxTotal": {
                            "name": "name880",
                            "schemaMappedType": "Extrinsic",
                            "Money": {
                                "currency": "currency239",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency239",
                                "content": 0
                            }
                        },
                        "taxTotal": {
                            "name": "name881",
                            "schemaMappedType": "Extrinsic",
                            "Money": {
                                "currency": "currency240",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency240",
                                "content": 0
                            }
                        }
                    },
                    "ModificationDetail": {
                        "name": "name882",
                        "code": "code32",
                        "startDate": "2006-05-04T18:13:51.0",
                        "endDate": "2006-05-04T18:13:51.0",
                        "Description": {
                            "xml:lang": "en-US",
                            "type": "type370",
                            "ShortName": "ShortName367"
                        },
                        "Extrinsic": {"name": "name883"}
                    }
                }
            },
            "TotalCharges": {
                "Money": {
                    "currency": "currency241",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency241",
                    "content": 0
                }
            },
            "TotalAllowances": {
                "Money": {
                    "currency": "currency242",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency242",
                    "content": 0
                }
            },
            "TotalAmountWithoutTax": {
                "Money": {
                    "currency": "currency243",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency243",
                    "content": 0
                }
            },
            "NetAmount": {
                "Money": {
                    "currency": "currency244",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency244",
                    "content": 0
                }
            },
            "DepositAmount": {
                "Money": {
                    "currency": "currency245",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency245",
                    "content": 0
                }
            },
            "DueAmount": {
                "Money": {
                    "currency": "currency246",
                    "alternateAmount": 0,
                    "alternateCurrency": "alternateCurrency246",
                    "content": 0
                }
            },
            "InvoiceDetailSummaryIndustry": {
                "InvoiceDetailSummaryRetail": {
                    "AdditionalAmounts": {
                        "TotalRetailAmount": {
                            "Money": {
                                "currency": "currency247",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency247",
                                "content": 0
                            }
                        },
                        "InformationalAmount": {
                            "Money": {
                                "currency": "currency248",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency248",
                                "content": 0
                            }
                        },
                        "GrossProgressPaymentAmount": {
                            "Money": {
                                "currency": "currency249",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency249",
                                "content": 0
                            }
                        },
                        "TotalReturnableItemsDepositAmount": {
                            "Money": {
                                "currency": "currency250",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency250",
                                "content": 0
                            }
                        },
                        "GoodsAndServiceAmount": {
                            "Money": {
                                "currency": "currency251",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency251",
                                "content": 0
                            }
                        },
                        "ExactAmount": {
                            "Money": {
                                "currency": "currency252",
                                "alternateAmount": 0,
                                "alternateCurrency": "alternateCurrency252",
                                "content": 0
                            }
                        }
                    }
                }
            }
        }
    }
}