import { Component, OnInit, Input, OnChanges, ViewChild } from '@angular/core';
import { AddressDataModel } from 'src/app/shared/model/Temporary/AddressDataModel';
import { AddressService } from 'src/app/address/address.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit , OnChanges{
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
   
  }
  @ViewChild('f') addressForm:NgForm;
  @Input() addressType: string;
  @Input() addressVal: AddressDataModel;
  @Input() group: string;
  constructor(private addressService: AddressService) { }

  ngOnInit() {
   
  }

}
