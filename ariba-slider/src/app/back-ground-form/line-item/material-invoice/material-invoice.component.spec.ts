import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialInvoiceComponent } from './material-invoice.component';

describe('MaterialInvoiceComponent', () => {
  let component: MaterialInvoiceComponent;
  let fixture: ComponentFixture<MaterialInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
