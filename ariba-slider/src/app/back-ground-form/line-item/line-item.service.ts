import { Injectable , EventEmitter} from '@angular/core';

@Injectable()
export class LineItemService {
  lineItemAdded= new EventEmitter();
  lineItemEdit=new EventEmitter();
  lineItemEdited = new EventEmitter();
  taxDetailAdded = new EventEmitter();
  taxDetailUpdated = new EventEmitter();
  constructor() { }
  addLineItem(data){
    this.lineItemAdded.emit(data);
  }
  updateLineItemData(data){
    this.lineItemEdited.emit(data);
  }

  addTaxDetail(data){
    this.taxDetailAdded.emit(data);
  }

  openLineItemEdit(data){
    this.lineItemEdit.next(data);
  }

  updateTaxDetail(data){
    this.taxDetailUpdated.next(data);
  }
}
