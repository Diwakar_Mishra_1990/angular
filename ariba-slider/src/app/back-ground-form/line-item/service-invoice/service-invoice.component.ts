import { Component, EventEmitter, Input, OnInit, Output, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { LineItemService } from '../line-item.service';

@Component({
  selector: 'app-service-invoice',
  templateUrl: './service-invoice.component.html',
  styleUrls: ['./service-invoice.component.css']
})
export class ServiceInvoiceComponent implements OnInit {
  @Input() isEditMode;
  @Output() serviceInvoiceAddedd = new EventEmitter();
  @Output() serviceInvoiceUpdated = new EventEmitter();
  @Input() lineItemData = {
    'customerPart': "",
    'description': "",
    'lineNumber': "",
    'price': "",
    'supplierPart': "",
    'total': "",
    'type': "service",
    'unitQuantity': "",
    'uom': "",
  };
  @ViewChild('f') serviceInvForm;
  @Input() index: string;
  constructor(private lineItemService: LineItemService, private elRef: ElementRef,
    private renderer: Renderer2, ) { }

  ngOnInit() {

    this.lineItemService.lineItemEdit.subscribe(
      (response) => {
        if (response.index == this.index) {
          this.index = response.index;
          this.serviceInvForm.setValue(response.value);
          this.lineItemData = response.value;
        }
      }
    );
  }
  submitForm(form) {
    form.value.type = "service";
    this.serviceInvoiceAddedd.emit(form.value);
  }

  updateLineItem(form) {
    form.value.type = "service";
    form.index = this.index;
    this.serviceInvoiceUpdated.emit(form);
  }
}
