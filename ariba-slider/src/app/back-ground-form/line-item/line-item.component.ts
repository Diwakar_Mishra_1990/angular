import { Component, OnInit, ElementRef, Renderer2, Output, Input } from '@angular/core';
import { EventEmitter } from 'events';
import { FormArray, FormGroup, FormControl } from '@angular/forms';
import { LineItemService } from './line-item.service';
import { LineItemDataModel } from 'src/app/shared/model/Temporary/LineItemDataModel';

@Component({
  selector: 'app-line-item',
  templateUrl: './line-item.component.html',
  styleUrls: ['./line-item.component.css']
})
export class LineItemComponent implements OnInit {
  isMaterialInvoice = false;
  isServiceInvoice = false;
  editMode = false;
  invoiceForm: FormGroup;
  dataToEdit;
  @Input() lineItems: LineItemDataModel[];
  @Output() updatePanelState = new EventEmitter();
  constructor(private lineItemService: LineItemService) { }

  ngOnChanges(){
    this.initForm();
  }

  getLineItemsControl(){
    return (<FormArray>this.invoiceForm.get('lineItems')).controls;
  }
  ngOnInit() {

    this.initForm();
    this.lineItemService.lineItemAdded.subscribe(
      (response) => {
        this.addLineItem(response);
      }
    );

    this.lineItemService.lineItemEdited.subscribe(
      (response) => {
        this.updateLineItem(response);
      }
    );

  }
  private initForm() {
    let formlineItems = new FormArray([]);
    for (var i = 0; i < this.lineItems.length; i++) {
      let taxDetailArray = new FormArray([]);
      for (var j = 0; j < this.lineItems[i].taxDetail.length; j++) {
        taxDetailArray.push(
          new FormGroup(
            {
              'category': new FormControl(this.lineItems[i].taxDetail[j].category),
              'description': new FormControl(this.lineItems[i].taxDetail[j].description),
              'exemptDetail': new FormControl(this.lineItems[i].taxDetail[j].exemptDetail),
              'location': new FormControl(this.lineItems[i].taxDetail[j].location),
              'percentage': new FormControl(this.lineItems[i].taxDetail[j].percentage),
              'taxAmount': new FormControl(this.lineItems[i].taxDetail[j].taxAmount),
              'taxRateType': new FormControl(this.lineItems[i].taxDetail[j].taxRateType),
              'taxRegime': new FormControl(this.lineItems[i].taxDetail[j].taxRegime),
              'taxableAmount': new FormControl(this.lineItems[i].taxDetail[j].taxableAmount),
              'vatDueTimeCode': new FormControl(this.lineItems[i].taxDetail[j].vatDueTimeCode),
              'withholdingType': new FormControl(this.lineItems[i].taxDetail[j].withholdingType)
            }
          )
        );
      }
      formlineItems.push(
        new FormGroup(
          {
            'type': new FormControl(this.lineItems[i].type),
            'lineNumber': new FormControl(this.lineItems[i].lineNumber),
            'customerPart': new FormControl(this.lineItems[i].customerPart),
            'description': new FormControl(this.lineItems[i].description),
            'price': new FormControl(this.lineItems[i].price),
            'supplierPart': new FormControl(this.lineItems[i].supplierPart),
            'total': new FormControl(this.lineItems[i].total),
            'unitQuantity': new FormControl(this.lineItems[i].unitQuantity),
            'uom': new FormControl(this.lineItems[i].uom),
            'taxDetails': taxDetailArray
          }
        )
      );
    }
    this.invoiceForm = new FormGroup(
      {
        'lineItems': formlineItems
      }
    );
  }

  onAddMaterialInvoice() {
    this.isMaterialInvoice = true;
    this.isServiceInvoice = false;
  }
  onAddServiceInvoice() {
    this.isServiceInvoice = true;
    this.isMaterialInvoice = false;
  }

  addMaterialInvoice(event) {
    this.lineItemService.addLineItem(event);
  }
  addServiceInvoice(event) {
    this.lineItemService.addLineItem(event);
  }



  addLineItem(event) {
    let lineItems = new FormArray([]);
    let taxDetails = new FormArray([]);
    lineItems = <FormArray>this.invoiceForm.get('lineItems');
    lineItems.push(
      new FormGroup(
        {
          'type': new FormControl(event.type),
          'lineNumber': new FormControl(event.lineNumber),
          'customerPart': new FormControl(event.customerPart),
          'description': new FormControl(event.description),
          'price': new FormControl(event.price),
          'supplierPart': new FormControl(event.supplierPart),
          'total': new FormControl(event.total),
          'unitQuantity': new FormControl(event.unitQuantity),
          'uom': new FormControl(event.uom),
          'taxDetails':taxDetails
        }
      )
    );
  }

  updateLineItem(event) {
    let lineItems = new FormArray([]);
    lineItems = <FormArray>this.invoiceForm.get('lineItems');
    lineItems.at(event.index).patchValue(
      {
        'lineNumber': event.value.lineNumber,
        'customerPart': event.value.customerPart,
        'description': event.value.lineNumber,
        'price': event.value.price,
        'supplierPart': event.value.supplierPart,
        'total': event.value.total,
        'unitQuantity': event.value.unitQuantity,
        'uom': event.value.uom,
        'type': event.value.type
      }
    )
  }

  editLineItem(event) {
    this.dataToEdit = event;
    if (event.value.type == "material") {
      this.isMaterialInvoice = true;
      this.isServiceInvoice = false;
    }
    else if (event.value.type == "service") {
      this.isServiceInvoice = true;
      this.isMaterialInvoice = false;
    }
  }
}
