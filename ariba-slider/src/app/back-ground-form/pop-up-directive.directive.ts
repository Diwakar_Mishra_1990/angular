import { Directive, HostBinding, HostListener, Renderer2, ElementRef } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class PopUpDirectiveDirective {
  @HostBinding('class.open') isOpen = false;
  @HostListener('click') toggleOpen() {
    this.isOpen = !this.isOpen;
  }
  constructor(private elRef: ElementRef,
    private renderer: Renderer2) { }
  @HostListener('document:click', ['$event.target', '$event'])
  public onClick(targetElement, event) {
    if (this.elRef) {
      const clickedInside = this.elRef.nativeElement.contains(targetElement);
      if (!clickedInside) {
        if (this.isOpen) {
          this.isOpen = !this.isOpen;
        }
      }
    }
  }
}
