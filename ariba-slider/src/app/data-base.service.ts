import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataBaseService {

  constructor(private http:HttpClient) { }
  dataRetrieved = new EventEmitter();

  postFormData(postData) {
    
    return this.http.post('https://sliderproject-3b9b0.firebaseio.com/invoiceDetail.json', postData).subscribe(
      (response)=>{
        console.log(response);
      }
    );
    
  }

 

  getFormData(){
    return this.http.get('https://sliderproject-3b9b0.firebaseio.com/invoiceDetail.json')
  }

  putFormData(eventData){
    return this.http.put('https://sliderproject-3b9b0.firebaseio.com/invoiceDetail.json',eventData).subscribe(
      (response)=>{
        console.log(response);
      }
    )
  }

}
