import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaxDetailComponent } from './tax-detail/tax-detail.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [TaxDetailComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports:[TaxDetailComponent]
})
export class TaxModule { }
