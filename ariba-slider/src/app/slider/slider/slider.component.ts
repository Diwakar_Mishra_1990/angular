import { ChangeDetectionStrategy, Component, Input, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { animate, state, AnimationEvent, style, transition, trigger } from '@angular/animations';
import { SliderState } from '../sliderModel/slider-state';
import { SliderService } from './slider.service';


@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('slide', [
      state(SliderState.OPEN, style({ transform: 'translateX(0%)' })),
      state(SliderState.CLOSE, style({ transform: 'translateX(75%)' })),
      transition('0 => 1', animate(100)),
      transition('1 => 0', animate(100))
    ])
  ]
})
export class SliderComponent implements OnInit {
  @Input() activePane: SliderState = SliderState.CLOSE;
  @Input() sliderId: string;
  @Input() customTrigger: boolean;
  @Input() panelOpen: boolean = false;
  @Input() showEditButton: string = "1";
  isDisabled: boolean = true;
  constructor(private el: ElementRef, private renderer: Renderer2,
    private sliderService: SliderService) { }

  ngOnInit() {

  }

  @Input()
  set top(val) {
    this.renderer.setStyle(this.el.nativeElement, 'top', `${val}`);
  }
  @Input()
  set marginRight(val) {
    this.renderer.setStyle(this.el.nativeElement, 'margin-right', `${val}`);
  }

  togglePane() {
    if (this.activePane == SliderState.CLOSE) {
      this.openPanel()
    }
    else {
      this.closePanel()
    }
  }

  toggleButtonView() {
    this.isDisabled = !this.isDisabled;
  }

  handleEventCompletion(event: AnimationEvent) {
    if (event.fromState == "void") {
      this.panelOpen = false;
    }
    if (event.fromState == SliderState.CLOSE && event.toState == SliderState.OPEN) {
      this.panelOpen = true;
    }
    else if (event.fromState == SliderState.OPEN && event.toState == SliderState.CLOSE) {
      this.panelOpen = false;
    }
  }
  handleEventStart(event) {
  }
  closePanel() {
    this.activePane = SliderState.CLOSE;
    this.renderer.setStyle(this.el.nativeElement, 'z-index', 0);
  }
  openPanel() {
    this.activePane = SliderState.OPEN;
    this.panelOpen = true;
    this.renderer.setStyle(this.el.nativeElement, 'z-index', 20);
  }
}
