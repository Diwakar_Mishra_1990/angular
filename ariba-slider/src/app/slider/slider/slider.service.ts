import { EventEmitter, Injectable } from '@angular/core';
@Injectable()
export class SliderService {
    close: EventEmitter<string>= new EventEmitter<string>();
    open: EventEmitter<string>= new EventEmitter<string>();
    closeAll(id: string) {
        this.close.emit(id);
    }
    openAll(id: string) {
        this.open.emit(id);
    }
}