import { Injectable, OnInit } from "@angular/core";
import { root } from "rxjs/internal/util/root";
import { Observable, Subject, BehaviorSubject, timer } from "rxjs";
import { Course } from "../model/course";
import { createHttpObservable } from "./util";
import { tap, map, shareReplay, retryWhen, delayWhen } from "rxjs/operators";
import { fromPromise } from "rxjs/internal-compatibility";

@Injectable({
    providedIn:root
})
export class Store implements OnInit{

    private subject = new BehaviorSubject<Course[]>([]);
    course$ : Observable<Course[]>=this.subject.asObservable();
    
    ngOnInit(): void {
        
        const http$ = createHttpObservable('/api/courses');

        http$
            .pipe(
                tap(() => console.log("HTTP request executed")),
                map(res => Object.values(res["payload"]) )
            ).subscribe(
                courses=>this.subject.next(courses)
            );

    }

    selectAdvancedCourses(): Observable<Course[]> {
        return this.filterByCategory('BEGINNER');
    }
    selectBeginnerCourses(): Observable<Course[]> {
        return this.filterByCategory('ADVANCED');
    }

    filterByCategory(category:string){
        return this.course$
        .pipe(
            map(courses => courses
                .filter(course => course.category == category))
        );
    }

    saveCourse(courseId: number, changes: any):any {
        //MODIFY IN CLIENT
        const courses = this.subject.getValue();
        const courseIndex = courses.findIndex(course =>course.id == courseId);
        const newCourses = courses.slice(0);
        newCourses[courseIndex] = {
            ...courses[courseIndex],
            ...changes
        }
        this.subject.next(newCourses);
        //PUT IN SERVER
        return fromPromise(
            fetch('/api/courses/${courseId}',
            {
                method: 'PUT',
                body: JSON.stringify(changes),
                headers: {
                    'content-type': 'application/json'
                }
            }
            )
        )
    }
}