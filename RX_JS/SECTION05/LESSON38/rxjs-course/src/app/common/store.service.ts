import { Injectable } from "@angular/core";
import { root } from "rxjs/internal/util/root";
import { Observable, Subject, BehaviorSubject } from "rxjs";
import { Course } from "../model/course";

@Injectable({
    providedIn:root
})
export class Store {
    private subject = new BehaviorSubject<Course[]>([]);
    course$ : Observable<Course[]>=this.subject.asObservable();

}