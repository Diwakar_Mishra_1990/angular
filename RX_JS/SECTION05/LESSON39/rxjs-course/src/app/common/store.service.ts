import { Injectable, OnInit } from "@angular/core";
import { root } from "rxjs/internal/util/root";
import { Observable, Subject, BehaviorSubject, timer } from "rxjs";
import { Course } from "../model/course";
import { createHttpObservable } from "./util";
import { tap, map, shareReplay, retryWhen, delayWhen } from "rxjs/operators";

@Injectable({
    providedIn:root
})
export class Store implements OnInit{

    private subject = new BehaviorSubject<Course[]>([]);
    course$ : Observable<Course[]>=this.subject.asObservable();
    
    ngOnInit(): void {
        
        const http$ = createHttpObservable('/api/courses');

        http$
            .pipe(
                tap(() => console.log("HTTP request executed")),
                map(res => Object.values(res["payload"]) )
            ).subscribe(
                courses=>this.subject.next(courses)
            );

    }

    selectAdvancedCourses(): Observable<Course[]> {
        return this.filterByCategory('BEGINNER');
    }
    selectBeginnerCourses(): Observable<Course[]> {
        return this.filterByCategory('ADVANCED');
    }

    filterByCategory(category:string){
        return this.course$
        .pipe(
            map(courses => courses
                .filter(course => course.category == category))
        );
    }
}