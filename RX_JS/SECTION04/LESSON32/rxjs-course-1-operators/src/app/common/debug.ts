import { Observable } from "rxjs"
import { tap } from "rxjs/operators"
import { CourseComponent } from "../course/course.component";

export  enum RxJsLoggingLevel{
    TRACE,
    DEBUG,
    INFO,
    ERROR
}

let rxJsLoggingLevel = RxJsLoggingLevel.INFO;

export function setRxJsLoggingLevel(level:RxJsLoggingLevel){
    rxJsLoggingLevel = level;
}
export const debug =  (level:any,message:any)=>{
    (source: Observable<any>) => source.pipe(
        tap(val=>{
            if(level>= rxJsLoggingLevel){
                console.log(message+ ' : '+val);
            }
        })
    )
}




