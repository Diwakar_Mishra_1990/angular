import { Observable } from "rxjs";


export function createHttpObservable(url): any {
    return Observable.create(observer => {
        // This is an abort controller
        const controller = new AbortController();
        // This is the signal for aborting the observable
        const signal = controller.signal;
        fetch('/api/courses', { signal }).
            then(response => {
                return response.json();
            })
            .then(body => {
                observer.next(body);
                observer.complete();
            })
            .catch(err => {
                observer.error(err);
            });
        return ()=>controller.abort(); // This is activated when the unsubscribe is called.
    });
}