import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Course} from "../model/course";
import {
    debounceTime,
    distinctUntilChanged,
    startWith,
    tap,
    delay,
    map,
    concatMap,
    switchMap,
    withLatestFrom,
    concatAll, shareReplay
} from 'rxjs/operators';
import {merge, fromEvent, Observable, concat} from 'rxjs';
import {Lesson} from '../model/lesson';
import { createHttpObservable } from '../common/util';


@Component({
    selector: 'course',
    templateUrl: './course.component.html',
    styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit, AfterViewInit {


    course$: Observable<Course>;
    lessons$: Observable<Lesson[]>;
    courseId:any

    @ViewChild('searchInput', { static: true }) input: ElementRef;

    constructor(private route: ActivatedRoute) {


    }

    ngOnInit() {

        const courseId = this.route.snapshot.params['id'];
        this.course$ = createHttpObservable('/api/courses/${courseId}')

    }

    ngAfterViewInit() {
        //Starts with to remove concatenation
        const lessons$ = fromEvent(this.input.nativeElement,'keyup').pipe(
            map(event => event.target.value),
            startWith(''),//start with is udsed to add event
            debounceTime(400),//Added debounce time of 400ms
            distinctUntilChanged(),
            switchMap(search =>this.loadLessons(search))
        )
        ;
        // this can be replaced as eith follows
        // const initialLesson$ = this.loadLessons();
        //this.lessons$ = concat(initialLesson$,searchLesson$);

    }

    loadLessons(search=''):Observable<Lesson[]>{
        return createHttpObservable('/api/lessons?courseId=${this.courseId}&pageSize=100$filter=${search}')
                        .pipe(
                            map(res=>res['payload'])
                        );
    }
}
