We're able to get our state from the store with the help of the async pipe but more importantly, with
the help of the injected store and the select method and just to make this really clear, this is just an observable.
So if you need it that state anywhere else but in the template, so in a place where you can't use the async
pipe, you of course don't have to. The async pipe is just an option, you can also manually subscribe to your selected state,
it's an observable and therefore of course, you can call subscribe. Now here, NgRx and Angular should also clear the subscription for you
but to be super safe, I would recommend storing this in a subscription property and try clearing it manually
as well just to avoid memory leaks and bugs. Here however, I don't need such a subscription and therefore, let's move on and let's dive into how we dispatch
actions. Where do I want to dispatch actions?
Well if we're talking about the shopping list part of the app, that would be in a shopping-edit component,
in the shopping-edit component, in the end we got our code to dispatch actions whenever we're done,
so whenever we're submitting our form to either update or add an ingredient,
these are the two places where we change our ingredients that are displayed and therefore the two
places where we change our shopping list state and therefore should change the store.
Now how do we dispatch actions? Again with the help of the store service.
So now in the shopping edit component, let's again inject a store of type store which you need to import from @ngrx/store,
so that hasn't changed, that import needs to be added.
And with it injected, again this is a generic type where you need to tell NgRx which type of data
is stored in that store and for the moment, we can simply copy that type from our shopping list component,
though later we'll have a more elegant approach for that
but for the moment, let's just copy that over here,
so this object with shopping list and then ingredients which is of type ingredient array and with the store
injected, we use it in the places where we want to dispatch an action.
We haven't set up an action for adding and updating ingredients yet,
so for the moment, let's simply take care about adding ingredients and let's dispatch an action there.
For this, instead of calling an ingredient on the shopping list service which we won't use anymore because
we're also not getting our data from there anymore,
keep in mind that in the shopping list component, we are now fetching our ingredients from the store, from the @ngrx/store,
so we have to change our data there as well, changing it in the service won't do anything because that's
not our data source anymore.
So now here in the shopping-edit component, we can access the injected store and besides the select
method which we already used,
there also is a dispatch method and we use that to, you guessed it, dispatch actions.
Now keep in mind that our actions for the shopping list area are defined here in the shopping list actions
file and thus far, we only got one which is the add ingredient action, it has a type
and it has a payload. So now we can create a new type, a new object based on that action class here and dispatch it. For that,
let me import everything as shopping list actions again because later, we will have multiple actions, from the store
folder and there, from the shopping list actions file and on that shopping list actions object which
now bundles together all exports, there we can create a new object based on the add ingredient class that's based in that shopping list
actions object, so which is based in that shopping list actions file. Add ingredient is just a class we have in here,
now this class has a type and a payload. Now it would be nice if we could pass the payload as an argument to the constructor, right
because then we would have all in one place and that of course is possible by simply editing the action
class a little bit, instead of adding payload just as a property like this, we can add a constructor function where we accept
a payload argument. I add public in front of that to use that TypeScript trick where payload is then
automatically converted into a public property with the same name as well and the payload argument here
and you could name this differently gets a value of type ingredient and
that's my constructor. Now this has to be public because of course I want to be able to access the payload
from inside my reducer where I'm extracting it to store that ingredient in the ingredients array. In
the shopping-edit component, now we can pass the ingredient which we want to create here or which we want to add here to the add
ingredient class constructor which we're calling with the new keyword.
So we simply pass new ingredient here and we're now taking advantage of our action class and this will
dispatch this action to our store then. So what the flow will be then is that as soon as we add an ingredient,
again updating won't have an effect right now but if we add an ingredient, we dispatch this action which
is clearly defined in the actions file to that store, to the @ngrx/store,
keep in mind that we only have one such store in the entire application, setup here in the app module
and that store is aware of the shopping list reducer because we have to create such a store with
all the reducers that matter and you will soon learn how to add multiple reducers.
So then the action automatically reaches all the reducers that our store knows,
so in this case this one reducer here and in that reducer, the action is therefore passed in as a second
argument and now we can check the different types of actions and react appropriately,
that is what's happening here. And therefore with all of that, we should be able to add sell it or anything
else to our list here and automatically see an update
and whilst this looks exactly like the behavior we had before already, this is now entirely powered by
NgRx because we're dispatching the action when we add something through NgRx and we're also displaying
the data based on our selection from NgRx, so we're using NgRx end-to-end for displaying and adding
ingredients. Now it's of course time to make sure that we can also update and delete our ingredients because these
are the things our shopping list service also supported
and since we're basically replacing it with the store, we should of course make sure that we handle these
cases with our reducer in our store as well.