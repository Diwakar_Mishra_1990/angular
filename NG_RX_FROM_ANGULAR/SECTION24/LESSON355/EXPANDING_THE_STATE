If we have a look at our shopping-edit component here, we of course load data in ngOnInit from
the shopping lists service, to be precise,
we actually manage our current editing state in the shopping lists service as well and we get our ingredient
which we're editing here from the shopping list service.
We're getting it with get ingredient and then we're setting this on the form,
so on the shopping list form. Hence the values we enter into the shopping list form are not coming
from our @ngrx/store but still from the shopping list service
and in addition to that, we also use the shopping list service to control whether we currently are editing
or not.
Now we do have that subscription to started editing which is managed in a shopping list service because
we do actually start the editing process from inside the shopping list component, not from inside the
shopping list edit component. From inside the shopping list component, we have the onEditItem function
where we can start editing. Now to translate this into the NgRx world,
it would make more sense if we dispatch an action here in the shopping list component to start editing,
then the @ngrx/store will automatically kind of use a subject behind the scenes to inform our entire
application about that
and then in the shopping-edit component, we could simply listen to our store and see if we started editing
and if we did, we can kind of run the same logic as here,
we would want to know which index, which item we are editing, so what I'm also passing around as information right now,
I'm also sharing the index here.
So we would want to manage this in NgRx and then when we get our ingredient here which we want to
edit, then we also want to get this from our @ngrx/store, that would be the idea here. Hence to implement this in an
easy to use way, we can go to our shopping list reducer and change our state for the first time.
We haven't done this before but now we need more than just the ingredients because now for a shopping
list to effectively update the different parts of our application that are involved here,
I also want to store the editedIngredient here which is of type ingredient and also the edited
ingredient index which is of type number,
so two new members, two new fields here in our initial state. Here however we don't need the types,
we need values because this is an object we're creating, not a type definition.
So the ingredient with which I start will be null and the number will be minus one,
it's not zero because zero would already be a valid index, minus one
is not. Now we'll automatically have one issue before we've done anything else,
we changed the structure of our state
and please remember that we have different places in our app, like the shopping list component where
we injected store and in all these places, we do share a print, a footprint, a definition of our store
or of the data in our store,
we define how the data in our store looks like, that it's having a shopping list part which holds our ingredients.
Now this is the part which won't really work anymore because now it's not just ingredients,
now we also have the editedIngredient and the editedIngredientsIndex and of course, we could now
add this here but that would be pretty cumbersome,
we would have to add this everywhere in our application and if we ever change the structure of our state,
of our shopping list state again, we again have to edit that everywhere and therefore, we can use a simpler
pattern which makes managing this a bit easier and more convenient. We can export an interface here to
create our own type definition which we can name state and that describes how our state for this
reducer looks like, that it has an ingredients property which holds an array of ingredients, that it has
an editedIngredient which is a single ingredient and that it has an editedIngredientIndex which is
a number.
Now we know that this initial state will be of that type, so we can already add that type assignment
here, so this is now type state, of this state here and the state we're getting here also will be of that type,
just to be super clear. Now with that state defined here, we can use that in the shopping list component
and all the other components here instead of this state or type definition but we can even take it a step further.
This is just the state for the shopping list reducer. Now right now that's the only reducer we have
but in the future, we'll have multiple reducers and then this shopping list part here is only a part
of our global state.
In the app module, we can see that the shopping list reducer is registered under the shopping list key
and indeed, in the places where we do inject our store, we do reference that shopping list key too.
So since we're already merging states into our own objects, we can export another interface here, App
State, to describe the application-wide state, not just the state for this reducer
and that would be shopping list which then in turn holds a reference to this state, maybe order it
like this though it doesn't matter, would have worked the other way around too
but now we're clear that our entire application state is about an object which has a shopping list area
where it then in turn we have data of this type stored in. And now we can go to the different places where we inject store,
like here in the shopping list component and reference this AppState and the convention for this, for the
import is that you import everything as from shopping list, from the store folder and there, the shopping
list reducer file. Now why from shopping list?
It's just a convention you also find in the official NgRx docs, you typically use that for describing
an import to your reducer and/or your state for a certain part of your application.
And now from our shopping list part of the NgRx state so to say, we import something and we use that
here, when we inject the store our type is now from shopping list AppState because this is now describing
our whole application state as it is seen from inside the shopping list reducer and as it is relevant
for when we use the shopping list reducer to be precise. Our AppState might have more than just that
but from the shopping list reducer's point of view and in this file, we're interested in the shopping
list reducer in the end, the AppState looks like this.
Now we get other places where we injected the store, shopping-edit component for example. There, I now
also want to use that from shopping list import, so import as from shopping list, from the store folder
and then the shopping list reducer file and then it's just the same logic here, when we inject the store
we reference from shopping list AppState. Now the same in the recipes service, in the recipes service where
we also inject the store, here
I also want to import everything as from shopping list from my shopping list
part of the application, the store folder, the shopping list reducer file and in the injection of the
store, we now reference from shopping list AppState. With that, I think I replaced all the places where
we use that, now we have a nicer type definition for our state that is more flexible because now if we add something
new, we only need to add it here in our state definition in the shopping lists reducer file and in
all the places where we inject that store, TypeScript will then know what's inside of that store.
But that was not the main problem we tried to solve, the main problem was that we want to manage the editedIngredient and its index
here in NgRx and we'll do that next.