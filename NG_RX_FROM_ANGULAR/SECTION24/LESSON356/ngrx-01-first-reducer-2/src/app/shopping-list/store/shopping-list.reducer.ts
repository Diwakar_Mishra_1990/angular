import { Action } from '@ngrx/store';

import { Ingredient } from '../../shared/ingredient.model';
import * as actions from './shopping-list.actions';

export interface AppState{
  shoppingList:State;
}

export interface State{
  ingredients:Ingredient[];
  editedIngredient: Ingredient;
  editedIngredientIndex:number
}

const initialState:State = {
  ingredients: [
    new Ingredient('Apples', 5),
    new Ingredient('Tomatoes', 10),
  ],
  editedIngredient :null,
  editedIngredientIndex:-1
};


export function shoppingListReducer(state = initialState, action: actions.ShoppingListActions) {
  switch (action.type) {
    case actions.ADD_INGREDIENT:
      return {
        ...state,
        ingredients: [...state.ingredients, action.payload]
      };
    case actions.ADD_INGREDIENTS:
      return{
        ... state,
        ingredients:[...state.ingredients, ...action.payload]
      }
    case actions.UPDATE_INGREDIENT:
      const ingredient = state.ingredients[action.payload.index];
      const updatedIngredient ={
        ...ingredient,
        ...action.payload.ingredient
      };
      const updatedIngredients =[...state.ingredients];
      updatedIngredients[action.payload.index] =  updatedIngredient;
      return{
        ...state,
        ingredients:updatedIngredients
      };
    case actions.DELETE_INGREDIENT:
    return{
      ...state,
      ingredients:state.ingredients.filter((ingredient,igindex)=>{
        return igindex!=action.payload
      })
    }
    case actions.START_EDIT:
      return {
        ...state,
        editedIngredientIndex: action.payload,
        editedIngredient :state.ingredients[action.payload]
      }
    case actions.STOP_EDIT:
      return {
        ...state,
        editedIngredientIndex:-1
      }
    default:
      return state;
  }
}
