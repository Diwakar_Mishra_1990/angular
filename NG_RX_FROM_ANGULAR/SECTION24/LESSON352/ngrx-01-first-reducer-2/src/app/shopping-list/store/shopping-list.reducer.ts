import { Action } from '@ngrx/store';

import { Ingredient } from '../../shared/ingredient.model';
import * as actions from './shopping-list.actions';

const initialState = {
  ingredients: [
    new Ingredient('Apples', 5),
    new Ingredient('Tomatoes', 10),
  ]
};

export function shoppingListReducer(state = initialState, action: actions.ShoppingListActions) {
  switch (action.type) {
    case actions.ADD_INGREDIENT:
      return {
        ...state,
        ingredients: [...state.ingredients, action.payload]
      };
    case actions.ADD_INGREDIENTS:
      return{
        ... state,
        ingredients:[...state.ingredients, ...action.payload]
      }
    default:
      return state;
  }
}
