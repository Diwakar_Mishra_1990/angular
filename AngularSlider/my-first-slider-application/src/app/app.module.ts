import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BackGroundFormModule } from './back-ground-form/back-ground-form.module';
import { SliderModule } from './slider/slider.module';
import { LineItemService } from './back-ground-form/line-item/line-item.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BackGroundFormModule,
    SliderModule,
    HttpClientModule
  ],
  providers: [LineItemService],
  bootstrap: [AppComponent]
})
export class AppModule { }
