import { TestBed } from '@angular/core/testing';

import { SliderTypeService } from './slider-type.service';

describe('SliderTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SliderTypeService = TestBed.get(SliderTypeService);
    expect(service).toBeTruthy();
  });
});
