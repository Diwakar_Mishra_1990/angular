import { Directive, OnInit, HostListener, ElementRef, Renderer, Renderer2 } from '@angular/core';
import { SliderComponent } from '../slider/slider.component';
import { SliderState } from '../sliderModel/slider-state';

@Directive({
  selector: '[appSliderDirective]'
})
export class SliderDirective implements OnInit {

  ngOnInit(): void {
  }
  constructor(private elRef: ElementRef,
    private renderer: Renderer2,
    private slider: SliderComponent) { }

  close() {
    this.slider.closePanel();
  }

  @HostListener('document:click', ['$event.target', '$event'])
  public onClick(targetElement, event) {
    if (this.elRef) {
      const clickedInside = this.elRef.nativeElement.contains(targetElement);
      if (clickedInside) {
        
        if (event.srcElement.attributes.formtrigger && this.slider.activePane === SliderState.OPEN) {
          this.slider.closePanel();
        }
      
      }
      if (!clickedInside) {
        let appTrigger= event.srcElement.attributes.apptrigger ;
        let name = event.srcElement.attributes.name;

        if(appTrigger){
          if(appTrigger.value == this.slider.sliderId){
            this.slider.openPanel();
          }
          else if(name && appTrigger.value+"_"+name.value== this.slider.sliderId){
            this.slider.openPanel();
          }
          else{
            this.close();
          }
        }
        else {
          this.close();
        }
      }
    }
  }
}