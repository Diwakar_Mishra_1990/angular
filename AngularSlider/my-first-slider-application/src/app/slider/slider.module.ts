import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderComponent } from './slider/slider.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SliderDirective } from './sliderDirective/slider-directive.directive';

@NgModule({
  declarations: [SliderComponent, SliderDirective],
  imports: [
    CommonModule,
    BrowserAnimationsModule
  ],
  exports:[
    SliderComponent , SliderDirective
  ]
})
export class SliderModule { }
