import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxDetatilTableComponent } from './tax-detatil-table.component';

describe('TaxDetatilTableComponent', () => {
  let component: TaxDetatilTableComponent;
  let fixture: ComponentFixture<TaxDetatilTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxDetatilTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxDetatilTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
