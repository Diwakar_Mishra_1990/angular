import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { LineItemService } from '../line-item.service';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-material-invoice',
  templateUrl: './material-invoice.component.html',
  styleUrls: ['./material-invoice.component.css']
})
export class MaterialInvoiceComponent implements OnInit {

  @Input() isEditMode: boolean;
  @Output() materialInvoiceAddedd = new EventEmitter();
  @Output() materialInvoiceUpdated = new EventEmitter();
  @Input() index: string;
  @Input() lineItemData = {
    'customerPart': "",
    'description': "",
    'lineNumber': "",
    'price': "",
    'supplierPart': "",
    'total': "",
    'type': "material",
    'unitQuantity': "",
    'uom': ""
  };
  @ViewChild('f') materialInvForm;
  ngOnInit() {
    this.lineItemService.lineItemEdit.subscribe(
      (response) => {
          if(response.index== this.index){
            this.index = response.index;
            this.materialInvForm.setValue(response.value);
            this.lineItemData = response.value;
          }
      }
    );
  }

  submitForm(form) {
    form.value.type = "material";
    this.materialInvoiceAddedd.emit(form.value);
  }

  updateLineItem(form) {
    form.value.type = "material";
    form.index = this.index;
    this.materialInvoiceUpdated.emit(form);
  }
  constructor(private lineItemService: LineItemService, private elRef: ElementRef,
    private renderer: Renderer2, ) { }
}