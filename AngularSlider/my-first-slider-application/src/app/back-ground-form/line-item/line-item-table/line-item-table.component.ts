import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Form, FormArray, FormGroup, FormControl } from '@angular/forms';
import { LineItemService } from '../line-item.service';

@Component({
  selector: 'app-line-item-table',
  templateUrl: './line-item-table.component.html',
  styleUrls: ['./line-item-table.component.css']
})
export class LineItemTableComponent implements OnInit {
  type: string = "material";
  @Input() group: string;
  @Input() lineData;
  topval: string = "0px";
  lineDataForm: FormGroup;
  index: string;
  isEditButtonEnabled: boolean = true;
  isMaterialInvoice: boolean = false;
  isServiceInvoice: boolean = false;
  @Output() lineItemEditData = new EventEmitter();
  constructor(private lineItemService: LineItemService) { }

  getTaxData(){
    return (<FormArray>this.lineDataForm.get('taxDetails')).controls;
  }
  ngOnInit() {
    let taxDetails = new FormArray([]);
    if (this.lineData.value.type == "material") {
      this.materialInvoice();
    }
    else if (this.lineData.value.type == "service") {
      this.serviceInvoice();
    }
    this.index = this.group;
    let taxDetailArray = new FormArray([]);
    let lineItemtaxDetails = this.lineData.value.taxDetails
    for (var i = 0; i < lineItemtaxDetails.length; i++) {
      taxDetailArray.push(new FormGroup({
        'category': new FormControl(lineItemtaxDetails[i].category),
        'description': new FormControl(lineItemtaxDetails[i].description),
        'exemptDetail': new FormControl(lineItemtaxDetails[i].exemptDetail),
        'location': new FormControl(lineItemtaxDetails[i].location),
        'percentage': new FormControl(lineItemtaxDetails[i].percentage),
        'taxAmount': new FormControl(lineItemtaxDetails[i].taxAmount),
        'taxRateType': new FormControl(lineItemtaxDetails[i].taxRateType),
        'taxRegime': new FormControl(lineItemtaxDetails[i].taxRegime),
        'taxableAmount': new FormControl(lineItemtaxDetails[i].taxableAmount),
        'vatDueTimeCode': new FormControl(lineItemtaxDetails[i].vatDueTimeCode),
        'withholdingType': new FormControl(lineItemtaxDetails[i].withholdingType)
      }));
    }
    this.lineDataForm = new FormGroup(
      {
        'type': new FormControl(this.lineData.value.type),
        'lineNumber': new FormControl(this.lineData.value.lineNumber),
        'customerPart': new FormControl(this.lineData.value.customerPart),
        'description': new FormControl(this.lineData.value.description),
        'price': new FormControl(this.lineData.value.price),
        'supplierPart': new FormControl(this.lineData.value.supplierPart),
        'total': new FormControl(this.lineData.value.total),
        'unitQuantity': new FormControl(this.lineData.value.unitQuantity),
        'uom': new FormControl(this.lineData.value.uom),
        'taxDetails': taxDetailArray
      }
    );
    this.lineItemService.lineItemEdit.subscribe(
      (response) => {
        if (this.index == response.index) {
          this.lineItemEditData.emit(response);
        }
      }
    );
    this.lineItemService.taxDetailUpdated.subscribe(
      (response)=>{
        if(this.index==response.lineIndex){          
          (<FormArray>this.lineDataForm.get('taxDetails')).at(response.taxIndex).patchValue(
            {
                'category': response.value.category,
                'description':response.value.description,
                'exemptDetail': response.value.exemptDetail,
                'location': response.value.location,
                'percentage': response.value.percentage,
                'taxAmount': response.value.taxAmount,
                'taxRateType': response.value.taxRateType,
                'taxRegime': response.value.taxRegime,
                'taxableAmount': response.value.taxableAmount,
                'vatDueTimeCode': response.value.vatDueTimeCode,
                'withholdingType': response.value.withholdingType
            }
          );
        }
      }
    );
    this.lineItemService.taxDetailAdded.subscribe(
      (response) => {
        if (this.index == response.index) {
          (<FormArray>this.lineDataForm.get('taxDetails')).push(
            new FormGroup(
              {
                'category': new FormControl(response.value.category),
                'description': new FormControl(response.value.description),
                'exemptDetail': new FormControl(response.value.exemptDetail),
                'location': new FormControl(response.value.location),
                'percentage': new FormControl(response.value.percentage),
                'taxAmount': new FormControl(response.value.taxAmount),
                'taxRateType': new FormControl(response.value.taxRateType),
                'taxRegime': new FormControl(response.value.taxRegime),
                'taxableAmount': new FormControl(response.value.taxableAmount),
                'vatDueTimeCode': new FormControl(response.value.vatDueTimeCode),
                'withholdingType': new FormControl(response.value.withholdingType)
              }
            )
          )
        }
      }
    );
    this.lineItemService.lineItemEdited.subscribe(
      (response) => {
        if (response.index == this.index) {
          this.lineDataForm = new FormGroup(
            {
              'type': new FormControl(response.value.type),
              'lineNumber': new FormControl(response.value.lineNumber),
              'customerPart': new FormControl(response.value.customerPart),
              'description': new FormControl(response.value.description),
              'price': new FormControl(response.value.price),
              'supplierPart': new FormControl(response.value.supplierPart),
              'total': new FormControl(response.value.total),
              'unitQuantity': new FormControl(response.value.unitQuantity),
              'uom': new FormControl(response.value.uom),
              'taxDetails': taxDetailArray
            }
          );
          this.lineData.value = response.value;
        }
      }
    )
  }
  editLineItem() {
    let data = {
      index: this.group,
      value: this.lineData.value
    };
    this.lineItemService.openLineItemEdit(data);
  }

  materialInvoice() {
    this.isMaterialInvoice = true;
    this.isServiceInvoice = false;
  }
  serviceInvoice() {
    this.isServiceInvoice = true;
    this.isMaterialInvoice = false;
  }
  updateMaterialInvoice(event) {
    this.lineItemService.updateLineItemData(event);
  }
  updateServiceInvoice(event) {
    this.lineItemService.updateLineItemData(event);
  }

  onAddTaxDetail(event) {
  }

}
