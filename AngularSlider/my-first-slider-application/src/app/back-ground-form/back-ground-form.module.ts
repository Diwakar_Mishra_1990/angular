import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackGroundComponent } from './back-ground/back-ground.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SliderModule } from '../slider/slider.module';
import { SliderService } from '../slider/slider/slider.service';
import { TaxModule } from '../tax/tax.module';
import { AddressModule } from '../address/address.module';
import { ToggleModule } from '../toggle/toggle.module';
import { TaxTypeComponent } from './tax-type/tax-type.component';
import { AddressComponent } from './address/address.component';
import { LineItemComponent } from './line-item/line-item.component';
import { PopUpDirectiveDirective } from './pop-up-directive.directive';
import { MaterialInvoiceComponent } from './line-item/material-invoice/material-invoice.component';
import { ServiceInvoiceComponent } from './line-item/service-invoice/service-invoice.component';
import { LineItemTableComponent } from './line-item/line-item-table/line-item-table.component';
import { TaxDetatilTableComponent } from './line-item/tax-detatil-table/tax-detatil-table.component';

@NgModule({
  declarations: [BackGroundComponent, 
    TaxTypeComponent, 
    AddressComponent, 
    LineItemComponent, 
    PopUpDirectiveDirective, 
    MaterialInvoiceComponent, 
    ServiceInvoiceComponent, 
    LineItemTableComponent, TaxDetatilTableComponent],
  imports: [
    CommonModule,
    FormsModule,
    SliderModule,
    TaxModule,
    AddressModule,
    ToggleModule,
    ReactiveFormsModule
  ],
  exports:[
    BackGroundComponent
  ],
  providers:[SliderService]
})
export class BackGroundFormModule { }
