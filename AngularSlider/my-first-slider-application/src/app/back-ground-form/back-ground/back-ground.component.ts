import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { InvoiceDataModel } from 'src/app/shared/model/Temporary/InvoiceDataModel';
import { AddressDataModel } from 'src/app/shared/model/Temporary/AddressDataModel';
import { LineItemDataModel } from 'src/app/shared/model/Temporary/LineItemDataModel';
import { TaxDetailModel } from 'src/app/shared/model/Temporary/TaxDetailModel';
import { AddressService } from 'src/app/address/address.service';
import { LineItemService } from '../line-item/line-item.service';
import { DataBaseService } from 'src/app/data-base.service';

@Component({
  selector: 'app-back-ground',
  templateUrl: './back-ground.component.html',
  styleUrls: ['./back-ground.component.css']
})
export class BackGroundComponent implements OnInit {

  @ViewChild('f') backGroundForm: NgForm;
  addLineItem: string = "Add Line Item";
  lineItemMenuList: string[] = ['Add Material Invoice', 'Add Service Invoice'];
  billfrom: String = "BILL FROM";
  data: InvoiceDataModel;
  //billFromaddressData: AddressDataModel;
  //billToaddressData: AddressDataModel;
  //lineItemData: LineItemDataModel[];
  @Output() postData = new EventEmitter<InvoiceDataModel>();
  constructor(private addressService: AddressService,
    private lineItemService: LineItemService,
    private databaseService: DataBaseService) {
    
  }

  ngOnInit() {
    this.createSampleData();  
    this.databaseService.getFormData().subscribe(
      (response) => {
        this.data=this.createBackgroundData(response);
      }
    );
    this.addressService.addressUpdated.subscribe(
      (response) => {
        if (response.billFrom) {
          let updatedBillFromAddress = new AddressDataModel(
            response.billFrom.addressLine1,
            response.billFrom.addressLine2,
            response.billFrom.addressLine3,
            response.billFrom.municipalty,
            response.billFrom.city,
            response.billFrom.state,
            response.billFrom.country);
          this.data.billFrom = updatedBillFromAddress
        }
        if (response.billTo) {
          let updatedBillToAddress = new AddressDataModel(
            response.billTo.addressLine1,
            response.billTo.addressLine2,
            response.billTo.addressLine3,
            response.billTo.municipalty,
            response.billTo.city,
            response.billTo.state,
            response.billTo.country);
          this.data.billTo = updatedBillToAddress;
        }
      }
    );
    this.lineItemService.lineItemEdited.subscribe(
      (response) => {
        let lineItem = this.data.lineItemDataModel[response.index];
        let updatedLineItemModel = new LineItemDataModel(
          response.value.type,
          response.value.lineNumber,
          response.value.customerPart,
          response.value.supplierPart,
          response.value.description,
          response.value.uom,
          response.value.unitQuantity,
          response.value.price,
          response.value.total,
          lineItem.taxDetail
        );
        this.data.lineItemDataModel[response.index] = updatedLineItemModel;
      }
    );
    this.lineItemService.lineItemAdded.subscribe(
      (response) => {
        this.data.lineItemDataModel.push(
          new LineItemDataModel(
            response.type,
            response.lineNumber,
            response.customerPart,
            response.supplierPart,
            response.description,
            response.uom,
            response.unitQuantity,
            response.price,
            response.total,
            []
          )
        );
      }
    );
    this.lineItemService.taxDetailAdded.subscribe(
      (response) => {
        this.data.lineItemDataModel[response.index].taxDetail.push(
          new TaxDetailModel(
            response.value.category,
            response.value.location,
            response.value.description,
            response.value.taxRegime,
            response.value.taxableAmount,
            response.value.taxRateType,
            response.value.percentage,
            response.value.taxAmount,
            response.value.exemptDetail,
            response.value.withholdingType,
            response.value.vatDueTimeCode
          )
        );
      }
    );
    this.lineItemService.taxDetailUpdated.subscribe(
      (response) => {
        this.data.lineItemDataModel[response.lineIndex].taxDetail[response.taxIndex] =
          new TaxDetailModel(
            response.value.category,
            response.value.location,
            response.value.description,
            response.value.taxRegime,
            response.value.taxableAmount,
            response.value.taxRateType,
            response.value.percentage,
            response.value.taxAmount,
            response.value.exemptDetail,
            response.value.withholdingType,
            response.value.vatDueTimeCode
          );
      }
    );
  }
  submitInvoice(form: NgForm) {
    this.postData.next(this.data);
  }





  createBackgroundData(response: any) {
    let invoiceID = response.invoiceId;
    let billFromaddressData = new AddressDataModel(
      response.billFrom.addressLine1,
      response.billFrom.addressLine2,
      response.billFrom.addressLine3,
      response.billFrom.municipalty,
      response.billFrom.city,
      response.billFrom.state,
      response.billFrom.country
    );

    let billToaddressData = new AddressDataModel(
      response.billTo.addressLine1,
      response.billTo.addressLine2,
      response.billTo.addressLine3,
      response.billFrom.municipalty,
      response.billTo.city,
      response.billTo.state,
      response.billTo.country
    );
    let lineItemData = this.createLineItemData(response.lineItemDataModel);
    return new InvoiceDataModel(invoiceID, billFromaddressData, billToaddressData, lineItemData);
  }
  createLineItemData(lineItemDataModel: any): LineItemDataModel[] {
    let lineItemModel = [];
    for (var i = 0; i < lineItemDataModel.length; i++) {
      let taxDetail = [];
      for (var j = 0; j < lineItemDataModel[i].taxDetail.length; j++) {
        taxDetail.push(
          new TaxDetailModel(
            lineItemDataModel[i].taxDetail[j].category,
            lineItemDataModel[i].taxDetail[j].location,
            lineItemDataModel[i].taxDetail[j].description,
            lineItemDataModel[i].taxDetail[j].taxRegime,
            lineItemDataModel[i].taxDetail[j].taxableAmount,
            lineItemDataModel[i].taxDetail[j].taxRateType,
            lineItemDataModel[i].taxDetail[j].percentage,
            lineItemDataModel[i].taxDetail[j].taxAmount,
            lineItemDataModel[i].taxDetail[j].exemptDetail,
            lineItemDataModel[i].taxDetail[j].withholdingType,
            lineItemDataModel[i].taxDetail[j].vatDueTimeCode
          )
        );
      }
      lineItemModel.push(
        new LineItemDataModel(
          lineItemDataModel[i].type,
          lineItemDataModel[i].lineNumber,
          lineItemDataModel[i].customerPart,
          lineItemDataModel[i].supplierPart,
          lineItemDataModel[i].description,
          lineItemDataModel[i].uom,
          lineItemDataModel[i].unitQuantity,
          lineItemDataModel[i].price,
          lineItemDataModel[i].total,
          taxDetail
        )
      );
    }
    return lineItemModel;
  }

  createSampleData() {
    let billFromaddressData = new AddressDataModel(
      "",
      "",
      "",
      "",
      "",
      "",
      ""
    );
    let billToaddressData = new AddressDataModel(
      "",
      "",
      "",
      "",
      "",
      "",
      ""
    );
    let lineItemData = [];
    this.data = new InvoiceDataModel("", billFromaddressData, billToaddressData, lineItemData);

  }

  getBillFrom() {
    if (this.data) {
      return this.data.billFrom;
    }
  }
  getBillTo() {
    if (this.data) {
      return this.data.billTo;
    }
  }
  getLineItem() {
    if (this.data) {
      return this.data.lineItemDataModel;
    }
  }
  getInvoiceID(){
    if (this.data) {
      return this.data.invoiceId;
    }
  }
}
