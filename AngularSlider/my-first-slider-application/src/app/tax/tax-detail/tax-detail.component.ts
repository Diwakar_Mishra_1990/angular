import { Component, OnInit, Input } from '@angular/core';
import { LineItemService } from 'src/app/back-ground-form/line-item/line-item.service';

@Component({
  selector: 'app-tax-detail',
  templateUrl: './tax-detail.component.html',
  styleUrls: ['./tax-detail.component.css']
})
export class TaxDetailComponent implements OnInit {
  @Input()isEditMode:boolean=false;
  @Input()lineIndex;
  @Input() taxIndex;

  @Input() taxData={
    'category': '1_1',
    'description': '1_1',
    'exemptDetail': '1_1',
    'location': '1_11_1',
    'percentage': '1_1',
    'taxAmount': '1_1',
    'taxRateType': '1_1',
    'taxRegime': '1_1',
    'taxableAmount': '1_1',
    'vatDueTimeCode': '1_1',
    'withholdingType': '1_1'
  }
  constructor(private lineItemService:LineItemService) { }

  ngOnInit() {
  }
  addTaxDetails(form){
    
  }
  submitForm(form){
    form.index=this.lineIndex;
    this.lineItemService.addTaxDetail(form);
  }

  updateTaxDetail(form){
    form.lineIndex= this.lineIndex;
    form.taxIndex = this.taxIndex;
    this.lineItemService.updateTaxDetail(form)
  }
}
