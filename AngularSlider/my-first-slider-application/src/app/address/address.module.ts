import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressComponent } from './address/address.component';
import { SliderModule } from '../slider/slider.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AddressComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [AddressComponent]
})
export class AddressModule { }
