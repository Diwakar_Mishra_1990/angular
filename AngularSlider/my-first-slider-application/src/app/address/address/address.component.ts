import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { AddressDataModel } from 'src/app/shared/model/Temporary/AddressDataModel';
import { NgForm } from '@angular/forms';
import { AddressService } from '../address.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {

  constructor(private addressService:AddressService) { }
  @ViewChild('f') addressForm:NgForm;
  @Input() addressVal: AddressDataModel;
  @Input() addressType: string;
  @Input() group:string;
  ngOnInit() {
   
  }
  updateValue(f) {
    this.addressService.updateAddress(f.value);
  }

  cancelChange() {

  } 
}
