import { Injectable, EventEmitter, Output } from '@angular/core';
import {  } from 'events';
import { AddressDataModel } from '../shared/model/Temporary/AddressDataModel';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  @Output()addressUpdated= new EventEmitter<AddressDataModel>();
  constructor() { }
  updateAddress(data){
    this.addressUpdated.next(data);
  }
}
