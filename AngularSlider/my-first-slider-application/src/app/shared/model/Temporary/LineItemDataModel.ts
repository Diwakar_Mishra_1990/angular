import { TaxDetailModel } from './TaxDetailModel';

export class LineItemDataModel {
    type:string;
    lineNumber: string;
    customerPart: string;
    supplierPart: string;
    description: string;
    uom: string;
    unitQuantity: string;
    price: string;
    total: string;
    taxDetail: TaxDetailModel[];

    constructor(
        type:string,
        lineNumber: string,
        customerPart: string,
        supplierPart: string,
        description: string,
        uom: string,
        unitQuantity: string,
        price: string,
        total: string,
        taxDetail: TaxDetailModel[]
    ) {
        this.type=type;
        this.lineNumber= lineNumber;
        this.customerPart= customerPart;
        this.supplierPart= supplierPart;
        this.description= description;
        this.uom= uom;
        this.unitQuantity= unitQuantity;
        this.price= price;
        this.total= total;
        this.taxDetail= taxDetail;
    }
}