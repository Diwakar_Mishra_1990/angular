export class TaxDetailModel {
    category: string;
    location: string;
    description: string;
    taxRegime: string;
    taxableAmount: string;
    taxRateType: string;
    percentage: string;
    taxAmount: string;
    exemptDetail: string;
    withholdingType: string;
    vatDueTimeCode: string;
    constructor(category: string,
        location: string,
        description: string,
        taxRegime: string,
        taxableAmount: string,
        taxRateType: string,
        percentage: string,
        taxAmount: string,
        exemptDetail: string,
        withholdingType: string,
        vatDueTimeCode: string) {

        this.category = category;
        this.location = location;
        this.description = description;
        this.taxRegime = taxRegime;
        this.taxableAmount = taxableAmount;
        this.taxRateType = taxRateType;
        this.percentage = percentage;
        this.taxAmount = taxAmount;
        this.exemptDetail = exemptDetail;
        this.withholdingType = withholdingType;
        this.vatDueTimeCode = vatDueTimeCode;

    }
  
}