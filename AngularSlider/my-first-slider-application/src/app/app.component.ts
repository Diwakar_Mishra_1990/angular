import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataBaseService } from './data-base.service';
import { LineItemDataModel } from './shared/model/Temporary/LineItemDataModel';
import { AddressDataModel } from './shared/model/Temporary/AddressDataModel';
import { InvoiceDataModel } from './shared/model/Temporary/InvoiceDataModel';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-first-slider-application';
  formData:InvoiceDataModel;
  constructor(private databaseServivce:DataBaseService){
    
  }

  submitFormData(eventData){
    this.databaseServivce.putFormData(eventData);
  }

 

}
