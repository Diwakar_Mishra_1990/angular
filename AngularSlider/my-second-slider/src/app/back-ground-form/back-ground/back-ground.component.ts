import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { invoiceDetailRequest } from 'src/app/shared/store/invoiceDetailStore';

@Component({
  selector: 'app-back-ground',
  templateUrl: './back-ground.component.html',
  styleUrls: ['./back-ground.component.css']
})
export class BackGroundComponent implements OnInit {

  @ViewChild('f') slForm:NgForm;
  invoiceData= invoiceDetailRequest;
  taxData = invoiceDetailRequest.
            InvoiceDetailRequest.
            InvoiceDetailOrder.
            InvoiceDetailServiceItem.
            Tax;
  
  
  //@ViewChild('nameInput') nameInputRef:ElementRef;
  //@ViewChild('amountInput') amountInputRef:ElementRef;
  //@Output() ingredientAdded = new EventEmitter<{name:string, amount:number}>(); // commenting because we are using services for communicating
  constructor() { }

  ngOnInit() {
    

  }
  onAddItem(form: NgForm){
    
  }
  onClear(){
   
    this.slForm.reset();
  }

  onDelete(){
    
  }
}
