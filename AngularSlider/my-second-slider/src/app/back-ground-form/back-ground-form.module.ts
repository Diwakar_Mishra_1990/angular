import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackGroundComponent } from './back-ground/back-ground.component';
import { FormsModule } from '@angular/forms';
import { SliderModule } from '../slider/slider.module';
import { SliderFormPanelModule } from '../slider-form-panel/slider-form-panel.module';
import { SliderService } from '../slider/slider/slider.service';
@NgModule({
  declarations: [BackGroundComponent],
  imports: [
    CommonModule,
    FormsModule,
    SliderModule,
    SliderFormPanelModule
  ],
  exports:[
    BackGroundComponent
  ]
})
export class BackGroundFormModule { }
