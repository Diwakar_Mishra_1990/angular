import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressFormPanelComponent } from './address-form-panel.component';

describe('AddressFormPanelComponent', () => {
  let component: AddressFormPanelComponent;
  let fixture: ComponentFixture<AddressFormPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressFormPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressFormPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
