import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressFormPanelComponent } from './address-form-panel/address-form-panel.component';

@NgModule({
  declarations: [AddressFormPanelComponent],
  imports: [
    CommonModule
  ],
  exports: [
    AddressFormPanelComponent
  ]
})
export class SliderFormPanelModule { }
