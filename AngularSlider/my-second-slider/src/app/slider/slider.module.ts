import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderComponent } from './slider/slider.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SliderService } from './slider/slider.service';
@NgModule({
  declarations: [SliderComponent],
  imports: [
    CommonModule,
    BrowserAnimationsModule
  ],
  exports:[
    SliderComponent
  ],
  providers:[SliderService]
})
export class SliderModule { }
