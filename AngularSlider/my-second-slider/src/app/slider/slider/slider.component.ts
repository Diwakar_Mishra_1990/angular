import { ChangeDetectionStrategy, Component, Input, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SliderState } from '../sliderModel/slider-state';
import { SliderService } from './slider.service';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css'],
  host:{
    '[class.opened]':'opened'
  }
})
export class SliderComponent implements OnInit {
  
  @Input() opened:boolean= true;
  panelOpen:boolean=false;
  

  ngOnInit() {
    
  }

  public toggle(){
    this.opened=!this.opened;
  }

  public open(){
    this.opened=true;
  }

  public close(){
    this.opened=false;
  }
  
  private _toggleDrawer(){
    this.renderer.setStyle();
  }
  constructor(private el: ElementRef, private renderer: Renderer2){}
}
