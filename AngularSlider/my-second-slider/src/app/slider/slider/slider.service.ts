import { EventEmitter } from '@angular/core';

export class SliderService {
    close: EventEmitter<string>= new EventEmitter<string>();
    closeAll(id: string) {
        this.close.emit(id);
    }
}